import Reactotron from 'reactotron-react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ReactotronFlipper from 'reactotron-react-native/dist/flipper';
import apisaucePlugin from 'reactotron-apisauce';
import {reactotronRedux as reduxPlugin} from 'reactotron-redux';

export const ReacotronLog = msg => {
  Reactotron.log(msg);
};

Reactotron.setAsyncStorageHandler(AsyncStorage)
  .configure({
    name: 'React Native Demo',
    createSocket: path => new ReactotronFlipper(path),
  })
  .useReactNative({
    // devTools: true,
    asyncStorage: false, // there are more options to the async storage.
    networking: {
      // optionally, you can turn it off with false.
      ignoreUrls: /symbolicate/,
    },
    editor: false, // there are more options to editor
    errors: {veto: stackFrame => false}, // or turn it off with false
    overlay: false, // just turning off overlay
  })
  .use(apisaucePlugin())
  .use(reduxPlugin())
  .connect();

console.tron = Reactotron;

const enhancer = Reactotron.createEnhancer();
export {enhancer};
