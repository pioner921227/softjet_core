 
## install
```bash
npm install https://gitlab.com/pioner921227/softjet_core.git
```

 ## add dependencies 
 
```json
{
  "dependencies": {
    "@gorhom/bottom-sheet": "3.6.5",
    "@gorhom/portal": "^1.0.4",
    "@react-native-async-storage/async-storage": "^1.14.1",
    "@react-native-community/geolocation": "^2.0.2",
    "@react-native-community/masked-view": "^0.1.10",
    "@react-native-community/netinfo": "^6.0.0",
    "@react-native-community/picker": "^1.8.1",
    "@react-native-html/parser": "^0.1.0",
    "@react-native-html/renderer": "^0.0.30",
    "@react-navigation/bottom-tabs": "^5.11.8",
    "@react-navigation/native": "^5.9.3",
    "@react-navigation/stack": "^5.14.3",
    "add": "^2.0.6",
    "axios": "^0.21.1",
    "decodable-js": "^1.1.15",
    "pouchdb-react-native": "^7.0.0-beta-1",
    "flipper": "^0.91.2",
    "lodash": "^4.17.21",
    "lottie-ios": "3.1.8",
    "lottie-react-native": "^4.0.2",
    "radio-buttons-react-native": "^1.0.4",
    "react-native-haptic-feedback": "^1.11.0",
    "react": "17.0.1",
    "react-native": "0.64.0",
    "react-native-bouncy-checkbox": "^2.0.0",
    "react-native-confirmation-code-field": "^6.5.1",
    "react-native-device-info": "^8.1.3",
    "react-native-fast-image": "^8.3.4",
    "react-native-flipper": "^0.91.2",
    "react-native-gesture-handler": "^1.10.3",
    "react-native-segmented-control-tab": "^3.4.1",
    "react-native-image-picker": "^3.8.1",
    "react-native-linear-gradient": "^2.5.6",
    "react-native-maps": "https://github.com/xDemon200/react-native-maps",
    "react-native-masked-text": "^1.13.0",
    "react-native-modal": "^11.10.0",
    "react-native-pager-view": "^5.1.3",
    "react-native-paper": "^4.7.2",
    "react-native-permissions": "^3.0.1",
    "react-native-picker-select": "^8.0.4",
    "react-native-portal": "^1.3.0",
    "react-native-reanimated": "^2.2.0",
    "react-native-redash": "^16.0.10",
    "react-native-restart": "^0.0.22",
    "react-native-safe-area-context": "^3.2.0",
    "react-native-screens": "^3.0.0",
    "react-native-svg": "^12.1.0",
    "react-native-tab-view": "^3.0.1",
    "react-native-vector-icons": "^8.1.0",
    "react-native-webview": "^11.6.2",
    "react-redux": "^7.2.3",
    "redux": "^4.0.5",
    "redux-persist": "^6.0.0",
    "redux-thunk": "^2.3.0",
    "uuid-js": "^0.7.5",
    "@react-native-firebase/messaging": "^12.1.0",
    "@react-native-firebase/app": "^12.1.0",
    "yarn": "^1.22.10"
  },

  "devDependencies": {
    "@babel/core": "^7.13.14",
    "@babel/runtime": "^7.13.10",
    "@react-native-community/eslint-config": "^2.0.0",
    "@types/config": "^0.0.38",
    "@types/jest": "^26.0.22",
    "@types/lodash": "^4.14.169",
    "@types/react": "^17.0.3",
    "@types/react-native": "^0.64.2",
    "@types/react-test-renderer": "^17.0.1",
    "@types/redux-logger": "^3.0.8",
    "babel-jest": "^26.6.3",
    "babel-plugin-module-resolver": "^4.1.0",
    "eslint": "^7.23.0",
    "jest": "^26.6.3",
    "metro-react-native-babel-preset": "^0.65.2",
    "react-native-typescript-transformer": "^1.2.13",
    "react-test-renderer": "17.0.1",
    "reactotron-apisauce": "^3.0.0",
    "reactotron-react-native": "^5.0.0",
    "reactotron-redux": "^3.1.3",
    "redux-logger": "^3.0.6",
    "typescript": "^4.2.3",
    "typescript-fsa": "^3.0.0",
    "typescript-fsa-reducers": "^1.2.2",
    "typescript-fsa-redux-thunk": "^2.10.1"
  }
}

```

## Install permissions 

```
example => https://github.com/zoontek/react-native-permissions
 #IOS
info.plist ->

	<key>NSAppleMusicUsageDescription</key>
	<string>Требуется доступ к медиа файлам, что бы загрузить в приложение</string>
	<key>NSBluetoothAlwaysUsageDescription</key>
	<string>Требуется доступ к Bluetooth что бы подключить гарнитуру</string>
	<key>NSBluetoothPeripheralUsageDescription</key>
	<string>Требуется доступ к Bluetooth что бы подключить гарнитуру</string>
	<key>NSCalendarsUsageDescription</key>
	<string>Требуется доступ к календарю, что бы поставить метку</string>
	<key>NSCameraUsageDescription</key>
	<string>Требуется доступ к камере для сканирования QR кода</string>
	<key>NSContactsUsageDescription</key>
	<string>Требуется доступ к контактам , что бы загрузить в приложение</string>
	<key>NSFaceIDUsageDescription</key>
	<string>Требуется доступ к Face ID что бы подтвердить личность</string>
	<key>NSLocationAlwaysAndWhenInUseUsageDescription</key>
	<string>Требуется доступ к геопозиции для отображения ближайших ресторанов относительно Вашей позиции</string>
	<key>NSLocationAlwaysUsageDescription</key>
	<string>Требуется доступ к геопозиции для отображения ближайших ресторанов относительно Вашей позиции</string>
	<key>NSLocationWhenInUseUsageDescription</key>
	<string>Требуется доступ к геопозиции для отображения ближайших ресторанов относительно Вашей позиции</string>
	<key>NSMicrophoneUsageDescription</key>
	<string>Требуется доступ к микрофону, что бы записать звук</string>
	<key>NSMotionUsageDescription</key>
	<string>Требуется доступ к данным движения устройства , для вызова меню</string>
	<key>NSPhotoLibraryAddUsageDescription</key>
	<string>Требуется доступ к изображениям для загрузки в приложение</string>
	<key>NSPhotoLibraryUsageDescription</key>
	<string>Требуется доступ к изображениям для загрузки в приложение</string>
	<key>NSRemindersUsageDescription</key>
	<string>Требуется доступ к напоминаниям, что бы приложения могло уведомить  об оплате</string>
	<key>NSSiriUsageDescription</key>
	<string>Требуется доступ к Siri для обращения к помощнику</string>
	<key>NSSpeechRecognitionUsageDescription</key>
	<string>Требуется доступ к разпознаванию речи для набора текста</string>

#Android

    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.ACCEPT_HANDOVER" />
    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
    <uses-permission android:name="android.permission.ACTIVITY_RECOGNITION" />
    <uses-permission android:name="android.permission.BODY_SENSORS" />
    <uses-permission android:name="android.permission.CAMERA" />
    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.READ_PHONE_STATE" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
```

## Android ScreenOrintation Manifest 
```
yarn add react-native-orientation-locker

react-native link react-native-orientation-locker
```


# App.tsx
```
import Orientation from 'react-native-orientation-locker';

...
  useEffect(() => {
    Orientation.lockToPortrait();
  }, []);

```


```
<activity
...js
  android:screenOrientation="portrait>
```






## Add fonts to project and Xcode project
#### add folder Resources in project Xcode 
####  add Fonts to folder Resources
####  add fonts to Infoplist -> Fonts provided by application

### add to package.json
```js
{
  //...
  "rnpm": {
    "assets": [
      "./src/assets/fonts/"
    ]
  }
}
```


## Add permissions for http 
+ Infoplist -> App Transport Security Settings -> Allow Arbitrary Loads = YES

Android Manifest 

<application
  ...
  android:usesCleartextTraffic="true">

Add key for Map

<meta-data
    android:name="com.google.android.geo.API_KEY"
    android:value="..."
/>


## install Gesture Responder System
https://docs.swmansion.com/react-native-gesture-handler/docs/


## install Reanimated 2 
https://docs.swmansion.com/react-native-reanimated/docs/installation



## Init index.js

```js
import {AppRegistry} from 'react-native';
//  import App from './App';
import {name as appName} from './app.json';
import {Config as newConfig} from './src/config/config';
import {Config} from 'softjet_core';
Config.Color = newConfig.Color
Config.BASE_URL = 'http://core.appsj.su';
import {App} from 'softjet_core';

AppRegistry.registerComponent(appName, () => App);
```

## UIKit

```js
import {ThemedButton} from 'softjet_core/src/ui-kit';
```

## Replace screen 
```js
import {ScreenConfig} from 'softjet_core/src/navigation';
import {Policy} from './src/screens/login/view/policy';
ScreenConfig.ProfileAuth = Policy;
```

## System api
```js
import {useDebounce} from 'softjet_core/src/system'
```
#### useTypedSelector 
#### useDebounce 
#### useInput 
#### rootNavigate 
#### resetRootNavigation
#### ApiService
#### TStore type rootStore
#### injectReducer 


## Navigation api 
```js
import {ScreenConfig,TabScreenConfig} from 'softjet_core/src/navigation'
```
####  ScreenConfig - screens set
#### TabScreenConfig - tab screens set


## Screen Delivery api
#### selectCityNextButtonConfig
#### addressListNextButtonConfig
#### selectOrderTypeModalConfig
#### onPressCreateNewAddressInAddressListRoute





