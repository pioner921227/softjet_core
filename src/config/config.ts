import {
  BUTTON_RADIUS,
  Color,
  DEFAULT_BUTTON_THEME,
  RADIUS,
  themeNames,
  UIStyles,
} from '../assets/styles'
import { BASE_URL } from './base-url'
import { SplashScreenTypes } from '../components/splash-screen'
import { Source } from 'react-native-fast-image'

type MakeStyle<T> = { [P in keyof T]: T[P] }

interface IConfig {
  APP_VERSION: string
  Color: { [P in keyof typeof Color]: string }
  UIStyles: MakeStyle<typeof UIStyles>
  RADIUS: MakeStyle<typeof RADIUS>
  BASE_URL: string
  BUTTON_RADIUS: number
  DEFAULT_BUTTON_THEME: keyof typeof themeNames
  themeNames: MakeStyle<typeof themeNames>
  ENABLE_CLEAR_LOCAL_STORE_WITH_INSTALL: boolean
  SPLASH_SCREEN: {
    SOURCE: Source
    TYPE: keyof typeof SplashScreenTypes | SplashScreenTypes
    DELAY: number
    ENABLE: boolean
  }
}

export const Config: IConfig = {
  APP_VERSION: '1.8',
  Color: Color,
  UIStyles: UIStyles,
  RADIUS: RADIUS,
  BASE_URL: BASE_URL,
  BUTTON_RADIUS: BUTTON_RADIUS,
  DEFAULT_BUTTON_THEME: DEFAULT_BUTTON_THEME,
  themeNames: themeNames,
  ENABLE_CLEAR_LOCAL_STORE_WITH_INSTALL: false,
  SPLASH_SCREEN: {
    SOURCE: require('../assets/tunec.json'),
    TYPE: SplashScreenTypes.lottie,
    DELAY: 4000,
    ENABLE: true,
  },
}
