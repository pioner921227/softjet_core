import { BottomSheetModalProvider } from '@gorhom/bottom-sheet'
import { PortalProvider } from '@gorhom/portal'
import { NavigationContainer } from '@react-navigation/native'
import React, { useLayoutEffect, useState } from 'react'
import { StatusBar, StyleSheet } from 'react-native'
import Orientation from 'react-native-orientation-locker'
import { SafeAreaProvider } from 'react-native-safe-area-context'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { BottomSheetMenuProvider } from './components/bottom-sheet-menu/bottom-sheet-modal-provider'
import { BottomSheetModalComponent } from './components/bottom-sheet-menu/bottom-sheet-modal/bottom-sheet-modal-component'
import { Modal } from './components/modal/modal'
import { ModalProvider } from './components/modal/modal-provider'
import { SplashScreen } from './components/splash-screen'
import { Config } from './config'
import { RootNavigator } from './navigation/root-navigator'
import { ErrorBoundary } from './system/helpers/error-boundary'
import { LocalStorage } from './system/helpers/local-storage'
import { navigationRef } from './system/helpers/root-navigation'
import { persistor, store } from './system/root-store/configure-store'
import { injectReducer, newReducers } from './system/root-store/inject-reducer'

export const localDb = new LocalStorage()

localDb.get('data').catch(() => {
  localDb.set({
    _id: 'data',
    id_org: null,
    id_order_type: null,
  })
})

const { Color } = Config

interface IApp {
  onLoadStore?: () => void
  reducers?: newReducers
}

export const App: React.FC<IApp> = ({ children, onLoadStore, reducers }) => {
  const [isLoading, setIsLoading] = useState(true)
  const onBefore = () => {
    setTimeout(
      () => {
        onLoadStore?.()
        setIsLoading(false)
      },
      Config.SPLASH_SCREEN.ENABLE ? Config.SPLASH_SCREEN.DELAY : 0,
    )
  }

  useLayoutEffect(() => {
    Orientation.lockToPortrait()
    if (reducers) {
      injectReducer(store, reducers)
      persistor.persist()
    }
  }, [])

  return (
    <ErrorBoundary>
      <SafeAreaProvider style={styles.container}>
        <BottomSheetMenuProvider>
          <Provider store={store}>
            <PersistGate onBeforeLift={onBefore} persistor={persistor}>
              <NavigationContainer ref={navigationRef}>
                <PortalProvider>
                  <ModalProvider>
                    <BottomSheetModalProvider>
                      <StatusBar barStyle={'dark-content'} />
                      <BottomSheetModalComponent />
                      <Modal />
                      {isLoading ? (
                        Config.SPLASH_SCREEN.ENABLE ? (
                          <SplashScreen
                            type={Config.SPLASH_SCREEN.TYPE}
                            source={Config.SPLASH_SCREEN.SOURCE}
                          />
                        ) : null
                      ) : (
                        <RootNavigator>{children}</RootNavigator>
                      )}
                    </BottomSheetModalProvider>
                  </ModalProvider>
                </PortalProvider>
              </NavigationContainer>
            </PersistGate>
          </Provider>
        </BottomSheetMenuProvider>
      </SafeAreaProvider>
    </ErrorBoundary>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Color.WHITE,
    height: '100%',
    width: '100%',
  },
  keyboardAvoiding: {
    flex: 1,
  },
})
