import {ReducerBuilder, reducerWithInitialState} from 'typescript-fsa-reducers';
import {initialStateProfile, IStoreProfile} from './store-profile';
import {Success} from 'typescript-fsa';
import {AsyncActionProfile} from './async-action-profile';
import {ActionsLogin} from '../../login/store/actions-login';
import {IGetUserDataResponse} from '../api/types';
import {decodeWithErrorHandling} from '../../../system/helpers/decode-with-error-handling';
import {userDataStruct} from './decode-structures';

const getUserDataHandlerStarted = (store: IStoreProfile): IStoreProfile => {
  return {
    ...store,
    isLoading: true,
    error: false,
  };
};

const getUserDataHandlerDone = (
  store: IStoreProfile,
  {result}: Success<any, IGetUserDataResponse>,
): IStoreProfile => {
  // const decodeRes = decodeWithErrorHandling(
  //   result,
  //   userDataStruct,
  //   '/api/user/getUser',
  // );

  return {
    ...store,
    isLoading: false,
    error: false,
    userData: result.data,
  };
};

const getUserDataHandlerFailed = (store: IStoreProfile): IStoreProfile => {
  return {
    ...store,
    isLoading: false,
    error: true,
  };
};

const resetHandler = (): IStoreProfile => {
  return {
    ...initialStateProfile,
  };
};

export const reducerProfile: ReducerBuilder<IStoreProfile> = reducerWithInitialState(
  initialStateProfile,
)
  .case(ActionsLogin.logoutAccount, resetHandler)
  .cases(
    [
      AsyncActionProfile.getUserData.async.started,
      AsyncActionProfile.setUserData.async.started,
    ],
    getUserDataHandlerStarted,
  )
  .cases(
    [
      AsyncActionProfile.getUserData.async.done,
      AsyncActionProfile.setUserData.async.done,
    ],
    getUserDataHandlerDone,
  )
  .cases(
    [
      AsyncActionProfile.getUserData.async.failed,
      AsyncActionProfile.setUserData.async.failed,
    ],
    getUserDataHandlerFailed,
  );
