import {asyncActionCreator} from '../../../system/root-store/action-creator';
import {RequestProfile} from '../api/request-profile';
import {IGetUserDataResponse, ISetUserDataProps} from '../api/types';

export class AsyncActionProfile {
  static getUserData = asyncActionCreator<void, IGetUserDataResponse, Error>(
    'PROFILE/GET_USER_DATA',
    RequestProfile.getUserData,
  );

  static setUserData = asyncActionCreator<
    ISetUserDataProps,
    IGetUserDataResponse,
    Error
  >('PROFILE/SET_USER_DATA', RequestProfile.setUserData);
}
