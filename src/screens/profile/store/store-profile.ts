import {IRequestLoading} from '../../../system/root-store/root-store';
import {IUserData} from '../api/types';

export interface IReferral {
  code: string;
  my_bonus: number;
  friend_bonus: number;
  ref_link: string;
}

export interface IStoreProfile extends IRequestLoading {
  userData: IUserData | null;
}

export const initialStateProfile: IStoreProfile = {
  userData: null,
  isLoading: false,
  error: false,
};
