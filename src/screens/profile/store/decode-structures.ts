import {T} from 'decodable-js';
import {IGetUserDataResponse} from '../api/types';

export const userDataStruct: IGetUserDataResponse = {
  data: {
    name: T.string,
    email: T.string,
    photo: T.string,
    gender: {id: T.number, label: T.string},
    phone: T.string,
    bonus: T.number,
    birthday: T.string,
    referal: {
      code: T.string,
      my_bonus: T.number,
      friend_bonus: T.number,
      ref_link: T.string,
    },
    id: T.number,
    bonus_card: T.string,
  },
};
