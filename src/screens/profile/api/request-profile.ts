import {ApiService} from '../../../system/api/api-service';
import {IGetUserDataResponse, ISetUserDataProps} from './types';

export class RequestProfile {
  static getUserData(): Promise<IGetUserDataResponse> {
    return ApiService.get('/api/user/getUser');
  }
  static setUserData(data: ISetUserDataProps): Promise<IGetUserDataResponse> {
    return ApiService.put('/api/user/changeUser', data);
  }
}
