import {IAddressItem} from '../../delivery/types/types';

export interface ISetUserDataProps {
  name?: string;
  email?: string;
  birthday?: string;
  gender?: number;
  image?: string;
}

export interface IGetUserDataResponse {
  data: IUserData;
}

export interface IUserData {
  name: string;
  email: string;
  photo: string;
  gender: {id: number; label: string};
  phone: string;
  bonus: number;
  birthday: string;
  referal: IReferral;
  last_address?: IAddressItem;
  id: number;
  bonus_card: string;
}

export interface IReferral {
  code: string;
  my_bonus: number;
  friend_bonus: number;
  ref_link: string;
}
