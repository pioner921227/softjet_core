import { StackScreenProps } from '@react-navigation/stack'
import React, { Fragment } from 'react'
import {
  InteractionManager,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native'
import FastImage from 'react-native-fast-image'
import { useDispatch } from 'react-redux'
import { useModal } from '../../../components'
import { ArrowRightWithoutLineIcon } from '../../../components/icons/arrow-right-without-line-icon'
import { CartIcon } from '../../../components/icons/cart-icon'
import { ExitIcon } from '../../../components/icons/exit-icon'
import { MapMarkerIcon } from '../../../components/icons/map-marker-icon'
import { ProfileIcon } from '../../../components/icons/profile-icon'
import { RubleWithCircleArrowIcon } from '../../../components/icons/ruble-with-circle-arrow-icon'
import { ListItemIcons } from '../../../components/list-item-icons/list-item-icons'
import { Config } from '../../../config'
import { Routes } from '../../../navigation/config/routes'
import { TRootStackParamList } from '../../../navigation/types/types'
import { clearLocalStore } from '../../../system'
import { FocusAwareStatusBar } from '../../../system/helpers/focus-aware-status-bar'
import { useImage } from '../../../system/hooks/use-image'
import { useTypedSelector } from '../../../system/hooks/use-typed-selector'
import {
  getAppInfo,
  getAppSettings,
  getCurrentOrderTypeId,
  getCurrentOrganisationId,
  getUserData,
} from '../../../system/selectors/selectors'
import { AsyncActionSystem } from '../../../system/store/async-action-system'
import { ActionsLogin } from '../../login/store/actions-login'

const { UIStyles, Color } = Config

export const createProfileRowItem = (
  title: string,
  Icon: React.FC,
  route: keyof typeof Routes,
  params?: any,
) => {
  return {
    title,
    Icon,
    route,
    params,
  }
}

export const profileRowItems = [
  createProfileRowItem('Мои заказы', CartIcon, Routes.Orders),
  createProfileRowItem(
    'Настройки профиля',
    ProfileIcon,
    Routes.ProfileSettings,
  ),
  createProfileRowItem('Изменить город', MapMarkerIcon, Routes.SelectCity, {
    enableBackButton: true,
  }),
  createProfileRowItem('Выйти', ExitIcon, Routes.LoginStack),
]

type TProps = StackScreenProps<TRootStackParamList, Routes.ProfileAuth>

export const ProfileAuth: React.FC<TProps> = React.memo(({ navigation }) => {
  const userData = useTypedSelector(getUserData)
  const appSettings = useTypedSelector(getAppSettings)
  const dispatch = useDispatch()
  const { show, close } = useModal()
  const currentOrderTypeId = useTypedSelector(getCurrentOrderTypeId)
  const currentOrganisationId = useTypedSelector(getCurrentOrganisationId)
  const appInfo = useTypedSelector(getAppInfo)

  const showModalHandler = (callback: () => void) => {
    show({
      title: 'Выйти из профиля?',
      titleStyle: { textAlign: 'center' },
      description: '',
      buttons: [
        { label: 'Отмена', onPress: close, styles: { modifier: 'bordered' } },
        { label: 'Ок', onPress: callback },
      ],
      buttonsDirection: 'row',
    })
  }

  const onPressHandler = (route: keyof typeof Routes, params?: any) => {
    if (route === Routes.LoginStack) {
      showModalHandler(() => {
        close()
        // InteractionManager.runAfterInteractions(() => {
        dispatch(ActionsLogin.logoutAccount())
        if (
          currentOrderTypeId !== undefined &&
          currentOrganisationId !== undefined
        ) {
          dispatch(
            AsyncActionSystem.logOutRemote({
              id_org: currentOrganisationId,
              order_type: currentOrderTypeId,
            }),
          )
        }
        clearLocalStore()
      })
      // })
    } else {
      //@ts-ignore
      navigation.push(route, params)
    }
  }

  const imageHeader = useImage(appInfo?.profile_background ?? '')

  return (
    <ScrollView bounces={false}>
      <FocusAwareStatusBar barStyle={'light-content'} />
      <View style={styles.headerContainer}>
        <View style={styles.headerTitleWrapper}>
          <Text style={styles.headerTitle}>Добрый день</Text>
          <Text style={styles.headerTitle}>{userData?.name}</Text>
        </View>
        <FastImage source={imageHeader} style={styles.image} />
      </View>
      <View style={styles.contentWrapper}>
        {appSettings?.app_bonus_hidden === false ? (
          <ListItemIcons
            disabled={true}
            onPress={() => {}}
            leftComponent={RubleWithCircleArrowIcon}
            rightComponent={() => (
              <Text style={styles.bonusValue}>{userData?.bonus} ₽</Text>
            )}
            title={'Бонусы'}
          />
        ) : null}

        {profileRowItems.map((el, index) => {
          return (
            <Fragment key={index}>
              <ListItemIcons
                containerStyle={{ marginTop: index === 4 ? 24 : 0 }}
                onPress={() => onPressHandler(el.route, el.params)}
                leftComponent={el.Icon}
                rightComponent={ArrowRightWithoutLineIcon}
                title={el.title}
              />
            </Fragment>
          )
        })}
      </View>
    </ScrollView>
  )
})

const styles = StyleSheet.create({
  container: {},
  headerContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerTitleWrapper: {
    position: 'absolute',
    alignItems: 'center',
    zIndex: 1,
  },
  headerTitle: {
    ...UIStyles.font24b,
    color: Color.WHITE,
  },
  image: {
    width: '100%',
    height: 220,
  },
  contentWrapper: {
    marginTop: 16,
    ...UIStyles.paddingH16,
  },
  bonusValue: {
    ...UIStyles.font15,
    color: Color.DARK,
  },
})
