import React, {useState} from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import {PhotoCameraSvg} from '../../../../components/icons/photo-camera-icon';
import {Config, ImageRepository} from '../../../../config';
import {launchCamera} from 'react-native-image-picker';
import FastImage from 'react-native-fast-image';
import {useHitSlop} from '../../../../system/hooks/use-hit-slop';

const {Color} = Config;

interface IAvatar {
  imageUrl: string;
  onChangeImage: (image: string) => void;
}

export const Avatar: React.FC<IAvatar> = React.memo(
  ({onChangeImage, imageUrl}) => {
    const [image, setImage] = useState(imageUrl);
    const [isLoadingImage, setIsLoadingImage] = useState(false);

    const onPress = () => {
      // launchImageLibrary(
      //   {mediaType: 'photo', includeBase64: true},
      //   ({base64, uri}) => {
      //     if (uri) {
      //       setImage(uri);
      //     }
      //     if (base64) {
      //       onChangeImage(base64);
      //     }
      //   },
      // );
      launchCamera(
        {mediaType: 'photo', cameraType: 'front', includeBase64: true},
        ({uri, base64}) => {
          if (uri) {
            setImage(uri);
          }
          if (base64) {
            onChangeImage(base64);
          }
        },
      );
    };

    const hitStop = useHitSlop(10);

    return (
      <View style={styles.container}>
        <View style={styles.avatarWrapper}>
          {isLoadingImage ? (
            <View
              style={{
                width: 100,
                height: 100,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <ActivityIndicator size={'large'} color={Color.PRIMARY} />
            </View>
          ) : null}
          <FastImage
            onLoadStart={() => setIsLoadingImage(true)}
            onLoadEnd={() => setIsLoadingImage(false)}
            style={styles.image}
            source={
              imageUrl
                ? {
                    uri: image,
                  }
                : ImageRepository.ProductImagePlaceholder
            }
          />

          <TouchableOpacity
            hitSlop={hitStop}
            onPress={onPress}
            style={styles.photoButton}>
            <PhotoCameraSvg fill={Color.WHITE} />
          </TouchableOpacity>
        </View>
      </View>
    );
  },
);

const styles = StyleSheet.create({
  container: {
    width: '100%',
    alignItems: 'center',
    marginTop: 16,
  },
  image: {
    height: 100,
    width: 100,
    borderRadius: 200,
  },
  avatarWrapper: {
    width: 100,
    height: 100,
  },
  photoButton: {
    width: 36,
    height: 36,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 2,
    position: 'absolute',
    bottom: 0,
    right: 0,
    borderColor: Color.WHITE,
    backgroundColor: Color.PRIMARY,
  },
});
