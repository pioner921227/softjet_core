import React from 'react';
import {IRadioButtonData} from '../../../../components/radio-button/radio-button';
import {StyleSheet, Text, View} from 'react-native';
import {RadioButtonCore} from '../../../../components/radio-button-new/radio-button-core';
import {Color} from '../../../../assets/styles';

export const genders = [
  {
    id: 1,
    label: 'Мужской',
  },
  {
    id: 2,
    label: 'Женский',
  },
];

interface IGenderRadioSelect {
  onSelect: (item: IRadioButtonData, index: number) => void;
  selectedIndex: number;
}

export const GenderRadioSelect: React.FC<IGenderRadioSelect> = ({
  onSelect,
  selectedIndex,
}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.label}>Ваш пол</Text>
      <RadioButtonCore
        valueForSelected={'id'}
        circleStyle={{color: Color.PRIMARY}}
        borderCircleStyle={{color: Color.PRIMARY}}
        data={genders}
        onSelect={onSelect}
        selectedIndex={selectedIndex}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 24,
  },
  label: {
    color: Color.GREY_400,
    marginBottom: 9,
  },
});
