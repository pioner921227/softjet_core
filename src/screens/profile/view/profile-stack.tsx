import React from 'react'
import { screenConfig } from '../../../navigation'
import { Routes } from '../../../navigation/config/routes'
import { screenOptions, Stack } from '../../../navigation/root-navigator'
import { useTypedSelector } from '../../../system/hooks/use-typed-selector'
import { getToken } from '../../../system/selectors/selectors'
import { ProfileSettings } from './profile-settings/profile-settings'

export const ProfileStack: React.FC = React.memo(() => {
  const token = useTypedSelector(getToken)

  return (
    <Stack.Navigator screenOptions={screenOptions}>
      {token ? (
        <>
          <Stack.Screen
            name={Routes.ProfileAuth}
            component={screenConfig.ProfileAuth}
          />
          <Stack.Screen name={Routes.Orders} component={screenConfig.Orders} />
          <Stack.Screen
            name={Routes.ProfileSettings}
            component={ProfileSettings}
          />
        </>
      ) : (
        <Stack.Screen
          name={Routes.ProfileNotAuth}
          component={screenConfig.ProfileNotAuth}
        />
      )}
    </Stack.Navigator>
  )
})
