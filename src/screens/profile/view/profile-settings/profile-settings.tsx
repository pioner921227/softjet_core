import React, {useEffect, useRef, useState} from 'react';
import {
  InteractionManager,
  Keyboard,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {InputWithLabel} from '../../../../components/input/input-with-label';
import {useInput} from '../../../../system/hooks/use-input';
import {Avatar} from '../components/avatar';
import {GenderRadioSelect, genders} from '../components/gender-radio-select';
import {IRadioButtonData} from '../../../../components/radio-button/radio-button';
import {ScreenHeaderTitleWithBackButton} from '../../../../components/screen-header/screen-header-title-with-back-button';
import {ThemedButton} from '../../../../components/buttons/themed-button';
import {useNavigation} from '@react-navigation/native';
import {Routes} from '../../../../navigation/config/routes';
import {Config} from '../../../../config';
import {useTypedSelector} from '../../../../system';
import {useDispatch} from 'react-redux';
import {AsyncActionProfile} from '../../store/async-action-profile';
import {getUserData} from '../../../../system/selectors/selectors';
import {KeyboardAvoidingScroll} from '../../../../components/keyboard-avoiding-scroll';
import {urlValidator} from '../../../../system/helpers/url-validator';
import {permissionCameraRequest} from '../../../../system/helpers/permissions/permission-camera-request';
import {useAndroidBackHandler} from '../../../../system/hooks/use-android-back-handler';
import {profileSettingsConfig} from './config';
import {useModal} from '../../../../components';
import {TextInputMask} from 'react-native-masked-text';
import {InputContainer} from '../../../../components/input/input-container';
import {InputLabel} from '../../../../components/input/input-label';

const {Color, UIStyles} = Config;

const dateMask = {
  mask: '99.99.9999',
};

export const ProfileSettings: React.FC = React.memo(() => {
  const nameInput = useInput('');
  const dateInput = useInput('');
  const emailInput = useInput('');
  const [gender, setGender] = useState<IRadioButtonData>(genders[0]);
  const [image, setImage] = useState('');
  const [wasOpenedModal, setWasOpenedModal] = useState(false);
  const [editableInputDate, setEditableInputDate] = useState(false);

  const dateInputRef = useRef<TextInputMask | null>(null)

  const {show, close} = useModal();
  const dispatch = useDispatch();

  const useData = useTypedSelector(getUserData);

  const {navigate} = useNavigation();

  const inputs = [
    {id: 0, title: 'Ваше имя', props: nameInput, placeholder: 'Имя'},
    {id: 1, title: 'Дата рождения', props: dateInput, placeholder: 'Дата'},
    {id: 2, title: 'Ваш E-mail', props: emailInput, placeholder: 'Email'},
  ];

  const onSelectGender = (item: IRadioButtonData, index: number) => {
    setGender(genders[index]);
  };

  const onPressSave = () => {
    dispatch(
        AsyncActionProfile.setUserData({
          name: nameInput.value,
          birthday: dateInput.value,
          email: emailInput.value,
          gender: gender?.id,
          // image: image,
        }),
    );
    navigate(Routes.ProfileAuth);
  };

  const onChangeImage = (image: string) => {
    setImage(image);

    dispatch(
        AsyncActionProfile.setUserData({
          image: image,
        }),
    );
  };

  const onFocusDateInput = () => {
    InteractionManager.runAfterInteractions(() => {
      setEditableInputDate(true);
    });
    if (!useData?.birthday && !wasOpenedModal) {
      show({
        title: 'Установите точную дату',
        description:
            'После установки, дату рождения нельзя будет изменить. Проверьте правильность указания данных',
        buttons: [{label: 'понятно', onPress: close}],
      });
      setWasOpenedModal(true);
    }
  };

  useEffect(() => {
    if (useData) {
      InteractionManager.runAfterInteractions(() => {
        nameInput.onChangeText(useData.name);
        dateInput.onChangeText(useData.birthday);
        emailInput.onChangeText(useData.email);
        setGender(useData.gender);
      });
    }
    profileSettingsConfig.isVisibleAvatar && permissionCameraRequest();
  }, []);

  useAndroidBackHandler();

  return (
      <SafeAreaView style={UIStyles.flex}>
        <KeyboardAvoidingScroll>
          <ScreenHeaderTitleWithBackButton
              title={profileSettingsConfig.headerTitle}
          />
          <View style={styles.container}>
            {profileSettingsConfig.isVisibleAvatar ? (
                <Avatar
                    onChangeImage={onChangeImage}
                    imageUrl={urlValidator(useData?.photo ?? '') || ''}
                />
            ) : null}

            {inputs?.map((el, index) => {
              if (index === 1) {
                return (
                    <InputContainer key={index} inputStyle={styles.input}>
                      <InputLabel label={el.title} />
                      <TextInputMask
                          ref={dateInputRef}
                          showSoftInputOnFocus={editableInputDate}
                          // disabled={!!useData?.birthday}
                          editable={!useData?.birthday}
                          testID={'phone-input'}
                          type="custom"
                          options={dateMask}
                          placeholderTextColor={Color.GREY_400}
                          style={styles.dateInput}
                          placeholder="ДД.ММ.ГГГГ"
                          keyboardType="numeric"
                          onFocus={onFocusDateInput}
                          {...dateInput}
                      />
                    </InputContainer>
                );
              }
              return (
                  <InputWithLabel
                      key={index}
                      placeholderText={el.placeholder}
                      wrapperStyle={styles.input}
                      {...el.props}
                      label={el?.title}
                      // onFocus={index === 1 ? onFocusDateInput : undefined}
                  />
              );
            })}

            <Text style={styles.description}>Ваш емайл для получения чеков</Text>
            <GenderRadioSelect
                selectedIndex={gender.id}
                onSelect={onSelectGender}
            />
            <ThemedButton
                rounded={true}
                wrapperStyle={{marginTop: 'auto', marginBottom: 10}}
                label={'Сохранить'}
                onPress={onPressSave}
            />
          </View>
        </KeyboardAvoidingScroll>
      </SafeAreaView>
  );
});

const styles = StyleSheet.create({
  container: {
    ...UIStyles.paddingH16,
    flex: 1,
  },
  description: {
    ...UIStyles.font13,
    color: Color.GREY_600,
    paddingTop: 12,
  },
  dateInput: {
    paddingVertical: 16,
  },
  input: {
    marginTop: 24,
  },
});
