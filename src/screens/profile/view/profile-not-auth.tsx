import React from 'react';
import {SafeAreaView, StyleSheet, Text, View} from 'react-native';
import {ScreenHeaderWithTitle} from '../../../components/screen-header/screen-header-with-title';
import {ProfileIcon} from '../../../components/icons/profile-icon';
import {ThemedButton} from '../../../components/buttons/themed-button';
import {Routes} from '../../../navigation/config/routes';
import {StackScreenProps} from '@react-navigation/stack';
import {TRootStackParamList} from '../../../navigation/types/types';
import {Config} from '../../../config';
import {CommonActions} from '@react-navigation/native';
import {NotAuthContent} from '../../../components/not-auth-content';

const {UIStyles, Color} = Config;

type TProps = StackScreenProps<TRootStackParamList, Routes.ProfileNotAuth>;

export const ProfileNotAuth: React.FC<TProps> = React.memo(({navigation}) => {
  const onPress = () => {
    navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [{name: Routes.LoginStack}],
      }),
    );
  };

  return (
    <SafeAreaView>
      <ScreenHeaderWithTitle>Профиль</ScreenHeaderWithTitle>
      <NotAuthContent onPress={onPress} buttonLabel={'Хочу'} />
    </SafeAreaView>
  );
});

const styles = StyleSheet.create({
  container: {},
  block: {
    // flex:1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 154,
    paddingHorizontal: 45,
  },
  descriptionWrapper: {
    marginTop: 54,
    alignItems: 'center',
  },
  button: {
    marginTop: 25,
    width: 97,
  },
});
