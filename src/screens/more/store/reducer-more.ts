import {ReducerBuilder, reducerWithInitialState} from 'typescript-fsa-reducers';
import {IGetAppInfoResponse, IGetAppSettingResponse} from '../api/types';
import {AsyncActionMore} from './async-action-more';
import {Success} from 'typescript-fsa';
import {initialStateMore, IStoreMore} from './store-more';
import {ActionMore} from './action-more';
import {ActionsLogin} from '../../login';
import {Decodable} from 'decodable-js';
import {appInfoStruct, appSettingsStruct} from './decode-structures';
import {decodeWithErrorHandling} from '../../../system/helpers/decode-with-error-handling';

const getAppInfoHandlerStarted = (store: IStoreMore): IStoreMore => {
  return {
    ...store,
    isLoading: true,
    error: false,
  };
};

const getAppInfoHandlerDone = (
  store: IStoreMore,
  {result}: Success<any, IGetAppInfoResponse>,
): IStoreMore => {
  // const decodeRes = decodeWithErrorHandling(
  //   result,
  //   appInfoStruct,
  //   '/api/info/getAppInfo',
  // );

  return {
    ...store,
    isLoading: false,
    error: false,
    appInfo: result,
  };
};

const getAppSettingsHandlerDone = (
  store: IStoreMore,
  {result}: Success<any, IGetAppSettingResponse>,
): IStoreMore => {
  // const decodeRes = decodeWithErrorHandling(
  //   result,
  //   appSettingsStruct,
  //   '/api/info/getAppSettings',
  // );
  return {
    ...store,
    isLoading: false,
    error: false,
    appSettings: result,
  };
};

const getAppInfoHandlerFailed = (store: IStoreMore): IStoreMore => {
  return {
    ...store,
    isLoading: false,
    error: true,
  };
};

const setServerUrlHandler = (
  store: IStoreMore,
  url: string | null,
): IStoreMore => {
  return {
    ...store,
    serverUrl: url,
  };
};

const setAppVersion = (store: IStoreMore, appVersion: string): IStoreMore => {
  return {
    ...store,
    appVersion,
  };
};

const resetHandler = (store: IStoreMore): IStoreMore => {
  return {
    ...initialStateMore,
    appInfo: store.appInfo,
  };
};

export const reducerMore: ReducerBuilder<IStoreMore> = reducerWithInitialState(
  initialStateMore,
)
  .case(ActionsLogin.logoutAccount, resetHandler)
  .case(AsyncActionMore.getAppInfo.async.started, getAppInfoHandlerStarted)
  .case(AsyncActionMore.getAppInfo.async.done, getAppInfoHandlerDone)
  .case(AsyncActionMore.getAppInfo.async.failed, getAppInfoHandlerFailed)
  .case(AsyncActionMore.getAppSettings.async.done, getAppSettingsHandlerDone)
  .case(ActionMore.setServerUrl, setServerUrlHandler)
  .case(ActionMore.setAppVersion, setAppVersion);
