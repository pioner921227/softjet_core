import {IGetAppInfoResponse, IGetAppSettingResponse} from '../api/types';
import {T} from 'decodable-js';

export const appSettingsStruct: IGetAppSettingResponse = {
  app_black_list: T.boolean,
  app_cashback_status: T.boolean,
  app_delivery_exists: T.boolean,
  app_modal_type_order: T.boolean,
  app_order_evaluation: T.boolean,
  app_org_change: T.boolean,
  app_phone_check: T.boolean,
  app_promocode_status: T.boolean,
  app_referal_status: T.boolean,
  app_repeat_order: T.boolean,
  app_required_street_selection: T.boolean,
  app_reviews_status: T.boolean,
  app_screen_type: T.string,
  app_state_disable_text: T.string,
  app_state_status: T.boolean,
  app_ver_api: T.string,
  app_white_list: T.boolean,
};

export const appInfoStruct: IGetAppInfoResponse = {
  instagram: T.string,
  facebook: T.string,
  whats: T.string,
  vk: T.string,
  organization_card: [T.string],
  qr_tag: T.string,
  phone: T.string,
  terms_of_use: T.string,
  time_work: T.string,
  delivery_settings: [
    {
      text_disabled: T.string,
      worktime_from: T.string,
      worktime_to: T.string,
    },
  ],
  privacy_policy: T.string,
  bonus_info: T.string_$,
  return_warranty: T.string,
  price_free: T.string,
  price_minimal: T.string,
  work_with_us: T.string,
  profile_background: T.string,
  payment_urls: {
    fail_url: T.string,
    success_url: T.string,
  },
};
