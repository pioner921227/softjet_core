import {IRequestLoading} from '../../../system/root-store/root-store';
import {IGetAppInfoResponse, IGetAppSettingResponse} from '../api/types';

export interface IStoreMore extends IRequestLoading {
  appInfo: IGetAppInfoResponse | null;
  appSettings: IGetAppSettingResponse | null;
  serverUrl: string | null;
  appVersion: string | null;
}

export const initialStateMore: IStoreMore = {
  isLoading: false,
  error: false,
  appInfo: null,
  appVersion: null,
  appSettings: null,
  serverUrl: null,
};
