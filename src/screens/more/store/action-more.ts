import {actionCreator} from '../../../system';

export class ActionMore {
  static setServerUrl = actionCreator<string | null>('MORE/SET_SERVER_URL');
  static setAppVersion = actionCreator<string>('MORE/SET_APP_VERSION');
}
