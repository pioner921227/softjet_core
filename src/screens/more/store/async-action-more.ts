import {asyncActionCreator} from '../../../system/root-store/action-creator';
import {IGetAppInfoResponse, IGetAppSettingResponse} from '../api/types';
import {RequestMore} from '../api/request-more';

export class AsyncActionMore {
  static getAppInfo = asyncActionCreator<void, IGetAppInfoResponse, Error>(
    'MORE/GET_APP_INFO',
    RequestMore.getAppInfo,
  );
  static getAppSettings = asyncActionCreator<
    void,
    IGetAppSettingResponse,
    Error
  >('MORE/GET_APP_SETTINGS', RequestMore.getAppSettings);
}
