export interface IGetAppInfoResponse {
  instagram: string;
  facebook: string;
  whats: string;
  vk: string;
  organization_card: string[];
  odnoklassniki: string;
  qr_tag: string;
  phone: string;
  time_work: string;
  delivery_settings: [
    {
      text_disabled: string;
      worktime_from: string;
      worktime_to: string;
    },
  ];
  privacy_policy: string;
  bonus_info?: string;
  return_warranty: string;
  price_free: string;
  price_minimal: string;
  work_with_us: string;
  profile_background: string;
  terms_of_use: string;
  payment_urls: {
    fail_url: string;
    success_url: string;
  };
  answer_options: AnswerOptionsItem[];
}

export interface AnswerOptionsItem {
  id: number;
  answer_option: string;
}

export interface IGetAppSettingResponse {
  app_black_list: boolean;
  app_cashback_status: boolean;
  app_delivery_exists: boolean;
  app_modal_type_order: boolean;
  app_order_evaluation: boolean;
  app_org_change: boolean;
  app_phone_check: boolean;
  app_promocode_status: boolean;
  app_referal_status: boolean;
  app_repeat_order: boolean;
  app_required_street_selection: boolean;
  app_reviews_status: boolean;
  app_screen_type: string;
  app_state_disable_text: string;
  app_state_status: boolean;
  app_ver_api: string;
  app_white_list: boolean;
  app_bonus_hidden: boolean;
}
