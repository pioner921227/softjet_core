import {ApiService} from '../../../system';
import {IGetAppInfoResponse, IGetAppSettingResponse} from './types';

export class RequestMore {
  static getAppSettings(): Promise<IGetAppSettingResponse> {
    return ApiService.post('/api/info/getAppSettings');
  }
  static getAppInfo(): Promise<IGetAppInfoResponse> {
    return ApiService.get('/api/info/getAppInfo');
  }
}
