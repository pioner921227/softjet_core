import React from 'react';
import { SafeAreaView, StyleSheet, View } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import { KeyboardAvoidingScroll } from '../../../../components';
import { ScreenHeaderTitleWithBackButton } from '../../../../components/screen-header/screen-header-title-with-back-button';
import { TitleWithValueRow } from '../../../../components/title-with-value-row';
import { Config } from '../../../../config';
import { useAndroidBackHandler } from '../../../../system/hooks/use-android-back-handler';
import { appInfoConfig } from './config';

const {UIStyles} = Config;

export const AppInfo = React.memo(() => {

  useAndroidBackHandler();

  return (
    <SafeAreaView>
      <ScreenHeaderTitleWithBackButton title={appInfoConfig.headerTitle} />
      <KeyboardAvoidingScroll>
        <View style={styles.content}>
          <TitleWithValueRow
            title={'Имя приложения'}
            value={DeviceInfo.getApplicationName()}
          />
          <TitleWithValueRow
            title={'Версия  приложения'}
            value={DeviceInfo.getVersion()}
          />
          <TitleWithValueRow
            title={'Версия сборки'}
            value={DeviceInfo.getBuildNumber()}
          />
          <TitleWithValueRow
            title={'Bundle ID'}
            value={DeviceInfo.getBundleId()}
          />
        </View>
      </KeyboardAvoidingScroll>
    </SafeAreaView>
  );
});

const styles = StyleSheet.create({
  content: {
    ...UIStyles.paddingH16,
    marginTop: 30,
  },
});
