import React, {useMemo} from 'react';
import {IOrganisation} from '../../../delivery/types/types';
import {Linking, StyleSheet, Text, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import {urlValidator} from '../../../../system/helpers/url-validator';
import {Config, ImageRepository} from '../../../../config';
import {CallButton} from './call-button';

const {UIStyles, Color, RADIUS} = Config;

interface IOrganisationListItemProps {
  item: IOrganisation;
}

export const OrganisationListItem: React.FC<IOrganisationListItemProps> = React.memo(
  ({item}) => {
    const image = useMemo(
      () =>
        item.img
          ? {uri: urlValidator(`/${item.img}`)}
          : ImageRepository.ProductImagePlaceholder,
      [item.img],
    );

    const onPressCall = () => {
      if (item.phone) {
        Linking.openURL(`tel:${item.phone}`);
      }
    };

    return (
      <View style={styles.container}>
        <FastImage source={image} style={styles.image} />
        <View style={styles.content}>
          <Text style={styles.title}>{item.title}</Text>
          <Text style={styles.description}>{item.description}</Text>
          <Text style={styles.workTime}>{item.worktime}</Text>
        </View>
        <View style={styles.phoneBlock}>
          <Text style={styles.phone}>{item.phone}</Text>
          <CallButton onPress={onPressCall} />
        </View>
      </View>
    );
  },
);

const styles = StyleSheet.create({
  container: {
    backgroundColor: Color.WHITE,
    borderRadius: RADIUS.LARGE,
    ...UIStyles.shadowNormal,
    marginBottom: 16,
  },

  image: {
    width: '100%',
    height: 194,
    borderTopLeftRadius: RADIUS.LARGE,
    borderTopRightRadius: RADIUS.LARGE,
    ...UIStyles.shadowNormal,
  },
  content: {
    paddingTop: 27,
    paddingHorizontal: 20,
  },
  title: {
    ...UIStyles.font18b,
    // fontSize: responsiveFontSize(18),
    color: Color.BLACK,
  },
  description: {
    paddingVertical: 12,
    ...UIStyles.font15,
    color: Color.GREY_600,
  },
  workTime: {
    ...UIStyles.font15,
    color: Color.SECONDARY,
    paddingBottom: 12,
  },
  phoneBlock: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    backgroundColor: Color.BACKGROUND_COLOR,
    paddingVertical: 17.5,
    width: '100%',
    borderBottomLeftRadius: RADIUS.LARGE,
    borderBottomRightRadius: RADIUS.LARGE,
    borderWidth: 2,
    borderColor: Color.WHITE,
  },
  phone: {
    ...UIStyles.font18b,
    // fontSize: getFontSize(18),
    // fontSize: responsiveFontSize(18),
    color: Color.BLACK,
  },
});
