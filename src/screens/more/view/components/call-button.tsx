import {StyleSheet, TouchableOpacity} from 'react-native';
import {CellPhoneIcon} from '../../../../components/icons/cell-phone-icon';
import React from 'react';
import {Config} from '../../../../config';
import {useHitSlop} from '../../../../system/hooks/use-hit-slop';

const {Color, RADIUS} = Config;

interface ICallButtonProps {
  onPress: () => void;
}

export const CallButton: React.FC<ICallButtonProps> = ({onPress}) => {
  const hitStop = useHitSlop(10);
  return (
    <TouchableOpacity
      hitSlop={hitStop}
      onPress={onPress}
      style={styles.cellPhoneWrapper}>
      <CellPhoneIcon fill={Color.WHITE} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  cellPhoneWrapper: {
    backgroundColor: Color.SECONDARY,
    width: 36,
    height: 36,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: RADIUS.SMALL,
  },
});
