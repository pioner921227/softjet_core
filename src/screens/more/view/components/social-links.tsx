import React from 'react'
import { Linking, StyleSheet, TouchableOpacity, View } from 'react-native'
import { FacebookIcon } from '../../../../components/icons/social/facebook_icon'
import { InstagramIcon } from '../../../../components/icons/social/instagram-icon'
import { VkIcon } from '../../../../components/icons/social/vk_icon'
import { IGetAppInfoResponse } from '../../api/types'
import { useHitSlop } from '../../../../system/hooks/use-hit-slop'
import { useTypedSelector } from 'softjet_core/src/system'
import { getCurrentOrganisation } from 'softjet_core/src/system/selectors/selectors'

export enum SocialLinksEnum {
  vk = 'social_link_vk',
  facebook = 'social_link_fb',
  instagram = 'social_link_instagram',
  odnoklassniki = 'social_link_ok',
}

type TSocialItem = {
  id: number
  Icon: React.FC
  fieldName: SocialLinksEnum
}

interface ISocialLinks {}

export const socialItems: TSocialItem[] = [
  { id: 0, Icon: FacebookIcon, fieldName: SocialLinksEnum.facebook },
  { id: 1, Icon: InstagramIcon, fieldName: SocialLinksEnum.instagram },
  { id: 2, Icon: VkIcon, fieldName: SocialLinksEnum.vk },
]

export const SocialLinks: React.FC<ISocialLinks> = () => {
  const currentOrganization = useTypedSelector(getCurrentOrganisation)

  const openLink = (link: string) => {
    Linking.openURL(link)
  }

  const hitStop = useHitSlop(5)

  return (
    <View style={styles.container}>
      {socialItems.map(({ Icon, id, fieldName }) => {
        if (!currentOrganization?.[fieldName]) {
          return
        }

        const onPress = () => {
          openLink(currentOrganization?.[fieldName])
        }

        return (
          <TouchableOpacity
            style={styles.button}
            hitSlop={hitStop}
            key={id}
            onPress={onPress}
          >
            <Icon />
          </TouchableOpacity>
        )
      })}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    flexDirection: 'row',
    // width: 144,
    alignItems: 'center',
    // justifyContent: 'space-between',
    alignSelf: 'center',
  },
  button: {
    marginHorizontal: 5,
  },
})
