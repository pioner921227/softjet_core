import React, {useCallback, useMemo} from 'react';
import {FlatList, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {useTypedSelector} from '../../../../system';
import {OrganisationListItem} from '../components/organisation-list-item';
import {Config} from '../../../../config';
import {ScreenHeaderTitleWithIcons} from '../../../../components/screen-header/screen-header-title-with-icons';
import {ArrowLeftIcon} from '../../../../components/icons/arrow-left-icon';
import {useNavigation} from '@react-navigation/native';
import {Routes} from '../../../../navigation';
import {StackNavigationProp} from '@react-navigation/stack';
import {TRootStackParamList} from '../../../../navigation/types/types';
import {BottomSheetMenuType} from '../../../delivery/view/organisation-list-with-map/organisation-list-with-map';
import {initialWindowMetrics} from 'react-native-safe-area-context/src/InitialWindow';
import {EmptyDataTitle} from '../../../../components/empty-data-title';
import {getOrganisations} from '../../../../system/selectors/selectors';
import {organisationListConfig} from './config';
import Animated from 'react-native-reanimated';
import {useAndroidBackHandler} from '../../../../system/hooks/use-android-back-handler';
import {ScrollableHeaderWithList} from '../../../../components/scrollable-header-with-list';

const {UIStyles, Color} = Config;

type TNavigation = StackNavigationProp<
  TRootStackParamList,
  Routes.SelectOrganisationInMap
>;

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

export const OrganisationList = React.memo(() => {
  const organisations = useTypedSelector(getOrganisations);
  const {goBack, navigate} = useNavigation<TNavigation>();

  const onPressGoToMap = () => {
    navigate(Routes.SelectOrganisationInMap, {
      bottomSheetMenuType: BottomSheetMenuType.Contacts,
    });
  };

  const GoTyMapButton = () => {
    return (
      <TouchableOpacity onPress={onPressGoToMap} style={styles.goToMapButton}>
        <Text style={styles.goToMapTitle}>На карте</Text>
      </TouchableOpacity>
    );
  };

  const renderItem = useCallback(
    ({item}) => <OrganisationListItem item={item} />,
    [],
  );

  const keyExtractor = useCallback(item => item.id?.toString(), []);

  const flatListContainerStyle = useMemo(() => {
    return {
      ...styles.contentContainer,
      paddingTop: 16,
    };
  }, []);

  useAndroidBackHandler();

  return (
    <ScrollableHeaderWithList
      header={
        <ScreenHeaderTitleWithIcons
          title={organisationListConfig.headerTitle}
          ComponentLeft={ArrowLeftIcon}
          titleWrapperStyle={styles.titleWrapper}
          onPressLeftIcon={goBack}
          componentRightStyle={styles.componentRightStyle}
          onPressRightIcon={onPressGoToMap}
          ComponentRight={GoTyMapButton}
        />
      }
      list={
        <AnimatedFlatList
          style={styles.flatList}
          ListEmptyComponent={EmptyDataTitle}
          initialNumToRender={3}
          windowSize={10}
          keyExtractor={keyExtractor}
          contentContainerStyle={flatListContainerStyle}
          data={organisations}
          renderItem={renderItem}
        />
      }
    />
  );
});

const styles = StyleSheet.create({
  goToMapTitle: {
    ...UIStyles.font15,
    color: Color.PRIMARY,
  },
  contentContainer: {
    ...UIStyles.paddingH16,
    paddingBottom: 80 + (initialWindowMetrics?.insets.bottom || 0),
    flexGrow: 1,
  },
  componentRightStyle: {
    flex: 2,
  },
  goToMapButton: {
    alignItems: 'flex-end',
  },
  flatList: {
    height: '100%',
  },
  titleWrapper: {},
});
