import {
  CompositeNavigationProp,
  useNavigation,
} from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'
import React from 'react'
import { Linking, ScrollView, StyleSheet, Text, View } from 'react-native'
import { Edge, SafeAreaView } from 'react-native-safe-area-context'
import { ArrowRightWithoutLineIcon } from '../../../../components/icons/arrow-right-without-line-icon'
import { WhatsappIcon } from '../../../../components/icons/whatsapp-icon'
import { ListItemIcons } from '../../../../components/list-item-icons/list-item-icons'
import { Config } from '../../../../config'
import { Routes } from '../../../../navigation/config/routes'
import { TRootStackParamList } from '../../../../navigation/types/types'
import { useTypedSelector } from '../../../../system'
import { getCurrentOrganisation } from '../../../../system/selectors/selectors'
import { CallButton } from '../components/call-button'
import { SocialLinks } from '../components/social-links'
import { moreConfig } from './config'

const edges: Edge[] = ['top']

const { UIStyles, Color, RADIUS } = Config

type TNavigationProps = CompositeNavigationProp<
  StackNavigationProp<TRootStackParamList, Routes.DeliveryAndPayment>,
  StackNavigationProp<TRootStackParamList, Routes.Policy>
>

type TNavigationPropsSecond = CompositeNavigationProp<
  StackNavigationProp<TRootStackParamList, Routes.OrganisationList>,
  StackNavigationProp<TRootStackParamList, Routes.AppInfo>
>

export const More: React.FC = React.memo(() => {
  const { navigate } = useNavigation<
    TNavigationProps & TNavigationPropsSecond
  >()
  // const appInfo = useTypedSelector(getAppInfo)
  const currentOrganization = useTypedSelector(getCurrentOrganisation)

  const openLink = (link?: string) => {
    if (link) {
      Linking.openURL(link)
    }
  }

  return (
    <SafeAreaView edges={edges}>
      <ScrollView
        contentContainerStyle={styles.contentContainerStyle}
        bounces={false}
      >
        <View style={styles.contentContainer}>
          {currentOrganization?.phone ? (
            <View style={styles.cellPhoneRowWrapper}>
              <View>
                <Text style={styles.phoneNumber}>
                  {currentOrganization?.phone}
                </Text>
                <Text style={styles.description}>
                  {currentOrganization?.time_work}
                </Text>
              </View>
              <CallButton
                onPress={() => openLink(`tel:${currentOrganization?.phone}`)}
              />
            </View>
          ) : null}

          {moreConfig.rowItems.map((el) =>
            el.isVisible ? (
              <ListItemIcons
                key={el.title}
                leftComponent={el.Icon}
                rightComponent={ArrowRightWithoutLineIcon}
                title={el.title}
                //@ts-ignore
                onPress={() => el.onPress(() => navigate(el.route))}
              />
            ) : null,
          )}
          {moreConfig.isVisibleWhatsAppRow ? (
            <ListItemIcons
              leftComponent={WhatsappIcon}
              rightComponent={ArrowRightWithoutLineIcon}
              title={'Обратная связь (Whatsapp)'}
              onPress={() => openLink(currentOrganization?.whats)}
            />
          ) : null}
        </View>
        {moreConfig.isVisibleSocialLinks ? <SocialLinks /> : null}
      </ScrollView>
    </SafeAreaView>
  )
})

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
  },
  contentContainer: {
    ...UIStyles.paddingH16,
  },
  cellPhoneRowWrapper: {
    marginTop: 12,
    paddingHorizontal: 20,
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: Color.WHITE,
    borderRadius: RADIUS.LARGE,
    ...UIStyles.shadowNormal,
  },
  phoneNumber: {
    ...UIStyles.font18b,
  },
  description: {
    marginTop: 4,
    ...UIStyles.font14,
    color: Color.GREY_400,
  },
  contentContainerStyle: {
    paddingBottom: 20,
  },
})
