import { MapMarkerIcon } from '../../../../components/icons/map-marker-icon'
import { Routes } from '../../../../navigation'
import { InfoInCircleIcon } from '../../../../components/icons'
import React from 'react'
import { SvgProps } from 'react-native-svg'

type Handler = (defaultHandler: () => void) => void

const defaultHandler = (handler: () => void) => {
  handler()
}

interface IMoreConfig<T = unknown & Routes> {
  rowItems: {
    title: string
    Icon: React.FC<SvgProps>
    route: T
    isVisible: boolean
    onPress: Handler
  }[]
  isVisibleWhatsAppRow: boolean
  isVisibleSocialLinks: boolean
}

export const moreConfig: IMoreConfig = {
  rowItems: [
    {
      title: 'Рестораны',
      Icon: MapMarkerIcon,
      route: Routes.OrganisationList,
      isVisible: true,
      onPress: defaultHandler,
    },
    {
      title: 'Доставка и оплата',
      Icon: InfoInCircleIcon,
      route: Routes.DeliveryAndPayment,
      isVisible: true,
      onPress: defaultHandler,
    },
    {
      title: 'Политика конфиденциальности',
      Icon: InfoInCircleIcon,
      route: Routes.Policy,
      isVisible: true,
      onPress: defaultHandler,
    },
    {
      title: 'О приложении',
      Icon: InfoInCircleIcon,
      route: Routes.AppInfo,
      isVisible: true,
      onPress: defaultHandler,
    },
  ],
  isVisibleWhatsAppRow: true,
  isVisibleSocialLinks: true,
}
