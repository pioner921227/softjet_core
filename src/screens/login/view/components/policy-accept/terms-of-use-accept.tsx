import React, {useRef} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import BouncyCheckbox from 'react-native-bouncy-checkbox';
import {Config} from '../../../../../config';
import {useHitSlop} from '../../../../../system/hooks/use-hit-slop';
const {Color, UIStyles} = Config;

interface IPolicyAcceptProps {
  isChecked: boolean;
  onToggle: () => void;
  onPressTermsOfUse: () => void;
}

export const TermsOfUseAccept: React.FC<IPolicyAcceptProps> = ({
  isChecked,
  onToggle,
  onPressTermsOfUse,
}) => {


  return (
    <View style={styles.container}>
      <BouncyCheckbox
        size={20}
        bounceEffect={1}
        bounceFriction={10}
        iconStyle={styles.checkboxIcon}
        fillColor={Color.BLACK}
        onPress={onToggle}
        isChecked={isChecked}
      />
      <View style={styles.textWrapper}>
        <Text style={styles.text}>Согласен с условиями</Text>
        <Text
          onPress={onPressTermsOfUse}
          style={[styles.text, {textDecorationLine: 'underline'}]}>
          Пользовательского соглашения
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  textWrapper: {
    marginTop: 16,
  },
  text: {
    ...UIStyles.font15,
    color: Color.GREY_200,
  },
  checkboxIcon:{
    borderColor: Color.BLACK,
    borderRadius: 4
  }
});
