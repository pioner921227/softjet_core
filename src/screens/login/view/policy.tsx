import React, {useRef} from 'react';
import {StyleSheet, View} from 'react-native';
import {ScreenHeaderTitleWithBackButton} from '../../../components/screen-header/screen-header-title-with-back-button';
import {Config} from '../../../config';
import {useAndroidBackHandler} from '../../../system/hooks/use-android-back-handler';
import {ScrollableHeaderWithList} from '../../../components/scrollable-header-with-list';
import Animated from 'react-native-reanimated';
import {HtmlParseAndView, HtmlStyles} from '@react-native-html/renderer';
import {useTypedSelector} from '../../../system';
import {getAppInfo} from '../../../system/selectors/selectors';
import {adaptiveSize} from '../../../system/helpers/responseive-font-size';
import {windowWidth} from '../../../system/helpers/window-size';

const {UIStyles, Color} = Config;

export const Policy: React.FC = React.memo(() => {
  useAndroidBackHandler();
  const scrollRef = useRef<Animated.ScrollView | null>(null);

  const appInfo = useTypedSelector(getAppInfo);

  return (
    <ScrollableHeaderWithList
      header={
        <ScreenHeaderTitleWithBackButton
          titleStyle={styles.headerTitle}
          title={'Политика конфиденциальности'}
        />
      }
      list={
        <Animated.ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{paddingBottom: 50}}
        >
          <View style={styles.contentContainer}>
            {appInfo?.privacy_policy ? (
              <HtmlParseAndView
                rawHtml={appInfo?.privacy_policy}
                htmlStyles={htmlStyles}
                containerStyle={styles.container}
                //@ts-ignore
                scrollRef={scrollRef.current}
              />
            ) : null}
            {/*<Text style={styles.text}>{textPolicy}</Text>*/}
          </View>
        </Animated.ScrollView>
      }
    />
  );
});

const styles = StyleSheet.create({
  contentContainer: {
    marginTop: 16,
    width: windowWidth,
    ...Config.UIStyles.paddingH16,
  },
  container: {
    marginTop: 20,
    ...UIStyles.flexGrow,
    ...UIStyles.paddingH16,
    paddingBottom: 50,
  },
  text: {
    color: Color.BLACK,
    ...UIStyles.font15,
  },
  headerTitle: {
    fontSize: 18,
  },
});

const htmlStyles: HtmlStyles = {
  text: {
    fontSize: adaptiveSize(16),
    lineHeight: 18 * 1.4,
  },
  paragraph: {
    marginVertical: 10,
  },
  image: {
    marginVertical: 0,
  },
  list: {
    marginVertical: 5,
  },
  h1: {
    fontSize: 30,
    lineHeight: 30 * 1.4,
    marginTop: 10,
    fontWeight: '500',
  },
  h2: {
    fontSize: 26,
    lineHeight: 26 * 1.4,
    marginTop: 10,
    fontWeight: '500',
  },
  h3: {
    fontSize: 24,
    lineHeight: 24 * 1.4,
    marginTop: 10,
    fontWeight: '500',
  },
  listItem: {
    marginVertical: 2,
  },
  listItemContent: {},
};

// const styles = StyleSheet.create({
//   container: {
//     marginTop: 20,
//     ...UIStyles.flexGrow,
//     ...UIStyles.paddingH16,
//     paddingBottom: 20,
//   },
// });
