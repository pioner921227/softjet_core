import React, {useEffect, useRef, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {windowWidth} from '../../../../system/helpers/window-size';
import {style} from '../../../../system/helpers/styles';
import {fonts} from '../../../../system/helpers/fonts';
import {CodeField, Cursor} from 'react-native-confirmation-code-field';
import {StackScreenProps} from '@react-navigation/stack';
import {TRootStackParamList} from '../../../../navigation/types/types';
import {Routes} from '../../../../navigation/config/routes';
import {AsyncActionsLogin} from '../../store/async-actions-login';
import {ArrowLeftIcon} from '../../../../components/icons/arrow-left-icon';
import {Config} from '../../../../config';
import {AsyncActionsDelivery} from '../../../delivery';
import {loginCodeConfig} from './config';
import {useTypedDispatch} from '../../../../system/hooks/use-typed-dispatch';
import {useTypedSelector} from '../../../../system';
import {
  getCartData,
  getCurrentAddress,
} from '../../../../system/selectors/selectors';
import {setApiToken} from '../../../../system/api/set-api-token';
import {sendErrorToServer} from '../../../../system/helpers/send-error-to-server';
import {useHitSlop} from '../../../../system/hooks/use-hit-slop';
import {useModal} from '../../../../components';
import {TabRootScreen} from '../../../../navigation/tab-root-screen';

const {Color, UIStyles} = Config;

type TProps = StackScreenProps<TRootStackParamList, Routes.LoginCode>;

export const LoginCode: React.FC<TProps> = React.memo(({route, navigation}) => {
  const {request_id, phoneNumber} = route.params || {};
  const [requestId, setRequestId] = useState<number>(request_id);
  const [timerValue, setTimerValue] = useState(loginCodeConfig.TIMER_VALUE);
  const [isError, setIsError] = useState(false);
  const [isVisibleTimer, setIsVisibleTimer] = useState(false);
  const timerID = useRef<NodeJS.Timer | null>(null);
  const currentAddress = useTypedSelector(getCurrentAddress);
  const cartData = useTypedSelector(getCartData);
  const {show, close} = useModal();

  const dispatch = useTypedDispatch();

  const [value, setValue] = useState('');

  const sendCode = async () => {
    try {
      const res = await dispatch(
        AsyncActionsLogin.sendVerificationCode({
          request_id: +requestId,
          code: +value,
        }),
      );

      if (!res?.token) {
        throw new Error(res?.error_msg);
      }

      setApiToken(res.token);

      setIsError(false);

      if (currentAddress) {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const {id, ...data} = currentAddress || {};
        await dispatch(
          AsyncActionsDelivery.saveNewAddress({
            ...data,
            delivery_exists: true,
          }),
        );
      }

      dispatch(AsyncActionsDelivery.getUserAddressList());

      if (loginCodeConfig.onSuccessVerificationCode) {
        loginCodeConfig.onSuccessVerificationCode();
        return;
      }

      if (cartData?.products?.length) {
        //@ts-ignore
        navigation.navigate(Routes.TabRootScreen, {screen: Routes.Cart});
        return;
      }

      navigation.navigate(Routes.SelectCity);
    } catch (e) {
      setIsError(true);
      show({
        title: 'Ошибка',
        description: e.message,
        buttons: [{label: 'Ok', onPress: close}],
      });
      // sendErrorToServer(e.toString(), 'Login');
      // console.log('AUTHORIZATION ERROR ', e);
    }
  };

  const clearTimer = () => {
    if (timerID.current) {
      clearInterval(timerID.current);
    }
  };

  const repeatVerificationHandler = async () => {
    const res = await dispatch(
      AsyncActionsLogin.getSms({phone: route.params.phoneNumber}),
    );
    if (res.request_id) {
      setRequestId(res.request_id);
    }
  };

  const onRepeatVerification = () => {
    setIsVisibleTimer(true);
    repeatVerificationHandler();

    timerID.current = setInterval(() => {
      if (timerValue >= 0) {
        setTimerValue(state => {
          if (state > 0) {
            return state - 1;
          } else {
            clearTimer();
            setIsVisibleTimer(false);
            return loginCodeConfig.TIMER_VALUE;
          }
        });
      }
    }, 1000);
  };

  useEffect(() => {
    if (value.length === loginCodeConfig.CODE_COUNT) {
      sendCode();
    }
  }, [value]);

  const goBackHandler = (): void => {
    navigation.goBack();
  };

  useEffect(() => {
    return clearTimer;
  }, []);

  const hitStop = useHitSlop(30);

  const ref = useRef<TextInput | null>(null);

  return (
    <SafeAreaView>
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={styles.root}
        bounces={false}
        showsVerticalScrollIndicator={false}>
        <TouchableOpacity
          hitSlop={hitStop}
          onPress={goBackHandler}
          style={styles.backButton}>
          <ArrowLeftIcon />
        </TouchableOpacity>
        <View style={{paddingHorizontal: 30}}>
          <Text style={styles.title}>Введите код из СМС</Text>
          <Text style={styles.description}>
            Мы отправили СМС с кодом на номер {phoneNumber || ''}
          </Text>
          <CodeField
            ref={ref}
            autoFocus={true}
            value={value}
            onChangeText={setValue}
            cellCount={loginCodeConfig.CODE_COUNT}
            rootStyle={styles.codeFieldRoot}
            keyboardType="number-pad"
            textContentType="oneTimeCode"
            renderCell={({index, symbol, isFocused}) => (
              <View
                key={index}
                style={[styles.cellRoot, isFocused && styles.focusCell]}>
                <Text style={styles.cellText}>
                  {symbol || (isFocused ? <Cursor /> : null)}
                </Text>
              </View>
            )}
          />
          <View style={{marginTop: 24}}>
            {isError && (
              <Text style={styles.failedCodeTitle}>
                Код указан не верно.{'\n'}Попробуйте еще раз
              </Text>
            )}
          </View>
        </View>

        <View style={{marginTop: 'auto', alignItems: 'center'}}>
          {isVisibleTimer ? (
            <Text style={styles.sendNewCodeTitle}>
              Отправка нового кода через {timerValue} сек
            </Text>
          ) : (
            <TouchableOpacity onPress={onRepeatVerification}>
              <Text style={styles.sendNewCodeTitle}>Отправить новый код</Text>
            </TouchableOpacity>
          )}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
});

const styles = StyleSheet.create({
  root: style.view({
    backgroundColor: Color.WHITE,
    ...UIStyles.paddingH16,
    // paddingHorizontal: windowWidth * 0.12,
    alignItems: 'center',
    height: '100%',
  }),
  codeFieldRoot: {
    color: Color.DARK,
  },
  title: style.text({
    textAlign: 'center',
    fontSize: windowWidth * 0.064,
    fontFamily: fonts.robotoBold,
    paddingTop: windowWidth * 0.17,
  }),
  description: style.text({
    fontSize: windowWidth * 0.048,
    fontFamily: fonts.robotoRegular,
    paddingTop: windowWidth * 0.04,
  }),
  cellRoot: style.view({
    marginTop: windowWidth * 0.09,
    width: windowWidth * 0.1,
    height: windowWidth * 0.13,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: Color.GREY_100,
    borderBottomWidth: windowWidth * 0.005,
    borderRadius: windowWidth * 0.002,
    marginHorizontal: windowWidth * 0.021,
  }),
  cellText: style.text({
    color: Color.BLACK,
    fontSize: windowWidth * 0.064,
    textAlign: 'center',
  }),
  focusCell: style.view({
    borderBottomColor: Color.BLACK,
    borderBottomWidth: windowWidth * 0.005,
  }),
  forgetPasswordButton: style.view({
    position: 'absolute',
    bottom: windowWidth * 0.06,
  }),
  forgetPasswordTitle: style.text({
    fontFamily: fonts.robotoRegular,
    fontSize: windowWidth * 0.04,
    color: Color.BACKGROUND_COLOR,
  }),
  forgetPasswordTitleSendCode: style.text({
    fontFamily: fonts.robotoRegular,
    fontSize: windowWidth * 0.04,
    color: Color.GREY_900,
  }),
  forgetPasswordTitleSendCodeTime: style.text({
    fontFamily: fonts.robotoBold,
    fontSize: windowWidth * 0.04,
    color: Color.BLACK,
  }),
  backArrowButton: style.view({
    paddingTop: windowWidth * 0.04,
    alignSelf: 'flex-start',
    marginLeft: windowWidth * -0.05,
  }),
  backArrow: style.image({
    width: windowWidth * 0.04,
    height: windowWidth * 0.04,
  }),
  failedCodeTitle: {
    ...UIStyles.font15,
    color: Color.PRIMARY,
    textAlign: 'center',
  },
  sendNewCodeTitle: {
    ...UIStyles.font15,
    color: Color.PRIMARY,
  },
  backButton: {
    marginTop: 20,
    zIndex: 2,
    marginLeft: 10,
    alignSelf: 'flex-start',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
