import {INextButtonConfig} from '../../../delivery';

interface ILoginCodeConfig extends INextButtonConfig<void> {
  onSuccessVerificationCode: null | (() => void);
  TIMER_VALUE: number;
  CODE_COUNT: number;
}

export const loginCodeConfig: ILoginCodeConfig = {
  nextButtonLabel: '',
  onPressNextButton: null,
  onSuccessVerificationCode: null,
  TIMER_VALUE: 10,
  CODE_COUNT: 4,
};
