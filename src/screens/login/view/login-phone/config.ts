import {INextButtonConfig} from '../../../delivery';
import {IGetSmsResponse} from '../../types/types';

interface ILoginPhoneConfig
  extends INextButtonConfig<{result: IGetSmsResponse}> {
  onPressClose: null | (() => void);
}

export const loginPhoneConfig: ILoginPhoneConfig = {
  onPressClose: null,
  onPressNextButton: null,
  nextButtonLabel: 'Далее',
};
