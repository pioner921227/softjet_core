import React, { useCallback, useEffect, useState } from "react";
import { TextInputMask } from "react-native-masked-text";
import { useInput } from "../../../../system/hooks/use-input";
import {
  InteractionManager,
  SafeAreaView,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { Routes } from "../../../../navigation/config/routes";
import { ThemedButton } from "../../../../components/buttons/themed-button";
import { windowWidth } from "../../../../system/helpers/window-size";
import { fonts } from "../../../../system/helpers/fonts";
import { style } from "../../../../system/helpers/styles";
import { TermsOfUseAccept } from "../components/policy-accept/terms-of-use-accept";
import { CloseCrossIcon } from "../../../../components/icons/close-cross-icon";
import { AsyncActionsLogin } from "../../store/async-actions-login";
import { StackScreenProps } from "@react-navigation/stack";
import { TRootStackParamList } from "../../../../navigation/types/types";
import { Config } from "../../../../config";
import { loginPhoneConfig } from "./config";
import { useTypedDispatch } from "../../../../system/hooks/use-typed-dispatch";
import { KeyboardAvoidingScroll } from "../../../../components/keyboard-avoiding-scroll";
import { AsyncActionsDelivery } from "../../../delivery";
import { adaptiveSize } from "../../../../system/helpers/responseive-font-size";
import { Decodable, T } from "decodable-js";
import { useDecodeErrorShowModal } from "../../../../system/hooks/use-decode-error-show-modal";
import { useIsLoading } from "../../../../system/hooks/use-is-loading";

const { Color, UIStyles, RADIUS } = Config;

const DecodeJsonStructure = {
  request_id: T.number,
};

type TProps = StackScreenProps<TRootStackParamList, Routes.LoginPhone>;

export const LoginPhone: React.FC<TProps> = React.memo(({ navigation }) => {
  const phoneInput = useInput("");
  const [isPolicyAccepted, setIsPolyAccepted] = useState(true);
  const dispatch = useTypedDispatch();
  const errorModal = useDecodeErrorShowModal();
  const { isLoading, setIsLoading } = useIsLoading();

  const onTogglePolicy = useCallback(() => {
    setIsPolyAccepted((state) => !state);
  }, []);

  const onPressClose = () => {
    if (loginPhoneConfig.onPressClose) {
      loginPhoneConfig.onPressClose();
      return;
    }
    navigation.navigate(Routes.SelectCity);
  };

  const openTermOfUse = useCallback(() => {
    navigation.navigate(Routes.TermsOfUse);
  }, []);

  const onPressNext = async () => {
    if (isPolicyAccepted) {
        setIsLoading(true);
      try {
      const result = await dispatch(
        AsyncActionsLogin.getSms({ phone: phoneInput.value })
      );

        if(result?.request_id){
          if (loginPhoneConfig.onPressNextButton) {
            loginPhoneConfig.onPressNextButton({ result });
            return;
          }
          navigation.push(Routes.LoginCode, {
            phoneNumber: phoneInput.value,
            request_id: result.request_id,
          });
        }
        else {
          throw new Error(result?.error_msg);
        }
      } catch (e) {
        errorModal(e.message);
      }
      setIsLoading(false);
    }
  };

  useEffect(() => {
    dispatch(AsyncActionsDelivery.getCities());
    dispatch(AsyncActionsDelivery.getOrderTypes());
  }, []);

  return (
    <KeyboardAvoidingScroll>
      <SafeAreaView>
        <View style={styles.content}>
          <TouchableOpacity onPress={onPressClose} style={styles.closeButton}>
            <CloseCrossIcon />
          </TouchableOpacity>
          <Text style={styles.enterPhoneTitle}>
            Введите номер{"\n"}телефона
          </Text>
          <View style={styles.phoneInputContainer}>
            <View style={styles.numberInputWrapper}>
              <TextInputMask
                maxLength={18}
                testID={"phone-input"}
                type="custom"
                options={{
                  mask: "+7 (999) 999-99-99",
                }}
                placeholderTextColor={Color.GREY_100}
                placeholder="+7 (925)123-45-67"
                style={styles.numberInputStyle}
                {...phoneInput}
                keyboardType="numeric"
              />
            </View>
          </View>
          <TermsOfUseAccept
            isChecked={isPolicyAccepted}
            onToggle={onTogglePolicy}
            onPressTermsOfUse={openTermOfUse}
          />
          <ThemedButton
            isLoading={isLoading}
            wrapperStyle={styles.buttonWrapper}
            onPress={onPressNext}
            label={loginPhoneConfig.nextButtonLabel}
            rounded={true}
            disabled={
              !isPolicyAccepted ||
              !(phoneInput.value.length === 18) ||
              isLoading
            }
          />
        </View>
      </SafeAreaView>
    </KeyboardAvoidingScroll>
  );
});

const styles = {
  content: style.view({
    ...UIStyles.paddingH16,
    alignItems: "center",
    height: "100%",
    backgroundColor: Color.WHITE,
  }),
  phoneInputContainer: style.view({
    width: "100%",
    paddingHorizontal: 30,
  }),
  enterPhoneTitle: style.text({
    textAlign: "center",
    paddingTop: 20,
    fontSize: adaptiveSize(24),
    fontFamily: fonts.robotoBold,
  }),
  numberInputWrapper: style.view({
    borderColor: Color.GREY_100,
    borderRadius: RADIUS.MEDIUM,
    borderWidth: 1,
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    height: 50,
    marginTop: 40,
  }),
  numberInputStyle: style.text({
    fontFamily: fonts.robotoBold,
    fontSize: adaptiveSize(20),
    color: Color.DARK,
    textAlign: "left",
    borderWidth: 1,
    borderColor: "transparent",
    height: 50,
    minWidth: adaptiveSize(180),
    // alignItems: 'flex-start',
    borderRadius: RADIUS.MEDIUM,
  }),
  continueButton: style.view({
    position: "absolute",
    width: windowWidth * 0.914,
  }),
  closeButton: style.view({
    width: 40,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "flex-start",
    // alignItems: 'flex-start',
    // marginTop: 17,
    // marginLeft: 5,
  }),
  buttonWrapper: {
    marginBottom: 10,
  },
};
