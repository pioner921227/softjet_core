import React from 'react'
import { Routes } from '../../../navigation/config/routes'
import { LoginPhone } from './login-phone/login-phone'
import { LoginCode } from './login-code/login-code'
import { screenOptions, Stack } from '../../../navigation/root-navigator'

export const LoginStack = React.memo(() => {
  return (
    <Stack.Navigator
      screenOptions={screenOptions}
      initialRouteName={Routes.LoginPhone}
    >
      <Stack.Screen name={Routes.LoginPhone} component={LoginPhone} />

      <Stack.Screen name={Routes.LoginCode} component={LoginCode} />
    </Stack.Navigator>
  )
})
