import React, {useRef} from 'react';
import {StyleSheet, View} from 'react-native';
import {useTypedSelector} from '../../../system';
import {getAppInfo} from '../../../system/selectors/selectors';
import {Config} from '../../../config';
import {ScreenHeaderTitleWithBackButton} from '../../../components/screen-header/screen-header-title-with-back-button';
import {adaptiveSize} from '../../../system/helpers/responseive-font-size';
import {HtmlParseAndView, HtmlStyles} from '@react-native-html/renderer';
import Animated from 'react-native-reanimated';
import {useAndroidBackHandler} from '../../../system/hooks/use-android-back-handler';
import {ScrollableHeaderWithList} from '../../../components/scrollable-header-with-list';

const {UIStyles} = Config;

export const TermsOfUse = React.memo(() => {
  const appInfo = useTypedSelector(getAppInfo);
  const scrollRef = useRef<Animated.ScrollView | null>(null);

  useAndroidBackHandler();

  return (
    <ScrollableHeaderWithList
      header={
        <ScreenHeaderTitleWithBackButton
          titleStyle={{fontSize: adaptiveSize(15)}}
          title={'Пользовательское соглашение'}
        />
      }
      list={
        <Animated.ScrollView
          showsVerticalScrollIndicator={false}
          ref={scrollRef}
          style={UIStyles.flexGrow}
          contentContainerStyle={styles.container}>
          <View style={styles.contentContainer}>
            {appInfo?.terms_of_use ? (
              <HtmlParseAndView
                rawHtml={appInfo?.terms_of_use}
                htmlStyles={htmlStyles}
                containerStyle={styles.container}
                //@ts-ignore
                scrollRef={scrollRef.current}
              />
            ) : null}
            {/*<Text style={styles.text}>{textPolicy}</Text>*/}
          </View>
        </Animated.ScrollView>
      }
    />
  );
});

const htmlStyles: HtmlStyles = {
  text: {
    fontSize: adaptiveSize(16),
    lineHeight: 18 * 1.4,
  },
  paragraph: {
    marginVertical: 10,
  },
  image: {
    marginVertical: 0,
  },
  list: {
    marginVertical: 5,
  },
  h1: {
    fontSize: 30,
    lineHeight: 30 * 1.4,
    marginTop: 10,
    fontWeight: '500',
  },
  h2: {
    fontSize: 26,
    lineHeight: 26 * 1.4,
    marginTop: 10,
    fontWeight: '500',
  },
  h3: {
    fontSize: 24,
    lineHeight: 24 * 1.4,
    marginTop: 10,
    fontWeight: '500',
  },
  listItem: {
    marginVertical: 2,
  },
  listItemContent: {},
};

const styles = StyleSheet.create({
  contentContainer: {},
  container: {
    marginTop: 20,
    ...UIStyles.flexGrow,
    ...UIStyles.paddingH16,
    paddingBottom: 50,
  },
});
