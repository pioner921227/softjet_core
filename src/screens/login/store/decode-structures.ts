import {IGetSmsResponse, ISendVerificationCodeResponse} from '../types/types';
import {T} from 'decodable-js/index';

export const decodeGetSmsStruct: IGetSmsResponse = {
  request_id: T.number,
};

export const decodeVerificationStruct: ISendVerificationCodeResponse = {
  token: T.string,
};
