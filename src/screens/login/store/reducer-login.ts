import {ReducerBuilder, reducerWithInitialState} from 'typescript-fsa-reducers';
import {AsyncActionsLogin} from './async-actions-login';
import {IStoreLogin, initialStateLogin} from './store-login';
import {IGetSmsResponse, ISendVerificationCodeResponse} from '../types/types';
import {Success} from 'typescript-fsa';
import {ActionsLogin} from './actions-login';
import {Decodable} from 'decodable-js';
import {
  decodeGetSmsStruct,
  decodeVerificationStruct,
} from './decode-structures';
import {sendErrorToServer} from '../../../system/helpers/send-error-to-server';
import {decodeWithErrorHandling} from '../../../system/helpers/decode-with-error-handling';

const withErrorHandling = <T>(callback: () => T, name: string): T | never => {
  try {
    return callback();
  } catch (e) {
    sendErrorToServer(e, name);
    throw new Error(e);
  }
};

const getSmsHandlerStarted = (store: IStoreLogin): IStoreLogin => {
  return {
    ...store,
    isLoading: true,
    error: false,
  };
};

const getSmsHandlerDone = (
  store: IStoreLogin,
  {result}: Success<any, IGetSmsResponse>,
): IStoreLogin => {

  return {
    ...store,
    isLoading: false,
    error: false,
    request_id: result?.request_id ?? store.request_id,
  };
};

const getSmsHandlerFailed = (store: IStoreLogin): IStoreLogin => {
  return {
    ...store,
    isLoading: false,
    error: true,
  };
};

const sendVerificationCodeHandlerStarted = (
  store: IStoreLogin,
): IStoreLogin => {
  return {
    ...store,
    isLoading: true,
    error: false,
  };
};
const sendVerificationCodeHandlerDone = (
  store: IStoreLogin,
  {result}: Success<any, ISendVerificationCodeResponse>,
): IStoreLogin => {
  // const decodeResult = decodeWithErrorHandling(
  //   result,
  //   decodeVerificationStruct,
  //   '/api/auth/authorization',
  // );

  if (!result.token) {
    return store;
  }

  return {
    ...store,
    isLoading: false,
    token: result.token,
    error: false,
  };
};

const sendVerificationCodeHandlerFailed = (store: IStoreLogin): IStoreLogin => {
  return {
    ...store,
    isLoading: false,
    error: true,
  };
};

const resetHandler = (): IStoreLogin => {
  return {
    ...initialStateLogin,
  };
};

export const reducerLogin: ReducerBuilder<IStoreLogin> = reducerWithInitialState(
  initialStateLogin,
)
  .case(ActionsLogin.logoutAccount, resetHandler)
  .case(AsyncActionsLogin.getSms.async.started, getSmsHandlerStarted)
  .case(AsyncActionsLogin.getSms.async.done, getSmsHandlerDone)
  .case(AsyncActionsLogin.getSms.async.failed, getSmsHandlerFailed)

  .case(
    AsyncActionsLogin.sendVerificationCode.async.started,
    sendVerificationCodeHandlerStarted,
  )
  .case(
    AsyncActionsLogin.sendVerificationCode.async.done,
    sendVerificationCodeHandlerDone,
  )
  .case(
    AsyncActionsLogin.sendVerificationCode.async.failed,
    sendVerificationCodeHandlerFailed,
  );
