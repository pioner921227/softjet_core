import {actionCreator} from '../../../system/root-store/action-creator';

export class ActionsLogin {
  static logoutAccount = actionCreator('LOGIN/LOGOUT_ACCOUNT');
}
