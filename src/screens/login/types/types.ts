export interface IGetSmsRequestData {
  phone: string;
}
export interface IGetSmsResponse {
  request_id: number;
  error_msg: string;
}

export interface ISendVerificationCodeRequestData {
  request_id: number;
  code: number;
}

export interface ISendVerificationCodeResponse {
  token?: string;
  error?: boolean;
  error_msg?: string;
  result_code?: number;
}
