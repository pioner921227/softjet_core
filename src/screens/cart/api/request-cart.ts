import {ApiService} from '../../../system/api/api-service';
import {
  IGetCartGiftsResponse,
  IGiftsNewItemResponse,
} from '../../cart-new/api/types';
import {
  IActivatePromoCodeRequest,
  ICartAddOrderRequest,
  ICartGetDataRequest,
  ICartGetDataResponse,
  IChangeProductCountInCartRequest,
  IClearRemoteCartDataRequest,
  IGetCartAdditionalProducts,
  IGetFreeTimeRequestData,
  IGetFreeTimeResponse,
  IGetPresentFromPriceDataRequest,
  IGetPresentFromPriceResponse,
} from './types';

export interface IMinRequestData
  extends Omit<ICartGetDataRequest, 'polygon_id'> {}

export class RequestCart {
  static getCartData(data: ICartGetDataRequest): Promise<ICartGetDataResponse> {
    return ApiService.post('/api/cart/getCart', data);
  }
  static cartAdd(data: ICartAddOrderRequest): Promise<ICartGetDataResponse> {
    return ApiService.post('/api/cart/add', data);
  }

  static activatePromoCode(
    data: IActivatePromoCodeRequest,
  ): Promise<ICartGetDataResponse> {
    return ApiService.post('/api/cart/promocode', data);
  }

  static changeProductCountInCart(
    data: IChangeProductCountInCartRequest,
  ): Promise<ICartGetDataResponse> {
    return ApiService.post('/api/cart/change', data);
  }

  static clearRemoteCart(data: IClearRemoteCartDataRequest) {
    return ApiService.post('/api/cart/clean', data);
  }

  static getPresentFromPrice(
    data: IGetPresentFromPriceDataRequest,
  ): Promise<IGetPresentFromPriceResponse> {
    return ApiService.get('/api/cart/cartAdditives', {params: data});
  }

  static getAdditionalProducts(
    data: IMinRequestData,
  ): Promise<IGetCartAdditionalProducts[]> {
    return ApiService.get('/api/content/getCartAddivies', {params: data});
  }
  static getFeeTime(
    data: IGetFreeTimeRequestData,
  ): Promise<IGetFreeTimeResponse> {
    return ApiService.post('/api/cart/getFreeTime', data);
  }

  static saveRemoteCart(data: IMinRequestData) {
    return ApiService.post('/api/cart/saveCart', data);
  }

  static initRemoteCart(data: IMinRequestData) {
    return ApiService.post('/api/cart/loadCart', data);
  }
  //---------------------------------

  static getOptions(data: IMinRequestData): Promise<IGetCartGiftsResponse> {
    return ApiService.post('/api/cart/getOptions', data);
  }

  static removePromoCode(
    data: ICartGetDataRequest,
  ): Promise<ICartGetDataResponse> {
    return ApiService.post('api/cart/removePromo', data);
  }

  static getGiftsNew(data: IMinRequestData): Promise<IGiftsNewItemResponse[]> {
    return ApiService.get('api/cart/getGiftsNew', {params: data});
  }
}
