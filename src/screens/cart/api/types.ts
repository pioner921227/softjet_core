export interface ICartGetDataRequest {
  order_type: number;
  id_org: number;
  polygon_id: number | null;
}

export interface IMinRequestData
  extends Omit<ICartGetDataRequest, "polygon_id"> {}

export interface ICartGetDataResponse {
  promocodes: string[];
  gifts_category_statuses: { [key: string]: boolean };
  gifts_category_remains_cost: { [key: string]: number };
  next_opened_gift_ids: { [key: string]: number };
  total_cost: number;
  total_count: number;
  discount_cost: number;
  discount_percent: number;
  actual_discount: number;
  max_bonus: number;
  min_count_cost: number;
  min_free_delivery_cost: number;
  original_total_cost: number;
  delivery_cost: number;
  products: ICartGetDataProductResponse[];
  error?: boolean;
  error_msg?: string;
  gifts_status: { [key: string]: boolean };
  disabled_options: boolean;
  self_pickup_discount: number;
  gifts_remains_cost: { [key: string]: number };
  selected_gift_modificator_ids: number[];
  repeat_order: {
    code: number;
    title: string;
    subtitle: string;
  };
}

export interface ICartGetDataProductResponse {
  key: string;
  additional_cost: number;
  total_cost: number;
  id: number;
  price: number;
  weight: number;
  count: number;
  max_count: number;
  img_url: string;
  title: string;
  desc: string;
  options: IProductOptions;
}

export interface IProductOptions {
  childs: IProductOptionsChild;
}

export interface IProductOptionsChild {
  additives: ICartOptionsListItem[];
  ingredients: ICartOptionsListItem[];
  size: ICartOptionsListItem[];
  type: ICartOptionsListItem[];
}

export interface ICartOptionsListItem {
  id: number;
  count: number;
  title: string;
}

export interface ICartAddOrderRequest extends ICartGetDataRequest {
  id: number;
  count: number;
  additives: ICartAddOrderItem[];
  size: ICartAddOrderItem[];
  type: ICartAddOrderItem[];
  ingredients: ICartAddOrderItem[];
}

interface ICartAddOrderItem {
  id: number;
  count: number;
}

export interface IActivatePromoCodeRequest extends ICartGetDataRequest {
  promocode: string;
}

export interface IChangeProductCountInCartRequest extends ICartGetDataRequest {
  key: string;
  count: number;
}

export interface IClearRemoteCartDataRequest extends ICartGetDataRequest {}

export interface IGetPresentFromPriceDataRequest extends ICartGetDataRequest {}

export interface IGetPresentFromPriceResponse {
  gift_from_cost: IPresentItem[];
}

export interface IPresentItem {
  remainder_cost: number;
  min_cost: number;
  min_count: number;
  title: string;
  imgUrl: string;
  desc: string;
  id_promo: number;
  modificators: IPresentModificator[];
}

export interface IPresentModificator {
  title: string;
  img_url: string;
  price: number;
  id: number;
}

export interface IGetFreeTimeRequestData {
  id_org: number;
}

export interface IGetFreeTimeResponse {
  freeTime: { [key: string]: string[] };
}

export interface IGetCartAdditionalProducts {
  id: string;
  title: string;
  parent: string;
  description: string;
  price: string;
  old_price: string;
  article: string;
  img_big: string;
  img_small: string;
}

export interface IGetCartGiftsResponse {
  max_select_count: number;
  options: ICartGiftsOptions;
}

export interface ICartGiftsOptions {
  min_cost: number;
  items: ICartGiftsOptionsItem[];
}

export enum CartGiftsOptionType {
  bonus = "bonus",
  delivery_sale = "delivery_sale",
  promo = "promo",
}

export interface ICartGiftsOptionsItem {
  id: number;
  title: string;
  value: number;
  type: keyof typeof CartGiftsOptionType;
}

export interface IGiftsNewItemResponse {
  id: number;
  gifts: IPresentItem[];
}
