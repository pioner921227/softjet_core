import { useNavigation } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import {
  InteractionManager,
  Keyboard,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
} from "react-native";
import { useBottomSheetMenu, useModal } from "../../../../components";
import { ThemedButton } from "../../../../components/buttons/themed-button";
import { CheckboxComponent } from "../../../../components/checkbox/checkbox-component";
import { Input } from "../../../../components/input/input";
import { InputWithRightElement } from "../../../../components/input/input-with-right-element";
import { KeyboardAvoidingScroll } from "../../../../components/keyboard-avoiding-scroll";
import {
  IModalMethods,
  ModalWithComponent,
} from "../../../../components/modal/modal-with-component";
import { IRadioButtonData } from "../../../../components/radio-button/radio-button";
import { ScreeHeaderTitleLeft } from "../../../../components/screen-header/scree-header-title-left";
import { ISelectCustomMethods } from "../../../../components/select-custom/select-custom";
import { Config } from "../../../../config";
import { Routes } from "../../../../navigation/config/routes";
import { TRootStackParamList } from "../../../../navigation/types/types";
import { useDebounce, useInput } from "../../../../system";
import { FocusAwareStatusBar } from "../../../../system/helpers/focus-aware-status-bar";
import { isSmallScreen } from "../../../../system/helpers/is-small-screen";
import { productDeclensionWord } from "../../../../system/helpers/product-declension-word";
import { useSelectOrderTypeHandler } from "../../../../system/hooks/use-select-order-type-handler";
import { useTypedDispatch } from "../../../../system/hooks/use-typed-dispatch";
import { useTypedSelector } from "../../../../system/hooks/use-typed-selector";
import {
  getCartData,
  getCurrentAddress,
  getCurrentOrderType,
  getCurrentOrganisation,
  getPolygonId,
  getToken,
} from "../../../../system/selectors/selectors";
import { catalogConfig } from "../../../catalog";
import { BSMContentCreateNewAddress } from "../../../delivery/view/components/bsm-content-create-new-address";
import { ICartGetDataProductResponse } from "../../api/types";
import { ActionCart } from "../../store/action-cart";
import { AsyncActionCart } from "../../store/async-action-cart";
import { AdditiveProducts } from "../components/additives-products/additive-products";
import { CartProductListItem } from "../components/cart-product-list-item";
import { DateTimePicker } from "../components/date-time-picker/date-time-picker";
import { EmptyCart } from "../components/empty-cart";
import { InfoRows } from "../components/info-rows";
import { PresentButton } from "../components/present-button";
import { PresentModalContent } from "../components/present-modal-content";
import { PromoInput } from "../components/promo-input";
import { cartConfig } from "./config";

const { Color, UIStyles } = Config;

interface IDebounceCallbackProps {
  key: string;
  count: number;
}

export const EmptyCartComponent = {
  Component: EmptyCart,
};

type TNavigation = StackNavigationProp<TRootStackParamList, Routes.Payment>;
type TNavigation2 = StackNavigationProp<
  TRootStackParamList,
  Routes.AcceptAddress
>;

export const Cart = React.memo(() => {
  const dispatch = useTypedDispatch();
  const cartData = useTypedSelector(getCartData);
  const token = useTypedSelector(getToken);
  const currentOrderType = useTypedSelector(getCurrentOrderType);
  const currentOrganisation = useTypedSelector(getCurrentOrganisation);
  const currentAddress = useTypedSelector(getCurrentAddress);
  const polygonId = useTypedSelector(getPolygonId);
  const additionalProducts = useTypedSelector(
    (state) => state.cart.additionalProducts
  );

  //@ts-ignore
  const [freeTimes, setFreeTimes] = useState<{ [key: string]: string[] }>([]);

  type Key = keyof typeof freeTimes;
  const dates = Object.keys(freeTimes ?? {}) as Key[];

  const [selectedDate, setSelectedDate] = useState<Key>(dates[0]);
  const [selectedTime, setSelectedTime] = useState("");

  const { max_bonus = 0 } = cartData || {};
  const commentInput = useInput("");
  const { show, close } = useModal();
  const { show: showBottomMenu, close: CloseBottomSheet } =
    useBottomSheetMenu();
  const [isCheckedBonus, setIsCheckedBonus] = useState(false);
  const bonusInput = useInput(max_bonus.toString() || "0");
  const modalPresentRef = useRef<IModalMethods | null>(null);

  const { navigate } = useNavigation<TNavigation & TNavigation2>();

  const [textErrorPromoCode, setTextErrorPromoCode] = useState("");

  const debounceCallback = useCallback(
    ({ key, count }: IDebounceCallbackProps) => {
      if (
        currentOrganisation?.id !== undefined &&
        currentOrderType?.id !== undefined
      ) {
        dispatch(
          AsyncActionCart.changeProductCountInCart({
            count,
            key,
            id_org: +currentOrganisation?.id,
            order_type: +currentOrderType?.id,
            polygon_id: polygonId,
          })
        );
      }
    },
    [currentOrderType?.id, currentOrganisation?.id, polygonId, dispatch]
  );

  const countDebounce = useDebounce<IDebounceCallbackProps>(
    debounceCallback,
    1000
  );

  const onSelectOrderType = useSelectOrderTypeHandler({
    finishCallback: () => {
      CloseBottomSheet();
    },
  });

  const snapPoints = useMemo(() => [350], []);

  const openBottomMenu = useCallback(() => {
    close();
    showBottomMenu({
      Component: () => (
        <BSMContentCreateNewAddress onPress={onSelectOrderType} />
      ),
      snapPoints,
    });
  }, [close, onSelectOrderType, showBottomMenu, snapPoints]);

  const onPressConfirm = useCallback(() => {
    close();
    InteractionManager.runAfterInteractions(() => {
      navigate(Routes.Payment);
    });
  }, [close, navigate]);

  const onPressGoToLogin = () => {
    close();
    InteractionManager.runAfterInteractions(() => {
      navigate(Routes.LoginStack);
    });
  };

  const onPressNext = useCallback(() => {
    if (!token) {
      show({
        title: cartConfig.authModal.title,
        description: cartConfig.authModal.description,
        buttons: [
          {
            label: cartConfig.authModal.closeButtonLabel,
            onPress: close,
            styles: { modifier: "bordered" },
          },
          {
            label: cartConfig.authModal.successButtonLabel,
            onPress: onPressGoToLogin,
          },
        ],
        buttonsDirection: "row",
      });
      return;
    }

    dispatch(ActionCart.setComment(commentInput.value));
    dispatch(
      ActionCart.setSelectedBonus(isCheckedBonus ? +bonusInput.value : 0)
    );

    if (cartConfig.datePicker.isVisible) {
      dispatch(
        ActionCart.setPreOrderDate(
          `${selectedDate ?? ""} ${selectedTime ?? ""}`
        )
      );
    }

    if (cartConfig.onPressNextButton) {
      cartConfig.onPressNextButton({
        isRemoted: currentOrderType?.isRemoted || false,
      });
      return;
    }

    if (currentOrderType?.isRemoted) {
      if (currentAddress) {
        navigate(Routes.AcceptAddress);
      } else {
        show({
          title: "Нет текущего адреса",
          description: "Для начала нужно создать адрес",
          buttons: [{ label: "Ok", onPress: close }],
        });
      }
    } else {
      show({
        title: `Вы заберете свой заказ из ${currentOrganisation?.address}?`,
        description: catalogConfig.paramsMenu.setAddressModalText,
        buttonsDirection: "row",
        buttons: [
          {
            label: "Изменить",
            onPress: openBottomMenu,
            styles: { modifier: "bordered" },
          },
          {
            label: "Подтвердить",
            onPress: onPressConfirm,
          },
        ],
      });
    }
  }, [
    selectedDate,
    selectedTime,
    bonusInput.value,
    commentInput.value,
    currentAddress,
    currentOrderType?.isRemoted,
    currentOrganisation?.address,
    isCheckedBonus,
    token,
  ]);

  const onChangeCount = useCallback(
    ({
      count,
      oldCount,
      item,
    }: {
      count: number;
      oldCount: number;
      item: ICartGetDataProductResponse;
    }) => {
      if (oldCount === count || count > item.max_count) {
        return;
      }
      //dispatch(
      //ActionCart.setSelectedProducts(
      //counterDispatchHelper({
      //count,
      //id: item.key,
      //items: selectedProducts || {},
      //}),
      //),
      //);
      countDebounce({ count, key: item.key });
    },
    [countDebounce]
  );

  const onPressAddAdditionalProduct = (id: number) => {
    if (currentOrganisation && currentOrderType) {
      dispatch(
        AsyncActionCart.cartAdd({
          id: id,
          count: 1,
          order_type: +currentOrderType?.id,
          id_org: +currentOrganisation?.id,
          additives: [],
          size: [],
          type: [],
          ingredients: [],
          polygon_id: polygonId,
        })
      );
    }
  };

  const activatePromoCodeHandler = useCallback(
    async (code: string) => {
      setTextErrorPromoCode("");
      if (
        code &&
        currentOrderType?.id &&
        currentOrganisation?.id !== undefined
      ) {
        const res = await dispatch(
          AsyncActionCart.activatePromoCode({
            promocode: code,
            order_type: currentOrderType?.id,
            id_org: +currentOrganisation.id,
            polygon_id: polygonId,
          })
        );
        if (res.error && res.error_msg) {
          setTextErrorPromoCode(res.error_msg);
        }
      }
    },
    [currentOrderType?.id, currentOrganisation?.id, polygonId, dispatch]
  );

  const toggleCheckBoxBonus = () => {
    setIsCheckedBonus((state) => !state);
  };

  const {
    total_count = 0,
    total_cost = 0,
    min_count_cost = 0,
    actual_discount = 0,
    promocodes = "",
  } = cartData || {};

  const headerSubTitleCreator = useCallback(
    (count: number) => {
      return `${count} ${productDeclensionWord(count)} на ${total_cost} ₽`;
    },
    [total_cost]
  );

  const onChangeText = useCallback(
    (value: string) => {
      if (value) {
        if (max_bonus >= +value) {
          bonusInput.onChangeText(parseInt(value).toString());
        } else {
          bonusInput.onChangeText(max_bonus?.toString());
        }
      } else {
        bonusInput.onChangeText("");
      }
    },
    [bonusInput, max_bonus]
  );

  const openPresentModal = useCallback(() => {
    modalPresentRef.current?.show();
  }, []);

  const onPressClosePresentModal = useCallback(() => {
    modalPresentRef.current?.close();
  }, [modalPresentRef]);

  const onSelectPresent = useCallback(
    (present: IRadioButtonData) => {
      modalPresentRef.current?.close();
      if (currentOrganisation?.id && currentOrderType?.id) {
        dispatch(
          AsyncActionCart.cartAdd({
            id: +present.id,
            count: 1,
            order_type: +currentOrderType.id,
            id_org: +currentOrganisation.id,
            additives: [],
            size: [],
            type: [],
            ingredients: [],
            polygon_id: polygonId,
          })
        );
      }
    },
    [currentOrderType, currentOrganisation, dispatch, polygonId]
  );

  useEffect(() => {
    bonusInput.onChangeText(max_bonus.toString());
  }, [max_bonus]);

  const isDisabledButton = useMemo(() => {
    if (!currentOrderType?.isRemoted || !cartData) {
      return false;
    }
    return cartData?.min_count_cost > cartData?.total_cost;
  }, [cartData, currentOrderType?.isRemoted]);

  const promoSelectRef = useRef<ISelectCustomMethods | null>(null);

  const getPromoRef = (ref: ISelectCustomMethods | null) => {
    promoSelectRef.current = ref;
  };

  // const closePromoSelect = useCallback(() => {
  //   promoSelectRef.current?.hide();
  //   Keyboard.dismiss();
  // }, []);

  const onSelectDate = useCallback(
    (index: number) => {
      setSelectedDate(dates[index]);
    },
    [dates]
  );

  const onSelectTime = useCallback(
    (index: number) => {
      const time = freeTimes?.[selectedDate]?.[index];
      if (time) {
        setSelectedTime(time);
      }
    },
    [freeTimes, selectedDate]
  );

  useEffect(() => {
    if (currentOrganisation?.id && currentOrderType?.id) {
      InteractionManager.runAfterInteractions(() => {
        dispatch(
          AsyncActionCart.getAdditionalProducts({
            order_type: currentOrderType.id,
            id_org: currentOrganisation.id,
          })
        );
        dispatch(
          AsyncActionCart.getPresentFromPrice({
            order_type: +currentOrderType?.id,
            id_org: currentOrganisation?.id,
            polygon_id: polygonId,
          })
        );
        dispatch(
          AsyncActionCart.getCartData({
            id_org: +currentOrganisation?.id,
            order_type: +currentOrderType?.id,
            polygon_id: polygonId,
          })
        );

        dispatch(
          AsyncActionCart.getFreeTime({ id_org: currentOrganisation.id })
        ).then((data) => setFreeTimes(data.freeTime));
      });
    }
  }, []);

  return (
    <SafeAreaView style={UIStyles.flex}>
      <FocusAwareStatusBar
        barStyle={"dark-content"}
        backgroundColor={Color.WHITE}
      />
      <KeyboardAvoidingScroll>
        <ScreeHeaderTitleLeft title={cartConfig.headerTitle} />

        {/*<Pressable onPress={closePromoSelect} style={styles.contentContainer}>*/}
        <View style={styles.contentContainer}>
          {cartData?.products && cartData?.products?.length !== 0 ? (
            <>
              <Text style={styles.titleTotal}>
                {headerSubTitleCreator(total_count)}
              </Text>
              {currentOrderType?.isRemoted && min_count_cost ? (
                <Text style={styles.minCountDeliveryText}>
                  Минимальная сумма на доставку {min_count_cost} ₽
                </Text>
              ) : null}
              {currentOrderType?.isRemoted &&
              cartData?.min_free_delivery_cost ? (
                <Text style={styles.minCountDeliveryText}>
                  Бесплатная доставка от {cartData?.min_free_delivery_cost} ₽
                </Text>
              ) : null}
              {cartConfig.datePicker.isVisible ? (
                <DateTimePicker
                  onSelectDate={onSelectDate}
                  onSelectTime={onSelectTime}
                  date={dates as string[]}
                  time={freeTimes?.[selectedDate] ?? []}
                />
              ) : null}

              {cartData.products.map((el) => {
                return (
                  <CartProductListItem
                    id={el.id}
                    key={el.key?.toString()}
                    options={el.options}
                    maxCount={el.max_count}
                    onChangeCount={(count, oldCount) =>
                      onChangeCount({ count, oldCount, item: el })
                    }
                    count={el.count}
                    description={el.desc}
                    title={el.title}
                    price={el.price}
                    image={el.img_url}
                  />
                );
              })}

              {cartConfig.isVisibleAdditiveProducts ? (
                <AdditiveProducts
                  onPressAdd={onPressAddAdditionalProduct}
                  data={additionalProducts}
                />
              ) : null}

              {cartConfig.isVisiblePresentButton ? (
                <PresentButton showModal={openPresentModal} />
              ) : null}
              <PromoInput
                getRef={getPromoRef}
                onPress={activatePromoCodeHandler}
              />
              {!!textErrorPromoCode && (
                <Text style={[styles.promoTitle, styles.promoCodeTitleError]}>
                  {textErrorPromoCode}
                </Text>
              )}
              {promocodes && !textErrorPromoCode ? (
                <Text style={[styles.promoTitle, styles.promoCodeTitle]}>
                  Промокод ����������римен��н. Ск��дка {actual_discount} ₽
                </Text>
              ) : null}

              <View style={styles.bonusCheckBoxContainer}>
                <CheckboxComponent
                  disabled={!max_bonus ?? true}
                  theme={"PRIMARY"}
                  title={`Оплатить бонусами\n${
                    max_bonus ? "Максимум" : "Баланс бонусов"
                  } ${max_bonus} ₽`}
                  isChecked={isCheckedBonus}
                  onToggle={toggleCheckBoxBonus}
                />
                {isCheckedBonus ? (
                  <InputWithRightElement
                    keyboardType={"numeric"}
                    RightComponent={() => <Text>₽</Text>}
                    inputStyle={styles.bonusInputWrapper}
                    textStyle={styles.textInput}
                    value={bonusInput.value}
                    onChangeText={onChangeText}
                  />
                ) : null}
              </View>
              <InfoRows
                isEnabledDeliverySale={true}
                isCheckedSaleForSelfDelivery={true}
                isRemoted={currentOrderType?.isRemoted}
                bonus={bonusInput.value}
                isCheckedBonus={isCheckedBonus}
                cartData={cartData}
              />
              <Input
                inputStyle={styles.inputStyle}
                {...commentInput}
                placeholderText={cartConfig.commentPlaceholder}
              />
              <ThemedButton
                wrapperStyle={styles.button}
                disabled={isDisabledButton}
                rounded={true}
                label={cartConfig.nextButtonLabel}
                onPress={onPressNext}
              />
            </>
          ) : (
            <EmptyCartComponent.Component />
          )}
        </View>
      </KeyboardAvoidingScroll>
      <ModalWithComponent ref={modalPresentRef}>
        <PresentModalContent
          onClose={onPressClosePresentModal}
          onSelect={onSelectPresent}
        />
      </ModalWithComponent>
    </SafeAreaView>
  );
});

const styles = StyleSheet.create({
  contentContainer: {
    flex: 1,
    ...UIStyles.paddingH16,
  },
  titleTotal: {
    ...UIStyles.font18b,
    color: Color.BLACK,
  },
  minCountDeliveryText: {
    marginTop: 6,
    ...UIStyles[isSmallScreen ? "font13" : "font15"],
    color: Color.GREY_600,
  },
  promoTitle: {
    marginTop: 8,
    ...UIStyles.font15,
  },
  promoCodeTitle: {
    color: Color.SUCCESS,
  },
  promoCodeTitleError: {
    color: Color.DANGER,
  },
  bonusCheckBoxContainer: {
    marginTop: 16,
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 16,
  },
  bonusInputWrapper: {
    width: 100,
    padding: 0,
    paddingVertical: 0,
  },
  textInput: {
    paddingVertical: 5,
  },
  inputStyle: {
    marginBottom: 20,
  },
  button: {
    marginBottom: 10,
  },
});
