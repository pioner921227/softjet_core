import { useNavigation } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import {
  InteractionManager,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
} from "react-native";
import { ActionCart, AsyncActionCart } from "../..";
import {
  KeyboardAvoidingScroll,
  useBottomSheetMenu,
  useModal,
} from "../../../../components";
import { IScrollPickerData } from "../../../../components/date-picker/components/scroll-picker/scroll-picker";
import { DatePicker } from "../../../../components/date-picker/date-picker";
import {
  IModalMethods,
  ModalWithComponent,
} from "../../../../components/modal/modal-with-component";
import { IRadioButtonData } from "../../../../components/radio-button/radio-button";
import { ScreeHeaderTitleLeft } from "../../../../components/screen-header/scree-header-title-left";
import { ISelectCustomMethods } from "../../../../components/select-custom/select-custom";
import { Config } from "../../../../config/config";
import { Routes } from "../../../../navigation";
import { TRootStackParamList } from "../../../../navigation/types/types";
import { useDebounce, useInput, useTypedSelector } from "../../../../system";
import { FocusAwareStatusBar } from "../../../../system/helpers/focus-aware-status-bar";
import { isSmallScreen } from "../../../../system/helpers/is-small-screen";
import { productDeclensionWord } from "../../../../system/helpers/product-declension-word";
import { adaptiveSize } from "../../../../system/helpers/responseive-font-size";
import { useSelectOrderTypeHandler } from "../../../../system/hooks/use-select-order-type-handler";
import { useTypedDispatch } from "../../../../system/hooks/use-typed-dispatch";
import {
  getCurrentAddress,
  getCurrentOrderType,
  getCurrentOrganisation,
  getPolygonId,
  getToken,
} from "../../../../system/selectors/selectors";
import { Input, InputWithRightElement, ThemedButton } from "../../../../ui-kit";
import { catalogConfig } from "../../../catalog";
import { BSMContentCreateNewAddress } from "../../../delivery/view/components/bsm-content-create-new-address";
import {
  CartGiftsOptionType,
  ICartGetDataProductResponse,
} from "../../api/types";
import { AdditiveProducts } from "../components/additives-products/additive-products";
import { CartProductListItem } from "../components/cart-product-list-item";
import { EmptyCart } from "../components/empty-cart";
import { InfoRows } from "../components/info-rows";
import { Options } from "../components/options/options";
import { PresentButton } from "../components/present-button";
import { PresentModalContent } from "../components/present-modal-content";
import { PromoInput } from "../components/promo-input";
import { cartConfig } from "./config";
const { UIStyles, Color } = Config;

interface IDebounceCallbackProps {
  key: string;
  count: number;
}

export const EmptyCartComponent = {
  Component: EmptyCart,
};

type TNavigation = StackNavigationProp<TRootStackParamList, Routes.Payment>;
type TNavigation2 = StackNavigationProp<
  TRootStackParamList,
  Routes.AcceptAddress
>;

export const CartNew = React.memo(() => {
  const dispatch = useTypedDispatch();
  // const cartData = useTypedSelector(getCartData);
  const cartData = useTypedSelector((state) => state.cart.cartData);
  const token = useTypedSelector(getToken);
  const currentOrderType = useTypedSelector(getCurrentOrderType);
  const currentOrganisation = useTypedSelector(getCurrentOrganisation);
  const currentAddress = useTypedSelector(getCurrentAddress);
  const polygonId = useTypedSelector(getPolygonId);
  const cartGifts = useTypedSelector((state) => state.cart.gifts);

  const additionalProducts = useTypedSelector(
    (state) => state.cart.additionalProducts
  );

  const [isLoading, setIsLoading] = useState(false);

  const [freeTimes_, setFreeTimes_] = useState<{
    dates: IScrollPickerData[];
    times: Array<IScrollPickerData[]>;
  }>({
    dates: [],
    times: [[]],
  });

  const formatingData = (values: string[]) => {
    return values?.map((el, index) => ({ id: index, value: el }));
  };

  const checkOrganisationAndOrderType = useMemo(() => {
    return (
      currentOrderType?.id !== undefined &&
      currentOrganisation?.id !== undefined
    );
  }, [currentOrganisation, currentOrderType]);

  const [selectedDate, setSelectedDate] = useState<{
    index: number;
    value: string;
    times: IScrollPickerData[];
  }>({
    index: 0,
    value: "",
    times: [{ id: 0, value: "" }],
  });

  const [selectedTime, setSelectedTime] = useState<{
    index: number;
    data: IScrollPickerData;
  }>({
    index: 0,
    data: { id: 0, value: "" },
  });

  const { max_bonus = 0 } = cartData || {};
  const commentInput = useInput("");
  const { show, close } = useModal();
  const { show: showBottomMenu, close: CloseBottomSheet } =
    useBottomSheetMenu();
  const bonusInput = useInput(max_bonus?.toString() || "0");
  const modalPresentRef = useRef<IModalMethods | null>(null);

  const [optionsData, setOptionsData] = useState<{
    selectedOptionsIds: Set<number>;
    isVisibleBonus: boolean;
    isVisiblePromo: boolean;
    enabledDeliverySale: boolean;
  }>({
    selectedOptionsIds: new Set(),
    isVisibleBonus: false,
    isVisiblePromo: false,
    enabledDeliverySale: false,
  });

  const { navigate } = useNavigation<TNavigation & TNavigation2>();

  const [textErrorPromoCode, setTextErrorPromoCode] = useState("");

  const debounceCallback = useCallback(
    async ({ key, count }: IDebounceCallbackProps) => {
      if (
        currentOrganisation?.id !== undefined &&
        currentOrderType?.id !== undefined
      ) {
        setIsLoading(true);
        const res = await dispatch(
          AsyncActionCart.changeProductCountInCart({
            count,
            key,
            id_org: +currentOrganisation?.id,
            order_type: +currentOrderType?.id,
            polygon_id: polygonId,
          })
        );
        setIsLoading(false);
        return res;
      }
    },
    []
  );

  const countDebounce = useDebounce<IDebounceCallbackProps>(
    debounceCallback,
    400
  );

  const onSelectOrderType = useSelectOrderTypeHandler({
    finishCallback: () => {
      CloseBottomSheet();
    },
  });

  const snapPoints = useMemo(() => [350], []);

  const openBottomMenu = useCallback(() => {
    close();
    showBottomMenu({
      Component: () => (
        <BSMContentCreateNewAddress onPress={onSelectOrderType} />
      ),
      snapPoints,
    });
  }, [close, onSelectOrderType, showBottomMenu, snapPoints]);

  const onPressConfirm = useCallback(() => {
    close();
    console.log(optionsData.enabledDeliverySale);
    InteractionManager.runAfterInteractions(() => {
      navigate(Routes.Payment, {
        enabledDeliverySale: optionsData.enabledDeliverySale,
      });
    });
  }, [close, navigate, optionsData]);

  const onPressGoToLogin = () => {
    close();
    InteractionManager.runAfterInteractions(() => {
      navigate(Routes.LoginStack);
    });
  };

  const onPressNext = useCallback(() => {
    if (!token) {
      show({
        title: cartConfig.authModal.title,
        description: cartConfig.authModal.description,
        buttons: [
          {
            label: cartConfig.authModal.closeButtonLabel,
            onPress: close,
            styles: { modifier: "bordered" },
          },
          {
            label: cartConfig.authModal.successButtonLabel,
            onPress: onPressGoToLogin,
          },
        ],
        buttonsDirection: "row",
      });
      return;
    }

    dispatch(ActionCart.setComment(commentInput.value));
    optionsData.isVisibleBonus &&
      dispatch(ActionCart.setSelectedBonus(+bonusInput.value));

    if (cartConfig.datePicker.isVisible) {
      dispatch(
        ActionCart.setPreOrderDate(
          `${selectedDate.value ?? ""} ${selectedTime.data.value ?? ""}`
        )
      );
    }

    if (cartConfig.onPressNextButton) {
      cartConfig.onPressNextButton({
        isRemoted: currentOrderType?.isRemoted || false,
      });
      return;
    }

    if (currentOrderType?.isRemoted) {
      if (currentAddress) {
        navigate(Routes.AcceptAddress);
      } else {
        show({
          title: "Нет текущего адреса",
          description: "Для начала нужно создать адрес",
          buttons: [{ label: "Ok", onPress: close }],
        });
      }
    } else {
      show({
        title: `Вы заберете свой заказ из ${currentOrganisation?.address}?`,
        description: catalogConfig.paramsMenu.setAddressModalText,
        buttonsDirection: "row",
        buttons: [
          {
            label: "Изменить",
            onPress: openBottomMenu,
            styles: { modifier: "bordered" },
          },
          {
            label: "Подтвердить",
            onPress: onPressConfirm,
          },
        ],
      });
    }
  }, [
    token,
    commentInput.value,
    optionsData.isVisibleBonus,
    bonusInput.value,
    currentOrderType?.isRemoted,
    selectedDate,
    selectedTime,
    currentAddress,
    currentOrganisation?.address,
    openBottomMenu,
    onPressConfirm,
  ]);

  const onChangeCount = async ({
    count,
    oldCount,
    item,
  }: {
    count: number;
    oldCount: number;
    item: ICartGetDataProductResponse;
  }) => {
    if (oldCount === count || count > item.max_count) {
      return;
    }

    countDebounce({ count, key: item.key });
  };

  const activatePromoCodeHandler = useCallback(
    async (code: string) => {
      setTextErrorPromoCode("");
      if (
        code &&
        currentOrderType?.id &&
        currentOrganisation?.id !== undefined
      ) {
        const res = await dispatch(
          AsyncActionCart.activatePromoCode({
            promocode: code,
            order_type: currentOrderType?.id,
            id_org: +currentOrganisation.id,
            polygon_id: polygonId,
          })
        );
        if (res.error && res.error_msg) {
          setTextErrorPromoCode(res.error_msg);
        }
      }
    },
    [currentOrderType?.id, currentOrganisation?.id, polygonId]
  );

  const clearPromoCode = async () => {
    if (checkOrganisationAndOrderType) {
      const res = await dispatch(
        AsyncActionCart.removePromoCode({
          order_type: currentOrderType!.id,
          id_org: currentOrganisation!.id,
          polygon_id: polygonId,
        })
      );
      dispatch(ActionCart.setCartData(res));
    }
  };

  const {
    total_count = 0,
    total_cost = 0,
    min_count_cost = 0,
    actual_discount = 0,
    promocodes = "",
    original_total_cost,
  } = cartData || {};

  const headerSubTitleCreator = useCallback(
    (count: number) => {
      return `${count} ${productDeclensionWord(
        count
      )} на ${original_total_cost} ₽`;
    },
    [total_cost]
  );

  const onChangeText = useCallback(
    (value: string) => {
      if (value) {
        if (max_bonus >= +value) {
          bonusInput.onChangeText(parseInt(value).toString());
        } else {
          bonusInput.onChangeText(max_bonus?.toString());
        }
      } else {
        bonusInput.onChangeText("");
      }
    },
    [bonusInput, max_bonus]
  );

  const openPresentModal = useCallback(() => {
    modalPresentRef.current?.show();
  }, []);

  const onPressClosePresentModal = useCallback(() => {
    modalPresentRef.current?.close();
  }, [modalPresentRef]);

  const onSelectPresent = useCallback(
    (present: IRadioButtonData) => {
      modalPresentRef.current?.close();
      if (currentOrganisation?.id && currentOrderType?.id) {
        dispatch(
          AsyncActionCart.cartAdd({
            id: +present.id,
            count: 1,
            order_type: +currentOrderType.id,
            id_org: +currentOrganisation.id,
            additives: [],
            size: [],
            type: [],
            ingredients: [],
            polygon_id: polygonId,
          })
        );
      }
    },
    [currentOrderType, currentOrganisation, dispatch]
  );

  const isDisabledButton = useMemo(() => {
    if (!currentOrderType?.isRemoted || !cartData) {
      return false;
    }
    return cartData?.min_count_cost > cartData?.total_cost;
  }, [cartData, currentOrderType?.isRemoted]);

  const promoSelectRef = useRef<ISelectCustomMethods | null>(null);

  const getPromoRef = (ref: ISelectCustomMethods | null) => {
    promoSelectRef.current = ref;
  };

  // const closePromoSelect = useCallback(() => {
  //   promoSelectRef.current?.hide();
  //   Keyboard.dismiss();
  // }, []);

  const onSelectDate = useCallback(
    (index: number) => {
      const times = freeTimes_.times[index]
      const date = freeTimes_.dates[index]
      if(times && date) {
        setSelectedDate({
          index,
          value: date.value,
          times,
        });
      }
    },
    [freeTimes_]
  );

  const onSelectTime = useCallback(
    (index: number) => {
      const value = selectedDate?.times?.[index];
      if(value){
        setSelectedTime({ index, data: value });
      } 
    },
    [freeTimes_, selectedDate]
  );

  const onChangeOptions = (
    id: number,
    isChecked: boolean,
    type: keyof typeof CartGiftsOptionType
  ) => {
    setOptionsData((state) => {
      const newState = { ...state };
      const has = state.selectedOptionsIds.has(id);
      state.selectedOptionsIds[has ? "delete" : "add"](id);
      type === CartGiftsOptionType.bonus &&
        (newState.isVisibleBonus = isChecked);
      if (type === CartGiftsOptionType.promo) {
        newState.isVisiblePromo = isChecked;
        actual_discount && clearPromoCode();
      }
      type === CartGiftsOptionType.delivery_sale &&
        (newState.enabledDeliverySale = isChecked);
      return newState;
    });
  };

  const onPressAddAdditionalProduct = (id: number) => {
    if (currentOrganisation && currentOrderType) {
      dispatch(
        AsyncActionCart.cartAdd({
          id: id,
          count: 1,
          order_type: +currentOrderType?.id,
          id_org: +currentOrganisation?.id,
          additives: [],
          size: [],
          type: [],
          ingredients: [],
          polygon_id: polygonId,
        })
      );
    }
  };

  // const isDisabledPromoButtons = () => {
  //   return (
  //     cartGifts?.max_select_count === 1 &&
  //     (optionsData.enabledDeliverySale ||
  //       optionsData.isVisibleBonus ||
  //       optionsData.isVisiblePromo)
  //   );
  // };
  //
  //
  const generatePromoCodeDescription = (discountCost: number) => {
    return discountCost > 0
      ? `Промокод применен. Скидка ${discountCost} ₽`
      : "Промокод применен. Товар добавлен";
  };

  useEffect(() => {
    if (!optionsData.isVisiblePromo && !!promocodes) {
      clearPromoCode();
    }
  }, [optionsData.isVisiblePromo]);

  useEffect(() => {
    bonusInput.onChangeText(max_bonus?.toString() | "0");
  }, [max_bonus]);

  useEffect(() => {
    if (currentOrganisation?.id && currentOrderType?.id) {
      InteractionManager.runAfterInteractions(() => {
        // AsyncActionCartGifts.getAdditionalProducts({
        //   order_type: currentOrderType.id,
        //   id_org: currentOrganisation.id,
        // }),
        dispatch(
          AsyncActionCart.getAdditionalProducts({
            order_type: currentOrderType.id,
            id_org: currentOrganisation.id,
          })
        );

        dispatch(
          AsyncActionCart.getPresentFromPrice({
            order_type: +currentOrderType?.id,
            id_org: currentOrganisation?.id,
            polygon_id: polygonId,
          })
        );
        dispatch(
          AsyncActionCart.getCartData({
            id_org: +currentOrganisation?.id,
            order_type: +currentOrderType?.id,
            polygon_id: polygonId,
          })
        );

        dispatch(
          AsyncActionCart.getFreeTime({ id_org: currentOrganisation.id })
        ).then((data) => {
          const dates = Object.keys(data.freeTime);
          const times = Object.values(data.freeTime);
          const formattedTimes = times.map((el) => formatingData(el));
          setFreeTimes_({
            dates: formatingData(dates),
            times: formattedTimes,
          });
          //setFreeTimes_(data.freeTime)
          setSelectedDate({
            index: 0,
            value: dates[0],
            times: formatingData(times[0]),
          });
        });

        dispatch(
          AsyncActionCart.getOptions({
            id_org: currentOrganisation.id,
            order_type: currentOrderType.id,
          })
        );

        // dispatch(
        //   AsyncActionCart.getGiftsNew({
        //     order_type: currentOrderType.id,
        //     id_org: currentOrganisation.id,
        //   }),
        // );
      });
    }
  }, []);

  return (
    <SafeAreaView style={UIStyles.flex}>
      <FocusAwareStatusBar
        barStyle={"dark-content"}
        backgroundColor={Color.WHITE}
      />
          {cartData?.products && cartData?.products?.length !== 0 ? (
      <KeyboardAvoidingScroll>
        <ScreeHeaderTitleLeft title={cartConfig.headerTitle} />

        {/*<Pressable onPress={closePromoSelect} style={styles.contentContainer}>*/}
        <View style={styles.contentContainer}>
            <>
              <Text style={styles.titleTotal}>
                {headerSubTitleCreator(total_count)}
              </Text>
              {currentOrderType?.isRemoted && min_count_cost ? (
                <Text style={styles.minCountDeliveryText}>
                  Минимальная сумма на доставку {min_count_cost} ₽
                </Text>
              ) : null}
              {currentOrderType?.isRemoted &&
              cartData?.min_free_delivery_cost ? (
                <Text style={styles.minCountDeliveryText}>
                  Бесплатная доставка от {cartData?.min_free_delivery_cost} ₽
                </Text>
              ) : null}

              {cartConfig.datePicker.isVisible ? (
                <DatePicker
                  onSuccess={() => {}}
                  selectedDateIndex={selectedDate.index}
                  selectedTimeIndex={selectedTime.index}
                  dates={freeTimes_.dates}
                  times={selectedDate.times}
                  onSelectTime={onSelectTime}
                  onSelectDate={onSelectDate}
                />
              ) : null}

              {cartData.products.map((el) => {
                return (
                  <CartProductListItem
                    isLoading={isLoading}
                    id={el.id}
                    key={el.key?.toString()}
                    options={el.options}
                    maxCount={el.max_count}
                    onChangeCount={(count, oldCount) =>
                      onChangeCount({ count, oldCount, item: el })
                    }
                    count={el.count}
                    description={el.desc}
                    title={el.title}
                    price={el.price}
                    image={el.img_url}
                  />
                );
              })}

              {additionalProducts?.length ? (
                <AdditiveProducts
                  onPressAdd={onPressAddAdditionalProduct}
                  data={additionalProducts}
                />
              ) : null}
              {cartConfig.isVisiblePresentButton ? (
                <PresentButton showModal={openPresentModal} />
              ) : null}

              <Options
                disabled={
                  optionsData.selectedOptionsIds.size >=
                    (cartGifts?.max_select_count ?? 1) ||
                  cartData.disabled_options
                }
                data={cartGifts?.options?.items || []}
                onChange={onChangeOptions}
                selectedOptions={optionsData.selectedOptionsIds}
              />

              {optionsData.isVisiblePromo ? (
                <>
                  <PromoInput
                    hasPromoCode={!!promocodes}
                    getRef={getPromoRef}
                    onPress={activatePromoCodeHandler}
                    clearPromoCode={clearPromoCode}
                  />

                  {!!textErrorPromoCode && (
                    <Text
                      style={[styles.promoTitle, styles.promoCodeTitleError]}
                    >
                      {textErrorPromoCode}
                    </Text>
                  )}
                  {!!promocodes && !textErrorPromoCode ? (
                    <Text style={[styles.promoTitle, styles.promoCodeTitle]}>
                      {generatePromoCodeDescription(+cartData.actual_discount)}
                    </Text>
                  ) : null}
                </>
              ) : null}

              {optionsData.isVisibleBonus ? (
                <View style={styles.bonusCheckBoxContainer}>
                  <View style={styles.maxBonusWrapper}>
                    <Text style={styles.maxBonusTitle}>
                      Максимум{" "}
                      <Text style={styles.maxBonusValue}>{max_bonus} ₽</Text>
                    </Text>
                  </View>
                  <InputWithRightElement
                    keyboardType={"numeric"}
                    RightComponent={() => <Text>₽</Text>}
                    inputStyle={styles.bonusInputWrapper}
                    textStyle={styles.textInput}
                    value={bonusInput.value?.toString()}
                    onChangeText={onChangeText}
                  />
                </View>
              ) : null}

              <InfoRows
                isCheckedSaleForSelfDelivery={optionsData.enabledDeliverySale}
                isEnabledDeliverySale={optionsData.enabledDeliverySale}
                isRemoted={currentOrderType?.isRemoted}
                bonus={bonusInput.value}
                isCheckedBonus={optionsData.isVisibleBonus}
                cartData={cartData}
              />
              <Input
                inputStyle={styles.inputStyle}
                {...commentInput}
                placeholderText={cartConfig.commentPlaceholder}
              />
              <ThemedButton
                wrapperStyle={styles.button}
                disabled={isDisabledButton}
                rounded={true}
                label={cartConfig.nextButtonLabel}
                onPress={onPressNext}
              />
            </>
        </View>
      </KeyboardAvoidingScroll>
          ) : (
            <EmptyCartComponent.Component />
          )}
      <ModalWithComponent ref={modalPresentRef}>
        <PresentModalContent
          onClose={onPressClosePresentModal}
          onSelect={onSelectPresent}
        />
      </ModalWithComponent>
    </SafeAreaView>
  );
});

const styles = StyleSheet.create({
  contentContainer: {
    flex: 1,
    ...UIStyles.paddingH16,
  },
  titleTotal: {
    ...UIStyles.font18b,
    color: Color.BLACK,
  },
  minCountDeliveryText: {
    marginTop: 6,
    ...UIStyles[isSmallScreen ? "font13" : "font15"],
    color: Color.GREY_600,
  },
  promoTitle: {
    marginTop: 8,
    ...UIStyles.font15,
  },
  promoCodeTitle: {
    color: Color.SUCCESS,
  },
  promoCodeTitleError: {
    color: Color.DANGER,
  },
  bonusCheckBoxContainer: {
    height: 36,
    marginTop: 16,
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 16,
  },
  bonusInputWrapper: {
    width: adaptiveSize(120),
    padding: 0,
    paddingVertical: 0,
  },
  textInput: {
    paddingVertical: adaptiveSize(5),
  },
  inputStyle: {
    marginBottom: 20,
  },
  button: {
    marginBottom: 10,
  },
  maxBonusWrapper: {
    flex: 1,
    justifyContent: "center",
  },
  maxBonusTitle: {
    ...UIStyles.font13,
    color: Color.GREY_600,
  },
  maxBonusValue: {
    ...UIStyles.font13b,
    color: Color.GREY_600,
  },
});
