import { INextButtonConfig } from '../../../delivery';
import { Config } from '../../../../config';
import { adaptiveSize } from '../../../../system/helpers/responseive-font-size';

interface ICartConfig extends INextButtonConfig<{ isRemoted: boolean }> {
  isVisiblePresentButton: boolean;
  headerTitle: string;
  commentPlaceholder: string;
  isVisibleDateTimePicker: boolean;
  isVisibleAdditiveProducts: boolean;
  emptyCartComponent: {
    iconSize: number;
    iconColor: string;
    isVisible: boolean;
  };
  datePicker: {
    isVisible: boolean;
    selectedColor: string;
  };
  authModal: {
    title: string;
    description: string;
    closeButtonLabel: string;
    successButtonLabel: string;
  };
}

export const cartConfig: ICartConfig = {
  headerTitle: 'Ваш заказ',
  nextButtonLabel: 'Оформить',
  onPressNextButton: null,
  isVisiblePresentButton: true,
  isVisibleDateTimePicker: true,
  isVisibleAdditiveProducts: false,
  emptyCartComponent: {
    iconSize: adaptiveSize(150),
    iconColor: Config.Color.PRIMARY,
    isVisible: true,
  },
  datePicker: {
    isVisible: false,
    selectedColor: Config.Color.SECONDARY,
  },
  commentPlaceholder: 'Комментарий к заказу',
  authModal: {
    title: 'Ошибка',
    description: 'Для оформления заказа нужно авторизоватся !',
    closeButtonLabel: 'Отмена',
    successButtonLabel: 'Авторизация',
  },
};
