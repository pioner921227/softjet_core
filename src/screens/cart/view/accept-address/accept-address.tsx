import React, { useCallback, useMemo } from "react";
import { SafeAreaView, StyleSheet, Text, TouchableOpacity } from "react-native";
import { useTypedSelector } from "../../../../system/hooks/use-typed-selector";
import {
  CreateAddressContent,
  TAcceptAddressProps,
} from "../../../delivery/view/components/create-address-content";
import { ScreenHeader } from "../../../../components/screen-header/screen-header";
import { HeaderTitle } from "../../../../components/screen-header/header-title";
import { Config } from "../../../../config";
import { StackNavigationProp, StackScreenProps } from "@react-navigation/stack";
import { TRootStackParamList } from "../../../../navigation/types/types";
import { Routes } from "../../../../navigation";
import { useBottomSheetMenu, useModal } from "../../../../components";
import { BSMContentCreateNewAddress } from "../../../delivery/view/components/bsm-content-create-new-address";
import { useSelectOrderTypeHandler } from "../../../../system/hooks/use-select-order-type-handler";
import { useNavigation } from "@react-navigation/native";
import {
  getCities,
  getCurrentAddress,
} from "../../../../system/selectors/selectors";
import { KeyboardAvoidingScroll } from "../../../../components/keyboard-avoiding-scroll";
import { isSmallScreen } from "../../../../system/helpers/is-small-screen";
import { acceptAddressConfig } from "./config";
import { isEqual } from "lodash";
import { useTypedDispatch } from "../../../../system/hooks/use-typed-dispatch";
import { useSnapForBottomModal } from "../../../../system/hooks/use-snap-for-bottom-modal";
import { ArrowLeftIcon } from "../../../../components/icons";
import { useIsLoading } from "../../../../system/hooks/use-is-loading";
import { ActionDelivery, AsyncActionsDelivery } from "../../../delivery";
import { useHitSlop } from "../../../../system/hooks/use-hit-slop";
import { useAndroidBackHandler } from "../../../../system/hooks/use-android-back-handler";

const { Color, UIStyles } = Config;

type TNavigation = StackNavigationProp<TRootStackParamList, Routes.Payment>;
type TProps = StackScreenProps<TRootStackParamList, Routes.AcceptAddress>;

export const AcceptAddress: React.FC<TProps> = React.memo(() => {
  const cities = useTypedSelector(getCities);
  const currentAddress = useTypedSelector(getCurrentAddress);
  const { show, close } = useModal();
  const { show: showBottomModal, close: closeBottomModal } =
    useBottomSheetMenu();
  const { navigate, goBack } = useNavigation<TNavigation>();
  const { isLoading, setIsLoading } = useIsLoading();

  const dispatch = useTypedDispatch();

  const onPressSave = async (data: TAcceptAddressProps) => {
    setIsLoading(true);
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { id, delivery_exists, id_org, user_id, ...address } =
      currentAddress || {};
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { cityId, ...allData } = data;
    const isEqualAddresses = isEqual(allData, address);
    if (!isEqualAddresses) {
      if (id !== undefined) {
        const newAddress = await dispatch(
          AsyncActionsDelivery.changeUserAddress({ id, ...data })
        );
        if (newAddress) {
          dispatch(ActionDelivery.setCurrentAddress(newAddress));
        }
      }
    }
    setIsLoading(false);
    navigate(Routes.Payment);
  };

  const onSelectOrderType = useSelectOrderTypeHandler({
    finishCallback: closeBottomModal,
  });

  const getButtonsWidth = useSnapForBottomModal();

  const snapPoints = useMemo(() => [getButtonsWidth(false)], [getButtonsWidth]);

  const onPressEdit = useCallback(() => {
    close();
    showBottomModal({
      Component: () => (
        <BSMContentCreateNewAddress onPress={onSelectOrderType} />
      ),
      snapPoints,
    });
  }, [close, onSelectOrderType, showBottomModal, snapPoints]);

  const onPressSetAddress = () => {
    show({
      title: "Изменить адрес?",
      description:
        "Если вы смените адрес, то вам придется заново составлять меню, так как в другом заведении может быть другое меню",
      buttons: [
        { label: "Отмена", onPress: close, styles: { modifier: "bordered" } },
        {
          label: "Изменить",
          onPress: onPressEdit,
        },
      ],
      buttonsDirection: "row",
    });
  };

  useAndroidBackHandler();

  const hitSlop = useHitSlop(10);

  return (
    <SafeAreaView style={UIStyles.flexGrow}>
      <KeyboardAvoidingScroll>
        <ScreenHeader
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <TouchableOpacity hitSlop={hitSlop} onPress={goBack}>
            <ArrowLeftIcon />
          </TouchableOpacity>
          <HeaderTitle>{acceptAddressConfig.headerTitle}</HeaderTitle>
          {acceptAddressConfig.enableEditAddressButtonInHeader ? (
            <TouchableOpacity onPress={onPressSetAddress}>
              <Text style={styles.headerLabelSetAddress}>Изменить адрес</Text>
            </TouchableOpacity>
          ) : null}
        </ScreenHeader>
        <Text style={styles.description}>
          Подтвердите адрес и оставьте комментарий
        </Text>
        <CreateAddressContent
          isLoading={isLoading}
          house={currentAddress?.house_num ?? ""}
          street={currentAddress?.street ?? ""}
          entrance={currentAddress?.entrance ?? ""}
          domophone={currentAddress?.doorphone ?? ""}
          flat={currentAddress?.flat_num ?? ""}
          floor={currentAddress?.floor ?? ""}
          disabledStreet={true}
          disabledSelect={true}
          disabledHome={true}
          buttonLabel={"Подтвердить"}
          cities={cities}
          onPressButton={onPressSave}
        />
      </KeyboardAvoidingScroll>
    </SafeAreaView>
  );
});

const styles = StyleSheet.create({
  headerLabelSetAddress: {
    ...UIStyles[isSmallScreen ? "font12" : "font15"],
    color: Color.PRIMARY,
  },
  description: {
    ...UIStyles[isSmallScreen ? "font13" : "font15"],
    ...UIStyles.paddingH16,
    color: Color.GREY_600,
    marginTop: 16,
    marginBottom: 24,
  },
});
