import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { useTypedSelector } from '../../../../system';
import { Config } from '../../../../config';
import { ThemedButton } from '../../../../components/buttons/themed-button';
import { IPresentModificator } from '../../api/types';
import {
  IRadioButtonData,
  RadioButtonCore,
} from '../../../../components/radio-button-new/radio-button-core';
import { getCart } from '../../../../system/selectors/selectors';

const { Color, UIStyles, RADIUS } = Config;

interface IPresentModalContent {
  onClose: () => void;
  onSelect: (present: IRadioButtonData) => void;
}

export const PresentModalContent: React.FC<IPresentModalContent> = React.memo(
  ({ onClose, onSelect }) => {
    const { presents, cartData } = useTypedSelector(getCart);
    const [
      selectedPresent,
      setSelectedPresent,
    ] = useState<IRadioButtonData | null>(null);

    const onSelectHandler = (item: IRadioButtonData) => {
      setSelectedPresent(item);
    };

    const onSelectPresent = () => {
      if (selectedPresent) {
        onSelect(selectedPresent);
      }
    };

    const convertModificators = (
      modificators: IPresentModificator[],
      minCost: number,
    ): IRadioButtonData[] => {
      if (modificators && minCost != null) {
        return modificators.map(el => ({
          id: el.id,
          label: el.title,
          disabled: minCost > (cartData?.total_cost || 0),
        }));
      } else {
        return [];
      }
    };

    const getIndex = (id: number) => {
      const item = presents?.find(el =>
        el.modificators.find(mod => mod.id === id),
      );
      const index = item?.modificators.findIndex(el => el.id === id);
      return {
        item: item ?? { id_promo: 0 },
        index: index ?? -1,
      };
    };

    return (
      <View>
        <Text style={styles.title}>Выберите подарок</Text>
        <View>
          {presents?.map(el => {
            const element = getIndex(selectedPresent?.id || 0);

            return (
              <View key={el.id_promo} style={styles.presentRowContainer}>
                <Text style={styles.subtitles}>{el.title}</Text>
                <RadioButtonCore
                  borderCircleStyle={{
                    color: Color.PRIMARY,
                  }}
                  circleStyle={{ color: Color.PRIMARY }}
                  data={convertModificators(el.modificators, el.min_count)}
                  onSelect={onSelectHandler}
                  labelStyle={styles.radioButtonLabel}
                  selectedIndex={
                    element?.item.id_promo === el.id_promo ? element.index : -1
                  }
                  itemStyle={styles.radioButtonItem}
                />
              </View>
            );
          })}
        </View>
        <View style={styles.buttonBlock}>
          <ThemedButton
            buttonStyle={styles.button}
            wrapperStyle={UIStyles.flex}
            modifier={'bordered'}
            rounded={true}
            label="Закрыть"
            onPress={onClose}
          />
          <ThemedButton
            disabled={!selectedPresent}
            buttonStyle={styles.button}
            wrapperStyle={styles.selectButtonWrapperStyle}
            rounded={true}
            label="Выбрать"
            onPress={onSelectPresent}
          />
        </View>
      </View>
    );
  },
);

const styles = StyleSheet.create({
  title: {
    ...UIStyles.font20b,
  },
  subtitlesBlock: {
    marginTop: 12,
  },
  subtitles: {
    ...UIStyles.font15,
    color: Color.GREY_500,
  },
  boxStyle: {
    paddingVertical: 12,
    margin: 0,
    marginTop: 0,
  },
  buttonBlock: {
    flexDirection: 'row',
    marginTop: 26,
  },
  button: {
    height: 50,
    borderRadius: RADIUS.MEDIUM,
  },
  selectButtonWrapperStyle: {
    flex: 1,
    marginLeft: 16,
  },
  presentRowContainer: {
    marginTop: 12,
  },
  radioButtonLabel: {
    ...UIStyles.font15,
    color: Color.DARK,
  },
  radioButtonItem: {
    paddingVertical: 12,
  },
});
