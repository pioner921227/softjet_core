import React, { useCallback, useMemo } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import { useTypedSelector } from '../../../../system';
import { Config } from '../../../../config';
import { getThemeColor } from '../../../../system/helpers/get-theme-color';
import { getCart } from '../../../../system/selectors/selectors';

const { Color, UIStyles, RADIUS } = Config;

interface IPresentButton {
  showModal: () => void;
}

export const PresentButton: React.FC<IPresentButton> = React.memo(
  ({ showModal }) => {
    const { presents, cartData } = useTypedSelector(getCart);

    const firstPresent = presents?.[0];

    const getPresent = useCallback(() => {
      if (presents && cartData) {
        return presents?.find(el => el.min_count >= cartData?.total_cost);
      }
    }, [cartData, presents]);

    const image = useMemo(
      () => ({ uri: getPresent()?.imgUrl || firstPresent?.imgUrl }),
      [firstPresent?.imgUrl, getPresent],
    );

    if (!presents?.length) {
      return null;
    }

    return (
      <TouchableOpacity onPress={showModal} style={styles.container}>
        <FastImage source={image} resizeMode={'cover'} style={styles.image} />
        <View style={styles.contentWrapper}>
          <Text style={styles.title}>
            Подарок за сумму в корзине выше{' '}
            {getPresent()?.min_count || presents?.[0].min_count}
          </Text>
          <View style={styles.buttonStyle}>
            <Text style={styles.buttonTitle}>Выбрать подарок</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  },
);

const styles = StyleSheet.create({
  container: {
    marginTop: 22,
  },
  contentWrapper: {
    height: '100%',
    position: 'absolute',
    paddingVertical: 16,
    width: 143,
    left: 16,
  },
  title: {
    ...UIStyles.font15b,
    color: Color.WHITE,
  },
  image: {
    width: '100%',
    height: 128,
    borderRadius: RADIUS.LARGE,
  },
  buttonStyle: {
    height: 28,
    marginTop: 'auto',
    backgroundColor: getThemeColor(),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: Config.BUTTON_RADIUS,
  },
  buttonTitle: {
    ...UIStyles.font12Db,
    textTransform: 'uppercase',
    color: Color.WHITE,
    fontSize: 12,
  },
});
