import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import {Color, RADIUS, UIStyles} from '../../../../../assets/styles';
import {Cart2Icon} from '../../../../../components/icons/cart-2-icon';
import { adaptiveSize } from '../../../../../system/helpers/responseive-font-size';
import {useImage} from '../../../../../system/hooks/use-image';

interface IAdditiveProductListItem {
  image: string;
  title: string;
  price: number;
  id: number;
  onPressAdd: (id: number) => void;
}

export const AdditiveProductListItem: React.FC<IAdditiveProductListItem> = React.memo(
  ({title, price, image, onPressAdd, id}) => {
    const image_ = useImage(image);

    const onPressAdd_ = () => {
      onPressAdd(id);
    };

    return (
      <View style={styles.container}>
        <FastImage style={styles.image} source={image_} />
        <View style={styles.leftContainer}>
          <Text style={{flexWrap: 'nowrap'}}>{title}</Text>
          <TouchableOpacity onPress={onPressAdd_} style={styles.button}>
            <Cart2Icon fill={Color.PRIMARY} width={13} height={13} />
            <Text style={styles.buttonLabel}>{price} ₽</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  },
);

const styles = StyleSheet.create({
  container: {
    // width: 200,
    height: adaptiveSize(110),
    borderColor: Color.PRIMARY_40,
    borderWidth: 1,
    borderRadius: RADIUS.MEDIUM,
    marginRight: 8,
    flexDirection: 'row',
    paddingHorizontal: 12,
    paddingVertical: 10,
    marginBottom: 10,
  },
  image: {
    width: adaptiveSize(80),
    height: adaptiveSize(80),
  },
  leftContainer: {
    width: adaptiveSize(100),
    marginLeft: 12,
  },
  button: {
    marginTop: 'auto',
    borderRadius: 88,
    alignSelf: 'flex-end',
    borderWidth: 1,
    borderColor: Color.PRIMARY,
    flexDirection: 'row',
    paddingHorizontal: 12,
    paddingVertical: 6,
  },
  buttonLabel: {
    ...UIStyles.font12b,
    color: Color.PRIMARY,
    marginLeft: 10,
  },
});
