import React from "react";
import { ScrollView, StyleSheet, View, Text } from "react-native";
import { AdditiveProductListItem } from "./additive-product-list-item";
import { IGetCartAdditionalProducts } from "../../../api/types";
import { Config } from "../../../../../config";
const { Color, UIStyles } = Config;

interface IAdditiveProducts {
  data: IGetCartAdditionalProducts[];
  onPressAdd: (index: number) => void;
}

export const AdditiveProducts: React.FC<IAdditiveProducts> = ({
  data,
  onPressAdd,
}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>С этими товарами покупают</Text>
      <ScrollView
        contentContainerStyle={styles.listContentContainer}
        showsHorizontalScrollIndicator={false}
        horizontal={true}
      >
        {data?.map((el) => {
          return (
            <AdditiveProductListItem
              onPressAdd={onPressAdd}
              key={el.id}
              id={+el.id}
              image={el.img_big}
              price={+el.price}
              title={el.title}
            />
          );
        })}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginHorizontal: -16,
    marginTop: 16,
  },
  listContentContainer: {
    paddingHorizontal: 16,
  },
  title: {
    marginLeft: 16,
    marginBottom: 16,
    ...UIStyles.font10b,
    fontWeight: "bold",
    color: Color.GREY_600,
    textTransform: "uppercase",
  },
});
