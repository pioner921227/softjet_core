import React, { Fragment } from "react";
import { ICartGetDataResponse } from "../../api/types";
import { Color } from "../../../../assets/styles";
import { TitleWithValueRow } from "../../../../components/title-with-value-row";

interface IInfoRows {
  isRemoted?: boolean;
  bonus?: string;
  isCheckedBonus: boolean;
  cartData: ICartGetDataResponse;
  isEnabledDeliverySale: boolean;
  isCheckedSaleForSelfDelivery: boolean;
}

export const InfoRows: React.FC<IInfoRows> = ({
  isRemoted = false,
  bonus = 0,
  isCheckedBonus,
  isCheckedSaleForSelfDelivery,
  cartData,
  isEnabledDeliverySale,
}) => {
  const {
    delivery_cost = 0,
    promocodes = 0,
    discount_cost = 0,
    total_cost = 0,
    actual_discount = 0,
    original_total_cost = 0,
    self_pickup_discount = 0,
  } = cartData || {};

  // const calculatePrice = () => {
  //   const _bonus = isCheckedBonus ? bonus : 0;
  //   return promocodes
  //     ? discount_cost + delivery_cost - +_bonus
  //     : isEnabledDeliverySale
  //     ? total_cost
  //     : original_total_cost + delivery_cost - +_bonus;
  // };

  const bonus_ = isCheckedBonus ? bonus : 0;
  
  const base_cost = promocodes
    ? discount_cost
    : isEnabledDeliverySale
    ? total_cost
    : original_total_cost;
  
  const values_cost = [base_cost, delivery_cost, -+bonus_];
  
  const price = values_cost.reduce((acc, el) => acc + el, 0);

  return (
    <Fragment>
      {isRemoted ? (
        <TitleWithValueRow
          title="Доставка"
          valueStyle={{ color: !delivery_cost ? Color.SUCCESS : Color.DARK }}
          value={!delivery_cost ? "Бесплатно" : `${delivery_cost} ₽`}
        />
      ) : null}
      <TitleWithValueRow title={"Итого"} value={`${original_total_cost} ₽`} />
      {isCheckedBonus ? (
        <TitleWithValueRow title="Оплачено бонусами" value={`${bonus} ₽`} />
      ) : null}
      {!!promocodes && actual_discount > 0 ? (
        <TitleWithValueRow
          title={"Промокод"}
          value={`- ${actual_discount} ₽`}
        />
      ) : null}

      {isCheckedSaleForSelfDelivery ? (
        <TitleWithValueRow
          title={"Скидка за самовывоз"}
          value={`${self_pickup_discount} ₽`}
        />
      ) : null}

      <TitleWithValueRow
        title={"Итого к оплате"}
        value={`${price} ₽`}
      />
    </Fragment>
  );
};
