import React, {useCallback, useMemo, useState} from 'react';

import {StyleSheet, TouchableOpacity, View} from 'react-native';

import {Color, UIStyles} from '../../../../assets/styles';
import {
  ISelectCustomMethods,
  ISelectItem,
  SelectCustom,
} from '../../../../components/select-custom/select-custom';
import {useInput, useTypedSelector} from '../../../../system';
import {adaptiveSize} from '../../../../system/helpers/responseive-font-size';
import {getBanners} from '../../../../system/selectors/selectors';
import {
  CloseCrossIcon,
  InputWithRightElement,
  ThemedButton,
} from '../../../../ui-kit';

interface IPromoInput {
  onPress: (text: string) => void;
  getRef?: (ref: ISelectCustomMethods | null) => void;
  hasPromoCode: boolean;
  clearPromoCode: () => void;
}

export const PromoInput: React.FC<IPromoInput> = React.memo(
  ({onPress, getRef, hasPromoCode, clearPromoCode}) => {
    const activePromoCodes = useTypedSelector(
      state => state.catalog.bannersActivePromoCode,
    );
    const banners = useTypedSelector(getBanners);

    const promoCodesForSelect = useMemo(
      () =>
        Array.isArray(banners)
          ? banners.filter(el => activePromoCodes.includes(+el.id))
          : [],
      [activePromoCodes, banners],
    );

    const promoForSelect: ISelectItem[] = useMemo(
      () =>
        promoCodesForSelect.map(el => ({
          label: el?.promocode,
          id: +el?.id,
          value: el?.promocode,
        })),
      [promoCodesForSelect],
    );

    const input = useInput('');
    const [selectedItem, setSelectedItem] = useState(promoForSelect[0]);

    const onPressHandler = useCallback(() => {
      onPress(input.value);
    }, [input.value, onPress]);

    const onSelect = useCallback(
      (item: ISelectItem) => {
        setSelectedItem(item);
        input.onChangeText(item.label);
      },
      [input],
    );

    const listHeight = useMemo(() => {
      if (promoForSelect.length < 5) {
        return promoForSelect.length * 50;
      }
      return 200;
    }, [promoForSelect.length]);

    const isButtonDisabled = input.value.length === 0;

    const RightComponent = useCallback(() => {
      return (
        <View style={styles.selectWrapper}>
          <SelectCustom
            containerStyle={styles.select}
            disabledSelectBackgroundColor={'transparent'}
            disabledSelectTextColor={Color.GREY_200}
            disabled={promoForSelect.length === 0}
            ref={getRef}
            listStyle={styles.listStyle}
            selectTitleStyle={styles.selectSelectedTitle}
            selectStyle={styles.selectStyle}
            listHeight={listHeight}
            data={promoForSelect}
            onSelect={onSelect}
            selectedItem={selectedItem}
          />

          {hasPromoCode ? (
            <TouchableOpacity
              onPress={clearPromoCode}
              style={styles.circleButton}>
              <CloseCrossIcon fill={Color.PRIMARY} />
            </TouchableOpacity>
          ) : (
            <ThemedButton
              disabled={isButtonDisabled}
              buttonStyle={UIStyles.flex}
              rounded={true}
              modifier={'bordered'}
              wrapperStyle={styles.button}
              labelStyle={UIStyles.font12}
              label={'Проверить'}
              onPress={onPressHandler}
            />
          )}
        </View>
      );
    }, [
      clearPromoCode,
      getRef,
      hasPromoCode,
      promoForSelect,
      listHeight,
      onSelect,
      selectedItem,
      isButtonDisabled,
      onPressHandler,
    ]);

    return (
      <InputWithRightElement
        {...input}
        placeholderText={'Введите промокод'}
        inputStyle={styles.inputStyle}
        RightComponent={RightComponent}
      />
    );
  },
);

const styles = StyleSheet.create({
  inputStyle: {
    paddingVertical: 11,
    alignItems: 'center',
    marginTop: 16,
    zIndex: 2,
  },
  selectWrapper: {
    flexDirection: 'row',
    // flex: 1,
  },
  selectSelectedTitle: {
    color: 'transparent',
    width: 0,
  },
  selectStyle: {
    borderWidth: 0,
    paddingHorizontal: 0,
    paddingVertical: 0,
    // marginRight: 25,
    // height: adaptiveSize(30),
    // width: adaptiveSize(10),
  },
  listStyle: {
    width: adaptiveSize(120),
    left: -70,
  },
  button: {
    width: adaptiveSize(97),
    height: adaptiveSize(28),
  },
  select: {
    // flex: 1,
    marginRight: 25,
    height: 10,
    width: 30,
  },
  circleButton: {
    width: adaptiveSize(28),
    height: adaptiveSize(28),
    borderRadius: 50,
    borderWidth: 1,
    borderColor: Color.PRIMARY,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
