import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {HorizontalPicker} from '../../../../../components/horizontal-picker/horizontal-picker';
import {Config} from '../../../../../config';

const {UIStyles, Color} = Config;

interface IDateTimePicker {
  onSelectDate: (index: number) => void;
  onSelectTime: (index: number) => void;
  date: string[];
  time: string[];
}

export const DateTimePicker: React.FC<IDateTimePicker> = ({
  onSelectDate,
  onSelectTime,
  date,
  time,
}) => {
  const [isVisibleTime, setIsVisibleTime] = useState(false);
  // const [selectedData, setSelectedData] = useState('');

  const onSelectDateHandler = (index: number) => {
    setIsVisibleTime(index !== 0);
    // setSelectedData(date[index]);
    onSelectDate(index);
  };

  const onSelectTimeHandler = (index: number) => {
    onSelectTime(index);
  };

  // LayoutAnimation.configureNext({
  //   duration: 500,
  //   create: {
  //     duration: 500,
  //     // delay: 200,
  //     type: LayoutAnimation.Types.spring,
  //     property: LayoutAnimation.Properties.scaleY,
  //     springDamping: 0.7,
  //   },
  //   update: {
  //     duration: 600,
  //     type: LayoutAnimation.Types.spring,
  //     property: LayoutAnimation.Properties.scaleY,
  //     springDamping: 0.7,
  //   },
  //   delete: {
  //     duration: 200,
  //     type: LayoutAnimation.Types.spring,
  //     property: LayoutAnimation.Properties.scaleY,
  //     springDamping: 0.7,
  //   },
  // });

  return (
    <View style={styles.container}>
      <HorizontalPicker
        contentContainerStyle={styles.pickerContent}
        data={date}
        onSelect={onSelectDateHandler}
      />
      {isVisibleTime ? (
        <>
          <Text style={styles.timePickerLabel}>Выберите время</Text>
          <HorizontalPicker
            contentContainerStyle={styles.pickerContent}
            data={time}
            onSelect={onSelectTimeHandler}
          />
        </>
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 16,
    marginHorizontal: -16,
  },
  timePickerLabel: {
    ...UIStyles.font13,
    color: Color.GREY_600,
    marginBottom: 8,
    marginTop: 18,
    ...UIStyles.paddingH16,
  },
  pickerContent: {
    ...UIStyles.paddingH16,
  },
});
