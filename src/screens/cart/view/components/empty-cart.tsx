import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {Config} from '../../../../config';
import {windowHeight} from '../../../../system/helpers/window-size';
import {useBottomTabBarHeight} from '@react-navigation/bottom-tabs';
import {ThemedButton} from '../../../../components/buttons/themed-button';
import {useNavigation} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {TRootStackParamList} from '../../../../navigation/types/types';
import {Routes} from '../../../../navigation';
import {adaptiveSize} from '../../../../system/helpers/responseive-font-size';
import {Cart2Icon} from '../../../../components/icons/cart-2-icon';
import {cartConfig} from '../cart/config';

const {Color, UIStyles} = Config;

type TNavigation = StackNavigationProp<TRootStackParamList, Routes.Catalog>;

const iconSize = cartConfig.emptyCartComponent.iconSize;

export const EmptyCart = () => {
  const tab = useBottomTabBarHeight();
  const {navigate} = useNavigation<TNavigation>();

  const onPress = () => {
    navigate(Routes.Catalog, {ignoreModal: true});
  };

  return (
    <View style={[styles.container, {transform: [{translateY: -tab}]}]}>
      <Cart2Icon
        width={iconSize}
        height={iconSize}
        fill={cartConfig.emptyCartComponent.iconColor}
      />
      <Text style={styles.title}>Тут пока ничего нет</Text>
      <Text style={styles.description}>
        Самое время выбрать что-то из нашего классного меню
      </Text>
      <View style={styles.buttonContainer}>
        <ThemedButton
          labelStyle={styles.buttonLabel}
          buttonStyle={styles.buttonStyle}
          label={'Перейти в меню'}
          onPress={onPress}
          rounded={true}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: windowHeight,
    paddingHorizontal: 45,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: 224,
    height: 143,
  },
  title: {
    marginTop: 25,
    ...UIStyles.font20b,
    color: Color.DARK,
  },
  description: {
    marginTop: 12,
    textAlign: 'center',
    ...UIStyles.font18,
    color: Color.GREY_600,
  },
  buttonContainer: {
    marginTop: 12,
    width: '100%',
  },
  buttonStyle: {
    height: 28,
    width: 130,
    alignSelf: 'center',
  },
  buttonLabel: {
    fontSize: adaptiveSize(12),
  },
});
