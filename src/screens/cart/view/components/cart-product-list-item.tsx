import React, { useEffect, useMemo, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import { Config, ImageRepository } from '../../../../config';
import { Counter } from '../../../../components/counter/counter';
import { urlValidator } from '../../../../system/helpers/url-validator';
import { IProductOptions } from '../../api/types';
import { useTypedSelector } from '../../../../system';
import { getSelectedProduct } from '../../../../system/selectors/selectors';

const { Color, UIStyles, RADIUS } = Config;

export interface ICartProductListItem {
  isLoading: boolean;
  image: string;
  id: number;
  title: string;
  description: string;
  price: number;
  count: number;
  onChangeCount: (count: number, oldCount: number) => void;
  maxCount: number;
  options: IProductOptions;
}

// const ingredients = [
//   {id: 0, title: 'Соус'},
//   {id: 1, title: 'Сметана'},
//   {id: 2, title: 'Перец'},
//   {id: 3, title: 'Соль'},
// ];
//
// const additives = [
//   {id: 0, title: 'Соус',count:1},
//   {id: 1, title: 'Сметана',count:2},
//   {id: 2, title: 'Перец',count:3},
//   {id: 3, title: 'Соль',count:1},
// ];

export const CartProductListItem: React.FC<ICartProductListItem> = ({
  image,
  isLoading,
  id,
  title,
  description,
  price,
  count,
  onChangeCount,
  maxCount,
  options,
}) => {
  const [countValue, setCountValue] = useState(count);

  const { size, ingredients, additives } = options?.childs || {};
  const selectedProduct = useTypedSelector(getSelectedProduct)



  const onChangeCountHandler = (count: number) => {
    if (maxCount >= count) {
      setCountValue(count);
      onChangeCount(count, selectedProduct?.[id]?.count ?? 0);
    }
  };

  useEffect(() => {
    setCountValue(count);
  }, [count]);

  const imageMemo = useMemo(
    () =>
      image
        ? { uri: urlValidator(image) }
        : ImageRepository.ProductImagePlaceholder,
    [],
  );

  const counterContainerStyle_ = useMemo(() => {
    return {
      ...styles.counterContainerStyle,
      borderColor: isLoading ? Color.GREY_400 : Color.PRIMARY_40
    }
  }, [isLoading])
  const counterButtonsStyle_ = useMemo(() => {
    return {
      fill: isLoading ? Color.GREY_400 : Color.PRIMARY
    }
  }, [isLoading])

  return (
    <View style={styles.container}>
      <FastImage source={imageMemo} resizeMode={'cover'} style={styles.image} />
      <View style={styles.textContainer}>
        <View>
          <Text style={styles.title}>{title}</Text>
          <Text numberOfLines={3} style={styles.description}>{description}</Text>
          {size?.[0]?.title ? (
            <Text style={styles.description}>Размер: {size?.[0]?.title}</Text>
          ) : null}

          <View style={styles.deletedIngredients}>
            {ingredients?.map(el => {
              return (
                <Text key={el.id} style={styles.deletedIngredientTitle}>
                  {' '}
                  -{el?.title}
                </Text>
              );
            })}
            {additives?.map(el => {
              return (
                <Text key={el.id} style={styles.additives}>
                  {' '}
                  +{el?.title} -{el.count}шт.
                </Text>
              );
            })}
          </View>
        </View>
        <View style={styles.counterRow}>
          <Text>{price} ₽</Text>
          <Counter
            disabled={isLoading}
            buttonLeftWrapperStyle={{ alignItems: 'center' }}
            buttonRightWrapperStyle={{ alignItems: 'center' }}
            containerStyle={counterContainerStyle_}
            buttonStyle={styles.counterButtonStyle}
            buttonIconStyle={counterButtonsStyle_}
            valueStyle={{ color: Color.BLACK }}
            value={countValue}
            onChange={onChangeCountHandler}
          />
        </View>
      </View>
      <View />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: 16,
  },
  textContainer: {
    flex: 1,
    justifyContent: 'space-between',
    marginLeft: 24,
  },
  image: {
    borderRadius: RADIUS.MEDIUM,
    width: 120,
    height: 120,
  },
  title: {
    ...UIStyles.font15b,
    color: Color.DARK,
  },
  description: {
    marginTop: 8,
    ...UIStyles.font13,
    color: Color.GREY_800,
  },
  counterContainerStyle: {
    width: 110,
    height: 32,
    paddingVertical: 0,
    paddingHorizontal: 0,
    borderRadius: RADIUS.MEDIUM,
    backgroundColor: 'transparent',
    borderWidth: 1,
    borderColor: Color.PRIMARY_40,
  },
  counterButtonStyle: {
    // flex: 1,
    backgroundColor: 'transparent',
    // padding: 0,
    paddingHorizontal: 5,
    // paddingVertical: 0,
  },
  counterRow: {
    ...UIStyles.flexRow,
    marginTop: 5,
  },
  deletedIngredients: {
    marginTop: 5,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  deletedIngredientTitle: {
    color: Color.PRIMARY,
  },
  additives: {
    color: Color.SUCCESS,
  },
});
