import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
// import { CheckBoxElement } from '../../../../../components/checkbox/checkbox-element'
import { CartGiftsOptionType } from '../../../api/types'
import { CheckBox } from '@softjet/react-native.components.check-box'
export enum CardOptions {
  bonus = 'bonus',
  delivery_sale = 'delivery_sale',
  promo = 'promo',
}

export type TOnToggleOption = (
  id: number,
  state: boolean,
  type: keyof typeof CartGiftsOptionType,
) => void

interface IOptionItem {
  isChecked: boolean
  onToggle: TOnToggleOption
  id: number
  title: string
  type: keyof typeof CartGiftsOptionType
  disabled: boolean
}

export const OptionItem: React.FC<IOptionItem> = React.memo(
  ({ id, isChecked, onToggle, title, type, disabled }) => {
    const onToggle_ = () => {
      onToggle(id, !isChecked, type)
    }

    return (
      <View style={styles.container}>
        <View style={styles.leftContent}>
          <CheckBox
            checkBoxStyle={styles.checkbox}
            titleStyle={styles.title}
            title={title}
            disabled={disabled}
            onToggle={onToggle_}
            isChecked={isChecked}
          />
        </View>
      </View>
    )
  },
)

const styles = StyleSheet.create({
  container: {
    paddingVertical: 12,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  leftContent: {
    flexDirection: 'row',
  },
  title: {
    marginLeft: 12,
  },
  checkbox: {
    borderWidth: 2,
  },
})
