import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Color, RADIUS, UIStyles } from '../../../../../assets/styles';
import { useTypedSelector } from '../../../../../system';
import { getCurrentOrderType } from '../../../../../system/selectors/selectors';
import { Divider } from '../../../../../ui-kit';
import { CartGiftsOptionType, ICartGiftsOptionsItem } from '../../../api/types';
import { OptionItem, TOnToggleOption } from './option-item';

interface IOptions {
  data: ICartGiftsOptionsItem[];
  onChange: TOnToggleOption;
  selectedOptions: Set<number>;
  disabled: boolean;
}

export const Options: React.FC<IOptions> = React.memo(
  ({ data, onChange, selectedOptions, disabled }) => {
    const currentOrderType = useTypedSelector(getCurrentOrderType);
    const isHiddenBonus = useTypedSelector(
      state => state.more.appSettings?.app_bonus_hidden,
    );

    return (
      <View style={styles.container}>
        <Text style={styles.title}>Выберите подарок или другие бонусы</Text>
        <Divider style={styles.divider} />
        {data?.map(el => {
          if (currentOrderType?.isRemoted && el.type === 'delivery_sale') {
            return null;
          }

          if (isHiddenBonus && el.type === CartGiftsOptionType.bonus) {
            return null;
          }

          return (
            <OptionItem
              disabled={!selectedOptions.has(el.id) && disabled}
              type={el.type}
              key={el.id}
              id={el.id}
              isChecked={selectedOptions.has(el.id)}
              title={el.title}
              onToggle={onChange}
            />
          );
        })}
      </View>
    );
  },
);

const styles = StyleSheet.create({
  container: {
    marginTop: 16,
    padding: 16,
    borderColor: Color.GREY_100,
    borderWidth: 1,
    borderRadius: RADIUS.MEDIUM,
  },
  divider: {
    marginTop: 16,
    marginBottom: 8,
  },
  title: {
    ...UIStyles.font13b,
  },
});
