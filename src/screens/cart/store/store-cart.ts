import {
  ICartGetDataResponse,
  IGetCartAdditionalProducts,
  IGetCartGiftsResponse,
  IGiftsNewItemResponse,
  IPresentItem,
} from '../api/types';
import { IRequestLoading } from '../../../system/root-store/root-store';

export interface ISelectedItems {
  [key: string]: { count: number };
}

export interface IStoreCart extends IRequestLoading {
  cartData: ICartGetDataResponse | null;
  additionalProducts: IGetCartAdditionalProducts[];
  presents: IPresentItem[] | null;
  enableClearCart: boolean;
  selectedProducts: ISelectedItems | null;
  selectedBonus: number;
  comment: string;
  preOrderDate: string;
  //---------
  gifts: IGetCartGiftsResponse | null;
  giftsNew: IGiftsNewItemResponse[];

}

export const initialStateCart: IStoreCart = {
  cartData: null,
  presents: null,
  enableClearCart: true,
  additionalProducts: [],
  selectedProducts: null,
  isLoading: false,
  error: false,
  selectedBonus: 0,
  comment: '',
  preOrderDate: '',
  gifts: null,
  giftsNew: []
};
