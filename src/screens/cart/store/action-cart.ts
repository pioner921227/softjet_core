import {actionCreator} from '../../../system/root-store/action-creator';
import {ISelectedItems} from './store-cart';
import {ICartGetDataResponse} from '../api/types';

export class ActionCart {
  static setSelectedProducts = actionCreator<ISelectedItems | null>(
    'CART/SELECT_PRODUCT',
  );
  static clearLocalCart = actionCreator('CART/CLEAR_LOCAL_CART');
  static setSelectedBonus = actionCreator<number>('CART/SET_SELECTED_BONUS');
  static setComment = actionCreator<string>('CART/SET_COMMENT');
  static setEnableClearCart = actionCreator<boolean>(
    'CART/SET_ENABLE_CLEAR_CART',
  );
  static setCartData = actionCreator<ICartGetDataResponse>(
    'CART/SET_CART_DATA',
  );
  static setPreOrderDate = actionCreator<string>('CART/SET_PRE_ORDER_DATE');
}
