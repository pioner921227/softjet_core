import {asyncActionCreator} from '../../../system/root-store/action-creator';
import {
  IActivatePromoCodeRequest,
  ICartAddOrderRequest,
  ICartGetDataRequest,
  ICartGetDataResponse,
  IChangeProductCountInCartRequest,
  IClearRemoteCartDataRequest,
  IGetCartAdditionalProducts,
  IGetFreeTimeRequestData,
  IGetFreeTimeResponse,
  IGetPresentFromPriceDataRequest,
  IGetPresentFromPriceResponse,
  IMinRequestData,
} from '../api/types';
import {RequestCart} from '../api/request-cart';
import {
  IGetCartGiftsResponse,
  IGiftsNewItemResponse,
} from '../../cart-new/api/types';

export class AsyncActionCart {
  static getCartData = asyncActionCreator<
    ICartGetDataRequest,
    ICartGetDataResponse,
    Error
  >('CART/GET_CART_DATA', RequestCart.getCartData);

  static cartAdd = asyncActionCreator<
    ICartAddOrderRequest,
    ICartGetDataResponse,
    Error
  >('CART/ADD_ORDER_TO_CART', RequestCart.cartAdd);

  static activatePromoCode = asyncActionCreator<
    IActivatePromoCodeRequest,
    ICartGetDataResponse,
    Error
  >('CART/ACTIVATE_PROMO_CODE', RequestCart.activatePromoCode);

  static changeProductCountInCart = asyncActionCreator<
    IChangeProductCountInCartRequest,
    ICartGetDataResponse,
    Error
  >('CART/CHANGE_PRODUCT_COUNT_IN_CART', RequestCart.changeProductCountInCart);

  static clearRemoteCart = asyncActionCreator<
    IClearRemoteCartDataRequest,
    void,
    Error
  >('CART/CLEAR_REMOTE_CART', RequestCart.clearRemoteCart);

  static getPresentFromPrice = asyncActionCreator<
    IGetPresentFromPriceDataRequest,
    IGetPresentFromPriceResponse,
    Error
  >('CART/GET_PRESENT_FROM_PRICE', RequestCart.getPresentFromPrice);

  static getFreeTime = asyncActionCreator<
    IGetFreeTimeRequestData,
    IGetFreeTimeResponse
  >('CART/GET_FREE_TIME', RequestCart.getFeeTime);

  static getAdditionalProducts = asyncActionCreator<
    IMinRequestData,
    IGetCartAdditionalProducts[]
  >('CART/GET_ADDITIONAL_PRODUCTS', RequestCart.getAdditionalProducts);

  //----------------------------
  static getOptions = asyncActionCreator<
    IMinRequestData,
    IGetCartGiftsResponse
  >('CART/GET_GIFTS', RequestCart.getOptions);

  static removePromoCode = asyncActionCreator<
    ICartGetDataRequest,
    ICartGetDataResponse
  >('CART/REMOVE_PROMO_CODE', RequestCart.removePromoCode);

  static getGiftsNew = asyncActionCreator<
    IMinRequestData,
    IGiftsNewItemResponse[]
  >('CART/GET_CART_ADDITIVES_NEW', RequestCart.getGiftsNew);

  static saveRemoteCart = asyncActionCreator<IMinRequestData, void, Error>(
    'CART/SAVE_REMOTE_CART',
    RequestCart.saveRemoteCart,
  );

  static initRemoteCart = asyncActionCreator<IMinRequestData, void, Error>(
    'CART/LOAD_REMOTE_CART',
    RequestCart.initRemoteCart,
  );
}
