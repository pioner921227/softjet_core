import { ReducerBuilder, reducerWithInitialState } from 'typescript-fsa-reducers';
import { initialStateCart, ISelectedItems, IStoreCart } from './store-cart';
import { AsyncActionCart } from './async-action-cart';
import { Success } from 'typescript-fsa';
import {
  ICartGetDataResponse,
  IGetCartAdditionalProducts,
  IGetPresentFromPriceResponse,
} from '../api/types';
import { ActionCart } from './action-cart';
import { ActionsLogin } from '../../login/store/actions-login';
import { AsyncActionOrders } from '../../orders';
import { IStoreCartGifts } from '../../cart-new/store/store-cart';
import { IGetCartGiftsResponse, IGiftsNewItemResponse } from '../../cart-new/api/types';

const getCartDataHandlerStarted = (store: IStoreCart): IStoreCart => {
  return {
    ...store,
    isLoading: true,
    error: false,
  };
};

const getCartDataHandlerDone = (
  store: IStoreCart,
  { result }: Success<any, ICartGetDataResponse>,
): IStoreCart => {
  return {
    ...store,
    isLoading: false,
    error: false,
    cartData: result && typeof result === 'object' ? result : store.cartData,
  };
};

const getCartDataHandlerFailed = (store: IStoreCart): IStoreCart => {
  return {
    ...store,
    isLoading: false,
    error: true,
  };
};

const activatePromoCodeHandlerStarted = (store: IStoreCart): IStoreCart => {
  return {
    ...store,
    isLoading: true,
    error: false,
  };
};

const activatePromoCodeHandlerDone = (
  store: IStoreCart,
  { result }: Success<any, ICartGetDataResponse>,
): IStoreCart => {
  return {
    ...store,
    isLoading: false,
    error: true,
    cartData: result.error ? store.cartData : result,
  };
};

const activatePromoCodeHandlerFailed = (store: IStoreCart): IStoreCart => {
  return {
    ...store,
    isLoading: false,
    error: true,
  };
};

const getAdditionalProductsHandlerDone = (
  store: IStoreCart,
  { result }: Success<any, IGetCartAdditionalProducts[]>,
): IStoreCart => {
  return {
    ...store,
    additionalProducts: result,
  };
};

const selectItemHandler = (
  store: IStoreCart,
  items: ISelectedItems | null,
): IStoreCart => {
  if (!items && !store.selectedProducts) {
    return { ...store };
  }

  return {
    ...store,
    selectedProducts: items,
  };
};

const setSelectedBonusHandler = (
  store: IStoreCart,
  value: number,
): IStoreCart => {
  return {
    ...store,
    selectedBonus: value,
  };
};

const setCommentHandler = (store: IStoreCart, value: string): IStoreCart => {
  return {
    ...store,
    comment: value,
  };
};

const setEnableClearCartHandler = (
  store: IStoreCart,
  value: boolean,
): IStoreCart => {
  return {
    ...store,
    enableClearCart: value,
  };
};

const getPresentFromPriceHandler = (
  store: IStoreCart,
  { result }: Success<any, IGetPresentFromPriceResponse>,
): IStoreCart => {
  return {
    ...store,
    presents: result?.gift_from_cost ?? store.presents,
  };
};

const setCartDataHandler = (
  store: IStoreCart,
  cartData: ICartGetDataResponse,
): IStoreCart => {
  return {
    ...store,
    cartData,
  };
};


const getCartGiftsHandler = (
  store: IStoreCart,
  { result }: Success<any, IGetCartGiftsResponse>,
): IStoreCart => {
  return {
    ...store,
    gifts: result
  };
};


const getGiftsNewHandlerDone = (
  store: IStoreCart,
  { result }: Success<any, IGiftsNewItemResponse[]>,
): IStoreCart => {
  return {
    ...store,
    giftsNew: result,
  };
};


const setPreOrderDate = (store: IStoreCart, value: string): IStoreCart => {
  return {
    ...store,
    preOrderDate: value,
  };
};

const resetHandler = (): IStoreCart => {
  return {
    ...initialStateCart,
  };
};

export const reducerCart: ReducerBuilder<IStoreCart> = reducerWithInitialState(
  initialStateCart,
)
  .cases([ActionsLogin.logoutAccount, ActionCart.clearLocalCart], resetHandler)
  .cases(
    [
      AsyncActionCart.getCartData.async.started,
      AsyncActionCart.changeProductCountInCart.async.started,
      AsyncActionCart.cartAdd.async.started,
    ],
    getCartDataHandlerStarted,
  )
  .cases(
    [
      AsyncActionCart.getCartData.async.done,
      AsyncActionCart.changeProductCountInCart.async.done,
      AsyncActionCart.cartAdd.async.done,
      AsyncActionOrders.repeatOrder.async.done,
    ],
    getCartDataHandlerDone,
  )
  .cases(
    [
      AsyncActionCart.cartAdd.async.failed,
      AsyncActionCart.getCartData.async.failed,
      AsyncActionCart.changeProductCountInCart.async.failed,
    ],
    getCartDataHandlerFailed
  )
  .case(
    AsyncActionCart.activatePromoCode.async.started,
    activatePromoCodeHandlerStarted,
  )
  .case(
    AsyncActionCart.activatePromoCode.async.done,
    activatePromoCodeHandlerDone,
  )
  .case(
    AsyncActionCart.activatePromoCode.async.failed,
    activatePromoCodeHandlerFailed,
  )
  .case(
    AsyncActionCart.getPresentFromPrice.async.done,
    getPresentFromPriceHandler,
  )
  .case(
    AsyncActionCart.getAdditionalProducts.async.done,
    getAdditionalProductsHandlerDone,
  )
  .case(AsyncActionCart.getGiftsNew.async.done, getGiftsNewHandlerDone)
  .case(AsyncActionCart.getOptions.async.done, getCartGiftsHandler)
  .case(ActionCart.setSelectedProducts, selectItemHandler)
  .case(ActionCart.setSelectedBonus, setSelectedBonusHandler)
  .case(ActionCart.setComment, setCommentHandler)
  .case(ActionCart.setEnableClearCart, setEnableClearCartHandler)
  .case(ActionCart.setCartData, setCartDataHandler)
  .case(ActionCart.setPreOrderDate, setPreOrderDate);
