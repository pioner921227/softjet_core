import React, { useCallback } from "react";
import { StyleSheet, Text, TextStyle, View } from "react-native";
import { Color, UIStyles } from "../../../../../assets/styles";
import { IOnChangeIngredientsItemData } from "../ingredients/ingredients-item";
import { ISelectedAdditive } from "../../product-detail";

interface IDescription {
  description: string;
  selectedIngredients: IOnChangeIngredientsItemData[];
  selectedAdditives: { [key: string]: ISelectedAdditive };
}

export const Description: React.FC<IDescription> = React.memo(
  ({ description, selectedIngredients, selectedAdditives }) => {
    const getStyle = useCallback((count: number): TextStyle => {
      return {
        color: count ? Color.SUCCESS : Color.GREY_600,
        textDecorationLine: count ? "none" : "line-through",
      };
    }, []);

    return (
      <View>
        <Text style={styles.desc}>{description}</Text>
        <View style={styles.ingredients}>
          {selectedIngredients.map((el) => {
            return (
              <Text key={el.id} style={[styles.desc, getStyle(el.count)]}>
                {el.title}{" "}
              </Text>
            );
          })}
          {Object.values(selectedAdditives).map((el) => {
            return (
              <Text key={el.id} style={[styles.desc, getStyle(el.count)]}>
                + {el.title}{" "}
              </Text>
            );
          })}
        </View>
      </View>
    );
  }
);

const styles = StyleSheet.create({
  desc: {
    ...UIStyles.font14,
    color: Color.GREY_500,
    marginBottom: 5,
  },
  ingredients: {
    flexDirection: "row",
    flexWrap: "wrap",
  },
});
