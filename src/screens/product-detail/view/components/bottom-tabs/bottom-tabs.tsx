import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import { Color, UIStyles } from '../../../../../assets/styles';
import React from 'react';
import { ThemedButton } from '../../../../../ui-kit';
import { windowWidth } from '../../../../../system/helpers/window-size';
import { adaptiveSize } from '../../../../../system/helpers/responseive-font-size';

const cartIcon = require('../../../../../assets/images/cart.png');

interface IBottomTabs {
  count: number;
  onPressAdd: () => void;
  onPressGoToCart: () => void;
  totalCost: number;
  isLoading: boolean;
}

export const BottomTabs: React.FC<IBottomTabs> = ({
  count,
  onPressGoToCart,
  onPressAdd,
  totalCost = 0,
  isLoading,
}) => {
  return (
    <View style={styles.container}>
      <View style={styles.flexWrapper}>
        <View style={styles.addButtonContainer}>
          <ThemedButton
            isLoading={isLoading}
            disabled={isLoading}
            label={`В корзину за ${totalCost} ₽`}
            onPress={onPressAdd}
            wrapperStyle={styles.addButtonWrapper}
            buttonStyle={styles.addButton}
            rounded={true}
          />
        </View>

        <TouchableOpacity onPress={onPressGoToCart} style={styles.toCartButton}>
          {count ? (
            <View style={styles.badge}>
              <Text style={styles.badgeValue}>{count}</Text>
            </View>
          ) : null}

          <FastImage source={cartIcon} style={styles.cartImage} />
          <Text style={styles.toCartLabel}>Заказ</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    position: 'absolute',
    justifyContent: 'space-between',
    height: adaptiveSize(72),
    bottom: 0,
    width: windowWidth,
    backgroundColor: Color.WHITE,
    borderTopColor: Color.BACKGROUND_COLOR,
    borderTopWidth: 1,
  },
  flexWrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  addButtonContainer: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  addButtonWrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  addButton: {
    marginTop: 0,
    width: adaptiveSize(220),
    height: adaptiveSize(40),
    alignSelf: 'center',
  },
  toCartButton: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: Color.WHITE,
    borderLeftColor: Color.BACKGROUND_COLOR,
    borderWidth: 1,
  },
  cartImage: {
    width: 20,
    height: 20,
  },
  toCartLabel: {
    ...UIStyles.font12,
    color: Color.GREY_600,
  },
  badge: {
    position: 'absolute',
    top: '15%',
    right: '35%',
    backgroundColor: Color.PRIMARY,
    width: adaptiveSize(14),
    height: adaptiveSize(14),
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  badgeValue: {
    ...UIStyles.font9b,
    color: Color.WHITE,
  },
});
