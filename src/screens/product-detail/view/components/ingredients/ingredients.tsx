import React, { useEffect, useState } from 'react';
import {
  Insets,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withTiming
} from 'react-native-reanimated';
import { Color, UIStyles } from '../../../../../assets/styles';
import { useLayout } from '../../../../../system/hooks/use-layout';
import { ArrowBottomWithoutLine } from '../../../../../ui-kit';
import { IIngredient } from '../../../../catalog';
import {
  IngredientsItem,
  IOnChangeIngredientsItem,
  IOnChangeIngredientsItemData
} from './ingredients-item';

interface IIngredients {
  ingredients: IIngredient[];
  onChange: IOnChangeIngredientsItem;
  selectedIngredients: IOnChangeIngredientsItemData[];
}

const hitSlop: Insets = { top: 10 };

export const Ingredients: React.FC<IIngredients> = ({
  ingredients,
  onChange,
  selectedIngredients,
}) => {
  const [isVisibleItems, setIsVisibleItems] = useState(false);

  const animateValue = useSharedValue(0);

  const animatedStyle = useAnimatedStyle(() => {
    return {
      height: animateValue.value,
    };
  });

  // const animatedIconStyle = useAnimatedStyle(() => {
  //   return {
  //     transform: [
  //       {
  //         rotateX: interpolate(
  //           animateValue.value,
  //           [0, ingredientsHeight],
  //           [0, 3.5],
  //         ).toString(),
  //       },
  //     ],
  //   };
  // });

  const toggleVisible = () => {
    setIsVisibleItems(state => !state);
  };

  const { onLayout, values } = useLayout();

  useEffect(() => {
    animateValue.value = withTiming(isVisibleItems ? values.height : 0, {
      duration: 300,
    });
  }, [isVisibleItems]);

  return (
    <View>
      <TouchableOpacity
        hitSlop={hitSlop}
        activeOpacity={0.8}
        onPress={toggleVisible}
        style={styles.labelContainer}>
        <Text style={styles.label}>Убрать ингредиенты</Text>
        {/*<Animated.View style={animatedIconStyle}>*/}
        <ArrowBottomWithoutLine />
        {/*</Animated.View>*/}
      </TouchableOpacity>
      <Animated.View style={animatedStyle}>
        <ScrollView>
          <View onLayout={onLayout}>
            {ingredients?.map(el => {
              const isChecked = selectedIngredients.find(
                item => item.id === el.id,
              );
              return (
                <IngredientsItem
                  isChecked={isChecked?.count === 1}
                  item={el}
                  key={el.id}
                  onChange={onChange}
                />
              );
            })}
          </View>
        </ScrollView>
      </Animated.View>
    </View>
  );
};

const styles = StyleSheet.create({
  label: {
    ...UIStyles.font15,
    color: Color.PRIMARY,
    marginRight: 10,
  },
  labelContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
