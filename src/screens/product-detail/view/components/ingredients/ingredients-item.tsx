import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Divider } from 'react-native-paper';
import { CheckBoxElement } from '../../../../../components/checkbox/checkbox-element';
import { IIngredient } from '../../../../catalog';


export interface IOnChangeIngredientsItemData {
  id: number;
  count: number;
  price: number;
  title: string;
}

export interface IOnChangeIngredientsItem {
  ({ id, count }: IOnChangeIngredientsItemData): void;
}

interface IIngredientsItem {
  item: IIngredient;
  onChange: IOnChangeIngredientsItem;
  isChecked: boolean;
}

export const IngredientsItem: React.FC<IIngredientsItem> = React.memo(
  ({ onChange, item, isChecked }) => {
    const onChangeHandler = (state: boolean) => {
      onChange({
        id: item.id,
        count: state ? 1 : 0,
        price: item.price,
        title: item.title,
      });
    };

    return (
      <>
        <View style={styles.container}>
          <Text>{item.title}</Text>
          <CheckBoxElement onChange={onChangeHandler} isChecked={isChecked} />
        </View>
        <Divider />
      </>
    );
  },
);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: 36,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
