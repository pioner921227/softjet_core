import React, {useMemo} from 'react';
import {StyleSheet, Text, TextStyle} from 'react-native';
import {Color, UIStyles} from '../../../../../assets/styles';

interface ILabel {
  text: string;
  style?: TextStyle;
}

export const Label: React.FC<ILabel> = ({text, style}) => {
  const style_ = useMemo(() => {
    return [styles.label, style];
  }, [style]);

  return <Text style={style_}>{text}</Text>;
};

const styles = StyleSheet.create({
  label: {
    ...UIStyles.font15,
    color: Color.GREY_400,
  },
});
