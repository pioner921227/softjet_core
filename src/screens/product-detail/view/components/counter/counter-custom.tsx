import { Color, UIStyles } from '../../../../../assets/styles';
import React from 'react';
import { Insets, StyleSheet } from 'react-native';
import { Counter, ICounter } from '../../../../../components/counter/counter';

const hitSlop: Insets = { top: 15, bottom: 15, left: 15, right: 15 };

export const CounterCustom: React.FC<ICounter> = ({
  value,
  onChange,
  ...props
}) => {
  return (
    <Counter
      hitSlopLeftButton={hitSlop}
      hitSlopRightButton={hitSlop}
      valueStyle={styles.value}
      buttonStyle={styles.button}
      buttonIconStyle={iconStyle}
      containerStyle={styles.container}
      value={value}
      onChange={onChange}
      {...props}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 'auto',
    height: 30,
    paddingHorizontal: 0,
    backgroundColor: 'transparent',
  },
  button: {
    width: 20,
    height: 20,
    borderRadius: 4,
    backgroundColor: 'transparent',
    paddingHorizontal: 0,
    borderWidth: 2,
    borderColor: Color.PRIMARY,
  },
  value: {
    ...UIStyles.font15b,
    color: Color.BLACK,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    width: 10,
  },
});

const iconStyle = {
  ...styles.icon,
  fill: Color.PRIMARY,
};
