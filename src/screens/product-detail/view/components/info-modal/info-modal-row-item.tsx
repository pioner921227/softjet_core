import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Color, UIStyles} from '../../../../../assets/styles';

interface IInfoModalRowItem {
  title: string;
  value: string;
}

export const InfoModalRowItem: React.FC<IInfoModalRowItem> = ({
  title,
  value,
}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      <Text style={styles.value}>{value}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 8,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
  },
  title: {
    ...UIStyles.font15,
    color: Color.WHITE,
    fontWeight: 'normal',
  },
  value: {
    ...UIStyles.font15b,
    color: Color.WHITE,
    fontWeight: 'bold',
  },
});
