import React from 'react'
import { StyleSheet, Text } from 'react-native'
import Animated, { ZoomIn, ZoomOut } from 'react-native-reanimated'
import { Color, RADIUS, UIStyles } from '../../../../../assets/styles'
import { InfoModalRowItem } from './info-modal-row-item'
import { StartLayoutAnimation } from '../../product-detail'
import { IEnergyItems } from '../../../../catalog'
import { adaptiveSize } from '../../../../../system/helpers/responseive-font-size'
import { windowHeight } from '../../../../../system/helpers/window-size'

interface IInfoModal {
  info: IEnergyItems
  setVisible: (state: boolean) => void
}

const modalHeight = adaptiveSize(200)

export const InfoModal: React.FC<IInfoModal> = React.memo(
  ({ info, setVisible }) => {
    const onSetVisible = () => {
      setVisible(false)
    }

    return (
      <Animated.View
        entering={ZoomIn.springify().stiffness(150).damping(15)}
        exiting={ZoomOut.duration(200)}
        onTouchStart={onSetVisible}
        style={styles.container}
      >
        <Text style={styles.title}>Пищевая ценность на 100г</Text>
        <InfoModalRowItem
          title={'Энерг. ценность'}
          value={`${info.energy_value} ккал`}
        />
        <InfoModalRowItem title={'Белки'} value={`${info.energy_protein} г`} />
        <InfoModalRowItem title={'Жиры'} value={`${info.energy_oils} г`} />
        <InfoModalRowItem
          title={'Углеводы'}
          value={`${info.energy_carbohydrates} г`}
        />
        <InfoModalRowItem title={'Вес'} value={`${info.energy_size || 0} г`} />
        {info.energy_allergens ? (
          <InfoModalRowItem
            title={'Аллергены'}
            value={`${info.energy_allergens}`}
          />
        ) : null}
      </Animated.View>
    )
  },
)

const styles = StyleSheet.create({
  container: {
    zIndex: 10,
    paddingHorizontal: 15,
    paddingVertical: 16,
    position: 'absolute',
    width: adaptiveSize(270),
    height: modalHeight,
    backgroundColor: Color.RGBA_600,
    borderRadius: RADIUS.LARGE,
    right: 16,
    ...UIStyles.shadowNormal,
    top: windowHeight / 2 - modalHeight / 2,
    // transform: [{translateY: -100}],
  },
  title: {
    ...UIStyles.font15b,
    color: Color.WHITE,
    textAlign: 'center',
  },
  evaluateContainer: {
    marginTop: 13,
    ...UIStyles.flexRow,
  },
  evaluateTitle: {
    ...UIStyles.font15,
    color: Color.WHITE,
    fontWeight: '600',
  },
  evaluateValueContainer: {
    backgroundColor: Color.SUCCESS,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: RADIUS.SMALL,
  },
  evaluateValue: {},
})
