import React, { useCallback } from 'react';
import { FlatList, StyleSheet, View } from 'react-native';
import { Label } from '../label/label';
import { AdditivesItem } from './additives-item';
import { UIStyles } from '../../../../../assets/styles';
import { ISelectedAdditive } from '../../product-detail';
import { IProductAdditive } from '../../../../catalog';

// const data = [
//   {id: 0, label: '1'},
//   {id: 1, label: '2'},
//   {id: 2, label: '3'},
//   {id: 3, label: '4'},
//   {id: 4, label: '5'},
// ];

interface IAdditives {
  data: IProductAdditive[];
  onChangeCount: (item: ISelectedAdditive) => void;
  selectedAdditives: { [key: string]: ISelectedAdditive };
}

export const Additives: React.FC<IAdditives> = React.memo(
  ({ data, onChangeCount, selectedAdditives }) => {
    const keyExtractor = useCallback(item => item?.id.toString(), []);

    const renderItem = useCallback(
      (props: { index: number; item: IProductAdditive }) => {
        // const count = selectedAdditives.find(el => el.id === props.item.id);

        return (
          <AdditivesItem
            // count={count?.count}
            count={selectedAdditives[props.item.id]?.count || 0}
            onChangeCount={onChangeCount}
            {...props}
          />
        );
      },
      [onChangeCount, selectedAdditives],
    );

    if (!Array.isArray(data) || data.length === 0) {
      return null;
    }

    return (
      <View style={styles.container}>
        <Label style={styles.label} text="Добавки" />
        <FlatList
          keyExtractor={keyExtractor}
          contentContainerStyle={styles.contentContainer}
          horizontal={true}
          data={data}
          renderItem={renderItem}
        />
      </View>
    );
  },
);

const styles = StyleSheet.create({
  container: {
    marginTop: 12,
  },
  label: {
    marginHorizontal: 16,
    marginBottom: 12,
  },
  contentContainer: {
    ...UIStyles.paddingH16,
  },
});
