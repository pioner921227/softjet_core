import React from "react";
import { StyleSheet, Text, View } from "react-native";
import FastImage from "react-native-fast-image";
import { CounterCustom } from "../counter/counter-custom";
import { ISelectedAdditive } from "../../product-detail";
import { IProductAdditive } from "../../../../catalog";
import { useImage } from "../../../../../system/hooks/use-image";
import { adaptiveSize } from "../../../../../system/helpers/responseive-font-size";
import { Config } from "../../../../../config";
const { Color, UIStyles, RADIUS } = Config;

interface IAdditivesItem {
  item: IProductAdditive;
  index: number;
  onChangeCount: (item: ISelectedAdditive) => void;
  count: number;
}

export const AdditivesItem: React.FC<IAdditivesItem> = React.memo(
  ({ item, index, onChangeCount, count }) => {
    const onChangeCountHandler = (count: number) => {
      onChangeCount({
        count,
        id: item.id,
        price: item.price,
        title: item.title,
      });
    };

    const image = useImage(item.img_url);

    return (
      <View style={styles.container}>
        <FastImage resizeMode={"cover"} style={styles.image} source={image} />
        <Text numberOfLines={2} adjustsFontSizeToFit={true}>
          {item.title}
        </Text>
        <Text style={styles.price}>{item.price} ₽</Text>
        <CounterCustom
          isVisibleMinus={!!count}
          isVisiblePlus={true}
          value={count}
          onChange={onChangeCountHandler}
        />
      </View>
    );
  }
);

const styles = StyleSheet.create({
  container: {
    height: 200,
    width: adaptiveSize(120),
    borderColor: Color.PRIMARY_40,
    borderRadius: RADIUS.MEDIUM,
    borderWidth: 1,
    marginRight: 8,
    alignItems: "center",
    padding: 8,
  },
  image: {
    borderRadius: RADIUS.SMALL,
    width: adaptiveSize(80),
    height: adaptiveSize(80),
    marginBottom: adaptiveSize(15),
  },
  bottomContainer: {
    width: "100%",
    marginTop: "auto",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  price: {
    ...UIStyles.font15,
    marginTop: "auto",
  },
  plusButton: {
    width: adaptiveSize(20),
    height: adaptiveSize(20),
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 2,
    borderColor: Color.PRIMARY,
    borderRadius: RADIUS.SMALL,
  },
});
