import { StackScreenProps } from '@react-navigation/stack'
import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import {
  Insets,
  InteractionManager,
  LayoutAnimation,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native'
import FastImage from 'react-native-fast-image'
import { TapGestureHandler } from 'react-native-gesture-handler'
import { Color, UIStyles } from '../../../assets/styles'
import { SegmentControlWithLabel } from '../../../components/segment-controls/segment-control-with-label'
import { ImageRepository } from '../../../config'
import { Routes } from '../../../navigation'
import { TRootStackParamList } from '../../../navigation/types/types'
import { useTypedSelector } from '../../../system'
import { urlValidator } from '../../../system/helpers/url-validator'
import { windowHeight, windowWidth } from '../../../system/helpers/window-size'
import { useAndroidBackHandler } from '../../../system/hooks/use-android-back-handler'
import { useTypedDispatch } from '../../../system/hooks/use-typed-dispatch'
import {
  getCartData,
  getCurrentOrderType,
  getCurrentOrganisation,
  getPolygonId,
} from '../../../system/selectors/selectors'
import { ScreenHeaderTitleWithBackButton } from '../../../ui-kit'
import { AsyncActionCart } from '../../cart'
import { Additives } from './components/additives/additives'
import { BottomTabs } from './components/bottom-tabs/bottom-tabs'
import { Description } from './components/description/description'
import { InfoModal } from './components/info-modal/info-modal'
import { Ingredients } from './components/ingredients/ingredients'
import { IOnChangeIngredientsItemData } from './components/ingredients/ingredients-item'

type TProps = StackScreenProps<TRootStackParamList, Routes.ProductDetail>

export const StartLayoutAnimation = () => {
  LayoutAnimation.configureNext({
    duration: 500,
    create: {
      type: LayoutAnimation.Types.spring,
      property: LayoutAnimation.Properties.scaleXY,
      springDamping: 0.8,
    },
    delete: {
      duration: 100,
      type: LayoutAnimation.Types.easeInEaseOut,
      property: LayoutAnimation.Properties.scaleXY,
      springDamping: 1,
    },
  })
}

export interface ISelectedAdditive {
  id: number
  count: number
  price: number
  title: string
}

const hitSlopInfoButton: Insets = { left: 10, right: 10, top: 10, bottom: 10 }

export const ProductDetail: React.FC<TProps> = React.memo(
  ({ route, navigation }) => {
    const { item, itemId } = route.params || {}
    const dispatch = useTypedDispatch()

    const infoButtonRef = useRef()

    const currentOrganisationId = useTypedSelector(getCurrentOrganisation)?.id
    const currentOrderTypeId = useTypedSelector(getCurrentOrderType)?.id
    const polygonId = useTypedSelector(getPolygonId)
    const cartData = useTypedSelector(getCartData)
    const [isLoading, setIsLoading] = useState(false)

    const [isVisibleInfo, setIsVisibleInfo] = useState(false)
    const [totalCost, setTotalCost] = useState(0)

    const [selectedAdditives, setSelectedAdditives] = useState<{
      [key: string]: ISelectedAdditive
    }>({})
    const [selectedIngredients, setSelectedIngredients] = useState<
      IOnChangeIngredientsItemData[]
    >(
      item?.ingredients?.map((el) => ({
        id: el.id,
        price: el.price,
        count: 1,
        title: el.title,
      })),
    )

    const [selectedSizeIndex, setSelectedSizeIndex] = useState(
      item?.sizes?.default_index,
    )
    const [selectedPitaIndex, setSelectedPitaIndex] = useState(
      item?.types?.default_index,
    )

    const sizes = useMemo(() => {
      return item?.sizes?.data?.map((el) => el.title)
    }, [item?.sizes])

    const pitas = useMemo(() => {
      return item?.types?.data?.map((el) => el.title)
    }, [item?.types])

    const image = useMemo(
      () =>
        item?.img_big
          ? { uri: urlValidator(item?.img_big) }
          : ImageRepository.ProductImagePlaceholder,
      [item?.img_big],
    )

    const onChangeCountAdditives = useCallback((item: ISelectedAdditive) => {
      InteractionManager.runAfterInteractions(() => {
        setSelectedAdditives((state) => {
          const newState = { ...state }
          if (item.count) {
            newState[item.id] = item
          } else {
            delete newState[item.id]
          }
          return newState
        })
      })
    }, [])

    const onChangeIngredients = useCallback(
      (item: IOnChangeIngredientsItemData) => {
        setSelectedIngredients((state) => {
          return state.reduce<IOnChangeIngredientsItemData[]>(
            (acc, el, index) => {
              if (el.id === item.id) {
                acc[index] = item
              } else {
                acc[index] = el
              }
              return acc
            },
            [],
          )
        })
      },
      [],
    )

    const toggleInfo = useCallback(() => {
      setIsVisibleInfo(true)
    }, [])

    const closeInfo = useCallback(() => {
      if (isVisibleInfo) {
        isVisibleInfo && setIsVisibleInfo(false)
      }
    }, [isVisibleInfo])

    const onPressAddToCart = useCallback(async () => {
      if (
        currentOrganisationId !== undefined &&
        currentOrderTypeId !== undefined
      ) {
        setIsLoading(true)

        await dispatch(
          AsyncActionCart.cartAdd({
            id_org: currentOrganisationId,
            order_type: currentOrderTypeId,
            polygon_id: polygonId,
            id: item.id,
            count: 1,
            additives: Object.values(selectedAdditives),
            size:
              selectedSizeIndex !== undefined
                ? [{ id: item?.sizes?.data?.[selectedSizeIndex]?.id, count: 1 }]
                : [],
            type:
              selectedPitaIndex !== undefined
                ? [{ id: item?.types?.data?.[selectedPitaIndex]?.id, count: 1 }]
                : [],
            ingredients: selectedIngredients.reduce<
              Array<{ id: number; count: number }>
            >((acc, el) => {
              !el.count && acc.push(el)
              return acc
            }, []),
          }),
        )
        setIsLoading(false)
      }
    }, [
      currentOrderTypeId,
      currentOrganisationId,
      dispatch,
      item.id,
      item.sizes,
      item.types,
      polygonId,
      selectedAdditives,
      selectedIngredients,
      selectedPitaIndex,
      selectedSizeIndex,
    ])

    const onPressGoToCart = useCallback(() => {
      navigation.navigate(Routes.Cart)
    }, [navigation])

    useEffect(() => {
      if (item === undefined) {
        return
      }
      const additivePrice = Object.values(selectedAdditives).reduce(
        (acc, el) => acc + el.price * el.count,
        0,
      )
      const selectedSize = item?.sizes?.data?.[selectedSizeIndex]
      const selectedPita = item?.types?.data?.[selectedPitaIndex]

      const total =
        item.price +
        additivePrice -
        (selectedSize?.price || 0) +
        (selectedPita?.price || 0)
      setTotalCost(total)
    }, [
      item.price,
      selectedAdditives,
      selectedSizeIndex,
      selectedPitaIndex,
      item?.sizes,
      item,
    ])

    useAndroidBackHandler()

    return (
      <SafeAreaView style={styles.container}>
        <TapGestureHandler
          numberOfTaps={1}
          onActivated={closeInfo}
          waitFor={infoButtonRef}
        >
          <ScrollView contentContainerStyle={styles.contentContainer}>
            <ScreenHeaderTitleWithBackButton
              style={styles.headerBackButton}
              title=""
            />
            {isVisibleInfo ? (
              <InfoModal setVisible={setIsVisibleInfo} info={item.energy} />
            ) : null}

            <FastImage source={image} style={styles.image} />
            <View style={styles.content}>
              <View style={styles.titleRow}>
                <Text style={styles.title}>{item.title}</Text>
                <TapGestureHandler
                  hitSlop={hitSlopInfoButton}
                  onActivated={toggleInfo}
                  ref={infoButtonRef}
                >
                  <View style={styles.infoCircle}>
                    <Text style={styles.infoIcon}>i</Text>
                    {/* <InfoInCircleIcon /> */}
                  </View>
                </TapGestureHandler>
              </View>
              <Description
                selectedAdditives={selectedAdditives}
                selectedIngredients={selectedIngredients}
                description={item?.desc}
              />
              {item.ingredients.length ? (
                <Ingredients
                  selectedIngredients={selectedIngredients}
                  onChange={onChangeIngredients}
                  ingredients={item.ingredients}
                />
              ) : null}

              <SegmentControlWithLabel
                label={'Размер'}
                values={sizes}
                selectedIndex={+selectedSizeIndex}
                onChange={setSelectedSizeIndex}
              />
              <SegmentControlWithLabel
                label={'Лаваш'}
                values={pitas}
                selectedIndex={selectedPitaIndex}
                onChange={setSelectedPitaIndex}
              />
            </View>
            <Additives
              selectedAdditives={selectedAdditives}
              onChangeCount={onChangeCountAdditives}
              data={item?.additives}
            />
          </ScrollView>
        </TapGestureHandler>
        <BottomTabs
          isLoading={isLoading}
          totalCost={totalCost}
          onPressAdd={onPressAddToCart}
          onPressGoToCart={onPressGoToCart}
          count={cartData?.products?.length || 0}
        />
      </SafeAreaView>
    )
  },
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerBackButton: {
    width: '100%',
    zIndex: 200,
  },
  contentContainer: {
    paddingBottom: 100,
  },
  image: {
    width: windowWidth,
    height: windowWidth,
  },
  titleRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 5,
  },
  content: {
    zIndex: 2,
    marginHorizontal: 16,
  },
  title: {
    ...UIStyles.font24b,
    color: Color.BLACK,
  },
  backDrop: {
    width: windowWidth,
    height: windowHeight,
    position: 'absolute',
  },
  infoCircle: {
    borderRadius: 50,
    width: 28,
    height: 28,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Color.BACKGROUND_COLOR,
  },
  infoIcon: {
    fontSize: 16,
    fontWeight: 'bold',
  },
})
