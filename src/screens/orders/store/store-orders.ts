import {IOrderItem} from '../api/types';
import {IRequestLoading} from '../../../system/root-store/root-store';

export interface IStoreOrdersHistory extends IRequestLoading {
  orders: IOrderItem[];
}

export const initialStateOrdersHistory: IStoreOrdersHistory = {
  orders: [],
  isLoading: false,
  error: false,
};
