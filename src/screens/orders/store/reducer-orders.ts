import {ReducerBuilder, reducerWithInitialState} from 'typescript-fsa-reducers';
import {initialStateOrdersHistory, IStoreOrdersHistory} from './store-orders';
import {AsyncActionOrders} from './async-action-orders';
import {Success} from 'typescript-fsa';
import {IGetOrdersResponse} from '../api/types';
import {ActionsLogin} from '../../login/store/actions-login';
import {decodeWithErrorHandling} from '../../../system/helpers/decode-with-error-handling';
import {orderHistory} from './decode-structures';
import {ActionsOrders} from './actions-orders';

const getOrderHistoryHandlerStarted = (
  store: IStoreOrdersHistory,
): IStoreOrdersHistory => {
  return {
    ...store,
    isLoading: true,
    error: false,
  };
};

const getOrderHistoryHandlerDone = (
  store: IStoreOrdersHistory,
  {result}: Success<any, IGetOrdersResponse>,
): IStoreOrdersHistory => {
  const decodeResult = decodeWithErrorHandling(
    result,
    orderHistory,
    '/api/user/getOrdersHistory',
  );

  return {
    ...store,
    orders: decodeResult.orders,
    isLoading: false,
    error: false,
  };
};

const getOrderHistoryHandlerFailed = (
  store: IStoreOrdersHistory,
): IStoreOrdersHistory => {
  return {
    ...store,
    isLoading: false,
    error: true,
  };
};

const setOrderHistoryItemRatingHandler = (
    store: IStoreOrdersHistory,
    { id, rating }: { id: number; rating: number }
): IStoreOrdersHistory => {
  const itemIndex = store.orders.findIndex((el)=>el.order_id === id)
  const orders = [...store.orders]

  if(itemIndex !== -1){
    orders[itemIndex].rating = rating
  }

  return {
    ...store,
    orders
  };
};

const resetHandler = () => {
  return {
    ...initialStateOrdersHistory,
  };
};

export const reducerOrders: ReducerBuilder<IStoreOrdersHistory> = reducerWithInitialState(
  initialStateOrdersHistory,
)
  .case(ActionsLogin.logoutAccount, resetHandler)
  .case(
    AsyncActionOrders.getOrders.async.started,
    getOrderHistoryHandlerStarted,
  )
  .case(AsyncActionOrders.getOrders.async.done, getOrderHistoryHandlerDone)
  .case(AsyncActionOrders.getOrders.async.failed, getOrderHistoryHandlerFailed)
  .case(ActionsOrders.setOrderItemRating, setOrderHistoryItemRatingHandler);
