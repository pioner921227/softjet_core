import {actionCreator} from '../../../system';

export class ActionsOrders {
  static setOrderItemRating = actionCreator<{id: number; rating: number}>(
    'ORDERS/SET_ITEM_RATING',
  );
}
