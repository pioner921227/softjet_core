import {asyncActionCreator} from '../../../system/root-store/action-creator';
import {RequestOrders} from '../api/request-orders';
import {
  IEvaluationOrderItemsRequestData,
  IEvaluationOrderRequestData,
  IGetOrderDataRequest,
  IGetOrderDataResponse,
  IGetOrdersResponse,
  IRepeatOrderDataRequest,
} from '../api/types';
import {ICartGetDataResponse} from '../../cart/api/types';

export class AsyncActionOrders {
  static getOrders = asyncActionCreator<void, IGetOrdersResponse, Error>(
    'ORDERS/GET_ORDERS',
    RequestOrders.getOrders,
  );
  static getOrderData = asyncActionCreator<
    IGetOrderDataRequest,
    IGetOrderDataResponse,
    Error
  >('ORDERS/GET_ORDER_DATA', RequestOrders.getOrderData);

  static repeatOrder = asyncActionCreator<
    IRepeatOrderDataRequest,
    ICartGetDataResponse,
    Error
  >('ORDERS/REPEAT_ORDER', RequestOrders.repeatOrder);

  static EvaluationOrder = asyncActionCreator<
    IEvaluationOrderRequestData,
    void,
    Error
  >('ORDERS/EVALUATION_ORDER', RequestOrders.evaluateOrder);

  static EvaluationOrderItems = asyncActionCreator<
    IEvaluationOrderItemsRequestData,
    void,
    Error
  >('ORDERS/EVALUATION_ORDER_ITEMS', RequestOrders.evaluateOrderItems);
}
