import {IGetOrdersResponse} from '../api/types';
import {T} from 'decodable-js';

export const orderHistory: IGetOrdersResponse = {
  orders: [
    {
      rating: T.number_$,
      total_cost: T.number_$,
      order_id: T.number,
      status_id: T.number,
      status_name: T.string,
      createdon: T.string,
      street: T.string,
      city: T.string,
      house_num: T.string,
      flat_num: T.string,
      container_color: T.string,
      status_wrapper_color: T.string,
      status_title_color: T.string,
    },
  ],
};
