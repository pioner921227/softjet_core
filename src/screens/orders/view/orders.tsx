import React, { useEffect, useRef, useState } from "react";
import {
  ActivityIndicator,
  BackHandler,
  InteractionManager,
  SafeAreaView,
  StyleSheet,
  View,
} from "react-native";
import { OrdersList } from "./components/orders-list";
import { AsyncActionOrders } from "../store/async-action-orders";
import { useTypedSelector } from "../../../system/hooks/use-typed-selector";
import { BottomSheetMenuOrderDetail } from "./components/bottom-sheet-menu-order-detail";
import { useBottomSheetMenu } from "../../../components/bottom-sheet-menu/bottom-sheet-modal-provider";
import {
  BSMElevationOrderContent,
  IOnPressEvaluationOrderData,
} from "../../../components/add-comment-bottom-sheet-content/bsm-elevation-order-content";
import { useTypedDispatch } from "../../../system/hooks/use-typed-dispatch";
import { BSMEvaluationOrderItemsContent } from "../../../components/bsm-evaluate-items-in-order-content/bsm-evaluation-order-items-content";
import {
  IEvaluationOrderItemsRequestData,
  IGetOrderDataResponse,
} from "../api/types";
import { Config } from "../../../config";
import { windowHeight } from "../../../system/helpers/window-size";
import { BottomSheetModalWithRef } from "../../../components";
import { BottomSheetModalMethods } from "@gorhom/bottom-sheet/lib/typescript/types";
import { ordersConfig } from "./config";
import { adaptiveSize } from "../../../system/helpers/responseive-font-size";
import { useNavigation } from "@react-navigation/native";
import { ActionsOrders } from "../store/actions-orders";

const { Color } = Config;

const snapPointsEvaluationOrderMenu = [adaptiveSize(350), adaptiveSize(550)];
const snapPointsEvaluateOrderItemsMenu = ["95%"];
const snapPointsOrderDetailMenu = ["90%"];

export const Orders = React.memo(() => {
  const dispatch = useTypedDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const orders = useTypedSelector((state) => state.ordersHistory?.orders);
  const { goBack } = useNavigation();

  const evaluationOrderItemsMenuRef = useRef<BottomSheetModalMethods | null>(
    null
  );
  const evaluationOrderMenuRef = useRef<BottomSheetModalMethods | null>(null);

  const [selectedOrderDetail, setSelectedOrderDetail] =
    useState<IGetOrderDataResponse>({} as IGetOrderDataResponse);

  const { show, close, isVisible } = useBottomSheetMenu();

  const onPressEvaluateOrder = ({
    rating,
    comment,
    variants,
  }: IOnPressEvaluationOrderData) => {
    close();

    dispatch(
      ActionsOrders.setOrderItemRating({
        id: selectedOrderDetail.order_id,
        rating: rating,
      })
    );

    dispatch(
      AsyncActionOrders.EvaluationOrder({
        order_id: selectedOrderDetail.order_id,
        general_rating: rating,
        answer_options: variants,
        comment: comment,
      })
    );
    if (ordersConfig.EVALUATE_ORDER_MENU.ENABLE_EVALUATE_ORDER_ITEM_MENU) {
      evaluationOrderMenuRef.current?.dismiss();
      openEvaluateItemsInOrderMenu();
    }
  };

  const openEvaluateOrderMenu = () => {
    close();
    evaluationOrderMenuRef.current?.present();
  };

  const openEvaluateItemsInOrderMenu = () => {
    evaluationOrderItemsMenuRef.current?.present();
  };

  const onPressEvaluateItemsInOrders = (
    data: Omit<IEvaluationOrderItemsRequestData, "orderId">
  ) => {
    dispatch(
      AsyncActionOrders.EvaluationOrderItems({
        orderId: selectedOrderDetail.order_id,
        ...data,
      })
    );
    evaluationOrderItemsMenuRef.current?.dismiss();
  };

  const openOrderDetail = (orderData: IGetOrderDataResponse) => {
    show({
      Component: () => (
        <SafeAreaView>
          <BottomSheetMenuOrderDetail
            orderData={orderData}
            onPressEvaluateOrder={openEvaluateOrderMenu}
          />
        </SafeAreaView>
      ),
      snapPoints: snapPointsOrderDetailMenu,
    });
  };

  const onSelectItem = async (orderId: number) => {
    if (!orderId) {
      return;
    }
    try {
      setIsLoading(true);
      const res = await dispatch(
        AsyncActionOrders.getOrderData({ order_id: orderId })
      );
      InteractionManager.runAfterInteractions(() => {
        setIsLoading(false);
        setSelectedOrderDetail(res);
      });
      openOrderDetail(res);
    } catch (e) {
      __DEV__ && console.log(e);
    }
  };

  useEffect(() => {
    dispatch(AsyncActionOrders.getOrders());
  }, []);

  useEffect(() => {
    const listener = BackHandler.addEventListener("hardwareBackPress", () => {
      if (isVisible) {
        close();
      } else {
        goBack();
      }
      return true;
    });
    return listener.remove;
  }, [isVisible]);

  // useAndroidBackHandler(close);

  return (
    <SafeAreaView>
      {isLoading ? (
        <View style={styles.activityIndicatorContainer}>
          <ActivityIndicator color={Color.PRIMARY} size={"large"} />
        </View>
      ) : null}

      <OrdersList onPress={onSelectItem} orders={orders} />
      <BottomSheetModalWithRef
        modalRef={evaluationOrderMenuRef}
        snapPoints={snapPointsEvaluationOrderMenu}
      >
        <BSMElevationOrderContent
          expand={evaluationOrderMenuRef.current?.expand}
          collapse={evaluationOrderMenuRef.current?.collapse}
          onPress={onPressEvaluateOrder}
        />
      </BottomSheetModalWithRef>

      <BottomSheetModalWithRef
        modalRef={evaluationOrderItemsMenuRef}
        snapPoints={snapPointsEvaluateOrderItemsMenu}
      >
        <BSMEvaluationOrderItemsContent
          onPress={onPressEvaluateItemsInOrders}
          orderData={selectedOrderDetail}
        />
      </BottomSheetModalWithRef>
    </SafeAreaView>
  );
});

const styles = StyleSheet.create({
  activityIndicatorContainer: {
    backgroundColor: Color.RGBA_200,
    position: "absolute",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    height: windowHeight,
    zIndex: 100,
  },
});
