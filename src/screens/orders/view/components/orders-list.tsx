import React, {useCallback} from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import { OrdersListItem} from './orders-list-item';
import {ScreenHeaderTitleWithBackButton} from '../../../../components/screen-header/screen-header-title-with-back-button';
import {IOrderItem} from '../../api/types';
import {Preloader} from '../../../../components/preloader';
import {useTypedSelector} from '../../../../system';
import {EmptyDataTitle} from '../../../../components/empty-data-title';

interface IOrdersHistoryList {
  orders: IOrderItem[];
  onPress: (orderId: number) => void;
}

export const OrdersList: React.FC<IOrdersHistoryList> = ({orders, onPress}) => {
  const isLoading = useTypedSelector(state => state.ordersHistory.isLoading);

  const renderItem = useCallback(
    ({item}: {item: IOrderItem}) => {
      const onPressHandler = () => {
        onPress(item.order_id);
      };

      return (
        <OrdersListItem
          onPress={onPressHandler}
          rating={item?.rating}
          address={`${item.city} ${item.street} ${item.house_num}`}
          statusColorLabel={item.status_title_color}
          statusColorWrapper={item.status_wrapper_color}
          containerColor={item.container_color}
          title={`Ваш заказ ${item.order_id}`}
          price={item.total_cost || 0}
          status={item.status_name}
          date={item.createdon}
        />
      );
    },
    [onPress],
  );

  const keyExtractor = useCallback(item => item.order_id?.toString(), []);



  return (
    <View>
      <FlatList
        style={styles.flatList}
        contentContainerStyle={styles.contentContainer}
        initialNumToRender={10}
        windowSize={10}
        ListEmptyComponent={isLoading ? Preloader : EmptyDataTitle}
        ListHeaderComponent={ListHeaderComponent}
        keyExtractor={keyExtractor}
        data={orders}
        renderItem={renderItem}
      />
    </View>
  );
};

const ListHeaderComponent = () => {
  return <ScreenHeaderTitleWithBackButton title={'Мои заказы'} />;
};

const styles = StyleSheet.create({
  flatList: {
    height: '100%',
  },
  contentContainer: {
    flexGrow: 1,
  },
});
