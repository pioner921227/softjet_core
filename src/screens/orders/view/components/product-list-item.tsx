import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import {urlValidator} from '../../../../system/helpers/url-validator';
import {ImageRepository} from '../../../../assets/image-repository';
import {Config} from '../../../../config';
import {IOrdersDataProduct} from '../../api/types';
import {Color} from '../../../../assets/styles';

const {RADIUS, UIStyles} = Config;

interface IProductListItem {
  item: IOrdersDataProduct;
}

// const additives: ICartOptionsListItem[] = [
//   {id: 0, title: 'Соус', count: 2},
//   {id: 1, title: 'Соус', count: 2},
//   {id: 2, title: 'Соус', count: 2},
//   {id: 3, title: 'Соус', count: 2},
// ];
//
// const size: ICartOptionsListItem[] = [{id: 0, title: 'Большая', count: 2}];
//
// const ingredients: ICartOptionsListItem[] = [
//   {id: 0, title: 'Соус', count: 2},
//   {id: 1, title: 'Масло', count: 2},
// ];

export const ProductListItem: React.FC<IProductListItem> = React.memo(
    ({item}) => {
      const {additives, size, ingredients} = item?.options?.childs || {};

      return (
          <View key={item.product_id} style={styles.container}>
            <FastImage
                style={styles.image}
                source={
                  item.img_url
                      ? {uri: urlValidator(item.img_url)}
                      : ImageRepository.ProductImagePlaceholder
                }
            />
            <View style={styles.contentBlock}>
              <Text style={styles.title}>{item.title}</Text>
              {size?.[0]?.title ? (
                  <Text style={styles.size}>
                    Размер:
                    {size?.[0]?.title}
                  </Text>
              ) : null}

              <View style={styles.descriptionRows}>
                {ingredients?.map(el => {
                  return (
                      <Text style={styles.ingredients}>
                        {'-'}
                        {el.title}{' '}
                      </Text>
                  );
                })}
              </View>

              <View style={styles.descriptionRows}>
                {additives?.map(el => {
                  return (
                      <Text style={styles.additives}>
                        {' '}
                        +{el.title} {'-'} {el.count}шт.
                      </Text>
                  );
                })}
              </View>
            </View>
            <View style={styles.priceContainer}>
              <Text style={styles.price}>{item.price} ₽</Text>
              <Text style={styles.count}>{item.count} шт</Text>
            </View>
          </View>
      );
    },
);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: 12,
    width: '100%',
  },
  image: {
    width: 74,
    height: 74,
    borderRadius: RADIUS.SMALL,
  },
  contentBlock: {
    flex: 1,
    marginLeft: 16,
    // justifyContent: 'center',
  },
  title: {
    ...UIStyles.font15b,
    paddingVertical: 4,
  },
  description: {
    ...UIStyles.font15,
  },
  price: {
    ...UIStyles.font15b,
  },
  count: {
    ...UIStyles.font15,
    marginTop: 5,
    textAlign: 'right',
  },
  weight: {
    ...UIStyles.font15,
  },
  size: {
    ...UIStyles.font12,
    color: Color.GREY_600,
  },
  ingredients: {
    ...UIStyles.font12,
    color: Color.PRIMARY,
  },
  additives: {
    ...UIStyles.font12,
    color: Color.SUCCESS,
  },
  priceContainer: {
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  descriptionRows: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
});
