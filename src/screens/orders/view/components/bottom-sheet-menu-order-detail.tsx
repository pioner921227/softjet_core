import React from "react";
import {
  InteractionManager,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from "react-native";
import { useModal } from "../../../../components";
import { Divider } from "../../../../components/divider";
import { TitleWithValueRow } from "../../../../components/title-with-value-row";
import { Config } from "../../../../config";
import { useTypedSelector } from "../../../../system";
import { adaptiveSize } from "../../../../system/helpers/responseive-font-size";
import { useTypedDispatch } from "../../../../system/hooks/use-typed-dispatch";
import {
  getAppSettings,
  getCurrentAddress,
  getCurrentCityId,
  getCurrentOrderTypeId,
  getCurrentOrganisationId,
} from "../../../../system/selectors/selectors";
import { ThemedButton } from "../../../../ui-kit";
import { IGetOrderDataResponse } from "../../api/types";
import { AsyncActionOrders } from "../../store/async-action-orders";
import { ProductListItem } from "./product-list-item";

const { Color, UIStyles, RADIUS } = Config;

interface IBottomSheetMenuOrderDetail {
  orderData: IGetOrderDataResponse;
  onPressEvaluateOrder: () => void;
}

export const BottomSheetMenuOrderDetail: React.FC<IBottomSheetMenuOrderDetail> =
  ({ orderData, onPressEvaluateOrder }) => {
    const currentAddress = useTypedSelector(getCurrentAddress);
    const currentCityId = useTypedSelector(getCurrentCityId);
    const currentOrganisationId = useTypedSelector(getCurrentOrganisationId);
    const currentOrderTypeId = useTypedSelector(getCurrentOrderTypeId);
    const appSettings = useTypedSelector(getAppSettings);

    const dispatch = useTypedDispatch();
    const { show, close } = useModal();

    const checkDeliveryData = () => {
      return (
        currentOrderTypeId !== orderData.order_type.id ||
        currentOrganisationId !== orderData.id_org ||
        currentCityId !== orderData.city_id ||
        (orderData.order_type.is_remoted &&
          currentAddress?.id !== orderData?.address_id)
      );
    };

    const onRepeatOrder = async () => {
      close();

      InteractionManager.runAfterInteractions(async () => {
        const res = await dispatch(
          AsyncActionOrders.repeatOrder({
            id_org: orderData.id_org,
            city_id: orderData.city_id,
            order_id: orderData.order_id,
            order_type: orderData.order_type.id,
            address_id: orderData.address_id,
          })
        );

        show({
          title: res.repeat_order.title,
          description: res.repeat_order.subtitle,
          buttons: [{ label: "Понятно", onPress: close }],
        });
      });
    };

    const onPressRepeatOrder = () => {
      checkDeliveryData();
      show({
        title: "Повторить заказ?",
        description:
          "Все товары будут добавлены в заказ. Обратите внимание, что не все товары могут быть доступны к заказу сейчас",
        buttons: [
          {
            label: "Отмена",
            onPress: close,
            styles: {
              modifier: "bordered",
            },
          },
          {
            label: "Повторить",
            onPress: onRepeatOrder,
          },
        ],
        buttonsDirection: "row",
      });
    };

    const isPaidDelivery = +orderData?.delivery_cost > 0;

    const getDeliveryCostValue = () => {
      return isPaidDelivery ? orderData.delivery_cost.toString() : "Бесплатно";
    };

    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
      >
        <Text style={styles.title}>
          Благодарим вас за заказ № {orderData?.order_id}
        </Text>
        <View style={styles.statusWrapper}>
          <Text style={styles.statusTitle}>{orderData?.status_name}</Text>
        </View>
        <TitleWithValueRow
          title={"Позиций в заказе"}
          value={orderData?.products?.length?.toString()}
        />
        <Divider />
        {orderData?.products?.length ? (
          <View style={{ paddingVertical: 16 }}>
            {orderData?.products?.map((el) => (
              <ProductListItem key={el.product_id} item={el} />
            ))}
          </View>
        ) : null}

        <Divider />
        <TitleWithValueRow title={"Дата заказа"} value={orderData?.createdon} />
        <TitleWithValueRow
          title={"Точка продаж"}
          value={orderData?.organisation_name}
        />

        {orderData?.order_type?.is_remoted ? (
          <TitleWithValueRow
            title={"Адрес доставки"}
            value={`${orderData?.city}, ${orderData?.street}, ${orderData?.house_num}, ${orderData?.flat_num}`}
          />
        ) : null}

        <TitleWithValueRow
          title={"Сумма заказа"}
          value={orderData?.original_total_cost?.toString()}
        />

        <TitleWithValueRow
          valueStyle={{ color: isPaidDelivery ? Color.BLACK : Color.SUCCESS }}
          title={"Доставка"}
          value={
            isPaidDelivery ? orderData.delivery_cost.toString() : "Бесплатно"
          }
        />
        <TitleWithValueRow
          title={"Оплачено бонусами"}
          value={orderData?.bonus_writeoff}
        />
          <TitleWithValueRow
            title={"Скидка по промокоду"}
            value={orderData?.discount?.toString() || "0"}
          />
        <TitleWithValueRow
          title={"Скидка за самовывоз"}
          value={orderData?.self_pickup_discount?.toString() || "0"}
        />
        <TitleWithValueRow
          title={"К оплате"}
          value={orderData?.total_cost?.toString()}
        />
        <TitleWithValueRow
          title={"Способ оплаты"}
          value={orderData?.payment_name}
        />
        {appSettings?.app_order_evaluation && !orderData.rating ? (
          <ThemedButton
            withGesture={true}
            modifier="bordered"
            rounded={true}
            label="Оценить заказ"
            onPress={onPressEvaluateOrder}
            wrapperStyle={{ marginBottom: 15 }}
          />
        ) : null}
        {appSettings?.app_repeat_order ? (
          <ThemedButton
            withGesture={true}
            theme="PRIMARY"
            rounded={true}
            label="повторить заказ"
            onPress={onPressRepeatOrder}
          />
        ) : null}
      </ScrollView>
    );
  };

const styles = StyleSheet.create({
  container: {
    ...UIStyles.paddingH16,
  },
  contentContainer: {
    paddingBottom: adaptiveSize(50),
  },
  title: {
    ...UIStyles.font24b,
    textAlign: "center",
    paddingHorizontal: adaptiveSize(70),
  },
  statusTitle: {
    ...UIStyles.font15,
    color: Color.WHITE,
  },
  statusWrapper: {
    backgroundColor: Color.SUCCESS,
    padding: adaptiveSize(16),
    borderRadius: RADIUS.MEDIUM,
    alignItems: "center",
    marginTop: adaptiveSize(16),
  },
  addCommentTitle: {
    ...UIStyles.font15,
    color: Color.PRIMARY,
    textAlign: "center",
  },
});
