import React, {useMemo} from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import {Config} from '../../../../config';
import {Divider} from '../../../../components/divider';
import {Rating} from '../../../../components/rating/rating';
import {adaptiveSize} from '../../../../system/helpers/responseive-font-size';

const {Color, UIStyles} = Config;

interface IMyOrderListItem {
  onPress: () => void;
  address: string;
  rating?: number;
  title: string;
  price: number;
  status: string;
  statusColorWrapper?: string;
  statusColorLabel?: string;
  containerColor?: string;
  containerStyle?: ViewStyle;
  date: string;
}


export const OrdersListItem: React.FC<IMyOrderListItem> = ({
  title,
  address,
  rating,
  price,
  date,
  status,
  statusColorLabel = Color.WHITE,
  statusColorWrapper = Color.SUCCESS,
  containerColor,
  containerStyle,
  onPress,
}) => {
  const containerStyle_ = useMemo(() => {
    return [
      styles.container,
      {backgroundColor: containerColor},
      containerStyle,
    ];
  }, [containerColor, containerStyle]);

  const statusWrapper_ = useMemo(() => {
    return [styles.statusWrapper, {backgroundColor: statusColorWrapper}];
  }, [statusColorWrapper]);

  const titleStyle_ = useMemo(() => {
    return [styles.statusTitle, {color: statusColorLabel}];
  }, [statusColorLabel]);

  return (
    <>
      <TouchableOpacity onPress={onPress} style={containerStyle_}>
        <View style={UIStyles.flexRow}>
          <Text style={styles.title}>{title}</Text>
          <View style={statusWrapper_}>
            <Text style={titleStyle_}>{status}</Text>
          </View>
        </View>
        {rating ? (
          <View style={styles.ratingWrapper}>
            <Rating
              containerStyle={{marginRight: adaptiveSize(12)}}
              rating={rating}
            />
            <Text style={styles.ratingTitle}>Ваша оценка</Text>
          </View>
        ) : null}

        <Text style={styles.address}>{address}</Text>
        <View style={styles.dateRow}>
          <Text style={styles.dateValue}>{date}</Text>
          <Text style={styles.priceValue}>{price} ₽</Text>
        </View>
      </TouchableOpacity>
      <Divider />
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    ...UIStyles.paddingH16,
    paddingVertical: adaptiveSize(12),
  },
  title: {
    ...UIStyles.font18b,
    color: Color.BLACK,
    fontWeight: 'bold',
  },
  statusWrapper: {
    backgroundColor: Color.SUCCESS,
    borderRadius: 40,
    paddingVertical: adaptiveSize(3),
    paddingHorizontal: adaptiveSize(6),
  },
  statusTitle: {
    ...UIStyles.font15,
    color: Color.WHITE,
  },
  address: {
    marginTop: adaptiveSize(12),
    ...UIStyles.font15,
    color: Color.GREY_800,
  },
  dateRow: {
    marginTop: adaptiveSize(12),
    ...UIStyles.flexRow,
  },
  dateValue: {
    ...UIStyles.font15,
    color: Color.GREY_400,
  },
  priceValue: {
    ...UIStyles.font18b,
    color: Color.BLACK,
  },
  ratingWrapper: {
    flexDirection: 'row',
    marginTop: adaptiveSize(12),
  },
  ratingTitle: {
    ...UIStyles.font15,
    color: Color.GREY_400,
  },
});
