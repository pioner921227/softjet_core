export const ordersConfig = {
  EVALUATE_ORDER_MENU: {
    IS_VISIBLE_IMAGE_PICKER: false,
    IS_VISIBLE_VARIANTS_CHECKBOX: true,
    ENABLE_EVALUATE_ORDER_ITEM_MENU: true,
  },
  EVALUATE_ORDER_ITEM_MENU: {},
};
