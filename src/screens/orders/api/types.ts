import {IProductOptions} from '../../cart/api/types';

export interface IGetOrdersResponse {
  orders: IOrderItem[];
}

export interface IOrderItem {
  rating?: number;
  total_cost?: number;
  order_id: number;
  status_id: number;
  status_name: string;
  createdon: string;
  street: string;
  city: string;
  house_num: string;
  flat_num: string;
  container_color: string;
  status_wrapper_color: string;
  status_title_color: string;
}

export interface IGetOrderDataRequest {
  order_id: number;
}
export interface IGetOrderDataResponse {
  cost: number;
  self_pickup_discount:number;
  original_total_cost:number;
  total_cost: number;
  order_id: number;
  status_id: number;
  status_color: string;
  status_name: string;
  payment_id: number;
  payment_name: string;
  createdon: string;
  comment: string;
  city: string;
  id_org: number;
  street: string;
  floor: string;
  address_id: number;
  region: {
    latitude: '';
    longitude: null;
  };
  house_num: string;
  flat_num: string;
  entrance: string;
  promocode: string;
  discount: string;
  actual_discount: number;
  rating: number | null;
  bonus_writeoff: string;
  bonus_accrual: string;
  city_id: number;
  order_type_title: string;
  order_type_id: number;
  order_type_is_remoted: string;
  products: IOrdersDataProduct[];
  delivery_cost: number;
  organisation_name: string;
  order_type: {
    title: string;
    id: number;
    is_remoted: boolean;
  };
  order_title: string;
}

export interface IOrdersDataProduct {
  title: string;
  product_id: number;
  count: number;
  price: number;
  options: IProductOptions;
  img_url: string;
  weight: number;
}

export interface IRepeatOrderDataRequest {
  order_id: number;
  id_org: number;
  order_type: number;
  address_id: number;
  city_id: number;
}

export interface IEvaluationOrderRequestData {
  order_id: number;
  general_rating: number;
  answer_options: number[];
  comment: string;
  photo?: string;
}

export interface IRatingItem {
  id: number;
  rating: number;
  comment?: string;
}

export interface IEvaluationOrderItemsRequestData {
  orderId: number;
  comment: string;
  ratings: IRatingItem[];
}
