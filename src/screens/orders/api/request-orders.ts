import {ApiService} from '../../../system/api/api-service';
import {
  IEvaluationOrderItemsRequestData,
  IEvaluationOrderRequestData,
  IGetOrderDataRequest,
  IGetOrderDataResponse,
  IGetOrdersResponse,
  IRepeatOrderDataRequest,
} from './types';
import {ICartGetDataResponse} from '../../cart/api/types';

export class RequestOrders {
  static getOrders(): Promise<IGetOrdersResponse> {
    return ApiService.get('/api/user/getOrdersHistory');
  }
  static getOrderData(
    data: IGetOrderDataRequest,
  ): Promise<IGetOrderDataResponse> {
    return ApiService.get('/api/user/getUserOrder', {
      params: data,
    });
  }
  static repeatOrder(
    data: IRepeatOrderDataRequest,
  ): Promise<ICartGetDataResponse> {
    return ApiService.post('/api/order/reorder', data);
  }

  static evaluateOrder(data: IEvaluationOrderRequestData) {
    return ApiService.post('/api/orders/rateOrder', data);
  }

  static evaluateOrderItems(data: IEvaluationOrderItemsRequestData) {
    return ApiService.post('/setproductrating.json', data);
  }
}
