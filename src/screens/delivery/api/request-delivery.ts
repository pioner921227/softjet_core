import {ApiService} from '../../../system/api/api-service';
import {IGetCitiesResponse, IOrderTypesItem} from '../types/types';
import {
  IAddressItemResponse,
  IChangeUserAddress,
  IDeleteUserAddressDataRequest,
  IGetOrganisationByAddressRequest,
  IGetOrganisationByAddressResponse,
  IGetOrganisationCommentResponse,
  IGetOrganisationsRequest,
  IGetOrganisationsResponse,
  ISaveNewAddressRequest,
} from './types';

export class RequestDelivery {
  static getCities(): Promise<IGetCitiesResponse> {
    return ApiService.get('/api/organizations/getCities');
  }
  static getOrganisations(
    data: IGetOrganisationsRequest,
  ): Promise<IGetOrganisationsResponse> {
    return ApiService.get('/api/organizations/getOrganizations', {
      params: data,
    });
  }
  static saveNewAddress(
    data: ISaveNewAddressRequest,
  ): Promise<IAddressItemResponse> {
    return ApiService.post('/api/user/saveUserAddress', data);
  }
  static deleteUserAddress(
    data: IDeleteUserAddressDataRequest,
  ): Promise<IAddressItemResponse[]> {
    return ApiService.delete('/api/user/removeUserAddress', {data});
  }

  static changeUserAddress(
    data: IChangeUserAddress,
  ): Promise<IAddressItemResponse> {
    return ApiService.put('/api/user/changeUserAddress', data);
  }

  static getUserAddressList(): Promise<IAddressItemResponse[]> {
    return ApiService.post('/api/user/getUserAddresses');
  }

  static getOrderTypes(): Promise<IOrderTypesItem[]> {
    return ApiService.get('/api/info/getOrderTypes');
  }

  static getOrganisationByAddress(
    data: IGetOrganisationByAddressRequest,
  ): Promise<IGetOrganisationByAddressResponse> {
    return ApiService.get('/api/organizations/getOrganizationByAddress', {
      params: data,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  }

  static getOrganisationComment(org_id: number): Promise<IGetOrganisationCommentResponse> {
    return ApiService.get(
      `/api/organizations/getOrganizationComment?id_org=${org_id}`,
    );
  }

}
