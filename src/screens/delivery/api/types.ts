import {IAddressItem, IOrganisation} from '../types/types';

export interface IGetOrganisationsRequest {
  order_type: number;
  city_id: number;
}
export interface IGetOrganisationsResponse {
  orgs: IOrganisation[];
}

export interface IChangeUserAddress
  extends Omit<IAddressItem, 'id_org' | 'user_id' | 'delivery_exists'> {
  id: number;
}

export interface ISaveNewAddressRequest
  extends Omit<IAddressItem, 'id_org' | 'user_id'> {
  // delivery_exists: boolean;
}

export interface IAddressItemResponse extends IAddressItem {
  id: number;
}

export interface IGetOrganisationByAddressRequest {
  country: string;
  city: string;
  street: string;
  house: string;
  city_id: number;
}

export interface IGetOrganisationByAddressResponse {
  organisation: IOrganisation_;
  brands: IBrand[];
  success: boolean;
  polygon_id: number;
}

export interface IOrganisation_ {
  description: string;
  id_org: number;
  title: string;
  work_status: boolean;
}

export interface IBrand {
  data: {
    brand: number;
  } & IOrganisation_;
  id_brand: number;
  message_title: string;
  message: string;
  title: string;
  image: string;
}

export interface IDeleteUserAddressDataRequest {
  id: number;
}

export interface IGetOrganisationCommentResponse {
  message_title: string;
  message: string;
  work_status: boolean;
}

