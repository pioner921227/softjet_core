import {Routes} from '../../navigation';
import {rootNavigate} from '../../system';
import {BottomSheetMenuType} from './view/organisation-list-with-map/organisation-list-with-map';
import {createOrderTypeModalConfig} from '../../components/bottom-sheet-menu/config';

export interface INextButtonConfig<T> {
  nextButtonLabel: string;
  onPressNextButton: null | ((params: T) => void);
}

export const selectOrderTypeModalConfig = createOrderTypeModalConfig({
  snapPoints: [350],
  online: (route?: keyof typeof Routes) =>
    rootNavigate(route || Routes.AddressCreate),
  offline: () =>
    rootNavigate(Routes.SelectOrganisationInMap, {
      bottomSheetMenuType: BottomSheetMenuType.Organisations,
      disableLoadOrganisations: true,
    }),
});
