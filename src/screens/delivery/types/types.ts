import { LatLng } from 'react-native-maps'

export interface IAddressItem {
  country: string
  city: string
  street: string
  house_num: string
  flat_num: number | string
  entrance: number | string
  floor: number | string
  doorphone: number | string
  hidden: boolean
  delivery_exists: boolean
  id_org: number
  user_id: number
  address_comment: string | number
}

export interface ICityItem {
  title: string
  id: number
  default_id_org: number
}

export interface IGetCitiesResponse {
  towns: ICityItem[]
}

export enum OrderTypeEnum {
  SelectAddress = 'select_address',
  Manager = 'manager',
  InOrganisation = 'in_organization',
  SelfDelivery = 'self_delivery',
}

export interface IOrderTypesItem {
  id: number
  name: string
  description: string
  isRemoted: boolean
  logo: string
  payment_methods: number[]
  show_on_main: boolean
  show_in_modals: boolean
  type: OrderTypeEnum
}

export interface IOrganisation {
  GPS: LatLng
  title: string
  id: number
  requisites: string
  img: string
  address: string
  description: string
  comment: string
  phone: string
  phone_name: string
  worktime: string
  whats: string
  time_work: string
  social_link_vk: string
  social_link_fb: string
  social_link_ok: string
  social_link_instagram: string
  order_days: number
  stickers: IStickerItem[]
  payments: [
    {
      id: number
      name: string
      is_online: boolean
      is_cash: boolean
    },
  ]
  deliveries: [
    {
      id: number
      name: string
    },
  ]
}

export interface IStickerItem {
  id: number
  title: string
  color_text: string
  color_bg: string
}
