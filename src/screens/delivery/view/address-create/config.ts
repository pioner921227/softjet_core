import {INextButtonConfig} from '../../config';
import {IGetOrganisationByAddressResponse} from '../../api/types';
import {TModalShowCallback} from '../../../../components/modal/modal-provider';

interface IAddressCreateConfig
  extends INextButtonConfig<{
    result: IGetOrganisationByAddressResponse;
    close: () => void;
    show: TModalShowCallback;
  }> {
  headerTitle: string;
  disabledCitySelect: boolean;
}

export const addressCreateConfig: IAddressCreateConfig = {
  nextButtonLabel: 'Сохранить адрес',
  onPressNextButton: null,
  headerTitle: 'Укажите адрес',
  disabledCitySelect: false,
};
