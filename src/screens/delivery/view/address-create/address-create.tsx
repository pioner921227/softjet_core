import React, {useEffect} from 'react';
import {useTypedSelector} from '../../../../system/hooks/use-typed-selector';
import {IModalButton, useModal} from '../../../../components/modal/modal-provider';
import {useNavigation} from '@react-navigation/native';
import {Routes} from '../../../../navigation/config/routes';
import {BottomSheetMenuType} from '../organisation-list-with-map/organisation-list-with-map';
import {AsyncActionsDelivery} from '../../store/async-actions-delivery';
import {StackNavigationProp, StackScreenProps} from '@react-navigation/stack';
import {TRootStackParamList} from '../../../../navigation/types/types';
import {
  CreateAddressContent,
  TCreateAddressContentCallback,
} from '../components/create-address-content';
import {ScreenHeaderTitleWithBackButton} from '../../../../components/screen-header/screen-header-title-with-back-button';
import {ActionDelivery} from '../../store/action-delivery';
import {DELIVERY_TYPE} from '../../store/store-delivery';
import {Config} from '../../../../config';
import {addressCreateConfig} from './config';
import {useTypedDispatch} from '../../../../system/hooks/use-typed-dispatch';
import {SafeAreaView} from 'react-native-safe-area-context';
import {
  getCities,
  getCurrentCityId,
  getToken,
} from '../../../../system/selectors/selectors';
import {KeyboardAvoidingScroll} from '../../../../components/keyboard-avoiding-scroll';
import {useIsLoading} from '../../../../system/hooks/use-is-loading';
import {useAndroidBackHandler} from '../../../../system/hooks/use-android-back-handler';
import { InteractionManager } from 'react-native';

type TNavigation = StackNavigationProp<
  TRootStackParamList,
  Routes.SelectOrganisationInMap
>;

type TProps = StackScreenProps<TRootStackParamList, Routes.AddressCreate>;


export const AddressCreate: React.FC<TProps> = React.memo(({route}) => {



  const cities = useTypedSelector(getCities);
  const currentCityId = useTypedSelector(getCurrentCityId);
  const token = useTypedSelector(getToken);
  const {isLoading, setIsLoading} = useIsLoading();

  const {show, close} = useModal();

  const dispatch = useTypedDispatch();
  const {navigate, reset} = useNavigation<TNavigation>();

  const success = async (orgId: number | undefined) => {
    if (currentCityId !== undefined && orgId !== undefined) {
      await dispatch(
        AsyncActionsDelivery.getOrganisations({
          city_id: currentCityId,
          order_type: DELIVERY_TYPE.DELIVERY,
        }),
      );
      dispatch(ActionDelivery.setCurrentOrganisation(+orgId.toString()));

      close();

      InteractionManager.runAfterInteractions(()=>{
        reset({
          index: 0,
          routes: [
            {
              name: Routes.TabRootScreen,
              params: {screen: Routes.Catalog, params: {ignoreModal: true}},
            },
          ],
        });

        })
      }
  };

  const onPressSave: TCreateAddressContentCallback = async data => {
    const {city, street, house_num, country, cityId} = data;

    if (cityId === undefined) {
      return;
    }

    dispatch(ActionDelivery.setCurrentCity(cityId));

    setIsLoading(true);

    try {

    const res = await dispatch(
      AsyncActionsDelivery.getOrganisationByAddress({
        city,
        street,
        city_id: +cityId,
        house: house_num,
        country,
      }),
    );

    if (token) {
      await dispatch(
        AsyncActionsDelivery.saveNewAddress({
          ...data,
          delivery_exists: res.success,
        }),
      );
    }

    setIsLoading(false);

    if (res?.success && !token) {
      dispatch(
        ActionDelivery.setCurrentAddress({
          ...data,
          delivery_exists: true,
          id: 0,
          id_org: 0,
          user_id: 0,
        }),
      );
    }

    if (addressCreateConfig.onPressNextButton) {
      addressCreateConfig.onPressNextButton({
        result: res,
        show,
        close,
      });
      return;
    }

    const onPressModalButton = () => {
      dispatch(
        ActionDelivery.setCurrentOrderType(DELIVERY_TYPE.SELF_DELIVERY_TYPE),
      );
      navigate(Routes.SelectOrganisationInMap, {
        bottomSheetMenuType: BottomSheetMenuType.Organisations,
      });
      close();
    };

    const getModalButtons = (): IModalButton[] => {
      if (res?.success) {
        if (res?.organisation?.work_status) {
          return [
            {
              label: 'Понятно',
              onPress: () => success(res.organisation.id_org),
            },
          ];
        }
        return [
          {label: 'Отмена', onPress: close, styles:{modifier:'bordered'}},
          {label: 'Понятно', onPress: ()=> success(res?.organisation?.id_org)},
        ];
      }
      return [
        {
          label: 'Изменить адрес',
          onPress: close,
          styles: {modifier: 'bordered'},
        },
        {
          label: 'Заберу сам',
          onPress: onPressModalButton,
          styles: {modifier: 'bordered'},
        },
      ];
    };

    show({
      title: res?.organisation?.title,
      description: res?.organisation?.description,
      buttons: getModalButtons(),
      buttonsDirection: 'row',
    });
    }
    catch(e){
      setIsLoading(false);
    }
  };

  useAndroidBackHandler(close);

  useEffect(() => {
    return close;
  }, []);

  return (
    <SafeAreaView style={Config.UIStyles.flexGrow}>
      <KeyboardAvoidingScroll>
        <ScreenHeaderTitleWithBackButton
          title={addressCreateConfig.headerTitle}
        />
        <CreateAddressContent
          isLoading={isLoading}
          buttonLabel={addressCreateConfig.nextButtonLabel}
          cities={cities}
          onPressButton={onPressSave}
        />
      </KeyboardAvoidingScroll>
    </SafeAreaView>
  );
});
