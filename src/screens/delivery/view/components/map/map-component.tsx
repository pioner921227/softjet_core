import MapView, {Region} from 'react-native-maps';
import {CustomMarker, IAddMarkerRefCallback, IOnPressMarker} from '../../../../../components/map/custom-marker';
import React, {PropsWithChildren} from 'react';
import {StyleSheet} from 'react-native';
import {IOrganisation} from '../../../types/types';

interface IMapProps {
  organisations: IOrganisation[];
  addMarkerRef: IAddMarkerRefCallback;
  onPressMarker: IOnPressMarker;
  initialRegion: Region;
}

export const MapComponent = React.memo(
  React.forwardRef<MapView, PropsWithChildren<IMapProps>>(
    ({organisations, addMarkerRef, onPressMarker, initialRegion}, ref) => {
      return (
        <MapView
          ref={ref}
          style={styles.map}
          showsUserLocation={true}
          initialRegion={initialRegion}>
          {organisations.length
            ? organisations.map(el => {
                return (
                  <CustomMarker
                    gps={el.GPS}
                    key={el.id}
                    id={el.id}
                    addMarkerRef={addMarkerRef}
                    onPressMarker={onPressMarker}
                    calloutText={el.title}
                    latitude={+el.GPS.latitude}
                    longitude={+el.GPS.longitude}
                  />
                );
              })
            : null}
        </MapView>
      );
    },
  ),
);

const styles = StyleSheet.create({
  map: {
    width: '100%',
    height: '60%',
  },
});
