import {KeyboardTypeOptions, StyleSheet, Text, View, ViewStyle} from 'react-native';
import {InputWithLabel} from '../../../../components/input/input-with-label';
import React, {Fragment, useEffect, useRef, useState} from 'react';
import {ThemedButton} from '../../../../components/buttons/themed-button';
import {ICityItem} from '../../types/types';
import {
  IUseInputReturnType,
  useInput,
} from '../../../../system/hooks/use-input';
import {useTypedSelector} from '../../../../system/hooks/use-typed-selector';
import {Config} from '../../../../config';
import {getCurrentCityId} from '../../../../system/selectors/selectors';
import {adaptiveSize} from '../../../../system/helpers/responseive-font-size';
import {getFontSize} from '../../../../system/helpers/is-small-screen';
import {IChangeUserAddress} from '../../api/types';
import {
  ISelectCustomMethods,
  ISelectItem,
  SelectCustom,
} from '../../../../components/select-custom/select-custom';
import {acceptAddressConfig} from '../../../cart/view/accept-address/config';
import {addressCreateConfig} from '../address-create/config';

const {UIStyles, Color} = Config;

export type TAcceptAddressProps = Omit<IChangeUserAddress, 'id'> & {
  cityId: number;
};

export type TCreateAddressContentCallback = (
  props: TAcceptAddressProps,
) => void;

interface ICreateAddressContent {
  cities: ICityItem[];
  onPressButton: TCreateAddressContentCallback;
  buttonLabel: string;
  city?: string;
  street?: string;
  house?: string | number;
  entrance?: string | number;
  flat?: string | number;
  floor?: string | number;
  domophone?: string | number;
  disabledSelect?: boolean;
  disabledStreet?: boolean;
  disabledHome?: boolean;
  isLoading?: boolean;
  address_comment?: string | number;
}

export const CreateAddressContent: React.FC<ICreateAddressContent> = ({
  cities,
  onPressButton,
  buttonLabel,
  city,
  street,
  house,
  entrance,
  flat,
  floor,
  domophone,
  disabledSelect,
  disabledStreet = false,
  disabledHome = false,
  isLoading = false,
  address_comment,
}) => {
  const selectCity = useInput(city || '');
  const inputStreet = useInput(street || '');
  const inputHouse = useInput(house?.toString() || '');
  const inputEntrance = useInput(entrance?.toString() || '');
  const inputFlat = useInput(flat?.toString() || '');
  const inputFloor = useInput(floor?.toString() ?? '');
  const inputDomophone = useInput(domophone?.toString() || '');
  const inputAddressName = useInput('');
  const inputAddressComment = useInput(address_comment?.toString() || '');

  const currentCityId = useTypedSelector(getCurrentCityId);

  const itemsForSelect = cities.map(el => ({
    id: +el.id,
    label: el.title,
    value: el.title,
  }));

  const currentItem = itemsForSelect.find(el => el.id === currentCityId);

  const [selectedItem, setSelectedItem] = useState(
    currentItem || itemsForSelect[0],
  );

  const onSelectCity = (item: ISelectItem) => {
    selectCity.onChangeText(item.label);
    // dispatch(ActionDelivery.setCurrentCity(+item.id));
    setSelectedItem(item);
  };

  const convertStringToNumber = (value: string) => {
    if (value === '') {
      return '';
    }
    return +value;
  };

  const data: TAcceptAddressProps = {
    city: selectCity.value,
    street: inputStreet.value,
    house_num: inputHouse.value,
    floor: convertStringToNumber(inputFloor.value),
    flat_num: inputFlat.value,
    doorphone: convertStringToNumber(inputDomophone.value),
    entrance: convertStringToNumber(inputEntrance.value),
    country: 'Россия',
    hidden: false,
    cityId: selectedItem.id,
    address_comment: inputAddressComment.value,
  };

  const onPressHandler = () => {
    onPressButton(data);
  };

  type TInputs = {
    label: string;
    props: IUseInputReturnType;
    keyboardType: KeyboardTypeOptions;
  };

  const inputs: TInputs[] = [
    {label: 'Дом', props: inputHouse, keyboardType: 'default'},
    {label: 'Подъезд', props: inputEntrance, keyboardType: 'numeric'},
    {label: 'Квартира', props: inputFlat, keyboardType: 'default'},
    {label: 'Этаж', props: inputFloor, keyboardType: 'numeric'},
    {label: 'Код домофона', props: inputDomophone, keyboardType: 'numeric'},
  ];

  useEffect(() => {
    if (currentCityId) {
      const selectedCity = cities.find(el => el.id === currentCityId);
      selectCity.onChangeText(selectedCity?.title || cities[0].title);
    }
  }, [currentCityId]);

  const selectRef = useRef<ISelectCustomMethods | null>(null);

  return (
    <View testID={'view'} accessible={true} style={styles.container}>
      <SelectCustom
        disabled={
          disabledSelect ??
          (addressCreateConfig.disabledCitySelect || cities.length === 1)
        }
        ref={selectRef}
        onSelect={onSelectCity}
        selectedItem={selectedItem}
        data={itemsForSelect}
      />
      <InputWithLabel
        disabled={disabledStreet}
        textStyle={styles.inputText}
        wrapperStyle={styles.input}
        label={'Улица'}
        {...inputStreet}
        placeholderText="Улица"
      />
      <Text style={styles.streetSubtitle}>Внимание! Название "Микрорайон" нужно писать полностью! Без сокращений!</Text>
      <View style={styles.inputRow}>
        {inputs.slice(0, 3).map((el, index) => {
          return (
            <Fragment key={index}>
              <InputWithLabel
                keyboardType={el.keyboardType}
                textStyle={styles.inputText}
                disabled={index === 0 && disabledHome}
                wrapperStyle={styles.bottomContent}
                label={el.label}
                {...el.props}
                placeholderText={el.label}
              />
              {index !== inputs.slice(0, 3).length - 1 && <RowSeparator />}
            </Fragment>
          );
        })}
      </View>
      <View style={styles.inputRow}>
        {inputs.slice(3).map((el, index) => {
          return (
            <Fragment key={index}>
              <InputWithLabel
                keyboardType={el.keyboardType}
                wrapperStyle={styles.bottomContent}
                label={el.label}
                textStyle={styles.inputText}
                {...el.props}
                placeholderText={el.label}
              />
              {index !== inputs.slice(3).length - 1 && <RowSeparator />}
            </Fragment>
          );
        })}
      </View>
      {acceptAddressConfig?.isVisibleAddressName && (
        <InputWithLabel
          textStyle={styles.inputText}
          placeholderText={'Название адреса'}
          wrapperStyle={styles.input}
          label={'Название адреса'}
          {...inputAddressName}
        />
      )}
      <InputWithLabel
        textStyle={styles.inputText}
        placeholderText={'Комментарий к адресу'}
        wrapperStyle={commentInput}
        label={'Комментарий к адресу'}
        {...inputAddressComment}
      />
      <ThemedButton
        isLoading={isLoading}
        wrapperStyle={styles.button}
        disabled={!inputHouse.value || !inputStreet.value || isLoading}
        rounded={true}
        label={buttonLabel}
        onPress={onPressHandler}
      />
    </View>
  );
};

const RowSeparator = () => {
  return <View style={{width: adaptiveSize(8)}} />;
};

const styles = StyleSheet.create({
  container: {
    ...UIStyles.paddingH16,
    flex: 1,
  },
  select: {
    marginTop: 16,
  },
  bottomContent: {
    marginTop: 24,
    flex: 1,
  },
  inputRow: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
  },
  inputText: {
    fontSize: getFontSize(14),
  },
  input: {
    marginTop: 24,
  },
  button: {
    marginBottom: 10,
  },
  streetSubtitle:{
    marginLeft: 5,
    marginTop: 5,
    ...UIStyles.font10,
    color: Color.GREY_400,
  }
});

const commentInput: ViewStyle = {
  ...styles.input,
  marginBottom: 20,
};
