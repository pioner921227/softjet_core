import React, { useCallback, useEffect, useRef, useState } from 'react'
import { FlatList, StyleSheet, Text, View } from 'react-native'
import { IOrganisation } from '../../../types/types'
import { Divider } from '../../../../../components/divider'
import { Config } from '../../../../../config'
import { IListItem } from '../../../../../components/list-item/list-item'
import { Preloader } from '../../../../../components/preloader'
import { EmptyDataTitle } from '../../../../../components/empty-data-title'
import { useTypedSelector } from '../../../../../system'

const { UIStyles } = Config

type propsFromListItem = Omit<IListItem, 'title'>

export interface IOrganisationListRenderItemProps extends propsFromListItem {
  data: IOrganisation
}

export interface IOrganisationListProps {
  data: IOrganisation[]
  onSelectItem: (item: IOrganisation) => void
  RenderItem: React.FC<IOrganisationListRenderItemProps>
  selectedItemId: number
}

export const OrganisationList: React.FC<IOrganisationListProps> = React.memo(
  ({ data, onSelectItem, RenderItem, selectedItemId }) => {
    const isLoading = useTypedSelector((state) => state.delivery.isLoading)
    const listRef = useRef<FlatList | null>(null)

    const onSelectHandler = (item: IOrganisation) => {
      onSelectItem(item)
    }

    const renderItem = ({ item }: { item: IOrganisation }) => (
      <RenderItem
        data={item}
        isSelected={item.id === selectedItemId}
        onSelect={() => onSelectHandler(item)}
      />
    )

    const keyExtractor = useCallback((item) => item.id?.toString(), [])

    useEffect(() => {
      const index = data.findIndex((el) => el.id === selectedItemId)
      if (index !== -1) {
        listRef.current?.scrollToIndex({ index, viewPosition: 0.5 })
      }
    }, [data, selectedItemId])

    return (
      <View style={styles.container}>
        <FlatList
          key={`organizationList_${isLoading}`}
          ref={listRef}
          data={data}
          style={styles.flatList}
          ListEmptyComponent={isLoading ? Preloader : EmptyDataTitle}
          contentContainerStyle={styles.contentContainer}
          ItemSeparatorComponent={Divider}
          keyExtractor={keyExtractor}
          renderItem={renderItem}
        />
      </View>
    )
  },
)

const styles = StyleSheet.create({
  container: {
    height: '100%',
  },
  flatList: {
    height: '100%',
  },
  contentContainer: {
    ...UIStyles.paddingH16,
    paddingBottom: 50,
    flexGrow: 1,
  },
})
