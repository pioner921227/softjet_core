import React, {useCallback, useMemo, useRef, useState} from 'react';
import {findNodeHandle, FlatList, ViewStyle} from 'react-native';
import {IAddressItemResponse} from '../../../api/types';
import {Divider} from '../../../../../components/divider';
import {ListItemWithDelete} from '../../../../../components/list-item/list-item-with-delete';
import {useDispatch} from 'react-redux';
import {AsyncActionsDelivery} from '../../../store/async-actions-delivery';
import {ActionDelivery} from '../../../store/action-delivery';
import Animated, {
  Transition,
  Transitioning,
  TransitioningView,
  useAnimatedScrollHandler,
} from 'react-native-reanimated';

interface IAddressListWithCheckedItem {
  items: IAddressItemResponse[];
  contentContainerStyle?: ViewStyle;
  onSelect: (item: IAddressItemResponse) => void;
  HeaderComponent?: React.FC;
  onScroll: ReturnType<typeof useAnimatedScrollHandler>;
}

const FlatListAnimated = Animated.createAnimatedComponent(FlatList);

export const AddressListWithCheckedItem: React.FC<IAddressListWithCheckedItem> = React.memo(
  ({items, contentContainerStyle, onSelect, HeaderComponent, onScroll}) => {
    const [selectedItem, setSelectedItem] = useState(-1);
    const dispatch = useDispatch();

    const ref = useRef<TransitioningView | null>(null);

    const onSelectHandler = useCallback((item: IAddressItemResponse) => {
      setSelectedItem(item.id);
      onSelect(item);
    }, []);

    const createTitle = (item: IAddressItemResponse) => {
      return `${item.city}  ${item.street}  ${item.house_num}  ${item.flat_num}`;
    };

    const onDelete = useCallback(
      (id: number) => {
        const newAddressList = items.map(el => {
          if (el.id === id) {
            el.hidden = true;
          }
          return el;
        });
        dispatch(AsyncActionsDelivery.deleteUserAddress({id}));
        dispatch(ActionDelivery.setAddressList(newAddressList));
      },
      [dispatch, items],
    );

    const keyExtractor = useCallback(item => item.id?.toString(), []);

    const renderItem = useCallback(
      ({item}: {item: IAddressItemResponse}) => {
        const onSelect = () => {
          onSelectHandler(item);
        };

        return item.hidden ? null : (
          <>
            <ListItemWithDelete
              id={item.id}
              onDelete={onDelete}
              title={createTitle(item)}
              isSelected={selectedItem === item.id}
              onSelect={onSelect}
            />
            <Divider />
          </>
        );
      },
      [onDelete, onSelectHandler, selectedItem],
    );

    const contentStyle = useMemo(() => {
      return {
        paddingBottom: 70,
        ...contentContainerStyle,
      };
    }, [contentContainerStyle]);

    if (!items || items.length === 0) {
      return null;
    }

    const transitions = (
      <Transition.Together>
        <Transition.Out type={'scale'} />
        <Transition.Change interpolation={'easeInOut'} />
      </Transition.Together>
    );

    return (
      <Transitioning.View ref={ref} transition={transitions}>
        <FlatListAnimated
          scrollEventThrottle={16}
          onScroll={onScroll}
          data={items}
          ListHeaderComponent={HeaderComponent}
          contentContainerStyle={contentStyle}
          keyExtractor={keyExtractor}
          //@ts-ignore
          renderItem={renderItem}
          showsVerticalScrollIndicator={false}
        />
      </Transitioning.View>
    );
  },
);
