import {Linking, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import {IOrganisationListRenderItemProps} from './organisation-list';
import {CellPhoneIcon} from '../../../../../components/icons/cell-phone-icon';
import {Config} from '../../../../../config';
import {ListItem} from '../../../../../components/list-item/list-item';
import Animated, {
  interpolate,
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated';

const {Color, UIStyles, RADIUS} = Config;

export const OrganisationListItemContact: React.FC<IOrganisationListRenderItemProps> = React.memo(
  ({data, onSelect, isSelected}) => {
    const [isOpened, setIsOpened] = useState(false);
    const onPressCall = () => {
      setIsOpened(state => !state);
      Linking.openURL(`tel:${data.phone}`);
    };

    const animValue = useSharedValue(50);

    const animatedWidth = useAnimatedStyle(() => {
      return {
        width: animValue.value,
        // position:'absolute',
        justifyContent: 'space-between',
        // right:0
      };
    });

    const size = useAnimatedStyle(() => {
      return {
        transform: [
          {scale: interpolate(animValue.value, [50, 70, 150], [0, 0, 1])},
        ],
        flexDirection: 'row',
      };
    });

    useEffect(() => {
      if (isOpened) {
        animValue.value = withTiming(50, {duration: 300});
      } else {
        animValue.value = withTiming(150, {duration: 300});
      }
    }, [isOpened]);

    return (
      <TouchableOpacity onPress={onSelect} style={styles.container}>
        <View>
          <ListItem
            containerStyle={{paddingBottom: 7.5}}
            title={data.title}
            onSelect={onSelect}
            isSelected={isSelected}
          />
          <Text style={styles.subTitle}>{data.worktime}</Text>
        </View>
        <TouchableOpacity
          onPress={onPressCall}
          style={styles.callButtonWrapper}>
          <CellPhoneIcon fill={Color.PRIMARY} />
        </TouchableOpacity>
      </TouchableOpacity>
    );
  },
);

const styles = StyleSheet.create({
  container: {
    ...UIStyles.flexRow,
    // paddingVertical:16,
    paddingBottom: 13.5,
  },
  subTitle: {
    color: Color.GREY_600,
  },
  callButtonWrapper: {
    // position: 'absolute',
    backgroundColor: Color.BACKGROUND_COLOR,
    borderRadius: RADIUS.MEDIUM,
    width: 32,
    height: 32,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
