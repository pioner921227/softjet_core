import React from 'react';
import {IThemedButton} from '../../../../components/buttons/themed-button';
import {useTypedSelector} from '../../../../system/hooks/use-typed-selector';
import {IOrderTypesItem} from '../../types/types';
import {IBottomSheetModalComponentProps} from '../../../../components/bottom-sheet-menu/bottom-sheet-modal-provider';
import {BottomSheetModalContentWithTitle} from '../../../../components/bottom-sheet-menu/bottom-sheet-modal/bottom-sheet-modal-content-with-title';

interface ICreateNewAddressModal extends IBottomSheetModalComponentProps {
  onPress: (type: IOrderTypesItem) => void;
}

export const BSMContentCreateNewAddress: React.FC<ICreateNewAddressModal> = ({
  onPress,
}) => {
  const orderTypes = useTypedSelector(state => state.delivery.orderTypes);

  const buttons: IThemedButton[] = orderTypes.reduce<IThemedButton[]>(
    (acc, el) => {
      if (el.show_on_main) {
        acc.push({
          label: el.description,
          onPress: () => onPress(el),
          rounded: true,
          modifier: 'bordered',
        });
      }
      return acc;
    },
    [],
  );

  return (
    <BottomSheetModalContentWithTitle
      title={'Создадим адрес?'}
      description={'Хотим убедиться, что на ваш адрес \n доступна доставка'}
      buttons={buttons}
    />
  );
};
