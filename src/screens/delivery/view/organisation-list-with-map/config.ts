import {INextButtonConfig} from '../../config';
import {IOrganisation} from '../../types/types';

interface IOrganisationListWithMapConfig extends INextButtonConfig<string> {
  onSelectItem: null | ((item: IOrganisation) => void);
}

export const organisationListWithMapConfig: IOrganisationListWithMapConfig = {
  nextButtonLabel: 'Выбрать ресторан',
  onPressNextButton: null,
  onSelectItem: null,
};
