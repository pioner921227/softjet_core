import { useNavigation } from '@react-navigation/native'
import { StackNavigationProp, StackScreenProps } from '@react-navigation/stack'
import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import { StyleSheet, useColorScheme, View } from 'react-native'
import MapView, { LatLng, Marker } from 'react-native-maps'
import { useModal } from '../../../../components'
import { BottomSheetContactList } from '../../../../components/bottom-sheet-menu/bottom-sheet-menu/bottom-sheet-contact-list'
import { BottomSheetOrganisationListWithButton } from '../../../../components/bottom-sheet-menu/bottom-sheet-menu/bottom-sheet-organisation-list-with-button'
import { ButtonBackCircle } from '../../../../components/buttons/button-back-circle'
import { ButtonGoToCurrentPosition } from '../../../../components/buttons/button-go-to-current-position'
import { IOnPressMarker } from '../../../../components/map/custom-marker'
import { Routes } from '../../../../navigation/config/routes'
import { TRootStackParamList } from '../../../../navigation/types/types'
import { convertCoordinatesToNumber } from '../../../../system/helpers/convert-coordinates-to-number'
import { FocusAwareStatusBar } from '../../../../system/helpers/focus-aware-status-bar'
import { getCurrentPositions } from '../../../../system/helpers/get-current-positions'
import { useTypedDispatch } from '../../../../system/hooks/use-typed-dispatch'
import { useTypedSelector } from '../../../../system/hooks/use-typed-selector'
import {
  getCurrentCityId,
  getCurrentOrderTypeId,
  getOrganisations,
} from '../../../../system/selectors/selectors'
import { ActionDelivery } from '../../store/action-delivery'
import { AsyncActionsDelivery } from '../../store/async-actions-delivery'
import { IOrganisation } from '../../types/types'
import { MapComponent } from '../components/map/map-component'
import { organisationListWithMapConfig } from './config'

export enum BottomSheetMenuType {
  Contacts = 'Contacts',
  Organisations = 'Organisations',
}

type TProps = StackScreenProps<
  TRootStackParamList,
  typeof Routes.SelectOrganisationInMap
>

type TNavigation = StackNavigationProp<
  TRootStackParamList,
  Routes.TabRootScreen
>

const emptyArray: IOrganisation[] = []

export const OrganisationListWithMap: React.FC<TProps> = React.memo(
  ({ route }) => {
    const mapRef = useRef<MapView>(null)
    const [currentLocation, setCurrentLocation] = useState<LatLng | null>(null)
    const [selectedOrganisationId, setSelectedOrganisationId] = useState(0)
    const { navigate } = useNavigation()
    const { show, close } = useModal()

    const theme = useColorScheme()

    const { reset } = useNavigation<TNavigation>()

    const [isLoading, setIsLoading] = useState(false)
    const dispatch = useTypedDispatch()

    const organisations = useTypedSelector(getOrganisations)
    const currentCityId = useTypedSelector(getCurrentCityId)
    const currentOrderTypeId = useTypedSelector(getCurrentOrderTypeId)
    const markers = useRef<{ [key: number]: Marker }>({})
    const bottomSheetMenuType = route.params?.bottomSheetMenuType

    const onSuccessHandler = () => {
      dispatch(
        ActionDelivery.setCurrentOrganisation(
          +selectedOrganisationId.toString(),
        ),
      )

      if (organisationListWithMapConfig.onPressNextButton) {
        organisationListWithMapConfig.onPressNextButton(
          selectedOrganisationId.toString(),
        )
        return
      }
      if (route.params?.successRoute) {
        navigate(route.params?.successRoute)
        return
      }

      requestAnimationFrame(() => {
        reset({
          index: 0,
          routes: [
            {
              name: Routes.TabRootScreen,
              params: {
                screen: Routes.Catalog,
                params: { ignoreModal: true, ignoreLoadCartData: true },
              },
            },
          ],
        })
      })
    }

    const onPressButton = useCallback(async () => {
      // getOrganisationsCommentHandler();
      const res = await dispatch(
        AsyncActionsDelivery.getOrganisationComment(selectedOrganisationId),
      )
      if (!res.work_status) {
        show({
          title: res.message_title,
          description: res.message,
          buttons: [
            {
              label: 'Отмена',
              onPress: close,
              styles: { modifier: 'bordered' },
            },
            {
              label: 'Понятно',
              onPress: () => {
                close()
                onSuccessHandler()
              },
            },
          ],
          buttonsDirection: 'row',
        })
        return
      }

      onSuccessHandler()
    }, [route.params?.successRoute, selectedOrganisationId])

    const animateToRegion = useCallback((props: LatLng) => {
      requestAnimationFrame(() => {
        mapRef.current?.animateToRegion(
          {
            ...props,
            longitudeDelta: 0.15,
            latitudeDelta: 0.15,
          },
          1000,
        )
      })
    }, [])

    const firstPositionDisplay = async (data: LatLng) => {
      const currentCoordinates = await getCurrentPositions()
      if (currentCoordinates) {
        setCurrentLocation(currentCoordinates)
      } else {
        animateToRegion(data)
      }
    }

    const onSelectListItem = useCallback((item: IOrganisation) => {
      if (organisationListWithMapConfig.onSelectItem) {
        organisationListWithMapConfig.onSelectItem(item)
        return
      }

      animateToRegion(convertCoordinatesToNumber(item.GPS))
      setTimeout(() => {
        markers.current[item.id]?.showCallout()
      }, 500)

      setSelectedOrganisationId(item.id)
    }, [])

    const animateToCurrentPosition = useCallback(async () => {
      const currentCoordinates = await getCurrentPositions()
      if (currentCoordinates) {
        animateToRegion(currentCoordinates)
      }
    }, [animateToRegion])

    const addMarkerRef = useCallback((ref: Marker | null, id: number) => {
      if (ref) {
        markers.current[id] = ref
      }
    }, [])

    useEffect(() => {
      if (currentCityId && currentOrderTypeId) {
        ;(async () => {
          if (route.params.disableLoadOrganisations) {
            return
          }
          setIsLoading(true)
          await dispatch(
            AsyncActionsDelivery.getOrganisations({
              city_id: +currentCityId,
              order_type: +currentOrderTypeId,
            }),
          )
          setIsLoading(false)
        })()
      }
    }, [])

    useEffect(() => {
      if (
        organisations?.length &&
        organisations[0].GPS.longitude &&
        organisations[0].GPS.latitude
      ) {
        firstPositionDisplay(convertCoordinatesToNumber(organisations[0]?.GPS))
      } else {
        animateToCurrentPosition()
      }
    }, [])

    const { longitude = 55.7558, latitude = 37.6173 } = useMemo(() => {
      return organisations?.[0]?.GPS || {}
    }, [organisations])

    const initialRegion = useMemo(
      () => ({
        latitude: +latitude,
        longitude: +longitude,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      }),
      [latitude, longitude],
    )

    const onPressMarker: IOnPressMarker = useCallback(
      (gps: LatLng, id: number) => {
        animateToRegion(convertCoordinatesToNumber(gps))
        setSelectedOrganisationId(id)
      },
      [],
    )

    return (
      <View style={styles.container}>
        <ButtonBackCircle />
        <FocusAwareStatusBar
          barStyle={theme === 'light' ? 'dark-content' : 'light-content'}
        />
        <MapComponent
          organisations={organisations}
          initialRegion={initialRegion}
          addMarkerRef={addMarkerRef}
          onPressMarker={onPressMarker}
          ref={mapRef}
        />
        <ButtonGoToCurrentPosition onPress={animateToCurrentPosition} />
        {bottomSheetMenuType === BottomSheetMenuType.Contacts ? (
          <BottomSheetContactList
            selectedItemId={selectedOrganisationId}
            data={isLoading ? emptyArray : organisations}
            onSelectItem={onSelectListItem}
          />
        ) : (
          <BottomSheetOrganisationListWithButton
            selectedItemId={selectedOrganisationId}
            onPressButton={onPressButton}
            data={isLoading ? emptyArray : organisations}
            onSelectItem={onSelectListItem}
          />
        )}
      </View>
    )
  },
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map: {
    width: '100%',
    height: '60%',
  },
})
