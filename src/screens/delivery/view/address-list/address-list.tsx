import React, { useEffect, useState } from 'react';
import {
  InteractionManager,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { Config } from '../../../../config';
import { useTypedSelector } from '../../../../system/hooks/use-typed-selector';
import { useNavigation } from '@react-navigation/native';
import { Routes } from '../../../../navigation/config/routes';
import { AddressListWithCheckedItem } from '../components/organisation-list/address-list-with-checked-item';
import { ScreenHeaderTitleWithBackButton } from '../../../../components/screen-header/screen-header-title-with-back-button';
import { StackNavigationProp, StackScreenProps } from '@react-navigation/stack';
import { TRootStackParamList } from '../../../../navigation/types/types';
import { ThemedButton } from '../../../../components/buttons/themed-button';
import { IAddressItemResponse } from '../../api/types';
import { ActionDelivery } from '../../store/action-delivery';
import { AsyncActionsDelivery } from '../../store/async-actions-delivery';
import { DELIVERY_TYPE } from '../../store/store-delivery';
import { addressListConfig } from './config';
import { useTypedDispatch } from '../../../../system/hooks/use-typed-dispatch';
import {
  getAddressList,
  getCities,
  getCurrentCityId,
  getToken,
} from '../../../../system/selectors/selectors';
import { useModal } from '../../../../components';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useIsLoading } from '../../../../system/hooks/use-is-loading';

import Animated, {
  interpolate,
  useAnimatedScrollHandler,
  useAnimatedStyle,
  useSharedValue,
  Extrapolate,
} from 'react-native-reanimated';
import { useLayout } from '../../../../system/hooks/use-layout';
import { useAndroidBackHandler } from '../../../../system/hooks/use-android-back-handler';
import { IModalButton } from '../../../../components/modal/modal-provider';

const { Color, UIStyles, RADIUS } = Config;

type TNavigation = StackNavigationProp<
  TRootStackParamList,
  Routes.AddressCreate
>;

type TProps = StackScreenProps<TRootStackParamList, Routes.LoginCode>;

export const AddressList: React.FC<TProps> = React.memo(({ navigation }) => {
  const userAddressList = useTypedSelector(getAddressList);
  const cities = useTypedSelector(getCities);
  const currentCityId = useTypedSelector(getCurrentCityId);
  const token = useTypedSelector(getToken);
  const { show, close } = useModal();
  const loading = useIsLoading();

  const dispatch = useTypedDispatch();
  const { reset } = useNavigation<TNavigation>();
  // const scrollY = useRef(new Animated.Value(0)).current;

  const animValue = useSharedValue(0);

  // const [headerHeight, setHeaderHeight] = useState(100);
  const { onLayout, values } = useLayout();

  const [
    selectedAddress,
    setSelectedAddress,
  ] = useState<IAddressItemResponse>();

  const onCreateNewAddress = () => {
    if (addressListConfig.onPressCreateNewAddress) {
      addressListConfig.onPressCreateNewAddress();
      return;
    }
    navigation.navigate(Routes.AddressCreate);
  };

  const onPressModalButton = () => {
    close();
    setTimeout(() => {
      reset({
        index: 0,
        routes: [
          {
            name: Routes.TabRootScreen,
            params: {
              screen: Routes.Catalog,
              params: { ignoreModal: true },
            },
          },
        ],
      });
    }, 100);
  };

  const onPressGoToMenu = async () => {
    if (selectedAddress?.id && currentCityId) {
      loading.setIsLoading(true);
      await dispatch(
        AsyncActionsDelivery.getOrganisations({
          city_id: +currentCityId,
          order_type: DELIVERY_TYPE.DELIVERY,
        }),
      );

      const res = await dispatch(
        AsyncActionsDelivery.getOrganisationByAddress({
          country: selectedAddress.country,
          city: selectedAddress.city,
          street: selectedAddress.street,
          house: selectedAddress.house_num,
          city_id: +currentCityId,
        }),
      );
      loading.setIsLoading(false);

      if (addressListConfig.onPressNextButton) {
        addressListConfig.onPressNextButton(res);
        return;
      }

      if (res?.success) {
        if (res?.organisation?.id_org) {
          dispatch(
            ActionDelivery.setCurrentOrganisation(res.organisation.id_org),
          );
        }
      }

      const getButtons = (): IModalButton[] => {
        if (res?.success) {
          if (res?.organisation?.work_status) {
            return [{label: 'Понятно', onPress: onPressModalButton}];
          }
          return [
            {label: 'Отмена', onPress: close, styles: {modifier: 'bordered'}},
            {label: 'Понятно', onPress: onPressModalButton},
          ];
        }
        return [{label: 'Понятно', onPress: close}];
      };

      show({
        title: res?.organisation?.title,
        description: res?.organisation?.description,
        buttons: getButtons(),
        buttonsDirection: 'row',
      });
    }
  };

  const animatedStyle = useAnimatedStyle(() => {
    return {
      position: 'absolute',
      width: '100%',
      alignSelf: 'center',
      zIndex: 2,
      opacity: interpolate(
        animValue.value,
        [0, values.height / 1.5],
        [1, 0],
        Extrapolate.CLAMP,
      ),
      transform: [
        {
          translateY: interpolate(
            animValue.value,
            [0, values.height],
            [0, -values.height],
            Extrapolate.CLAMP,
          ),
        },
      ],
    };
  });

  useEffect(() => {
    const currentCity = cities.find(el => el.title === selectedAddress?.city);
    if (currentCity) {
      dispatch(ActionDelivery.setCurrentCity(+currentCity.id));
    }
    if (selectedAddress) {
      dispatch(ActionDelivery.setCurrentAddress(selectedAddress));
      addressListConfig.onSelectItemCallback?.(selectedAddress);
    }
  }, [selectedAddress?.id]);

  const onScrollHandler = useAnimatedScrollHandler(event => {
    animValue.value = event.contentOffset.y;
  });

  useEffect(() => {
    if (token) {
      dispatch(AsyncActionsDelivery.getUserAddressList());
    }
  }, []);

  // const onLayout = useLayout(({height}) => {
  //   setHeaderHeight(height);
  // });

  useAndroidBackHandler();

  return (
    <SafeAreaView edges={['top']} style={UIStyles.flex}>
      <View style={styles.contentContainer}>
        <Animated.View style={animatedStyle} onLayout={onLayout}>
          <ScreenHeaderTitleWithBackButton
            style={{ paddingHorizontal: 0 }}
            title={addressListConfig.headerTitle}
          />
          <TouchableOpacity
            onPress={onCreateNewAddress}
            style={styles.addNewAddressWrapper}>
            <Text>Добавить адрес доставки</Text>
          </TouchableOpacity>
        </Animated.View>
        {userAddressList?.length > 0 ? (
          <AddressListWithCheckedItem
            contentContainerStyle={{ paddingTop: 120 }}
            onScroll={onScrollHandler}
            // reference={onScrollHandler}
            onSelect={setSelectedAddress}
            items={userAddressList}
          />
        ) : null}

        <ThemedButton
          isLoading={loading.isLoading}
          disabled={!selectedAddress || loading.isLoading}
          wrapperStyle={styles.buttonWrapper}
          rounded={true}
          label={addressListConfig.nextButtonLabel}
          onPress={onPressGoToMenu}
        />
      </View>
    </SafeAreaView>
  );
});

const styles = StyleSheet.create({
  contentContainer: {
    backgroundColor: '#fff',
    flex: 1,
    ...UIStyles.paddingH16,
    overflow: 'hidden',
    // paddingTop: 16,
  },
  buttonWrapper: {
    paddingBottom: 15,
    // marginBottom: 10,
    backgroundColor: Color.WHITE,
  },
  addNewAddressWrapper: {
    borderWidth: 1,
    borderColor: Color.GREY_100,
    borderRadius: RADIUS.MEDIUM,
    paddingVertical: 19.5,
    ...UIStyles.paddingH16,
    ...UIStyles.flexRow,
  },
  header: {
    position: 'absolute',
    flex: 1,
    zIndex: 2,
    width: '100%',
    alignSelf: 'center',
  },
});
