import {
  IAddressItemResponse,
  IGetOrganisationByAddressResponse,
} from '../../api/types';
import {INextButtonConfig} from '../../config';

interface IAddressListConfig
  extends INextButtonConfig<IGetOrganisationByAddressResponse> {
  onSelectItemCallback: null | ((item?: IAddressItemResponse) => void);
  onPressCreateNewAddress: null | (() => void);
  headerTitle: string;
}

export const addressListConfig: IAddressListConfig = {
  nextButtonLabel: 'Перейти в меню',
  onPressNextButton: null,
  onSelectItemCallback: null,
  onPressCreateNewAddress: null,
  headerTitle: 'Адрес доставки',
};
