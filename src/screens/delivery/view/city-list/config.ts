import {INextButtonConfig} from '../../config';

interface ICityListConfig extends INextButtonConfig<number> {
  headerTitle: string;
}

export const cityListConfig: ICityListConfig = {
  nextButtonLabel: 'Сохранить',
  onPressNextButton: null,
  headerTitle: 'Выберите город',
};
