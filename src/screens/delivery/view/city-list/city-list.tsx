import React, { useCallback, useEffect, useMemo, useState } from "react";
import { FlatList, SafeAreaView, StyleSheet, View } from "react-native";
import { ScreenHeaderWithTitle } from "../../../../components/screen-header/screen-header-with-title";
import { useDispatch } from "react-redux";
import { useTypedSelector } from "../../../../system/hooks/use-typed-selector";
import { Config } from "../../../../config";
import { Divider } from "../../../../components/divider";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { ThemedButton } from "../../../../components/buttons/themed-button";
import { BSMContentCreateNewAddress } from "../components/bsm-content-create-new-address";
import { ActionDelivery } from "../../store/action-delivery";
import { useBottomSheetMenu } from "../../../../components/bottom-sheet-menu/bottom-sheet-modal-provider";
import {
  CompositeNavigationProp,
  useNavigation,
} from "@react-navigation/native";
import { StackNavigationProp, StackScreenProps } from "@react-navigation/stack";
import { TRootStackParamList } from "../../../../navigation/types/types";
import { Routes } from "../../../../navigation/config/routes";
import { IOrderTypesItem } from "../../types/types";
import { selectOrderTypeModalConfig } from "../../config";
import { cityListConfig } from "./config";
import { ListItemWithChecked } from "../../../../components/list-item/list-item-with-checked";
import {
  getAddressList,
  getCities,
  getCurrentCity,
  getOrderTypes,
  getOrganisations,
} from "../../../../system/selectors/selectors";
import { ScreenHeaderTitleWithBackButton } from "../../../../components/screen-header/screen-header-title-with-back-button";
import Animated, {
  Extrapolate,
  interpolate,
  useAnimatedScrollHandler,
  useAnimatedStyle,
  useSharedValue,
} from "react-native-reanimated";
import { useLayout } from "../../../../system/hooks/use-layout";
import { adaptiveSize } from "../../../../system/helpers/responseive-font-size";
import { AsyncActionsDelivery } from "../../store/async-actions-delivery";
import { useTypedDispatch } from "../../../../system/hooks/use-typed-dispatch";
import { useAndroidBackHandler } from "../../../../system/hooks/use-android-back-handler";

const { UIStyles } = Config;

export type TNavigationPropsOrderTypeModal = CompositeNavigationProp<
  StackNavigationProp<TRootStackParamList, Routes.SelectOrganisationInMap>,
  StackNavigationProp<TRootStackParamList, Routes.AddressCreate>
>;

type TNavigationProps = StackScreenProps<
  TRootStackParamList,
  Routes.SelectCity
>;

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

export const CityList: React.FC<TNavigationProps> = React.memo(
  ({ route, navigation }) => {
    const dispatch = useTypedDispatch();
    const [selectedCityId, setSelectedCityId] = useState<number | null>(null);
    const currentCity = useTypedSelector(getCurrentCity);
    const addressList = useTypedSelector(getAddressList);
    const cities = useTypedSelector(getCities);
    const orderTypes = useTypedSelector(getOrderTypes);
    const insets = useSafeAreaInsets();
    // const [headerHeight, setHeaderHeight] = useState(20);
    const animValue = useSharedValue(0);

    const { show, close } = useBottomSheetMenu();

    const onSelectOrderType = (type: IOrderTypesItem) => {
      dispatch(ActionDelivery.setCurrentOrderType(+type?.id));
      close();
      setTimeout(async () => {
        if (type?.isRemoted) {
          selectOrderTypeModalConfig.routes.online(
            addressList?.length > 0 ? Routes.AddressList : Routes.AddressCreate
          );
        } else {
          if (selectedCityId) {
            const organisations = await dispatch(
              AsyncActionsDelivery.getOrganisations({
                city_id: selectedCityId,
                order_type: +type.id,
              })
            );

            if (organisations.orgs && organisations.orgs.length === 1) {
              dispatch(
                ActionDelivery.setCurrentOrganisation(organisations?.orgs[0].id)
              );
              navigation.reset({
                index: 0,
                routes: [
                  {
                    name: Routes.TabRootScreen,
                    params: {
                      screen: Routes.Catalog,
                      params: {
                        ignoreModal: true,
                      },
                    },
                  },
                ],
              });
              return;
            }
          }
          selectOrderTypeModalConfig.routes.offline();
        }
      }, 100);
    };

    const snapPoints = useMemo(() => {
      const count = orderTypes.filter((el)=>el.show_on_main).length
      return [100 + count * adaptiveSize(60)];
    }, [orderTypes]);

    const onPressSave = () => {
      if (selectedCityId) {
        if (cityListConfig.onPressNextButton) {
          cityListConfig.onPressNextButton(selectedCityId);
        } else {
          show({
            Component: () => (
              <BSMContentCreateNewAddress
                onPress={onSelectOrderType}
                close={close}
              />
            ),
            snapPoints: snapPoints,
            dismissOnPanDown: selectOrderTypeModalConfig.dismissOnPanDown,
          });
        }
        dispatch(ActionDelivery.setCurrentCity(+selectedCityId));
      }
    };

    useEffect(() => {
      if (cities.length > 0) {
        setSelectedCityId(currentCity?.id || cities[0]?.id);
      }
    }, [cities]);

    useEffect(() => {
      if (cities.length === 1) {
        onPressSave();
      }
    }, [selectedCityId]);

    const keyExtractor = useCallback((item) => item?.id?.toString(), []);

    const RenderItem = useCallback(
      ({ item }) => {
        const onPress = () => {
          setSelectedCityId(item?.id);
        };

        return (
          <ListItemWithChecked
            isSelected={item?.id === selectedCityId}
            onSelect={onPress}
            title={item?.title}
          />
        );
      },
      [selectedCityId]
    );

    const { onLayout, values } = useLayout();

    const onScroll = useAnimatedScrollHandler((event) => {
      animValue.value = event.contentOffset.y;
    });

    const translateYStyle = useAnimatedStyle(() => {
      return {
        top: insets.top + 10,
        width: "100%",
        zIndex: 2,
        position: "absolute",
        opacity: interpolate(
          animValue.value,
          [0, values.height / 1.5],
          [1, 0],
          Extrapolate.CLAMP
        ),
        transform: [
          {
            translateY: interpolate(
              animValue.value,
              [0, values.height],
              [0, -values.height],
              Extrapolate.CLAMP
            ),
          },
        ],
      };
    });

    const contentContainerStyle = useMemo(() => {
      return {
        ...styles.contentContainerList,
        paddingTop: values.height + 20,
        paddingBottom: 50,
      };
    }, [values.height]);

    useAndroidBackHandler(close);

    return (
      <SafeAreaView style={UIStyles.flex}>
        <Animated.View style={translateYStyle} onLayout={onLayout}>
          {route.params?.enableBackButton ? (
            <ScreenHeaderTitleWithBackButton title="Выберите город" />
          ) : (
            <ScreenHeaderWithTitle>
              {cityListConfig.headerTitle}
            </ScreenHeaderWithTitle>
          )}
        </Animated.View>
        <View style={styles.listWrapper}>
          <AnimatedFlatList
            onScroll={onScroll}
            scrollEventThrottle={16}
            contentContainerStyle={contentContainerStyle}
            ItemSeparatorComponent={Divider}
            data={cities}
            //@ts-ignore
            renderItem={RenderItem}
            keyExtractor={keyExtractor}
          />
        </View>
        <ThemedButton
          rounded={true}
          disabled={selectedCityId === null}
          wrapperStyle={StyleSheet.flatten([
            styles.buttonContainer,
            { marginBottom: -insets.bottom + 10 },
          ])}
          label={cityListConfig.nextButtonLabel}
          onPress={onPressSave}
        />
      </SafeAreaView>
    );
  }
);

const styles = StyleSheet.create({
  buttonContainer: {
    backgroundColor: Config.Color.WHITE,
    paddingBottom: 10,
    ...UIStyles.paddingH16,
  },
  listWrapper: {
    ...UIStyles.paddingH16,
    ...UIStyles.flex,
  },
  contentContainerList: {
    ...UIStyles.paddingV16,
    paddingBottom: 10,
  },
});
