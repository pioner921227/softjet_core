import React, {useRef} from 'react';
import {SafeAreaView, ScrollView, StyleSheet, Text, View} from 'react-native';
import {ScreenHeaderTitleWithBackButton} from '../../../components/screen-header/screen-header-title-with-back-button';
import {textPolicy} from '../../login/view/text';
import {Config} from '../../../config';
import {useAndroidBackHandler} from '../../../system/hooks/use-android-back-handler';
import {ScrollableHeaderWithList} from '../../../components/scrollable-header-with-list';
import Animated from 'react-native-reanimated';
import {HtmlParseAndView, HtmlStyles} from '@react-native-html/renderer';
import {adaptiveSize} from '../../../system/helpers/responseive-font-size';
import {useTypedSelector} from '../../../system';
import {getAppInfo} from '../../../system/selectors/selectors';

const {Color, UIStyles} = Config;

export const DeliveryAndPayment: React.FC = React.memo(() => {
  useAndroidBackHandler();
  const scrollRef = useRef<Animated.ScrollView | null>(null);

  const appInfo = useTypedSelector(getAppInfo);

  return (
    <ScrollableHeaderWithList
      header={<ScreenHeaderTitleWithBackButton title="Доставка и оплата" />}
      list={
        <Animated.ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{paddingBottom: 50}}>
          <View style={styles.contentContainer}>
            {appInfo?.return_warranty ? (
              <HtmlParseAndView
                rawHtml={appInfo?.return_warranty}
                htmlStyles={htmlStyles}
                containerStyle={styles.container}
                //@ts-ignore
                scrollRef={scrollRef.current}
              />
            ) : null}
          </View>
        </Animated.ScrollView>
      }
    />
  );
});

const htmlStyles: HtmlStyles = {
  text: {
    fontSize: adaptiveSize(16),
    lineHeight: 18 * 1.4,
  },
  paragraph: {
    marginVertical: 10,
  },
  image: {
    marginVertical: 0,
  },
  list: {
    marginVertical: 5,
  },
  h1: {
    fontSize: 30,
    lineHeight: 30 * 1.4,
    marginTop: 10,
    fontWeight: '500',
  },
  h2: {
    fontSize: 26,
    lineHeight: 26 * 1.4,
    marginTop: 10,
    fontWeight: '500',
  },
  h3: {
    fontSize: 24,
    lineHeight: 24 * 1.4,
    marginTop: 10,
    fontWeight: '500',
  },
  listItem: {
    marginVertical: 2,
  },
  listItemContent: {},
};

const styles = StyleSheet.create({
  contentContainer: {
    flexGrow: 1,
    marginTop: 16,
    // ...Config.UIStyles.paddingH16,
  },
  text: {
    color: Color.BLACK,
    ...UIStyles.font15,
  },
  container: {
    marginTop: 20,
    ...UIStyles.flexGrow,
    ...UIStyles.paddingH16,
    paddingBottom: 20,
  },
  headerTitle: {
    fontSize: 18,
  },
});
