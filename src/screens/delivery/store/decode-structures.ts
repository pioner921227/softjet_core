import {IGetCitiesResponse, IOrderTypesItem, IOrganisation, OrderTypeEnum} from '../types/types';
import {T} from 'decodable-js/index';
import {IGetOrganisationsResponse} from '../api/types';

export const decodeGetCitiesStruct: IGetCitiesResponse = {
  towns: [{title: T.string, id: T.number, default_id_org: T.number}],
};

const struct: IOrganisation = {
  GPS: {
    latitude: T.string,
    longitude: T.string,
  },
  title: T.string,
  id: T.number,
  requisites: T.string,
  img: T.string,
  address: T.string,
  description: T.string,
  comment: T.string,
  phone: T.string,
  phone_name: T.string,
  worktime: T.string,
  stickers: [{
    id: T.number,
    title: T.string,
    color_bg: T.string,
    color_text : T.string,
  }],
  order_days: T.number,
  payments: [
    {
      id: T.number,
      name: T.string,
      is_online: T.boolean,
      is_cash: T.boolean,
    },
  ],
  deliveries: [
    {
      id: T.number,
      name: T.string,
    },
  ],
};

export const decodeGetOrganisationsResponseStruct: IGetOrganisationsResponse = {
  orgs: [struct],
};

export const decodeGetOrderTypeResponseStruct: IOrderTypesItem[] = [
  {
    description: T.string,
    id: T.number,
    isRemoted: T.boolean,
    logo: T.string,
    name: T.string,
    show_on_main: T.boolean,
    payment_methods: [T.number],
    show_in_modals: T.boolean,
    type: T.string as OrderTypeEnum,
  },
];
