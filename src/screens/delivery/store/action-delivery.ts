import {actionCreator} from '../../../system/root-store/action-creator';
import {IAddressItemResponse} from '../api/types';

export class ActionDelivery {
  static setCurrentCity = actionCreator<number>('DELIVERY/SET_CURRENT_CITY');

  static setCurrentOrderType = actionCreator<number>(
    'DELIVERY/SET_CURRENT_ORDER_TYPE',
  );
  static setCurrentOrganisation = actionCreator<number>(
    'DELIVERY/SET_CURRENT_ORGANISATION',
  );
  static setCurrentAddress = actionCreator<IAddressItemResponse>(
    'DELIVERY/SET_CURRENT_ADDRESS',
  );

  static setCurrentAddressWithId = actionCreator<number>(
    'DELIVERY/SET_CURRENT_ADDRESS_WITH_ID',
  );

  static setAddressList = actionCreator<IAddressItemResponse[]>(
    'DELIVERY/SET_ADDRESS_LIST',
  );
  // static setMinFreeDeliveryCost = actionCreator<number>(
  //   'DELIVERY/SET_MIN_FREE_DELIVERY_COST',
  // );
}
