import {ICityItem, IOrderTypesItem, IOrganisation} from '../types/types';
import {IAddressItemResponse} from '../api/types';

export enum DELIVERY_TYPE {
  SELF_DELIVERY_TYPE = 1,
  DELIVERY = 2,
}

export interface IStoreDelivery {
  isLoading: boolean;
  error: boolean;
  addressList: IAddressItemResponse[];
  cities: ICityItem[];
  organisations: IOrganisation[];
  orderTypes: IOrderTypesItem[];
  currentCity: ICityItem | null;
  currentAddress: IAddressItemResponse | null;
  currentOrganisation: IOrganisation | null;
  currentOrderType: IOrderTypesItem | null;
  polygonId: number | null;
}

export const initialStateDelivery: IStoreDelivery = {
  isLoading: false,
  error: false,
  addressList: [],
  cities: [],
  organisations: [],
  orderTypes: [],
  currentCity: null,
  currentOrganisation: null,
  currentOrderType: null,
  currentAddress: null,
  polygonId: null,
};
