import {
  ReducerBuilder,
  reducerWithInitialState,
} from 'typescript-fsa-reducers'
import { initialStateDelivery, IStoreDelivery } from './store-delivery'
import { AsyncActionsDelivery } from './async-actions-delivery'
import { Success } from 'typescript-fsa'
import { IGetCitiesResponse, IOrderTypesItem } from '../types/types'
import {
  IAddressItemResponse,
  IGetOrganisationByAddressResponse,
  IGetOrganisationsResponse,
} from '../api/types'
import { ActionDelivery } from './action-delivery'
import { ActionsLogin } from '../../login'
import { Decodable, T } from 'decodable-js'
import {
  decodeGetCitiesStruct,
  decodeGetOrderTypeResponseStruct,
  decodeGetOrganisationsResponseStruct,
} from './decode-structures'

const getCitiesHandlerStarted = (store: IStoreDelivery): IStoreDelivery => {
  return {
    ...store,
    isLoading: true,
    error: false,
  }
}

const getCitiesHandlerDone = (
  store: IStoreDelivery,
  { result }: Success<any, IGetCitiesResponse>,
): IStoreDelivery => {
  const decodeResult = Decodable(result, decodeGetCitiesStruct)

  return {
    ...store,
    isLoading: false,
    error: false,
    cities: decodeResult.towns,
  }
}

const getCitiesHandlerFailed = (store: IStoreDelivery): IStoreDelivery => {
  return {
    ...store,
    isLoading: false,
    error: true,
  }
}
//--------------------
const getOrganisationsHandlerStarted = (
  store: IStoreDelivery,
): IStoreDelivery => {
  return {
    ...store,
    isLoading: true,
    error: false,
  }
}

const getOrganisationsHandlerDone = (
  store: IStoreDelivery,
  { result }: Success<any, IGetOrganisationsResponse>,
): IStoreDelivery => {
  // const decodeResult = Decodable(result, decodeGetOrganisationsResponseStruct)

  return {
    ...store,
    isLoading: false,
    error: false,
    organisations: result.orgs,
  }
}

const getOrganisationsHandlerFailed = (
  store: IStoreDelivery,
): IStoreDelivery => {
  return {
    ...store,
    isLoading: false,
    error: true,
  }
}

const getUserAddressListHandlerStarted = (
  store: IStoreDelivery,
): IStoreDelivery => {
  return {
    ...store,
    error: false,
    isLoading: true,
  }
}

const saveUserAddressHandler = (
  store: IStoreDelivery,
  { result }: Success<any, IAddressItemResponse>,
): IStoreDelivery => {
  const addressIndex = store.addressList?.findIndex(
    (el) => el?.id === result?.id,
  )
  const isHas = addressIndex !== -1

  return {
    ...store,
    currentAddress: result?.delivery_exists ? result : store.currentAddress,
    addressList:
      result && !isHas ? [...store?.addressList, result] : store.addressList,
  }
}

const changeUserAddressHandler = (
  store: IStoreDelivery,
  { result }: Success<any, IAddressItemResponse>,
): IStoreDelivery => {
  const addressList = [...store.addressList]

  const addressListFiltered = addressList.filter((el) => el.id !== result.id)
  addressListFiltered.push(result)

  return {
    ...store,
    // currentAddress: result ? result : store.currentAddress,
    addressList: addressListFiltered,
  }
}

const getUserAddressListHandlerDone = (
  state: IStoreDelivery,
  { result }: Success<any, IAddressItemResponse[]>,
): IStoreDelivery => {
  return {
    ...state,
    error: false,
    isLoading: false,
    addressList: result ?? null,
  }
}

const getUserAddressListHandlerFailed = (
  state: IStoreDelivery,
): IStoreDelivery => {
  return {
    ...state,
    error: true,
    isLoading: false,
  }
}

const setCurrentCityHandler = (
  state: IStoreDelivery,
  id: number,
): IStoreDelivery => {
  const currentCity = state.cities.find((el) => el.id === id) ?? null
  return {
    ...state,
    currentCity,
  }
}

const setCurrentOrderTypeHandler = (
  state: IStoreDelivery,
  id: number,
): IStoreDelivery => {
  const currentOrderType = state.orderTypes?.find((el) => +el.id === id) ?? null
  return {
    ...state,
    currentOrderType,
  }
}

const setCurrentOrganisationHandler = (
  state: IStoreDelivery,
  id: number,
): IStoreDelivery => {
  const selectedOrganisation =
    state.organisations?.find((el) => +el.id === id) ?? null
  return {
    ...state,
    currentOrganisation: selectedOrganisation,
  }
}

const getOrderTypesHandlerDone = (
  state: IStoreDelivery,
  { result }: Success<any, IOrderTypesItem[]>,
): IStoreDelivery => {
  const decodeRes = Decodable(result, decodeGetOrderTypeResponseStruct)

  return {
    ...state,
    orderTypes: decodeRes,
  }
}

const getOrganisationByAddressHandlerDone = (
  state: IStoreDelivery,
  { result }: Success<any, IGetOrganisationByAddressResponse>,
): IStoreDelivery => {
  const decodeResult = Decodable(result, { polygon_id: T.number_$ })

  return {
    ...state,
    polygonId: decodeResult.polygon_id ?? null,
  }
}

const setAddressListHandler = (
  store: IStoreDelivery,
  addressList: IAddressItemResponse[],
): IStoreDelivery => {
  return {
    ...store,
    addressList,
  }
}

const setCurrentAddressHandler = (
  state: IStoreDelivery,
  address: IAddressItemResponse,
): IStoreDelivery => {
  return {
    ...state,
    currentAddress: address,
  }
}

const setCurrentAddressWithIdHandler = (
  state: IStoreDelivery,
  id: number,
): IStoreDelivery => {
  const currentAddress = state.addressList.find((el) => el.id === id)

  return {
    ...state,
    currentAddress: currentAddress ?? state.currentAddress,
  }
}

// const setMinFreeDeliveryCost = (
//   store: IStoreDelivery,
//   value: number,
// ): IStoreDelivery => {
//   return {
//     ...store,
//   };
// };

const resetHandler = (store: IStoreDelivery): IStoreDelivery => {
  return {
    ...store,
    currentAddress: null,
    addressList: [],
    polygonId: null,
  }
}

export const reducerDelivery: ReducerBuilder<IStoreDelivery> = reducerWithInitialState(
  initialStateDelivery,
)
  .case(ActionsLogin.logoutAccount, resetHandler)
  .case(AsyncActionsDelivery.getCities.async.started, getCitiesHandlerStarted)
  .case(AsyncActionsDelivery.getCities.async.done, getCitiesHandlerDone)
  .case(AsyncActionsDelivery.getCities.async.failed, getCitiesHandlerFailed)
  //--------------------
  .case(
    AsyncActionsDelivery.getOrganisations.async.started,
    getOrganisationsHandlerStarted,
  )
  .case(
    AsyncActionsDelivery.getOrganisations.async.done,
    getOrganisationsHandlerDone,
  )
  .case(
    AsyncActionsDelivery.getOrganisations.async.failed,
    getOrganisationsHandlerFailed,
  )
  //--------------------
  .cases(
    [
      AsyncActionsDelivery.saveNewAddress.async.started,
      AsyncActionsDelivery.getUserAddressList.async.started,
      AsyncActionsDelivery.deleteUserAddress.async.started,
      AsyncActionsDelivery.changeUserAddress.async.started,
    ],
    getUserAddressListHandlerStarted,
  )
  .case(
    // AsyncActionsDelivery.saveNewAddress.async.done,
    AsyncActionsDelivery.getUserAddressList.async.done,
    // AsyncActionsDelivery.deleteUserAddress.async.done,
    getUserAddressListHandlerDone,
  )
  .cases(
    [
      AsyncActionsDelivery.saveNewAddress.async.failed,
      AsyncActionsDelivery.getUserAddressList.async.failed,
      AsyncActionsDelivery.deleteUserAddress.async.failed,
      AsyncActionsDelivery.changeUserAddress.async.failed,
    ],
    getUserAddressListHandlerFailed,
  )
  .case(AsyncActionsDelivery.saveNewAddress.async.done, saveUserAddressHandler)
  .case(
    AsyncActionsDelivery.changeUserAddress.async.done,
    changeUserAddressHandler,
  )
  //--------------------
  .case(AsyncActionsDelivery.getOrderTypes.async.done, getOrderTypesHandlerDone)
  .case(
    AsyncActionsDelivery.getOrganisationByAddress.async.done,
    getOrganisationByAddressHandlerDone,
  )
  .case(ActionDelivery.setCurrentCity, setCurrentCityHandler)
  // .case(ActionDelivery.setSelectedOrderType, setSelectedOrderTypeIdHandler)
  .case(ActionDelivery.setCurrentOrderType, setCurrentOrderTypeHandler)
  .case(ActionDelivery.setCurrentOrganisation, setCurrentOrganisationHandler)
  .case(ActionDelivery.setCurrentAddress, setCurrentAddressHandler)
  .case(ActionDelivery.setAddressList, setAddressListHandler)
  .case(ActionDelivery.setCurrentAddressWithId, setCurrentAddressWithIdHandler)
