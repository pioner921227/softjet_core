export interface ICreateOrderDataRequest {
  bill: number;
  comment: string;
  payment_type: number;
  order_type: number;
  bonus: number;
  person: number;
  time_appointment: string;
  address_id?: number;
  id_org: number;
  polygon_id: number | null;
  enabled_delivery_sale?: boolean;
}

export interface ICreateOrderResponse {
  order_id: number;
  payment_url: string;
  cost:number;
}
