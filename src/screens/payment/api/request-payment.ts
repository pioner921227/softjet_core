import {ICreateOrderDataRequest, ICreateOrderResponse} from './types';
import {ApiService} from '../../../system';

export class RequestPayment {
  static createOrder(
    data: ICreateOrderDataRequest,
  ): Promise<ICreateOrderResponse> {
    return ApiService.post('/api/cart/createOrder', data);
  }
  static checkPayment(
    data: ICreateOrderDataRequest,
  ): Promise<ICreateOrderResponse> {
    return ApiService.post('/api/cart/checkPayment', data);
  }
}
