import WebView from 'react-native-webview';
import {BackHandler, SafeAreaView, StyleSheet} from 'react-native';
import React, {useCallback, useEffect, useMemo, useRef} from 'react';
import {StackNavigationProp, StackScreenProps} from '@react-navigation/stack';
import {TRootStackParamList} from '../../../navigation/types/types';
import {Routes} from '../../../navigation';
import {
  WebViewErrorEvent,
  WebViewNavigationEvent,
} from 'react-native-webview/lib/WebViewTypes';
import {AsyncActionPayment} from '../store/async-action-payment';
import {useTypedDispatch} from '../../../system/hooks/use-typed-dispatch';
import {Decodable, T} from 'decodable-js';
import {useNavigation} from '@react-navigation/native';
import {useTypedSelector} from '../../../system';
import {getAppInfo} from '../../../system/selectors/selectors';
import {SvgProps} from 'react-native-svg';
import {Config} from '../../../config';
import {ButtonBackCircle} from '../../../components/buttons/button-back-circle';
const {Color} = Config;

type TProp = StackScreenProps<TRootStackParamList, Routes.PaymentWebView>;
type TNavigation = StackNavigationProp<
    TRootStackParamList,
    Routes.PaymentFinish
    >;

export const PaymentWebView: React.FC<TProp> = React.memo(({route}) => {
  const dispatch = useTypedDispatch();
  const {reset, goBack, navigate} = useNavigation<TNavigation>();
  const oldUrl = useRef<string>('');
  const appInfo = useTypedSelector(getAppInfo);

  const onChangeUrl = useCallback(
      async ({nativeEvent}: WebViewNavigationEvent | WebViewErrorEvent) => {
        const {url} = nativeEvent || {};
        if (
            appInfo?.payment_urls?.fail_url &&
            url?.includes(appInfo.payment_urls.success_url)
        ) {
          const res = await dispatch(
              AsyncActionPayment.checkPayment(route.params.data),
          );
          const decodeRes = Decodable(res, {
            payment_url: T.string,
            order_id: T.number,
          });
          reset({
            index: 0,
            routes: [
              {
                name: Routes.PaymentFinish,
                params: {isOnline: true, orderId: decodeRes.order_id},
              },
            ],
          });
        } else if (
            appInfo?.payment_urls.fail_url &&
            url?.includes(appInfo.payment_urls.fail_url)
        ) {
          navigate(Routes.Payment, {isVisiblePaymentErrorModal: true});
          reset({
            index: 0,
            routes: [
              {
                name: Routes.Payment,
                params: {isVisiblePaymentErrorModal: true},
              },
            ],
          });
        }
      },
      [route.params.data, oldUrl.current],
  );

  const url = useMemo(() => ({uri: route.params.url}), [route.params.url]);

  useEffect(() => {
    const handler = BackHandler.addEventListener('hardwareBackPress', () => {
      goBack();
      return true;
    });
    return handler.remove;
  }, []);

  return (
      <SafeAreaView style={styles.container}>
        <ButtonBackCircle
            buttonStyle={styles.backButton}
            iconStyle={backButtonIcon}
        />
        <WebView
            bounces={false}
            onLoadStart={onChangeUrl}
            allowsBackForwardNavigationGestures={true}
            // onNavigationStateChange={onChangeUrl}
            source={url}
            style={styles.webView}
        />
      </SafeAreaView>
  );
});

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
  },
  webView: {
    flex: 1,
  },
  backButton: {
    backgroundColor: Color.GREY_800,
  },
});

const backButtonIcon: SvgProps = {
  fill: Color.WHITE,
};
