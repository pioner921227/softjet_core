import React, {useEffect} from 'react';
import {SafeAreaView, ScrollView, StyleSheet, Text, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import {ScreeHeaderTitleLeft} from '../../../components/screen-header/scree-header-title-left';
import {ImageRepository} from '../../../assets/image-repository';
import {Config} from '../../../config';
import {ThemedButton} from '../../../components/buttons/themed-button';
import {PaymentFinishOrderListItem} from './components/payment-finish-order-list-item';
import {useNavigation} from '@react-navigation/native';
import {Routes} from '../../../navigation';
import {StackNavigationProp, StackScreenProps} from '@react-navigation/stack';
import {TRootStackParamList} from '../../../navigation/types/types';
import {useTypedSelector} from '../../../system';
import {useDispatch} from 'react-redux';
import {ActionCart} from '../../cart';
import {getCart, getCartData} from '../../../system/selectors/selectors';
import {useClearCart} from '../../../system/hooks/use-clear-cart';
import {adaptiveSize} from '../../../system/helpers/responseive-font-size';

const {Color, UIStyles} = Config;

type TNavigationProps = StackNavigationProp<
  TRootStackParamList,
  Routes.Catalog
>;

type TProps = StackScreenProps<TRootStackParamList, Routes.PaymentFinish>;

export const PaymentFinish: React.FC<TProps> = ({route}) => {
  const {reset} = useNavigation<TNavigationProps>();
  const dispatch = useDispatch();
  const {selectedBonus} = useTypedSelector(getCart);
  const clearRemoteCart = useClearCart({clearLocalCart: false});

  const {isOnline, orderId} = route.params || {};
  const cartData = useTypedSelector(getCartData);
  const {
    total_cost = 0,
    discount_cost = 0,
    delivery_cost = 0,
    promocodes = '',
  } = cartData || {};

  const onPressNewOrder = () => {
    reset({
      index: 0,
      routes: [
        {
          name: Routes.TabRootScreen,
          params: {
            screen: Routes.Catalog,
            params: {ignoreModal: true, ignoreGetCatalog: true},
          },
        },
      ],
    });
  };

  useEffect(() => {
    clearRemoteCart();
    return () => {
      dispatch(ActionCart.clearLocalCart());
    };
  }, []);

  return (
    <SafeAreaView style={UIStyles.flex}>
      <ScrollView contentContainerStyle={{paddingBottom: adaptiveSize(20)}}>
        <ScreeHeaderTitleLeft title={'Мой заказ'} />
        <View style={styles.contentContainer}>
          <View style={styles.imageContainer}>
            <FastImage
              source={ImageRepository.PaymentFinishImage}
              style={styles.image}
              resizeMode={'contain'}
            />
            {isOnline ? (
              <>
                <Text style={styles.title}>
                  Заказ {orderId} успешно оплачен!
                </Text>
                <Text style={styles.description}>
                  Ожидайте курьера в скором времени
                </Text>
              </>
            ) : (
              <>
                <Text style={styles.title}>Заказ {orderId} оформлен!</Text>
                <Text style={styles.description}>
                  Пожалуйста, приготовьте к оплате{' '}
                  <Text style={styles.totalCostValue}>
                    {route.params?.price ?? 0}
                    ₽
                  </Text>
                </Text>
              </>
            )}
          </View>
          <Text style={styles.orderListTitle}>Что в заказе</Text>
          <View style={{width: '100%'}}>
            {cartData?.products?.map(el => {
              return (
                <PaymentFinishOrderListItem
                  key={el.id}
                  image={el.img_url}
                  title={el.title}
                  count={el.count}
                  price={el.price}
                  ingredients={'Соус'}
                />
              );
            })}
          </View>
          {delivery_cost ? (
            <Text style={styles.deliveryCost}>
              Доставка: {cartData?.delivery_cost} ₽
            </Text>
          ) : null}
        </View>
      </ScrollView>
      <ThemedButton
        modifier={'bordered'}
        wrapperStyle={{
          marginTop: 'auto',
          paddingHorizontal: 16,
          marginBottom: 10,
        }}
        rounded={true}
        label={'сделать новый заказ'}
        onPress={onPressNewOrder}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {},
  contentContainer: {
    ...UIStyles.paddingH16,
    flex: 1,
  },
  imageContainer: {
    paddingHorizontal: 34,
  },
  image: {
    width: '100%',
    height: 133,
    marginTop: 96,
  },
  title: {
    ...UIStyles.font24b,
    color: Color.SUCCESS,
    textAlign: 'center',
    marginTop: 12,
  },
  description: {
    ...UIStyles.font20b,
    color: Color.GREY_600,
    textAlign: 'center',
    marginTop: 12,
  },
  totalCostValue: {
    color: Color.DANGER,
  },
  orderListTitle: {
    textAlign: 'center',
    ...UIStyles.font20b,
    color: Color.BLACK,
    marginTop: adaptiveSize(40),
    marginBottom: adaptiveSize(16),
  },
  deliveryCost: {
    ...UIStyles.font15,
  },
});
