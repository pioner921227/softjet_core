import { useIsFocused, useNavigation } from "@react-navigation/native";
import { StackNavigationProp, StackScreenProps } from "@react-navigation/stack";
import { Decodable, T } from "decodable-js";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { ScrollView, StyleSheet, View } from "react-native";
import {
  SafeAreaView,
  useSafeAreaInsets,
} from "react-native-safe-area-context";
import { KeyboardAvoidingScroll, useModal } from "../../../../components";
import { ThemedButton } from "../../../../components/buttons/themed-button";
import { Divider } from "../../../../components/divider";
import { NotAuthContent } from "../../../../components/not-auth-content";
import { IRadioButtonData } from "../../../../components/radio-button/radio-button";
import { ScreenHeaderTitleWithBackButton } from "../../../../components/screen-header/screen-header-title-with-back-button";
import { Config } from "../../../../config";
import { Routes } from "../../../../navigation";
import { TRootStackParamList } from "../../../../navigation/types/types";
import { useTypedSelector } from "../../../../system";
import { productDeclensionWord } from "../../../../system/helpers/product-declension-word";
import { useAndroidBackHandler } from "../../../../system/hooks/use-android-back-handler";
import { useInput } from "../../../../system/hooks/use-input";
import { useTypedDispatch } from "../../../../system/hooks/use-typed-dispatch";
import {
  getCart,
  getCurrentAddress,
  getCurrentOrderType,
  getCurrentOrganisation,
  getPolygonId,
  getToken,
} from "../../../../system/selectors/selectors";
import { AsyncActionPayment } from "../../store/async-action-payment";
import { PaymentMethodsRadioButtons } from "../components/payment-methods-radio-buttons";
import { PaymentRowWithInput } from "../components/payment-row-with-input";
import { PaymentValueRow } from "../components/payment-value-row";
import { PaymentsImagesRow } from "../components/payments-images-row";
import { paymentConfig } from "./config";

const { Color, UIStyles } = Config;

enum paymentMethodId {
  online = 1,
  currierWithCard = 4,
  currierWithCash = 3,
}

const paymentArray: IRadioButtonData[] = [
  { id: paymentMethodId.online, label: "Онлайн оплата" },
  { id: paymentMethodId.currierWithCard, label: "Оплата картой курьеру" },
  { id: paymentMethodId.currierWithCash, label: "Оплата наличными курьеру" },
];

type TNavigation = StackNavigationProp<
  TRootStackParamList,
  Routes.PaymentFinish
>;

type TProps = StackScreenProps<TRootStackParamList, Routes.Payment>;

export const Payment: React.FC<TProps> = React.memo(({ route }) => {
  const { navigate, reset } = useNavigation<TNavigation>();
  const { bottom } = useSafeAreaInsets();
  const dispatch = useTypedDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const { show, close } = useModal();
  const isFocused = useIsFocused();

  const { comment, cartData, selectedBonus } = useTypedSelector(getCart);
  const payments = useTypedSelector(getCurrentOrganisation)?.payments;
  const currentAddressId = useTypedSelector(getCurrentAddress)?.id;
  const currentOrganisation = useTypedSelector(getCurrentOrganisation);
  const currentOrderType = useTypedSelector(getCurrentOrderType);
  const polygonId = useTypedSelector(getPolygonId);
  const token = useTypedSelector(getToken);
  const preOrderDate = useTypedSelector((state) => state.cart.preOrderDate);

  const [currentPaymentMethod, setCurrentPaymentMethod] = useState(
    payments?.[0]
  );
  const [selectedIndexPayment, setSelectedIndexPayment] = useState(0);

  const inputChangeFromPayment = useInput("");

  const paymentMethodsForRadioButtons = useMemo(() => {
    const arr = payments
      ?.map((el) => {
        return { id: el.id, label: el.name, disabled: false };
      })
      .filter((el) => currentOrderType?.payment_methods.includes(el.id));
    return arr ?? paymentArray;
  }, [currentOrderType?.payment_methods, payments]);

  const {
    discount_cost = 0,
    delivery_cost = 0,
    promocodes = "",
    actual_discount = 0,
    total_count = 0,
    total_cost = 0,
    self_pickup_discount = 0,
    original_total_cost = 0,
  } = cartData || {};

  const onSelectPaymentMethod = (
    paymentMethod: IRadioButtonData,
    index: number
  ) => {
    setSelectedIndexPayment(index);
    setCurrentPaymentMethod(payments?.[index]);
  };

  const onPressNext = async () => {
    if (
      currentOrderType?.id !== undefined &&
      currentOrganisation?.id !== undefined &&
      currentPaymentMethod?.id !== undefined
    ) {
      const data = {
        id_org: currentOrganisation.id,
        address_id: currentAddressId,
        bill: +inputChangeFromPayment.value,
        comment,
        payment_type: currentPaymentMethod.id,
        order_type: +(currentOrderType.id || 1),
        bonus: selectedBonus,
        person: 1,
        time_appointment: preOrderDate,
        polygon_id: polygonId,
        enabled_delivery_sale: route.params?.enabledDeliverySale ?? true,
      };

      setIsLoading(true);
      const res = await dispatch(AsyncActionPayment.createOrder(data));
      setIsLoading(false);

      const decodeRes = Decodable(res, {
        order_id: T.number,
        payment_url: T.string,
        cost: T.number_$,
      });

      if (paymentConfig.onPressNextButton) {
        paymentConfig.onPressNextButton({
          isOnline: currentPaymentMethod?.is_online,
          orderId: decodeRes.order_id,
          price: decodeRes.cost ?? 0,
        });
        return;
      }

      if (currentPaymentMethod.is_online && decodeRes.payment_url) {
        navigate(Routes.PaymentWebView, { url: decodeRes.payment_url, data });
      } else if (!currentPaymentMethod.is_online && decodeRes.order_id !== 0) {
        reset({
          index: 0,
          routes: [
            {
              name: Routes.PaymentFinish,
              params: {
                isOnline: currentPaymentMethod?.is_online,
                orderId: decodeRes.order_id,
                price: decodeRes.cost,
              },
            },
          ],
        });
      }
    }
  };

  const showPaymentErrorModal = () => {
    show({
      title: "Оплата не прошла",
      description:
        "Ошибка банка или недостаточно средств на карте. Проверьте баланс или попробуйте другую карту.",
      buttons: [{ label: "Понятно", onPress: close }],
    });
  };

  // const calculatePrice = () => {
  //   return (
  //     (promocodes
  //       ? discount_cost
  //       : route.params?.enabledDeliverySale
  //       ? total_cost
  //       : original_total_cost) +
  //     delivery_cost -
  //     selectedBonus
  //   );
  // };

  const baseCost = promocodes 
  ? discount_cost
  : route.params?.enabledDeliverySale
  ? total_cost
  : original_total_cost;

  const valuesCost = [baseCost, delivery_cost, -selectedBonus]
  const price = valuesCost.reduce((acc,el)=> acc + el, 0);



  const isPaidDelivery = useMemo(() => {
    return delivery_cost > 0;
  }, [delivery_cost]);

  const onPressLogin = useCallback(() => {
    reset({
      index: 0,
      routes: [
        { name: Routes.LoginStack, params: { screen: Routes.LoginPhone } },
      ],
    });
  }, []);

  useEffect(() => {
    if (isFocused && route.params?.isVisiblePaymentErrorModal) {
      showPaymentErrorModal();
    }
  }, [isFocused]);

  useAndroidBackHandler();

  if (!token) {
    return (
      <SafeAreaView>
        <NotAuthContent onPress={onPressLogin} buttonLabel="Хочу" />
      </SafeAreaView>
    );
  }

  return (
    <SafeAreaView edges={['top','bottom']} style={UIStyles.flex}>
        <ScreenHeaderTitleWithBackButton
          titleStyle={{ textAlign: "left" }}
          title={paymentConfig.headerTitle}
        />
        <KeyboardAvoidingScroll>
        <View style={styles.contentContainer}>
          <PaymentMethodsRadioButtons
            onSelectPaymentMethod={onSelectPaymentMethod}
            data={paymentMethodsForRadioButtons}
            selectedIndexPayment={selectedIndexPayment}
          />

          <PaymentValueRow
            titleStyle={UIStyles.font18b}
            title={"Ваш заказ"}
            value={`${total_count} ${productDeclensionWord(total_count)}`}
          />
          <Divider />
            <View style={{marginBottom: 20}}>
              <PaymentValueRow
                valueStyle={{ fontWeight: "bold" }}
                title={"Сумма заказа"}
                value={`${original_total_cost} ₽`}
              />
              {actual_discount ? (
                <PaymentValueRow
                  valueStyle={{fontWeight:"bold"}}
                  title={"Скидка по промокоду"}
                  value={`${actual_discount} ₽`}
                />
              ) : null}
              <PaymentValueRow
                valueStyle={{ fontWeight: "bold" }}
                title={"Скидка за самовывоз"}
                value={`${
                  route.params?.enabledDeliverySale ? self_pickup_discount ?? 0 : 0
                } ₽`}
              />
              <PaymentValueRow
                valueStyle={{ fontWeight: "bold" }}
                title={"Оплачено бонусами"}
                value={`${selectedBonus} ₽`}
              />
              {currentOrderType?.isRemoted && (
                <PaymentValueRow
                  title={"Доставка"}
                  valueStyle={{
                    fontWeight: "bold",
                    color: isPaidDelivery ? Color.BLACK : Color.SUCCESS,
                  }}
                  value={isPaidDelivery ? `${delivery_cost} ₽` : "Бесплатно"}
                />
              )}

              <PaymentValueRow
                valueStyle={{ fontWeight: "bold", color: Color.GREY_600 }}
                title={"К оплате"}
                value={`${price} ₽`}
              />

              {currentPaymentMethod?.is_cash ? (
                <PaymentRowWithInput {...inputChangeFromPayment} />
              ) : (
                <>
                  <PaymentsImagesRow />
                  {/*<Text style={styles.requisites}>Реквизиты получателя</Text>*/}
                </>
              )}
            </View>
          <ThemedButton
            isLoading={isLoading}
            disabled={isLoading}
            rounded={true}
            wrapperStyle={{ marginTop: "auto" }}
            label={`${
              currentPaymentMethod?.is_online ? "Оплатить" : "Оформить"
            } ${price} ₽`}
            onPress={onPressNext}
          />
        </View>
        </KeyboardAvoidingScroll>
    </SafeAreaView>
  );
});

const styles = StyleSheet.create({
  contentContainer: {
    flex: 1,
    marginTop: 24,
    ...UIStyles.paddingH16,
  },
  orderCountRow: {
    ...UIStyles.flexRow,
  },
  orderCountTitle: {
    ...UIStyles.font18b,
    color: Color.DARK,
  },
  orderCountValue: {
    ...UIStyles.font15,
    color: Color.DARK,
  },
  requisites: {
    textAlign: "center",
    ...UIStyles.font15,
    color: Color.GREY_400,
    marginTop: 16,
    marginBottom: 20,
  },
});
