import {INextButtonConfig} from '../../../delivery';

interface IPaymentConfig
  extends INextButtonConfig<{
    isOnline: boolean;
    orderId: number;
    price:number;
  }> {
  headerTitle: string;
}

export const paymentConfig: IPaymentConfig = {
  nextButtonLabel: '',
  onPressNextButton: null,
  headerTitle: 'Способ оплаты',
};
