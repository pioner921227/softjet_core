import {
  IRadioButtonData,
  RadioButtonCore,
} from '../../../../components/radio-button-new/radio-button-core';
import React from 'react';
import {Config} from '../../../../config';
import {StyleSheet} from 'react-native';
const {Color, UIStyles} = Config;

interface IPaymentMethodsRadioButtons {
  selectedIndexPayment: number;
  data: IRadioButtonData[];
  onSelectPaymentMethod: (item: IRadioButtonData, index: number) => void;
}

export const PaymentMethodsRadioButtons: React.FC<IPaymentMethodsRadioButtons> = React.memo(
  ({selectedIndexPayment, onSelectPaymentMethod, data}) => {
    return (
      <RadioButtonCore
        itemStyle={styles.paymentMethodItem}
        labelStyle={styles.paymentMethodLabel}
        borderCircleStyle={{color: Color.PRIMARY}}
        circleStyle={{color: Color.PRIMARY}}
        containerStyle={styles.radioButtons}
        selectedIndex={selectedIndexPayment}
        data={data}
        onSelect={onSelectPaymentMethod}
      />
    );
  },
);

const styles = StyleSheet.create({
  radioButtons: {
    marginBottom: 20,
  },
  paymentMethodLabel: {
    ...UIStyles.font15,
    color: Color.DARK,
    marginLeft: 12,
  },
  paymentMethodItem: {
    paddingVertical: 15.5,
  },
});
