import React, {useMemo} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import {Config, ImageRepository} from '../../../../config';
import {urlValidator} from '../../../../system/helpers/url-validator';
import {
  IProductOptionsChild,
} from '../../../cart/api/types';

const {Color, UIStyles} = Config;

interface IPaymentFinishOrderListItem {
  title: string;
  options: IProductOptionsChild;
  count: number;
  price: number;
  image: string;
}

// const additives: ICartOptionsListItem[] = [
//   {id: 0, title: 'Соус', count: 2},
//   {id: 1, title: 'Соус', count: 2},
//   {id: 2, title: 'Соус', count: 2},
//   {id: 3, title: 'Соус', count: 2},
// ];
//
// const size: ICartOptionsListItem[] = [{id: 0, title: 'Большая', count: 2}];
//
// const ingredients: ICartOptionsListItem[] = [
//   {id: 0, title: 'Соус', count: 2},
//   {id: 1, title: 'Масло', count: 2},
// ];

export const PaymentFinishOrderListItem: React.FC<IPaymentFinishOrderListItem> = React.memo(
    ({title, options, price, count, image}) => {
      const imageMemo = useMemo(
          () =>
              image
                  ? {uri: urlValidator(image)}
                  : ImageRepository.ProductImagePlaceholder,
          [image],
      );

      const {size = [], ingredients = [], additives = []} = options || {};

      return (
          <View style={styles.container}>
            <FastImage
                source={imageMemo}
                style={styles.image}
                resizeMode={'cover'}
            />
            <View style={styles.titleContainer}>
              <Text style={styles.title}>{title}</Text>

              {size?.[0]?.title ? (
                  <Text style={styles.description}>Размер: {size?.[0]?.title}</Text>
              ) : null}

              <View>
                <View style={styles.deletedIngredients}>
                  {ingredients?.map(el => {
                    return (
                        <Text key={el.id} style={styles.deletedIngredientTitle}>
                          -{el?.title}{' '}
                        </Text>
                    );
                  })}
                </View>
                <View style={styles.deletedIngredients}>
                  {additives?.map(el => {
                    return (
                        <Text key={el.id} style={styles.additives}>
                          +{el?.title} - {el.count}шт.{' '}
                        </Text>
                    );
                  })}
                </View>
              </View>
              <View style={styles.priceWrapper}>
                <Text style={styles.countValue}>{count} шт.</Text>
                <Text style={styles.priceValue}>{price} ₽.</Text>
              </View>
            </View>
          </View>
      );
    },
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 3.5,
    marginBottom: 12,
  },
  title: {
    ...UIStyles.font16b,
  },
  ingredients: {
    marginTop: 8,
  },
  titleContainer: {
    flex: 3,
    marginLeft: 16,
  },
  priceWrapper: {
    marginTop: 8,
    flexDirection: 'row',
  },
  countValue: {
    ...UIStyles.font15,
    color: Color.SUCCESS,
    marginRight: 10,
  },
  image: {
    width: 74,
    height: 74,
  },
  priceValue: {
    ...UIStyles.font15,
    color: Color.BLACK,
  },
  deletedIngredients: {
    marginTop: 5,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  deletedIngredientTitle: {
    color: Color.PRIMARY,
  },
  additives: {
    color: Color.SUCCESS,
  },
  description: {
    marginTop: 8,
    ...UIStyles.font13,
    color: Color.GREY_800,
  },
});
