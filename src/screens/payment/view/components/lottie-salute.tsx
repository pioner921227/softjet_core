import LottieView from 'lottie-react-native';
import React, {useEffect, useRef} from 'react';

interface ILottieSplashScreen {
  setIsFinished: (state: boolean) => void;
}

export const LottieSalute: React.FC<ILottieSplashScreen> = ({
  setIsFinished,
}) => {
  const ref = useRef<LottieView | null>(null);

  const onFinish = () => {
    setIsFinished(true);
  };

  useEffect(() => {
    ref.current?.play();
  }, []);

  return (
    <LottieView
      style={{zIndex: 10}}
      ref={ref}
      loop={false}
      onAnimationFinish={onFinish}
      resizeMode={'contain'}
      source={require('../../../../assets/salut.json')}
    />
  );
};
