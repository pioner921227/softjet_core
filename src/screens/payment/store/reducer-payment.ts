import {ReducerBuilder, reducerWithInitialState} from 'typescript-fsa-reducers';
import {initialStatePayment, IStorePayment} from './store-payment';

export const reducerCart: ReducerBuilder<IStorePayment> = reducerWithInitialState(
  initialStatePayment,
);
