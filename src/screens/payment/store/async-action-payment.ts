import {asyncActionCreator} from '../../../system';
import {ICreateOrderDataRequest, ICreateOrderResponse} from '../api/types';
import {RequestPayment} from '../api/request-payment';

export class AsyncActionPayment {
  static createOrder = asyncActionCreator<
    ICreateOrderDataRequest,
    ICreateOrderResponse,
    Error
  >('PAYMENT/CREATE_ORDER', RequestPayment.createOrder);
  static checkPayment = asyncActionCreator<
    ICreateOrderDataRequest,
    ICreateOrderResponse,
    Error
  >('PAYMENT/CHECK_PAYMENT', RequestPayment.checkPayment);
}
