import {IBannerItem, ICategoryItem, IProductItem} from '../types/types';

export interface IGetBannersDataRequest {
  id_org: number;
  order_type: number;
}

export interface IGetProductsRequest {
  id_org: number;
  order_type: number;
  page_size?: number;
}

export interface IGetProductsResponse {
  category_title: string;
  count_items: number;
  data: IProductItem[];
}

export interface IGetCategoriesDataRequest {
  id_org: number;
  order_type: number;
}

export interface IGetCategoriesResponse {
  data: ICategoryItem[];
}

export interface IGetHomepageDataResponse {
  promotions: IBannerItem[];
  products: IProductItem[];
  catalog_first: ICategoryItem[];
  catalog_second: ICategoryItem[];
}
