import { ApiService } from '../../../system/api/api-service'
import { ICatalogData, TGetBannerListResponse } from '../types/types'
import {
  IGetBannersDataRequest,
  IGetCategoriesDataRequest,
  IGetCategoriesResponse,
  IGetProductsRequest,
  IGetProductsResponse,
} from './types'

export class RequestCatalog {
  static getBanners(
    data: IGetBannersDataRequest,
  ): Promise<TGetBannerListResponse> {
    return ApiService.get('/api/promotions/getPromotions', { params: data })
  }
  static getProducts(data: IGetProductsRequest): Promise<IGetProductsResponse> {
    return ApiService.get('/api/content/getProducts', { params: data })
  }

  static getCatalog(data: IGetProductsRequest): Promise<ICatalogData> {
    return ApiService.get('/api/content/getCatalog', { params: data })
  }

  static getCategories(
    data: IGetCategoriesDataRequest,
  ): Promise<IGetCategoriesResponse> {
    return ApiService.get('/api/content/getParents', { params: data })
  }
}
