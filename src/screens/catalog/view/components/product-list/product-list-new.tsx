import React, {
  PropsWithChildren,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react'
import { FlatList, StyleProp, StyleSheet, View, ViewStyle } from 'react-native'
import { ImageStyle } from 'react-native-fast-image'
import {
  Directions,
  FlingGestureHandler,
  NativeViewGestureHandler,
} from 'react-native-gesture-handler'
import Animated, {
  useAnimatedScrollHandler,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated'
import { counterDispatchHelper } from '../../../../../system/helpers/counter-dispatch-helper'
import { windowWidth } from '../../../../../system/helpers/window-size'
import { useDebounce } from '../../../../../system/hooks/use-debounce'
import { useTypedDispatch } from '../../../../../system/hooks/use-typed-dispatch'
import { useTypedSelector } from '../../../../../system/hooks/use-typed-selector'
import {
  getCartData,
  getCurrentOrderTypeId,
  getCurrentOrganisationId,
  getPolygonId,
  getSelectedProduct,
} from '../../../../../system/selectors/selectors'
import { ActionCart } from '../../../../cart'
import { AsyncActionCart } from '../../../../cart/store/async-action-cart'
import { IProductItem } from '../../../types/types'
import { IProductListItemComponent } from './product-list-item'
import { ProductListItemSmall } from './product-list-item-small'

interface IProductList {
  productIds: number[]
  productImageStyle?: StyleProp<ImageStyle>
  onScrollFiled?: (e: any) => void
  productContainerStyle?: ViewStyle
  animValue?: Animated.SharedValue<number>
}
interface IDebounceCallbackProps {
  key: string
  count: number
  orderType: number
  organisationId: number
}

interface IProductListItemConfig {
  Component: React.FC<IProductListItemComponent>
  onPress: ((item: IProductItem) => void) | null
}

export const ProductListItemConfig: IProductListItemConfig = {
  Component: ProductListItemSmall,
  onPress: null,
}

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList)

export const ProductListNew = React.memo(
  React.forwardRef<FlatList, PropsWithChildren<IProductList>>(
    ({ productIds, productContainerStyle, animValue }, ref) => {
      const dispatch = useTypedDispatch()
      const selectedProducts = useTypedSelector(getSelectedProduct)
      const cartData = useTypedSelector(getCartData)
      const currentOrderTypeId = useTypedSelector(getCurrentOrderTypeId)
      const currentOrganisationId = useTypedSelector(getCurrentOrganisationId)
      const polygonId = useTypedSelector(getPolygonId)
      const [isLoading, setIsLoading] = useState(false)

      const products_data = useTypedSelector(
        (state) => state.catalog.catalogData?.products,
      )

      const dispatchCallback = async ({
        key,
        count,
        orderType,
        organisationId,
      }: IDebounceCallbackProps) => {
        setIsLoading(true)
        await dispatch(
          AsyncActionCart.changeProductCountInCart({
            key,
            count,
            order_type: +orderType,
            id_org: +organisationId,
            polygon_id: polygonId,
          }),
        )
        setIsLoading(false)
      }

      const debounce = useDebounce<IDebounceCallbackProps>(
        dispatchCallback,
        400,
      )

      const setSelectedProductHandler = () => {
        if (cartData?.products) {
          const newData = cartData.products.reduce<{
            [key: string]: { count: number }
          }>((acc, el) => {
            acc[el.id] = { count: el.count }
            return acc
          }, {})
          dispatch(ActionCart.setSelectedProducts(newData))
        }
      }

      const onSetCount = useCallback(
        async (id: number, count: number) => {
          if (selectedProducts?.[id]?.count === count) {
            return
          }
          dispatch(
            ActionCart.setSelectedProducts(
              counterDispatchHelper({
                id: id.toString(),
                count,
                items: selectedProducts || {},
              }),
            ),
          )
        },
        [selectedProducts],
      )

      const onPressAdd = useCallback(
        async (item: IProductItem) => {
          if (
            currentOrganisationId !== undefined &&
            currentOrderTypeId !== undefined
          ) {
            setIsLoading(true)
            const sizeId = item?.sizes?.data?.[item?.sizes?.default_index]?.id

            onSetCount(item.id, 1)

            await dispatch(
              AsyncActionCart.cartAdd({
                id: +item.id,
                count: 1,
                order_type: +currentOrderTypeId,
                id_org: +currentOrganisationId,
                additives: [],
                size: sizeId !== undefined ? [{ id: sizeId, count: 1 }] : [],
                type: [],
                ingredients: [],
                polygon_id: polygonId,
              }),
            )
            setIsLoading(false)
          }
        },
        [
          currentOrderTypeId,
          currentOrganisationId,
          dispatch,
          onSetCount,
          polygonId,
        ],
      )

      const onChangeCount = useCallback(
        (item: IProductItem, newCount: number) => {
          const key = cartData?.products?.find((el) => el?.id === +item?.id)
            ?.key
          if (
            key &&
            currentOrganisationId !== undefined &&
            currentOrderTypeId !== undefined
          ) {
            onSetCount(item.id, newCount)
            debounce({
              key,
              count: newCount,
              orderType: +currentOrderTypeId,
              organisationId: +currentOrganisationId,
            })
          }
        },
        [cartData?.products, currentOrderTypeId, currentOrganisationId],
      )

      useEffect(() => {
        if (cartData?.products?.length) {
          setSelectedProductHandler()
        } else if (
          !cartData ||
          !cartData.products ||
          cartData.products.length === 0
        ) {
          selectedProducts && dispatch(ActionCart.setSelectedProducts(null))
        }
      }, [cartData?.products])

      const keyExtractorVerticalList = useCallback(
        (item) => item.toString(),
        [],
      )

      const contentContainerStyle_: ViewStyle = useMemo(() => {
        return {
          paddingBottom: 50,
          paddingTop: 20,
          width: windowWidth,
        }
      }, [])

      const verticalListRenderItem = useCallback(
        (props) => {
          const item = products_data?.[props.item.toString()]

          if (!item) {
            return null
          }

          return (
            <View key={props.item} style={styles.renderItemVerticalList}>
              <ProductListItemConfig.Component
                isLoading={isLoading}
                containerStyle={productContainerStyle}
                count={selectedProducts?.[item.id]?.count || 0}
                item={item}
                onPressAdd={onPressAdd}
                onChangeCount={onChangeCount}
              />
            </View>
          )
        },
        [onChangeCount, selectedProducts, products_data, isLoading],
      )

      const scrollY = useSharedValue(0)

      const onScroll = useAnimatedScrollHandler(
        {
          onScroll: (e) => {
            scrollY.value = e.contentOffset.y
          },
        },
        [],
      )

      const listRef = useRef()

      return (
        <View>
          <FlingGestureHandler
            simultaneousHandlers={listRef}
            direction={Directions.UP}
            onActivated={() => {
              if (animValue) {
                animValue.value = withTiming(1, { duration: 500 })
              }
            }}
          >
            <FlingGestureHandler
              simultaneousHandlers={listRef}
              direction={Directions.DOWN}
              onActivated={() => {
                if (animValue && scrollY.value < 20) {
                  animValue.value = withTiming(0, { duration: 500 })
                }
              }}
            >
              <Animated.View>
                <NativeViewGestureHandler ref={listRef}>
                  <AnimatedFlatList
                    bounces={false}
                    onScroll={onScroll}
                    alwaysBounceVertical={true}
                    ref={ref}
                    showsVerticalScrollIndicator={false}
                    keyExtractor={keyExtractorVerticalList}
                    contentContainerStyle={contentContainerStyle_}
                    initialNumToRender={5}
                    maxToRenderPerBatch={5}
                    windowSize={5}
                    scrollEventThrottle={16}
                    data={productIds}
                    renderItem={verticalListRenderItem}
                  />
                </NativeViewGestureHandler>
              </Animated.View>
            </FlingGestureHandler>
          </FlingGestureHandler>
        </View>
      )
    },
  ),
)

const styles = StyleSheet.create({
  container: {
    width: windowWidth,
    // flex: 1,
  },
  flatList: {
    width: '100%',
    height: '100%',
  },
  separator: {
    height: 19,
  },

  tabView: {
    backgroundColor: '#fff',
    paddingVertical: 10,
  },
  tabWrapper: {
    zIndex: 2,
  },
  tabItem: {
    justifyContent: 'center',
    height: 50,
    marginRight: 10,
    borderRadius: 20,
  },
  renderItemVerticalList: {
    //height: PRODUCT_ITEM_HEIGHT,
    width: windowWidth,
    marginBottom: 20,
  },
  contentContainer: {},
})
