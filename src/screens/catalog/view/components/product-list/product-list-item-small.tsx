import React, { useCallback, useMemo } from 'react'
import { IProductListItemComponent } from './product-list-item'
import { Insets, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import FastImage from 'react-native-fast-image'
import { urlValidator } from '../../../../../system/helpers/url-validator'
import { ImageRepository } from '../../../../../assets/image-repository'
import { ThemedButton } from '../../../../../components/buttons/themed-button'
import { Config } from '../../../../../config'
import { adaptiveSize } from '../../../../../system/helpers/responseive-font-size'
import { Counter } from '../../../../../components/counter/counter'
import { IProductItem, ProductCardType } from '../../../types/types'
import { CustomFastImage } from '../../../../../components/custom-fast-image'
import { rootNavigate } from '../../../../../system'
import { Routes } from '../../../../../navigation/config/routes'
const { Color, UIStyles, RADIUS } = Config

interface productListItemSmallConfig {
  onPressCard: ((item: IProductItem) => void) | null
  enabledOpenCart?: boolean
}

export const productListItemSmallConfig: productListItemSmallConfig = {
  onPressCard: null,
  enabledOpenCart: true,
}

const hitSlopLeftButton: Insets = { left: 30, right: 15, top: 15, bottom: 15 }
const hitSlopRightButton: Insets = { left: 15, right: 30, top: 15, bottom: 15 }

export const ProductListItemSmall: React.FC<IProductListItemComponent> = ({
  onChangeCount,
  onPressAdd,
  item,
  count,
  containerStyle,
  enabledOpenCart,
}) => {
  const isConstruct = item.card_type === ProductCardType.construct

  const image = useMemo(
    () =>
      item.img_big
        ? { uri: urlValidator(item.img_big) }
        : ImageRepository.ProductImagePlaceholder,
    [item.img_big],
  )

  const onPressHandler = () => {
    onPressAdd(item)
  }

  const onPressCard = () => {
    if (productListItemSmallConfig.onPressCard) {
      productListItemSmallConfig.onPressCard?.(item)
      return
    }
    rootNavigate(Routes.ProductDetail, { item })
  }

  const onChangeCountHandler = useCallback(
    (count: number) => {
      onChangeCount(item, count)
    },
    [item, onChangeCount],
  )

  const containerStyle_ = useMemo(() => {
    return [styles.container, containerStyle]
  }, [containerStyle])

  return (
    <TouchableOpacity
      disabled={!productListItemSmallConfig.enabledOpenCart}
      onPress={onPressCard}
      activeOpacity={0.8}
      style={containerStyle_}
    >
      <FastImage source={image} style={styles.image} />
      <View style={styles.content}>
        <Text style={styles.title}>{item.title}</Text>
        <Text numberOfLines={3} style={styles.description}>
          {item.desc}
        </Text>
        <View style={styles.priceWithButtonContainer}>
          <Text style={styles.price}>{item.max_price} ₽</Text>
          <View style={styles.buttonsWrapper}>
            {count && !isConstruct ? (
              <Counter
                hitSlopRightButton={hitSlopRightButton}
                hitSlopLeftButton={hitSlopLeftButton}
                buttonRightWrapperStyle={styles.counterButtonWrapper}
                buttonLeftWrapperStyle={styles.counterButtonWrapper}
                containerStyle={styles.counterContainer}
                valueStyle={styles.counterValue}
                buttonStyle={styles.counterButtons}
                buttonIconStyle={counterButtonIcon}
                value={count}
                onChange={onChangeCountHandler}
              />
            ) : (
              <ThemedButton
                labelStyle={styles.buttonLabel}
                buttonStyle={styles.button}
                label={isConstruct ? 'Выбрать' : 'Добавить'}
                onPress={onPressHandler}
                modifier={'bordered'}
                rounded={true}
              />
            )}
          </View>
        </View>
      </View>
    </TouchableOpacity>
  )
}

const counterButtonIcon = {
  fill: Color.PRIMARY,
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    ...UIStyles.paddingH16,
    backgroundColor: Color.WHITE,
  },
  content: {
    flex: 1,
    marginLeft: 24,
  },
  title: {
    ...UIStyles.font15,
    color: Color.DARK,
  },
  description: {
    ...UIStyles.font15,
    color: Color.GREY_600,
    marginTop: 12,
  },
  price: {
    ...UIStyles.font18,
    color: Color.DARK,
  },
  image: {
    width: 120,
    height: 120,
    borderRadius: RADIUS.LARGE,
  },
  button: {
    height: 30,
    width: adaptiveSize(90),
  },
  buttonLabel: {
    fontSize: adaptiveSize(12),
  },
  priceWithButtonContainer: {
    //flexDirection: "row",
    flex: 1,
    marginTop: 12,
    justifyContent: 'space-between',
    // backgroundColor:'red'
  },
  counterButtonWrapper: {
    alignItems: 'center',
  },
  counterContainer: {
    marginTop: 'auto',
    width: adaptiveSize(106),
    backgroundColor: 'transparent',
    borderColor: Color.PRIMARY_40,
    borderWidth: 1,
    borderRadius: RADIUS.MEDIUM,
    height: adaptiveSize(32),
    paddingHorizontal: 0,
  },
  counterValue: {
    color: Color.BLACK,
  },
  counterButtons: {
    backgroundColor: 'transparent',
    // width: 20,
    paddingHorizontal: 0,
  },
  buttonsWrapper: {
    alignSelf: 'flex-end',
  },
})
