import React, {useEffect, useMemo, useRef, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Config} from '../../../../../config';
import {ThemedButton} from '../../../../../components/buttons/themed-button';
import {Counter} from '../../../../../components/counter/counter';
import {IProductItem} from '../../../types/types';
import {useTypedSelector} from '../../../../../system';
import {
  Transition,
  Transitioning,
  TransitioningView,
} from 'react-native-reanimated';
import {Stickers} from '../stickers';

const {Color, UIStyles} = Config;

export interface IProductListItemContent {
  count: number;
  item: IProductItem;
  onPressAdd: (item: IProductItem) => void;
  onChangeCount: (item: IProductItem, count: number) => void;
}

export const ProductListItemContent: React.FC<IProductListItemContent> = React.memo(
  ({onChangeCount, count, onPressAdd, item}) => {
    const isLoading = useTypedSelector(state => state.cart.isLoading);
    const [disabled, setDisabled] = useState(false);

    const onChange = (value: number) => {
      ref.current?.animateNextTransition();
      onChangeCount(item, value);
    };

    const onPressAddHandler = () => {
      ref.current?.animateNextTransition();
      onPressAdd(item);
      setDisabled(true);
    };

    useEffect(() => {
      if (!isLoading) {
        setDisabled(false);
      }
    }, [isLoading]);

    const ref = useRef<TransitioningView | null>(null);

    const transition = (
      <Transition.Together>
        <Transition.In durationMs={200} type="scale" delayMs={200} />
        <Transition.Out durationMs={200} type="scale" />
      </Transition.Together>
    );

    return (
      <View style={styles.container}>
        <Stickers stickers={item.stickers} />
        <Text style={styles.title}>{item.title}</Text>
        {item.desc ? <Text style={styles.description}>{item.desc}</Text> : null}
        <View style={styles.priceAndWeightRow}>
          <Text style={styles.priceValue}>{item.price} ₽</Text>
          <Text style={styles.weightValue}>
            {item.energy?.energy_size || 0} г
          </Text>
        </View>

        <Transitioning.View ref={ref} transition={transition}>
          {count >= 1 ? (
            <Counter
              key={'counter'}
              disabled={disabled}
              containerStyle={{
                marginTop: 18,
                opacity: disabled ? 0.3 : 1,
              }}
              onChange={onChange}
              value={count}
            />
          ) : (
            <ThemedButton
              key={'button'}
              rounded={true}
              wrapperStyle={{marginTop: 18}}
              buttonStyle={{height: 50}}
              modifier={'bordered'}
              label={'Добавить'}
              onPress={onPressAddHandler}
            />
          )}
        </Transitioning.View>
      </View>
    );
  },
);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 32,
    marginTop: 27,
  },
  title: {
    ...UIStyles.font20b,
  },
  description: {
    flexWrap: 'wrap',
    marginTop: 18,
    ...UIStyles.font15,
    color: Color.GREY_500,
  },
  priceAndWeightRow: {
    marginTop: 18,
    ...UIStyles.flexRow,
  },
  priceValue: {
    ...UIStyles.font24b,
    color: Color.DARK,
  },
  weightValue: {
    ...UIStyles.font15,
    color: Color.GREY_600,
  },
});
