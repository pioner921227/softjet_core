import {
  Animated,
  FlatList,
  ImageStyle,
  Keyboard,
  StyleSheet,
  View,
  ViewStyle,
} from "react-native";
import { IProductItem } from "../../../types/types";
import React, { useCallback, useContext, useEffect } from "react";
import {
  IProductListItemComponent,
  ProductListItem,
} from "./product-list-item";
import { useDispatch } from "react-redux";
import { useTypedSelector } from "../../../../../system/hooks/use-typed-selector";
import { AsyncActionCart } from "../../../../cart/store/async-action-cart";
import { useDebounce } from "../../../../../system/hooks/use-debounce";
import { ActionCart } from "../../../../cart";
import { counterDispatchHelper } from "../../../../../system/helpers/counter-dispatch-helper";
import { CatalogContext } from "../../catalog";
import { Preloader } from "../../../../../components/preloader";
import { EmptyDataTitle } from "../../../../../components/empty-data-title";
import {
  getCartData,
  getCurrentOrderTypeId,
  getCurrentOrganisationId,
  getPolygonId,
  getSelectedProduct,
} from "../../../../../system/selectors/selectors";

interface IProductList {
  componentWrapperStyle?: ViewStyle;
  contentContainerStyle?: ViewStyle;
  products: IProductItem[];
  onScrollHandler?: boolean;
  onScroll?: () => void;
  onMomentumScrollBegin?: () => void;
  onScrollEndDrag?: () => void;
  onMomentumScrollEnd?: () => void;
  onGetRef?: (ref: FlatList) => void;
  productImageStyle?: ImageStyle;
  enableEmptyComponent?: boolean;
  enabledOpenCart?: boolean;
}
interface IDebounceCallbackProps {
  key: string;
  count: number;
  orderType: number;
  organisationId: number;
}

interface IProductListItemConfig {
  Component: React.FC<IProductListItemComponent>;
  onPress: ((item: IProductItem) => void) | null;
}

export const ProductListItemConfig: IProductListItemConfig = {
  Component: ProductListItem,
  onPress: null,
};

export const ProductList: React.FC<IProductList> = React.memo(
  ({
    componentWrapperStyle,
    contentContainerStyle,
    products,
    onScrollHandler = true,
    onScroll,
    onGetRef,
    onMomentumScrollBegin,
    onMomentumScrollEnd,
    onScrollEndDrag,
    productImageStyle,
    enableEmptyComponent = true,
    enabledOpenCart,
  }) => {
    const dispatch = useDispatch();
    const { scrollY } = useContext(CatalogContext);
    const selectedProducts = useTypedSelector(getSelectedProduct);
    const cartData = useTypedSelector(getCartData);
    const currentOrderTypeId = useTypedSelector(getCurrentOrderTypeId);
    const currentOrganisationId = useTypedSelector(getCurrentOrganisationId);
    const polygonId = useTypedSelector(getPolygonId);

    const isLoading = useTypedSelector(
      (state) => state.catalog.isLoadingProducts
    );

    const dispatchCallback = ({
      key,
      count,
      orderType,
      organisationId,
    }: IDebounceCallbackProps) => {
      dispatch(
        AsyncActionCart.changeProductCountInCart({
          key,
          count,
          order_type: +orderType,
          id_org: +organisationId,
          polygon_id: polygonId,
        })
      );
    };

    const debounce = useDebounce<IDebounceCallbackProps>(
      dispatchCallback,
      1000
    );

    const onPressAdd = (item: IProductItem) => {
      if (
        currentOrganisationId !== undefined &&
        currentOrderTypeId !== undefined
      ) {
        const sizeId = item?.sizes?.data?.[item?.sizes?.default_index]?.id;
        onSetCount(item.id, 1);
        dispatch(
          AsyncActionCart.cartAdd({
            id: +item.id,
            count: 1,
            order_type: +currentOrderTypeId,
            id_org: +currentOrganisationId,
            additives: [],
            size: sizeId !== undefined ? [{ id: sizeId, count: 1 }] : [],
            type: [],
            ingredients: [],
            polygon_id: polygonId,
          })
        );
      }
    };
    const onChangeCount = (item: IProductItem, newCount: number) => {
      const key = cartData?.products?.find((el) => el?.id === +item?.id)?.key;
      if (
        key &&
        currentOrganisationId !== undefined &&
        currentOrderTypeId !== undefined
      ) {
        onSetCount(item.id, newCount);
        debounce({
          key,
          count: newCount,
          orderType: +currentOrderTypeId,
          organisationId: +currentOrganisationId,
        });
      }
    };

    const onSetCount = (id: number, count: number) => {
      if (selectedProducts?.[id]?.count === count) {
        return;
      }

      dispatch(
        ActionCart.setSelectedProducts(
          counterDispatchHelper({ id, count, items: selectedProducts || {} })
        )
      );
    };

    useEffect(() => {
      if (cartData?.products?.length) {
        const newData = cartData?.products.reduce<{
          [key: string]: { count: number };
        }>((acc, el) => {
          acc[el.id] = { count: el.count };
          return acc;
        }, {});

        dispatch(ActionCart.setSelectedProducts(newData));

        // cartData.products.forEach((el) => {
        //   onSetCount(el?.id, el?.count);
        // });
      } else if (
        !cartData ||
        !cartData.products ||
        cartData.products.length === 0
      ) {
        selectedProducts && dispatch(ActionCart.setSelectedProducts(null));
      }
    }, [cartData?.products]);

    const renderItem = ({ item }: { item: IProductItem }) => {
      return (
        <View style={componentWrapperStyle}>
          <ProductListItemConfig.Component
            enabledOpenCart={enabledOpenCart}
            item={item}
            imageStyle={productImageStyle}
            onPressAdd={ProductListItemConfig.onPress || onPressAdd}
            count={selectedProducts?.[item.id]?.count || 0}
            onChangeCount={onChangeCount}
          />
        </View>
      );
    };

    const keyExtractor = useCallback((item) => item.id, []);

    return (
      <View style={styles.container}>
        <Animated.FlatList
          ref={onGetRef}
          style={styles.flatList}
          ListEmptyComponent={
            isLoading ? Preloader : enableEmptyComponent ? EmptyDataTitle : null
          }
          showsVerticalScrollIndicator={false}
          removeClippedSubviews={true}
          maxToRenderPerBatch={10}
          windowSize={10}
          onMomentumScrollBegin={onMomentumScrollBegin}
          onScrollEndDrag={onScrollEndDrag}
          onMomentumScrollEnd={onMomentumScrollEnd}
          scrollToOverflowEnabled={true}
          scrollEventThrottle={16}
          // getItemLayout={getItemLayout}
          initialNumToRender={2}
          onScroll={
            onScrollHandler
              ? Animated.event(
                  [
                    {
                      nativeEvent: {
                        contentOffset: { y: scrollY || new Animated.Value(0) },
                      },
                    },
                  ],
                  {
                    useNativeDriver: true,
                  }
                )
              : () => {
                  onScroll?.();
                  Keyboard.dismiss();
                }
          }
          ItemSeparatorComponent={Separator}
          data={isLoading ? [] : products}
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={contentContainerStyle}
          keyExtractor={keyExtractor}
          renderItem={renderItem}
        />
      </View>
    );
  }
);

const Separator = React.memo(() => {
  return <View style={{ height: 19 }} />;
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  flatList: {
    width: "100%",
    height: "100%",
  },
});
