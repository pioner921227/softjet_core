import React, { useCallback, useMemo, useState } from 'react'
import {
  Keyboard,
  StyleProp,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native'
import FastImage, { ImageStyle } from 'react-native-fast-image'
import { Config } from '../../../../../config'
import {
  IProductListItemContent,
  ProductListItemContent,
} from './product-list-item-content'
import { InfoInCircleTransparentIcon } from '../../../../../components/icons/info-in-corcle-transparent-icon'
import { EnergyDataOverlay } from './energy-data-overlay'
import { useImage } from '../../../../../system/hooks/use-image'

const { Color, UIStyles, RADIUS } = Config

export interface IProductListItemComponent extends IProductListItemContent {
  ratingWrapperStyle?: ViewStyle
  imageStyle?: StyleProp<ImageStyle>
  containerStyle?: ViewStyle
  enabledOpenCart?: boolean
  isLoading?: boolean
}

export const ProductListItem: React.FC<IProductListItemComponent> = React.memo(
  ({ ratingWrapperStyle, imageStyle, containerStyle, item, ...rest }) => {
    const [isVisibleEnergyData, setIsVisibleEnergyData] = useState(false)

    const toggleEnergyData = useCallback(() => {
      setIsVisibleEnergyData((state) => !state)
      Keyboard.dismiss()
    }, [])

    const image = useImage(item.img_big)

    const imageStyleMemo: StyleProp<ImageStyle> = useMemo(
      () =>
        imageStyle
          ? StyleSheet.flatten([styles.image, imageStyle])
          : styles.image,
      [imageStyle],
    )

    const ratingStyle = useMemo(() => {
      return {
        ...styles.ratingWrapper,
        ...ratingWrapperStyle,
      }
    }, [ratingWrapperStyle])

    const containerStyle_ = useMemo(
      () => [styles.container, containerStyle],
      [],
    )

    return (
      <View style={containerStyle_}>
        <TouchableOpacity onPress={toggleEnergyData}>
          <FastImage resizeMode="cover" style={imageStyleMemo} source={image} />
          <View style={styles.overlay} />
          <TouchableOpacity
            onPress={toggleEnergyData}
            style={styles.buttonInfo}
          >
            <InfoInCircleTransparentIcon fill={Config.Color.WHITE} />
          </TouchableOpacity>
          {item.rating && typeof item.rating === 'number' ? (
            <View style={ratingStyle}>
              <Text style={styles.ratingValue}>{item?.rating}</Text>
            </View>
          ) : null}

          {isVisibleEnergyData ? (
            <EnergyDataOverlay
              rating={item?.rating?.toString()}
              onPress={toggleEnergyData}
              energyData={item?.energy}
            />
          ) : null}
        </TouchableOpacity>
        <ProductListItemContent item={item} {...rest} />
      </View>
    )
  },
)

const styles = StyleSheet.create({
  container: {
    // width: '100%',
    // paddingBottom: 28,
    // marginBottom:20,
    // backgroundColor:Color.WHITE,

    flexGrow: 1,
    // flex:1,
    width: '100%',
    paddingBottom: 18,
    // marginHorizontal: 16,
    backgroundColor: Color.WHITE,
    // borderRadius: 20,
    marginBottom: 18,
    overflow: 'hidden',
  },
  ratingWrapper: {
    position: 'absolute',
    right: -2,
    backgroundColor: Color.SUCCESS,
    width: 45,
    height: 38,
    borderBottomLeftRadius: RADIUS.SMALL,
    alignItems: 'center',
    justifyContent: 'center',
  },
  ratingValue: {
    ...UIStyles.font15,
    fontWeight: '500',
    color: Color.WHITE,
  },
  buttonInfo: {
    position: 'absolute',
    right: 29.6,
    bottom: 14.6,
  },
  overlay: {
    alignSelf: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: Config.Color.RGBA_100,
    position: 'absolute',
    // borderRadius: 20,
  },
  image: {
    alignSelf: 'center',
    width: '100%',
    height: 229,
    // borderRadius: 20,
  },
})
