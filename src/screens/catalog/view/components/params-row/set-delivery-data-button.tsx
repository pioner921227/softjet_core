import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { ArrowBottomWithoutLine } from "../../../../../components/icons";
import React, { useMemo } from "react";
import { Config } from "../../../../../config";
const { RADIUS, Color, UIStyles } = Config;

interface ISetDeliveryDataButton {
  onPress: () => void;
  title: string;
  subtitle?: string;
  enableSubtitle: boolean;
}

export const SetDeliveryDataButton: React.FC<ISetDeliveryDataButton> = ({
  onPress,
  title,
  subtitle,
  enableSubtitle,
}) => {
  // const subtitle_ = useMemo(() => {
  //   return subtitle?.slice(0, 30) + ((subtitle?.length ?? 0) > 30 ? "..." : "");
  // }, []);
  return (
    <TouchableOpacity onPress={onPress} style={styles.button}>
      <View style={styles.titleWrapper}>
        <Text style={styles.title}>{title}</Text>
        <ArrowBottomWithoutLine stroke={Color.GREY_600} />
      </View>
      {enableSubtitle && subtitle ? (
        <Text numberOfLines={1} style={styles.description}>{subtitle}</Text>
      ) : null}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    ...UIStyles.paddingH16,
    ...UIStyles.flexRow,
    marginTop: 15.5,
  },
  button: {
    padding: 8,
    borderRadius: RADIUS.SMALL,
  },
  titleWrapper: {
    flexDirection: "row",
    alignItems: "center",
  },
  title: {
    ...UIStyles.font15b,
    color: Color.DARK,
    marginRight: 7,
  },
  description: {
    ...UIStyles.font13,
    color: Color.GREY_600,
  },
});
