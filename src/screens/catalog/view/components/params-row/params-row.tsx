import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {Config} from '../../../../../config';
import {SettingsIcon} from '../../../../../components/icons/settings-icon';
import {SearchIcon} from '../../../../../components/icons/search-icon';
import {useTypedSelector} from '../../../../../system';
import {catalogConfig, deliveryDataTypesForButton} from '../../../config';
import {
    getCurrentAddress,
    getCurrentCity,
    getCurrentOrderType,
    getCurrentOrganisation
} from '../../../../../system/selectors/selectors';
import {SetDeliveryDataButton} from './set-delivery-data-button';

const {Color, UIStyles, RADIUS} = Config;

interface IParamsRow {
    onPressSearch: () => void;
    onPressSorting: () => void;
    onPressSetAddress: () => void;
}

export const ParamsRow: React.FC<IParamsRow> = React.memo(
    ({onPressSearch, onPressSetAddress, onPressSorting}) => {
        const currentOrderType = useTypedSelector(getCurrentOrderType);
        const currentOrganisation = useTypedSelector(getCurrentOrganisation);
        const currentAddress = useTypedSelector(getCurrentAddress);
        const currentCity = useTypedSelector(getCurrentCity);

        const getAddress = () => {
            if (currentOrderType?.isRemoted) {
                const {city = '', street = '', house_num = ''} = currentAddress || {};
                return `${city} ${street} ${house_num}`;
            }
            return currentOrganisation?.title || '';
        };

        const getButtonData = (type: keyof typeof deliveryDataTypesForButton) => {
            switch (type) {
                case 'address':
                    return getAddress() ?? 'Адрес';
                case 'city':
                    return currentCity?.title || 'Город';
                default:
                    return currentOrderType?.name ?? 'Тип Доставки';
            }
        };

        const getTitle = () => {
            return getButtonData(catalogConfig.setDeliveryDataResource.titleType);
        };

        const getSubtitle = () => {
            return getButtonData(catalogConfig.setDeliveryDataResource.subtitleType);
        };

        return (
            <View style={styles.container}>
            <View style={{width:'70%'}}>
                <SetDeliveryDataButton
                    onPress={onPressSetAddress}
                    title={getTitle()}
                    subtitle={getSubtitle()}
                    enableSubtitle={catalogConfig.setDeliveryDataResource.enableSubtitle}
                />
                </View>
                <View style={UIStyles.flexRow}>
                    {catalogConfig.paramsMenu.isVisibleSortingButton ? (
                        <TouchableOpacity onPress={onPressSorting} style={styles.button}>
                            <SettingsIcon />
                        </TouchableOpacity>
                    ) : null}

                    {catalogConfig.paramsMenu.isVisibleSearchButton ? (
                        <TouchableOpacity onPress={onPressSearch} style={searchButton}>
                            <SearchIcon />
                        </TouchableOpacity>
                    ) : null}
                </View>
            </View>
        );
    },
);

const styles = StyleSheet.create({
    container: {
        ...UIStyles.paddingH16,
        ...UIStyles.flexRow,
        marginTop: 15.5,
    },
    button: {
        padding: 8,
        borderRadius: RADIUS.SMALL,
    },
    titleWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    title: {
        ...UIStyles.font15b,
        color: Color.DARK,
        marginRight: 7,
    },
    description: {
        ...UIStyles.font13,
        color: Color.GREY_600,
    },
});

const searchButton = StyleSheet.flatten([styles.button, {marginLeft: 8}]);
