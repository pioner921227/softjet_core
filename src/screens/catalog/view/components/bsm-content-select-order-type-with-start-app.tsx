import React from 'react';
import {IOrderTypesItem} from '../../../delivery/types/types';
import {BottomSheetModalContentWithTitle} from '../../../../components/bottom-sheet-menu/bottom-sheet-modal/bottom-sheet-modal-content-with-title';
import {IThemedButton} from '../../../../components/buttons/themed-button';
import {useTypedSelector} from '../../../../system';
import {useDispatch} from 'react-redux';
import {IAddressItemResponse} from '../../../delivery/api/types';
import {
  getCurrentOrderType,
  getCurrentOrganisation,
} from '../../../../system/selectors/selectors';

interface IBottomSheetModalSelectOrderTypeContent {
  lastAddress?: IAddressItemResponse | null;
  orderTypes: IOrderTypesItem[];
  onPress: (type: IOrderTypesItem) => void;
  close: () => void;
}

//BSM = BottomSheetModal

export const BSMContentSelectOrderTypeWithStartApp: React.FC<IBottomSheetModalSelectOrderTypeContent> = React.memo(
  ({lastAddress, orderTypes, onPress, close}) => {
    const dispatch = useDispatch();
    const {city, street, house_num} = lastAddress || {};

    const buttonsFromServer: IThemedButton[] = orderTypes?.reduce<IThemedButton[]>(
      (acc, el) => {
        if (el.show_on_main) {
          acc.push({
            label: el.description,
            rounded: true,
            modifier: 'bordered',
            onPress: () => onPress(el),
          });
        }
        return acc;
      },
      [],
    );


    const currentOrderType = useTypedSelector(getCurrentOrderType);
    const currentOrganisation = useTypedSelector(getCurrentOrganisation);

    const isVisibleButtonWithLastAddress = () => {
      if (!currentOrderType?.isRemoted) {
        return !!currentOrganisation?.title;
      }
      return !!(city && house_num);
    };

    const labelGenerate = () => {
      if (!currentOrderType?.isRemoted) {
        return `${currentOrganisation?.title}`;
      }
      return `${city} ${street} ${house_num}`;
    };

    const onPressOnCurrentAddress = () => {
      close();
      // if (currentOrganisation?.id !== undefined && currentOrderType?.id) {
      //   dispatch(
      //     AsyncActionsCatalog.getProducts({
      //       id_org: +currentOrganisation?.id,
      //       order_type: +currentOrderType.id,
      //     }),
      //   );
      //   dispatch(
      //     AsyncActionsCatalog.getCategories({
      //       order_type: +currentOrderType?.id,
      //       id_org: currentOrganisation?.id,
      //     }),
      //   );
      // }
    };

    const buttonsAll: IThemedButton[] = [
      {
        label: labelGenerate(),
        onPress: onPressOnCurrentAddress,
        rounded: true,
      },
      ...buttonsFromServer,
    ];

    return (
      <BottomSheetModalContentWithTitle
        title={'Как хотите получить \n заказ?'}
        description={'Хотим убедиться, что на ваш адрес \n доступна доставка'}
        buttons={
          isVisibleButtonWithLastAddress() ? buttonsAll : buttonsFromServer
        }
      />
    );
  },
);
