import React, { useMemo } from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import FastImage from 'react-native-fast-image'
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import { useDispatch } from 'react-redux'
import { IBottomSheetModalComponentProps } from '../../../../components/bottom-sheet-menu/bottom-sheet-modal-provider'
import { ThemedButton } from '../../../../components/buttons/themed-button'
import { Config } from '../../../../config'
import { Routes } from '../../../../navigation'
import { rootNavigate, useTypedSelector } from '../../../../system'
import { useImage } from '../../../../system/hooks/use-image'
import { ActionCatalog } from '../../store/action-catalog'
import { BannerTypesEnum, IBannerItem } from '../../types/types'

const { Color, UIStyles } = Config

interface IBottomSheetModalBanners extends IBottomSheetModalComponentProps {
  item: IBannerItem
  isVisibleButton?: boolean
}

export const BSMContentBannerData: React.FC<IBottomSheetModalBanners> = ({
  item,
  isVisibleButton = true,
}) => {
  const insets = useSafeAreaInsets()
  const dispatch = useDispatch()
  const bannersActivePromoCode = useTypedSelector(
    (state) => state.catalog.bannersActivePromoCode,
  )

  const isActivate = bannersActivePromoCode.includes(+item?.id)

  const activatePromocode = () => {
    if (item?.promocode && !isActivate && item?.id !== undefined) {
      dispatch(ActionCatalog.setActivePromoCodes(item.id))
    }
  }

  const goToProductDetail = () => {
    rootNavigate(Routes.ProductDetail, { itemId: item.product_id })
  }

  const pressHandlers: Record<BannerTypesEnum, () => void> = {
    promo: activatePromocode,
    product: goToProductDetail,
    info: () => {},
  }

  const onPress = () => {
    pressHandlers[item.banner_type ?? 'promo']?.()
  }

  const titleVariants: Record<BannerTypesEnum, string> = {
    [BannerTypesEnum.promo]: isActivate
      ? item?.promocode
      : 'Активировать промокод',
    [BannerTypesEnum.product]: 'Перейти к товару',
    [BannerTypesEnum.info]: '',
  }

  const image = useImage(item?.img_big)

  return (
    <>
      <ScrollView style={styles.container}>
        <View style={styles.contentWrapper}>
          <FastImage
            source={image}
            resizeMode={'cover'}
            style={styles.bannerImage}
          />
          <Text style={styles.bannersTitle}>{item?.title}</Text>
          <Text style={styles.bannerDescription}>{item?.content}</Text>
        </View>
      </ScrollView>
      {isVisibleButton && item?.promocode ? (
        <ThemedButton
          buttonStyle={styles.button}
          withGesture={true}
          wrapperStyle={{ marginBottom: insets.bottom, paddingHorizontal: 46 }}
          rounded={true}
          label={titleVariants[item.banner_type ?? 'promo']}
          onPress={onPress}
        />
      ) : null}
    </>
  )
}

const styles = StyleSheet.create({
  container: {},
  contentWrapper: {
    paddingHorizontal: 46,
  },
  bannersTitle: {
    ...UIStyles.font20b,
    color: Color.DARK,
    textAlign: 'center',
    marginTop: 12,
  },
  bannerImage: {
    borderRadius: 15,
    width: '100%',
    height: 104,
  },
  bannerDescription: {
    ...UIStyles.font15,
    color: Color.GREY_600,
    marginTop: 16,
  },
  button: {
    height: 50,
    marginBottom: 10,
  },
})
