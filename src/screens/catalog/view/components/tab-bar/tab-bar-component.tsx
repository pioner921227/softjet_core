import {
  NavigationState,
  SceneRendererProps,
  TabBar,
} from 'react-native-tab-view';
import {Animated, StyleSheet} from 'react-native';
import React, {useContext, useMemo} from 'react';

import {Config} from '../../../../../config';
import {TabBarLabel} from './tab-bar-label';
import {CatalogContext} from '../../catalog';
import {CATALOG_HEADER_HEIGHT} from '../tab-catalog/tab-catalog-header';

const {Color} = Config;

type TAllProps = SceneRendererProps & {navigationState: NavigationState<any>};

interface TabBarComponentProps extends TAllProps {}

export const TabBarComponent: React.FC<TabBarComponentProps> = ({...props}) => {
  const {scrollY} = useContext(CatalogContext);

  const y = scrollY.interpolate({
    inputRange: [0, CATALOG_HEADER_HEIGHT],
    outputRange: [0, -CATALOG_HEADER_HEIGHT],
    extrapolateRight: 'clamp',
  });

  const style = useMemo(
    () =>
      StyleSheet.flatten([{transform: [{translateY: y}]}, styles.stylesTabBar]),
    [y],
  );

  scrollY.setOffset(0);

  return (
    <Animated.View style={style}>
      <TabBar
        {...props}
        bounces={false}
        scrollEnabled={true}
        pressOpacity={1}
        pressColor={'transparent'}
        style={styles.tabBarContainer}
        tabStyle={styles.tabStyle}
        renderLabel={TabBarLabel}
        indicatorStyle={styles.indicator}
      />
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  stylesTabBar: {
    zIndex: 2,
    backgroundColor: Color.WHITE,
    position: 'absolute',
    width: '100%',
    top: CATALOG_HEADER_HEIGHT,
  },
  tabBarContainer: {
    elevation: 0,
    shadowOpacity: 0,
    backgroundColor: Color.WHITE,
    padding: 0,
    margin: 0,
  },
  tabStyle: {
    backgroundColor: Color.WHITE,
    width: 'auto',
    padding: 0,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  indicator: {backgroundColor: 'transparent'},
});
