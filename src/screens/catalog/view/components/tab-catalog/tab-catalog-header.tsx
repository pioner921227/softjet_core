import { Animated, BackHandler, StyleSheet } from "react-native";
import { BannerList } from "../../../../../components/baner-list/banner-list";
import { ParamsRow } from "../params-row/params-row";
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import { IBannerItem } from "../../../types/types";
import { BottomSheetModalWithRef } from "../../../../../components/bottom-sheet-menu/bottom-sheet-modal/bottom-sheet-modal-with-ref";
import { BSMContentBannerData } from "../bsm-content-banner-data";
import { BottomSheetModalMethods } from "@gorhom/bottom-sheet/lib/typescript/types";
import {
  ModalSorting,
  SortVariant,
} from "../../../../../components/modal/modal-sorting";
import { ModalSearch } from "../../../../../components/modal/modal-search/modal-search";
import { IModalMethods } from "../../../../../components/modal/modal-with-component";
import { BSMContentCreateNewAddress } from "../../../../delivery/view/components/bsm-content-create-new-address";
import { useTypedSelector } from "../../../../../system";
import { useDispatch } from "react-redux";
import { ActionCatalog } from "../../../store/action-catalog";
import { sortingHelper } from "../../../../../system/helpers/sorting-helper";
import { BSMContentSelectOrderTypeWithStartApp } from "../bsm-content-select-order-type-with-start-app";
import { useIsFocused } from "@react-navigation/native";
import { useSelectOrderTypeHandler } from "../../../../../system/hooks/use-select-order-type-handler";
import { Routes } from "../../../../../navigation";
import { Config } from "../../../../../config";
import { catalogConfig } from "../../../config";
import { StackNavigationProp } from "@react-navigation/stack";
import { TRootStackParamList } from "../../../../../navigation/types/types";
import {
  getBanners,
  getCartData,
  getCurrentAddress,
  getCurrentOrderType,
  getCurrentOrganisation,
  getOrderTypes,
  getProducts,
} from "../../../../../system/selectors/selectors";
import { adaptiveSize } from "../../../../../system/helpers/responseive-font-size";
import {
  IModalComponentMethods,
  ModalComponent,
} from "../../../../../components/modal/modal-component";

const { Color } = Config;

interface ITabCatalogHeader {
  scrollY: Animated.Value;
  ignoreModal: boolean;
}

type TNavigation = StackNavigationProp<
  TRootStackParamList,
  Routes.SelectOrganisationInMap
>;

export type TSetOrderTypeCallback =
  | ((cityId: number | undefined) => void)
  | null;

export interface ISetOrderTypeRoutes {
  online: TSetOrderTypeCallback;
  offline: TSetOrderTypeCallback;
}

export const setOrderTypeRoutes: ISetOrderTypeRoutes = {
  online: null,
  offline: null,
};

export const CATALOG_HEADER_HEIGHT = 204;

// const searchModalRef = React.createRef();

export const TabCatalogHeader: React.FC<ITabCatalogHeader> = React.memo(
  ({ ignoreModal, scrollY }) => {
    const bannerModalRef = useRef<BottomSheetModalMethods | null>(null);
    const setOrderTypeModalRef = useRef<BottomSheetModalMethods | null>(null);
    const currentOrganisation = useTypedSelector(getCurrentOrganisation);

    const [isVisibleSearchModal, setIsVisibleSearchModal] = useState(false);

    const initialOrderTypeModalRef = useRef<BottomSheetModalMethods | null>(
      null
    );

    const isFirstLoadApp = useTypedSelector(
      (state) => state.system.isFirstLoadApp
    );

    const modalWarningRef = useRef<IModalComponentMethods | null>(null);

    const banners = useTypedSelector(getBanners);
    const products = useTypedSelector(getProducts);
    const cartData = useTypedSelector(getCartData);
    const orderTypes = useTypedSelector(getOrderTypes);
    const currentAddress = useTypedSelector(getCurrentAddress);
    const currentOrderType = useTypedSelector(getCurrentOrderType);

    const dispatch = useDispatch();

    const refModalSorting = useRef<IModalMethods | null>(null);

    const isFocused = useIsFocused();
    const [selectedBanner, setSelectedBanner] = useState<IBannerItem>(
      banners?.[0]
    );

    const y = scrollY.interpolate({
      inputRange: [0, CATALOG_HEADER_HEIGHT],
      outputRange: [0, -CATALOG_HEADER_HEIGHT],
      extrapolateRight: "clamp",
    });

    const opacity = scrollY.interpolate({
      inputRange: [0, CATALOG_HEADER_HEIGHT],
      outputRange: [1, 0],
      extrapolateRight: "clamp",
    });

    const stylesHeader = useMemo(
      () =>
        StyleSheet.flatten([
          {
            transform: [{ translateY: y }],
            opacity: opacity,
            backgroundColor: Color.WHITE,
          },
          styles.header,
        ]),
      [opacity, y]
    );

    const onPressBannerHandler = useCallback(
      (banner: IBannerItem) => {
        setSelectedBanner(banner);
        bannerModalRef.current?.present();
      },
      [bannerModalRef]
    );

    const openSearch = () => {
      setIsVisibleSearchModal(true);
      // searchModalRef.current?.show();
    };

    const closeSearch = () => {
      setIsVisibleSearchModal(false);
    };

    const onSorting = useCallback(
      (type: keyof typeof SortVariant) => {
        dispatch(ActionCatalog.sortingProducts(sortingHelper(products, type)));
        refModalSorting.current?.close();
      },
      [products]
    );

    const openSorting = () => {
      refModalSorting.current?.show();
    };

    const onSelectOrderType = useSelectOrderTypeHandler({
      offline: setOrderTypeRoutes.offline,
      online: setOrderTypeRoutes.online,
      willStartCallback: () => {
        setOrderTypeModalRef.current?.dismiss();
        initialOrderTypeModalRef.current?.dismiss();
      },
    });

    const openSetOrders = () => {
      if (catalogConfig.onPressSetOrderType) {
        catalogConfig.onPressSetOrderType();
        return;
      }

      setOrderTypeModalRef.current?.present();
    };

    const getButtonsHeight = useCallback(
      (withLastAddress?: boolean, isInitial?: boolean) => {
        const count = orderTypes?.filter((el)=>el.show_on_main).length;


        const headerHeight = adaptiveSize(isInitial ? 150 : 100);
        const bottomMargin = adaptiveSize(20);
        const buttonsHeight = adaptiveSize(40 + 16);
        if (withLastAddress) {
          return headerHeight + (count + 1) * buttonsHeight;
        }
        return headerHeight + count * adaptiveSize(40 + 16);
      },
      [orderTypes?.length]
    );

    const snapCreateNewAddress = useMemo(
      () => [getButtonsHeight()],
      [getButtonsHeight]
    );
    const snapPointBanner = useMemo(() => ["60%"], []);

    const hasLastAddress = () => {
      if (currentOrderType?.id === undefined) {
        return false;
      }
      if (currentOrderType.isRemoted && currentAddress?.id === undefined) {
        return false;
      }
      if (
        !currentOrderType.isRemoted &&
        currentOrganisation?.id === undefined
      ) {
        return false;
      }
      return true;
    };

    const snapInitialModalSetOrderType = useMemo(
      () => [getButtonsHeight(hasLastAddress(), true)],
      [getButtonsHeight]
    );

    const onCloseInitialSetOrderTypeModal = useCallback(() => {
      requestAnimationFrame(() => {
        initialOrderTypeModalRef.current?.dismiss();
      });
    }, []);

    useEffect(() => {
      const backHandler = BackHandler.addEventListener(
        "hardwareBackPress",
        () => {
          bannerModalRef.current?.dismiss();
          return true;
        }
      );

      // if (
      //   !ignoreModal &&
      //   isFocused &&
      //   isFirstLoadApp &&
      //   catalogConfig.enableInitialModalOrderType
      // ) {
        // initialOrderTypeModalRef?.current?.present();
      // }
      return () => {
        backHandler.remove();
      };
    }, []);

    useEffect(() => {
      if (!isFocused) {
        setIsVisibleSearchModal(false);
      }
    }, [isFocused]);

    return (
      <>
        <Animated.View style={stylesHeader}>
          <BannerList banners={banners} onPress={onPressBannerHandler} />
          <ParamsRow
            onPressSorting={openSorting}
            onPressSetAddress={openSetOrders}
            onPressSearch={openSearch}
          />
        </Animated.View>
        <BottomSheetModalWithRef
          modalRef={bannerModalRef}
          snapPoints={snapPointBanner}
        >
          <BSMContentBannerData
            isVisibleButton={catalogConfig.isVisibleButtonInBannerInfoModal}
            item={selectedBanner}
          />
        </BottomSheetModalWithRef>

        <ModalSorting reference={refModalSorting} onSorting={onSorting} />

        <BottomSheetModalWithRef
          modalRef={setOrderTypeModalRef}
          snapPoints={snapCreateNewAddress}
        >
          <BSMContentCreateNewAddress onPress={onSelectOrderType} />
        </BottomSheetModalWithRef>
        <ModalComponent ref={modalWarningRef} />
        {/* {catalogConfig.enableInitialModalOrderType ? ( */}
        {/*   <BottomSheetModalWithRef */}
        {/*     dismissOnPanDown={false} */}
        {/*     modalRef={initialOrderTypeModalRef} */}
        {/*     snapPoints={snapInitialModalSetOrderType} */}
        {/*   > */}
        {/*     <BSMContentSelectOrderTypeWithStartApp */}
        {/*       close={onCloseInitialSetOrderTypeModal} */}
        {/*       onPress={onSelectOrderType} */}
        {/*       orderTypes={orderTypes} */}
        {/*       lastAddress={currentAddress} */}
        {/*     /> */}
        {/*   </BottomSheetModalWithRef> */}
        {/* ) : null} */}
        {isVisibleSearchModal ? (
          <ModalSearch
            closeSearch={closeSearch}
            isVisible={isVisibleSearchModal}
          />
        ) : null}
      </>
    );
  }
);

const styles = StyleSheet.create({
  header: {
    zIndex: 2,
    height: CATALOG_HEADER_HEIGHT,
    position: "absolute",
    width: "100%",
  },
});
