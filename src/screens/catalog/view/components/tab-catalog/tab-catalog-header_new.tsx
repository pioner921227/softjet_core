import { BottomSheetModalMethods } from '@gorhom/bottom-sheet/lib/typescript/types'
import { useIsFocused } from '@react-navigation/native'
import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import { BackHandler, InteractionManager } from 'react-native'
import { useDispatch } from 'react-redux'
import { BannerList } from '../../../../../components/baner-list/banner-list'
import { BottomSheetModalWithRef } from '../../../../../components/bottom-sheet-menu/bottom-sheet-modal/bottom-sheet-modal-with-ref'
import {
  IModalComponentMethods,
  ModalComponent,
} from '../../../../../components/modal/modal-component'
import { ModalSearch } from '../../../../../components/modal/modal-search/modal-search'
import {
  ModalSorting,
  SortVariant,
} from '../../../../../components/modal/modal-sorting'
import { IModalMethods } from '../../../../../components/modal/modal-with-component'
import { useTypedSelector } from '../../../../../system'
import { adaptiveSize } from '../../../../../system/helpers/responseive-font-size'
import {
  sortingHelper,
  sortingHelperNew,
} from '../../../../../system/helpers/sorting-helper'
import { useSelectOrderTypeHandler } from '../../../../../system/hooks/use-select-order-type-handler'
import {
  getBanners,
  getCurrentAddress,
  getCurrentOrderType,
  getCurrentOrganisation,
  getOrderTypes,
  getProducts,
} from '../../../../../system/selectors/selectors'
import { BSMContentCreateNewAddress } from '../../../../delivery/view/components/bsm-content-create-new-address'
import { catalogConfig } from '../../../config'
import { ActionCatalog } from '../../../store/action-catalog'
import { IBannerItem } from '../../../types/types'
import { BSMContentBannerData } from '../bsm-content-banner-data'
import { BSMContentSelectOrderTypeWithStartApp } from '../bsm-content-select-order-type-with-start-app'
import { ParamsRow } from '../params-row/params-row'

interface ITabCatalogHeader {
  ignoreModal: boolean
}

export type TSetOrderTypeCallback =
  | ((cityId: number | undefined) => void)
  | null

export interface ISetOrderTypeRoutes {
  online: TSetOrderTypeCallback
  offline: TSetOrderTypeCallback
}

export const setOrderTypeRoutes: ISetOrderTypeRoutes = {
  online: null,
  offline: null,
}

export const CATALOG_HEADER_HEIGHT = 204

// const searchModalRef = React.createRef();

export const TabCatalogHeaderNew: React.FC<ITabCatalogHeader> = React.memo(
  ({ ignoreModal }) => {
    const bannerModalRef = useRef<BottomSheetModalMethods | null>(null)
    const setOrderTypeModalRef = useRef<BottomSheetModalMethods | null>(null)

    const [isVisibleSearchModal, setIsVisibleSearchModal] = useState(false)

    const initialOrderTypeModalRef = useRef<BottomSheetModalMethods | null>(
      null,
    )

    const isFirstLoadApp = useTypedSelector(
      (state) => state.system.isFirstLoadApp,
    )

    const modalWarningRef = useRef<IModalComponentMethods | null>(null)

    const banners = useTypedSelector(getBanners)
    const products = useTypedSelector(getProducts)
    const orderTypes = useTypedSelector(getOrderTypes)
    const currentAddress = useTypedSelector(getCurrentAddress)
    const currentOrderType = useTypedSelector(getCurrentOrderType)
    const currentOrganisation = useTypedSelector(getCurrentOrganisation)

    const catalogData = useTypedSelector((state) => state.catalog.catalogData)

    const dispatch = useDispatch()

    const refModalSorting = useRef<IModalMethods | null>(null)

    const isFocused = useIsFocused()
    const [selectedBanner, setSelectedBanner] = useState<IBannerItem>(
      banners?.[0],
    )

    const onPressBannerHandler = useCallback(
      (banner: IBannerItem) => {
        setSelectedBanner(banner)
        bannerModalRef.current?.present()
      },
      [bannerModalRef],
    )

    const openSearch = useCallback(() => {
      setIsVisibleSearchModal(true)
      // searchModalRef.current?.show();
    }, [])

    const closeSearch = useCallback(() => {
      setIsVisibleSearchModal(false)
    }, [])

    const onSorting = useCallback(
      (type: keyof typeof SortVariant) => {
        if (catalogData) {
          InteractionManager.runAfterInteractions(() => {
            dispatch(
              // ActionCatalog.sortingProducts(sortingHelper(products, type)),
              ActionCatalog.sortingProducts(
                sortingHelperNew(
                  catalogData.products,
                  catalogData.entities,
                  type,
                ),
              ),
            )
          })
        }

        refModalSorting.current?.close()
      },
      [dispatch, products],
    )

    const openSorting = useCallback(() => {
      refModalSorting.current?.show()
    }, [])

    const onSelectOrderType = useSelectOrderTypeHandler({
      offline: setOrderTypeRoutes.offline,
      online: setOrderTypeRoutes.online,
      willStartCallback: () => {
        setOrderTypeModalRef.current?.dismiss()
        // initialOrderTypeModalRef.current?.dismiss();
      },
    })

    const openSetOrders = useCallback(() => {
      if (catalogConfig.onPressSetOrderType) {
        catalogConfig.onPressSetOrderType()
        return
      }
      setOrderTypeModalRef.current?.present()
    }, [])

    const getButtonsHeight = useCallback(
      (withLastAddress?: boolean, isInitial?: boolean) => {
        const count = orderTypes.filter((el) => el.show_on_main).length

        const headerHeight = adaptiveSize(isInitial ? 150 : 100)
        const bottomMargin = adaptiveSize(40)
        const buttonsHeight = adaptiveSize(40 + 16)
        if (withLastAddress) {
          return headerHeight + (count + 1) * buttonsHeight + bottomMargin
        }
        return headerHeight + count * adaptiveSize(40 + 16) + bottomMargin
      },
      [orderTypes?.length],
    )
    const snapCreateNewAddress = useMemo(() => [getButtonsHeight()], [
      getButtonsHeight,
    ])

    const snapPointBanner = useMemo(() => ['60%'], [])

    const hasLastAddress = () => {
      if (currentOrderType?.id === undefined) {
        return false
      }
      if (currentOrderType.isRemoted && currentAddress?.id === undefined) {
        return false
      }
      if (
        !currentOrderType.isRemoted &&
        currentOrganisation?.id === undefined
      ) {
        return false
      }
      return true
    }

    const snapInitialModalSetOrderType = useMemo(
      () => [getButtonsHeight(hasLastAddress(), true)],
      [getButtonsHeight],
    )

    const onCloseInitialSetOrderTypeModal = useCallback(() => {
      // initialOrderTypeModalRef.current?.dismiss();
    }, [])

    useEffect(() => {
      const backHandler = BackHandler.addEventListener(
        'hardwareBackPress',
        () => {
          bannerModalRef.current?.dismiss()
          return true
        },
      )

      if (
        !ignoreModal &&
        isFocused &&
        isFirstLoadApp &&
        catalogConfig.enableInitialModalOrderType
      ) {
        // requestAnimationFrame(() => {
        //   initialOrderTypeModalRef?.current?.present();
        // });
      }

      return () => {
        backHandler.remove()
      }
    }, [])

    useEffect(() => {
      if (!isFocused) {
        setIsVisibleSearchModal(false)
      }
    }, [isFocused])

    return (
      <>
        {banners?.length ? (
          <BannerList banners={banners} onPress={onPressBannerHandler} />
        ) : null}
        <ParamsRow
          onPressSorting={openSorting}
          onPressSetAddress={openSetOrders}
          onPressSearch={openSearch}
        />
        <BottomSheetModalWithRef
          modalRef={bannerModalRef}
          snapPoints={snapPointBanner}
        >
          <BSMContentBannerData
            isVisibleButton={catalogConfig.isVisibleButtonInBannerInfoModal}
            item={selectedBanner}
          />
        </BottomSheetModalWithRef>

        <ModalSorting reference={refModalSorting} onSorting={onSorting} />

        <BottomSheetModalWithRef
          modalRef={setOrderTypeModalRef}
          snapPoints={snapCreateNewAddress}
        >
          <BSMContentCreateNewAddress onPress={onSelectOrderType} />
        </BottomSheetModalWithRef>
        <ModalComponent ref={modalWarningRef} />

        {catalogConfig.enableInitialModalOrderType ? (
          <BottomSheetModalWithRef
            dismissOnPanDown={false}
            modalRef={initialOrderTypeModalRef}
            snapPoints={snapInitialModalSetOrderType}
          >
            <BSMContentSelectOrderTypeWithStartApp
              close={onCloseInitialSetOrderTypeModal}
              onPress={onSelectOrderType}
              orderTypes={orderTypes}
              lastAddress={currentAddress}
            />
          </BottomSheetModalWithRef>
        ) : null}
        {isVisibleSearchModal ? (
          <ModalSearch
            closeSearch={closeSearch}
            isVisible={isVisibleSearchModal}
          />
        ) : null}
      </>
    )
  },
)
