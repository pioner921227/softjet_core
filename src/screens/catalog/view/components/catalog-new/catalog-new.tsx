import React, {useCallback, useMemo} from 'react';
import {
  FlatList,
  StyleSheet,
  Text,
  TextStyle,
  View,
  ViewStyle,
} from 'react-native';
import Animated, {
  Extrapolate,
  interpolate,
  runOnUI,
  scrollTo,
  useAnimatedRef,
  useAnimatedScrollHandler,
  useAnimatedStyle,
  useSharedValue,
} from 'react-native-reanimated';
import {Config} from '../../../../../config';
import {windowWidth} from '../../../../../system/helpers/window-size';
import {ICategoryItem} from '../../../types/types';
import {ProductListNew} from '../product-list/product-list-new';
import {TabBar, TRouteKey} from './components/tab-bar/tab-bar';

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

interface Catalog {
  HeaderTopComponent: React.FC;
  routes: ICategoryItem[];
  tabItemSelectedColor?: string;
  tabBarStyle?: ViewStyle;
  tabItemStyle?: ViewStyle;
  tabItemTextStyle?: TextStyle;
  onSelectTab: (key: TRouteKey) => number[];
}

export const CatalogContext = React.createContext({
  tabItemSelectedColor: '#ccc',
  tabBarStyle: {},
  tabItemStyle: {},
  tabItemTextStyle: {},
});

export const CatalogNew: React.FC<Catalog> = React.memo(
  ({
    HeaderTopComponent,
    tabItemSelectedColor,
    routes,
    tabBarStyle,
    tabItemStyle,
    tabItemTextStyle,
    onSelectTab,
  }) => {
    const animatedValueHorizontalList = useSharedValue(0);
    const animValue = useSharedValue(0);

    const selectedIndex_ = useSharedValue(0);

    const contentListRef = useAnimatedRef<FlatList>();

    const getProducts = useCallback(
      (id: number) => {
        return onSelectTab(id);
      },
      [onSelectTab],
    );

    const verticalListRef = useAnimatedRef<FlatList>();

    const horizontalListRenderItem = useCallback(
      ({item}) => {
        return (
          <ProductListNew
            animValue={animValue}
            ref={verticalListRef}
            productIds={getProducts(item.id)}
          />
        );
      },
      [getProducts],
    );

    const setIndexHandler = useCallback(
      (index: number) => {
        runOnUI(value => {
          'worklet';
          scrollTo(contentListRef, value * windowWidth, 0, false);
        })(index);
      },
      [contentListRef],
    );

    const headerTopBlockAnimatedStyle = useAnimatedStyle(() => {
      return {
        zIndex: 1,
        width: '100%',
        height: 210,
        opacity: interpolate(
          animValue.value,
          [0, 1],
          [1, 0],
          Extrapolate.CLAMP,
        ),
      };
    }, []);

    const getItemLayoutHorizontalList = useCallback(
      (data, index) => ({
        length: windowWidth,
        offset: windowWidth * index,
        index,
      }),
      [],
    );
    //
    const keyExtractorHorizontalList = useCallback(
      item => item?.id?.toString(),
      [],
    );

    const contextData_ = useMemo(() => {
      return {
        tabItemSelectedColor: tabItemSelectedColor ?? '#ccc',
        tabBarStyle: tabBarStyle ?? {},
        tabItemStyle: tabItemStyle ?? {},
        tabItemTextStyle: tabItemTextStyle ?? {},
      };
    }, []);

    const onScrollHorizontalList = useAnimatedScrollHandler(
      (event, ctx: {index: number}) => {
        animatedValueHorizontalList.value = event.contentOffset.x;
        const index = Math.round(event.contentOffset.x / windowWidth);
        if (ctx.index !== index) {
          selectedIndex_.value = index;
        }
      },
      [],
    );

    const bottomTabsAnimStyle = useAnimatedStyle(() => {
      return {
        borderBottomWidth: interpolate(
          animValue.value,
          [0, 1],
          [0, 2],
          Extrapolate.CLAMP,
        ),
        borderBottomColor: Config.Color.GREY_50,
      };
    });

    const containerAnimatedStyle = useAnimatedStyle(() => {
      return {
        transform: [
          {translateY: interpolate(animValue.value, [0, 1], [0, -210])},
        ],
      };
    });

    return (
      <CatalogContext.Provider value={contextData_}>
        <Animated.View style={containerAnimatedStyle}>
          <Animated.View style={headerTopBlockAnimatedStyle}>
            <HeaderTopComponent />
          </Animated.View>
          <Animated.View style={[styles.tabWrapper, bottomTabsAnimStyle]}>
            {routes.length ? (
              <TabBar
                animValue={animatedValueHorizontalList}
                selectedIndex={selectedIndex_}
                onSelect={setIndexHandler}
                routes={routes}
              />
            ) : null}
          </Animated.View>
          {routes.length ? (
            <AnimatedFlatList
              onScroll={onScrollHorizontalList}
              keyExtractor={keyExtractorHorizontalList}
              getItemLayout={getItemLayoutHorizontalList}
              windowSize={5}
              maxToRenderPerBatch={5}
              initialNumToRender={5}
              ref={contentListRef}
              scrollEventThrottle={1}
              showsHorizontalScrollIndicator={false}
              pagingEnabled={true}
              horizontal={true}
              data={routes}
              renderItem={horizontalListRenderItem}
            />
          ) : (
            <View />
          )}
        </Animated.View>
      </CatalogContext.Provider>
    );
  },
);

const styles = StyleSheet.create({
  tabView: {
    backgroundColor: '#fff',
    paddingVertical: 10,
  },
  tabWrapper: {
    zIndex: 2,
  },
  tabItem: {
    justifyContent: 'center',
    height: 50,
    marginRight: 10,
    borderRadius: 20,
  },
  renderItemVerticalList: {
    width: windowWidth,
  },
  contentContainer: {},
  horizontalListRenderItem: {
    width: windowWidth,
    transform: [{translateY: -200}],
  },
});
