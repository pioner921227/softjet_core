import {
  StyleSheet,
  Text,
  TextStyle,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useCallback, useContext, useMemo} from 'react';
import Animated, {useAnimatedStyle} from 'react-native-reanimated';
import {CatalogContext} from './catalog-new';
import {IRouteItem, TRouteKey} from './components/tab-bar/tab-bar';
import {Config} from '../../../../../config';
import {catalogConfig} from '../../../config';

const {Color} = Config;

interface ITabItem {
  item: IRouteItem;
  index: number;
  onSelect: (index: number, key: TRouteKey) => void;
  selectedIndex: Animated.SharedValue<number>;
}

const TextAnimated = Animated.createAnimatedComponent(Text);

const TouchableOpacityAnimated = Animated.createAnimatedComponent(
  TouchableOpacity,
);

export const VALUE_FOR_ITEM_WIDTH = 10;
export const INDENT = 30;

export const getTabWidth = (title: string) => {
  return title.length * VALUE_FOR_ITEM_WIDTH + INDENT;
};

export const TabItem: React.FC<ITabItem> = React.memo(
  ({item, index, onSelect, selectedIndex}) => {
    const {tabItemSelectedColor, tabItemStyle, tabItemTextStyle} = useContext(
      CatalogContext,
    );

    const onSelectHandler = useCallback(() => {
      onSelect(index, item.id);
    }, [index, item.id, onSelect]);

    const labelStyleAnimated = useAnimatedStyle(() => {
      return {
        color: selectedIndex.value === index ? Color.WHITE : Color.GREY_600,
      };
    }, []);

    const labelStyle_ = useMemo(
      () => [
        {
          ...tabItemTextStyle,
          width: getTabWidth(item.title),
          textAlign: 'center',
          fontSize: 15,
          fontWeight: 'bold',
        },
        labelStyleAnimated,
      ],
      [(item.title, tabItemTextStyle)],
    );

    const containerStyle = useAnimatedStyle(() => {
      return {
        backgroundColor:
          selectedIndex.value === index ? tabItemSelectedColor : Color.WHITE,
      };
    }, []);

    const style_ = useMemo(() => {
      return [styles.tabItem, tabItemStyle, containerStyle];
    }, [index, tabItemSelectedColor, tabItemStyle]);

    return (
      <TouchableOpacityAnimated onPress={onSelectHandler} style={style_}>
        <TextAnimated style={labelStyle_}>{item.title}</TextAnimated>
      </TouchableOpacityAnimated>
    );
  },
);

const styles = StyleSheet.create({
  tabItem: {
    justifyContent: 'center',
    height: 38,
    // marginRight: 10,
    borderRadius: catalogConfig.categoryTabBorderRadius,
    // paddingHorizontal: 10,
  },
});
