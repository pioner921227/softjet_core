import {FlatList, Platform, StyleSheet} from 'react-native';
import React, {useCallback, useContext, useMemo} from 'react';
import {Color} from '../../../../../../../assets/styles';
import {getTabWidth, TabItem} from '../../tab-item';
import {CatalogContext} from '../../catalog-new';
import Animated, {
  interpolate,
  scrollTo,
  useAnimatedReaction,
  useAnimatedRef,
} from 'react-native-reanimated';
import {windowWidth} from '../../../../../../../system/helpers/window-size';

export type TRouteKey = number | string;

export interface IRouteItem {
  id: number;
  title: string;
}

export interface ITabBar {
  routes: IRouteItem[];
  onSelect: (index: number, key: TRouteKey) => void;
  selectedIndex: Animated.SharedValue<number>;
  animValue: Animated.SharedValue<number>;
}

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

const createOffset = (offset: number, width: number) => {
  return offset > windowWidth / 2
    ? offset - width - windowWidth / 2 + width / 2 + 16
    : 0;
};

export const TabBar: React.FC<ITabBar> = React.memo(
  ({routes, selectedIndex, onSelect, animValue}) => {
    const {tabBarStyle} = useContext(CatalogContext);

    const inputRange = useMemo(() => routes.map((_, i) => i * windowWidth), [
      routes,
    ]);

    const outputRange = useMemo(() => {
      let value = 0;
      return routes.map(el => {
        value += getTabWidth(el.title);
        return createOffset(value, getTabWidth(el.title));
      });
    }, [routes]);

    const renderTabItem = useCallback(props => {
      return (
        <TabItem {...props} onSelect={onSelect} selectedIndex={selectedIndex} />
      );
    }, []);

    const keyExtractor = useCallback(item => item?.id?.toString(), []);

    const tabStyle_ = useMemo(() => {
      return [styles.tabBarContentContainer, tabBarStyle];
    }, [tabBarStyle]);

    const ref = useAnimatedRef<FlatList>();

    useAnimatedReaction(
      () => {
        return animValue.value;
      },
      data => {
        //@ts-ignore
        scrollTo(ref, interpolate(data, inputRange, outputRange), 0, true);
      },
    );

    return (
      <AnimatedFlatList
        keyExtractor={keyExtractor}
        showsVerticalScrollIndicator={false}
        ref={ref}
        // animatedProps={props}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={tabStyle_}
        removeClippedSubviews={true}
        initialNumToRender={5}
        // updateCellsBatchingPeriod={300}
        maxToRenderPerBatch={5}
        windowSize={5}
        scrollEventThrottle={1}
        horizontal={true}
        data={routes}
        renderItem={renderTabItem}
      />
    );
  },
);

const styles = StyleSheet.create({
  tabBarContentContainer: {
    // backgroundColor: Color.WHITE,
    paddingVertical: 10,
  },
  tabShadow: {
    backgroundColor: Color.WHITE,
    // borderRadius: 8,
    ...Platform.select({
      android: {},
      ios: {
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 5,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.5,
        zIndex: 2,
      },
    }),
  },
});
