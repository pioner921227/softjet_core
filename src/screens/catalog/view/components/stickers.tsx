import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {ISticker} from '../../types/types';
import {Config} from '../../../../config';

const {UIStyles, RADIUS} = Config;

interface IStickers {
  stickers: ISticker[];
}

export const Stickers: React.FC<IStickers> = ({stickers}) => {
  return stickers?.length ? (
    <View style={styles.container}>
      {stickers.map(el => {
        return (
          <View
            key={el.id}
            style={[styles.wrapper, {backgroundColor: el.color_bg}]}>
            <Text style={[styles.label, {color: el.color_text}]}>
              {el.title}
            </Text>
          </View>
        );
      })}
    </View>
  ) : null;
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  label: {
    ...UIStyles.font9b,
    paddingVertical: 2,
    paddingHorizontal: 4,
  },
  wrapper: {
    borderRadius: RADIUS.SMALL,
    marginRight: 4,
    marginBottom: 2,
  },
});
