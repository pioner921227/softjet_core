import React, { useCallback } from 'react'
import { Animated, SafeAreaView, StyleSheet } from 'react-native'
import { useTypedSelector } from '../../../system/hooks/use-typed-selector'
import { RouteProp, useRoute } from '@react-navigation/native'
import { TRootStackParamList } from '../../../navigation/types/types'
import { Routes } from '../../../navigation'
import { CatalogNew } from './components/catalog-new/catalog-new'
import { TabCatalogHeaderNew } from './components/tab-catalog/tab-catalog-header_new'
import { Color } from '../../../assets/styles'
import { TRouteKey } from './components/catalog-new/components/tab-bar/tab-bar'
import { useInitCatalog } from '../../../system/hooks/use-init-catalog'
import { ICategoryItem } from '../store/decode-structures'
import { catalogConfig } from '..'

export const CatalogContext = React.createContext({
  scrollY: new Animated.Value(0),
  offset: 0,
})

const emptyArray: ICategoryItem[] = []

export const Catalog: React.FC = React.memo(() => {
  const route = useRoute<RouteProp<TRootStackParamList, Routes.Catalog>>()

  const catalog = useTypedSelector((state) => state.catalog.catalogData)

  const sortedCatalog = useTypedSelector((state) => state.catalog.sortedCatalog)

  useInitCatalog(route.params?.ignoreGetCatalog)

  const filteredProductsForCategory = useCallback(
    (id: TRouteKey) => {
      if (sortedCatalog) {
        return sortedCatalog.entities[id]
      } else {
        return []
      }
    },
    [sortedCatalog],
  )

  const HeaderComponent = useCallback(() => {
    return <TabCatalogHeaderNew ignoreModal={true} />
  }, [])

  return (
    <SafeAreaView style={styles.container}>
      <CatalogNew
        onSelectTab={filteredProductsForCategory}
        tabBarStyle={styles.tabBarStyle}
        routes={catalog?.categories || emptyArray}
        tabItemSelectedColor={catalogConfig.categoryTabSelectedColor}
        HeaderTopComponent={HeaderComponent}
      />
    </SafeAreaView>
  )
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tabBarStyle: {
    paddingHorizontal: 16,
  },
})
