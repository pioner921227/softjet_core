export enum BannerTypesEnum {
  promo = 'promo',
  product = 'product',
  info = 'info',
}

export interface IBannerItem {
  id: number
  title: string
  content: string
  date_end: string
  product_id: number
  promocode: string
  type: number
  img_big: string
  img_small: string
  url: string
}

export enum ProductCardType {
  base = 'base',
  construct = 'construct',
  halves = 'halves',
}

export interface IProductItem {
  id: number
  title: string
  category_ids: number[]
  parent: number
  allRating: number
  article: number
  rating: number
  price: number
  original_price: number
  max_price: number
  desc: string
  content: string
  popular: number
  card_type: keyof typeof ProductCardType
  favorite: number
  img_big: string
  img_small: string
  bonus_add: number
  times_bought: number
  remains_count: number
  remains_text: string
  barcode: string
  pieces_per_package: number
  energy: IEnergyItems
  tags_list: []
  additives: IProductAdditive[]
  ingredients: IIngredient[]
  types: ITypes
  analogs: []
  stickers: ISticker[]
  sizes: IProductSize
}

export interface IIngredient {
  master: number
  id: number
  title: string
  price: number
  old_price: number
  original_price: number
}
export interface ITypes extends IProductSize {}

export interface IProductSize {
  default_index: number
  data: Array<IIngredient>
}

export interface IProductAdditive extends IIngredient {
  img_url: string
}

export interface ISticker {
  master: number
  id: number
  title: string
  color_text: string
  color_bg: string
  id_org: number
}

export interface IEnergyItems {
  energy_size: number
  energy_value: number
  energy_allergens: number
  energy_carbohydrates: number
  energy_protein: number
  energy_oils: number
}

// export interface ICategoryItem {
//   id: number;
//   parent: number;
//   title: string;
//   description: string;
//   image: string;
// }

export interface ICategoryItem {
  id: number
  title: string
}

export interface IEntitiesCatalog {
  [key: string]: number[]
}

export interface ICatalogData {
  products: { [key: string]: IProductItem }
  categories: Array<ICategoryItem>
  entities: IEntitiesCatalog
}

export type TGetBannerListResponse = Array<IBannerItem>
