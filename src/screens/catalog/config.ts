import { Config } from '../../config'

export enum setOrderTypeButtonTitleResource {
  currentAddress = 'currentAddress',
  currentCity = 'currentCity',
}

export enum deliveryDataTypesForButton {
  city = 'city',
  address = 'address',
  orderType = 'orderType',
}

interface ICatalogConfig {
  enableInitialModalOrderType: boolean
  onPressSetOrderType: null | (() => void)
  setOrderTypeButtonTitleResource: keyof typeof setOrderTypeButtonTitleResource
  setDeliveryDataResource: {
    titleType: keyof typeof deliveryDataTypesForButton
    subtitleType: keyof typeof deliveryDataTypesForButton
    enableSubtitle: boolean
  }
  categoryTabSelectedColor: string
  categoryTabBorderRadius: number
  isVisibleButtonInBannerInfoModal: boolean
  isVisibleStickerMenu: boolean
  paramsMenu: {
    isVisibleSearchButton: boolean
    isVisibleSortingButton: boolean
    setAddressModalText: string
  }
}

export const catalogConfig: ICatalogConfig = {
  enableInitialModalOrderType: true,
  onPressSetOrderType: null,
  setOrderTypeButtonTitleResource:
    setOrderTypeButtonTitleResource.currentAddress,
  setDeliveryDataResource: {
    titleType: deliveryDataTypesForButton.orderType,
    subtitleType: deliveryDataTypesForButton.address,
    enableSubtitle: true,
  },
  categoryTabSelectedColor: Config.Color.SECONDARY,
  categoryTabBorderRadius: Config.RADIUS.CATEGORY_ITEM_RADIUS,
  isVisibleButtonInBannerInfoModal: true,
  isVisibleStickerMenu: true,
  paramsMenu: {
    isVisibleSearchButton: true,
    isVisibleSortingButton: true,
    setAddressModalText:
      'Если вы смените адрес, то вам придется заново составлять меню, так как в другом заведении может быть другое меню',
  },
}
