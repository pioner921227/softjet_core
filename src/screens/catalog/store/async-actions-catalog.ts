import { asyncActionCreator } from '../../../system/root-store/action-creator'
import { RequestCatalog } from '../api/request-catalog'
import { ICatalogData, TGetBannerListResponse } from '../types/types'
import {
  IGetBannersDataRequest,
  IGetCategoriesDataRequest,
  IGetCategoriesResponse,
  IGetProductsRequest,
  IGetProductsResponse,
} from '../api/types'

export class AsyncActionsCatalog {
  static getBanners = asyncActionCreator<
    IGetBannersDataRequest,
    TGetBannerListResponse,
    Error
  >('CATALOG/GET_BANNERS_LIST', RequestCatalog.getBanners)
  static getProducts = asyncActionCreator<
    IGetProductsRequest,
    IGetProductsResponse,
    Error
  >('CATALOG/GET_PRODUCTS', RequestCatalog.getProducts)
  static getCatalog = asyncActionCreator<
    IGetProductsRequest,
    ICatalogData,
    Error
  >('CATALOG/GET_CATALOG', RequestCatalog.getCatalog)
  static getCategories = asyncActionCreator<
    IGetCategoriesDataRequest,
    IGetCategoriesResponse,
    Error
  >('CATALOG/GET_CATEGORIES', RequestCatalog.getCategories)
}
