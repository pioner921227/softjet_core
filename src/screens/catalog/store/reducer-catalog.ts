import {
  ReducerBuilder,
  reducerWithInitialState,
} from 'typescript-fsa-reducers'
import { initialStateCatalog, IStoreCatalog } from './store-catalog'
import { Success } from 'typescript-fsa'
import {
  ICatalogData,
  IEntitiesCatalog,
  IProductItem,
  TGetBannerListResponse,
} from '../types/types'
import { AsyncActionsCatalog } from './async-actions-catalog'
import { IGetCategoriesResponse, IGetProductsResponse } from '../api/types'
import { ActionsLogin } from '../../login/store/actions-login'
import { ActionCatalog } from './action-catalog'
import { Decodable } from 'decodable-js'
import {
  decodeBannersStruct,
  decodeCategoriesStruct,
} from './decode-structures'

const getBannersListHandlerStarted = (store: IStoreCatalog): IStoreCatalog => {
  return {
    ...store,
    isLoadingBanners: true,
    error: false,
  }
}

const getBannersListHandlerDone = (
  store: IStoreCatalog,
  { result }: Success<any, TGetBannerListResponse>,
): IStoreCatalog => {
  // const decodeBanners = Decodable(result, [decodeBannersStruct]);

  return {
    ...store,
    isLoadingBanners: false,
    error: false,
    banners: result,
  }
}

const getBannersListHandlerFailed = (store: IStoreCatalog): IStoreCatalog => {
  return {
    ...store,
    isLoadingBanners: false,
    error: true,
  }
}

const getCatalogHandlerDone = (
  store: IStoreCatalog,
  { result }: Success<any, ICatalogData>,
): IStoreCatalog => {
  return {
    ...store,
    catalogData: result,
    sortedCatalog: result,
  }
}

const getProductsHandlerStarted = (store: IStoreCatalog): IStoreCatalog => {
  return {
    ...store,
    isLoadingProducts: true,
    error: false,
  }
}

const getProductsHandlerDone = (
  store: IStoreCatalog,
  { result }: Success<any, IGetProductsResponse>,
): IStoreCatalog => {
  // const normalized = result?.data?.reduce<{[key: string]: IProductItem[]}>(
  //   (acc, el) => {
  //     el.category_ids.forEach(id => {
  //       if (acc[id] === undefined) {
  //         acc[id] = [el];
  //       } else {
  //         acc[id].push(el);
  //       }
  //     });
  //     return acc;
  //   },
  //   {},
  // );

  return {
    ...store,
    isLoadingProducts: false,
    error: false,
    products: result?.data ?? store.products,
    sortedProducts: result?.data ?? store.sortedProducts,
    // normalizedProducts: normalized,
  }
}

const getProductsHandlerFailed = (store: IStoreCatalog): IStoreCatalog => {
  return {
    ...store,
    isLoadingProducts: false,
    error: true,
  }
}

const getCategoriesHandlerStarted = (store: IStoreCatalog): IStoreCatalog => {
  return {
    ...store,
    isLoadingCategories: true,
    error: false,
  }
}

const getCategoriesHandlerDone = (
  store: IStoreCatalog,
  { result }: Success<any, IGetCategoriesResponse>,
): IStoreCatalog => {
  const categories =
    result?.data && Array.isArray(result.data) ? result.data : store.categories

  const decodeResult = Decodable(result, decodeCategoriesStruct)

  return {
    ...store,
    isLoadingCategories: false,
    error: false,
    // categories: decodeResult.data,
    categories: decodeResult.data,
  }
}

const getCategoriesHandlerFailed = (store: IStoreCatalog): IStoreCatalog => {
  return {
    ...store,
    isLoadingCategories: false,
    error: true,
  }
}

const sortingProductsHandler = (
  store: IStoreCatalog,
  entities: IEntitiesCatalog,
): IStoreCatalog => {
  const newData = store.sortedCatalog ? { ...store.sortedCatalog } : null
  newData && (newData.entities = entities)

  return {
    ...store,
    sortedCatalog: newData,
  }
}

const setActivePromoCodesHandler = (
  store: IStoreCatalog,
  id: number,
): IStoreCatalog => {
  const ids = store.bannersActivePromoCode.filter((el) => el !== id)
  ids.push(id)

  return {
    ...store,
    bannersActivePromoCode: ids,
  }
}

const resetHandler = (store: IStoreCatalog): IStoreCatalog => {
  return {
    ...store,
    bannersActivePromoCode: [],
  }
}

export const reducerCatalog: ReducerBuilder<IStoreCatalog> = reducerWithInitialState(
  initialStateCatalog,
)
  .case(ActionsLogin.logoutAccount, resetHandler)
  .case(
    AsyncActionsCatalog.getBanners.async.started,
    getBannersListHandlerStarted,
  )
  .case(AsyncActionsCatalog.getBanners.async.done, getBannersListHandlerDone)
  .case(
    AsyncActionsCatalog.getBanners.async.failed,
    getBannersListHandlerFailed,
  )
  .case(
    AsyncActionsCatalog.getCategories.async.started,
    getCategoriesHandlerStarted,
  )
  .case(AsyncActionsCatalog.getCategories.async.done, getCategoriesHandlerDone)
  .case(
    AsyncActionsCatalog.getCategories.async.failed,
    getCategoriesHandlerFailed,
  )
  .case(
    AsyncActionsCatalog.getProducts.async.started,
    getProductsHandlerStarted,
  )
  .case(AsyncActionsCatalog.getCatalog.async.done, getCatalogHandlerDone)
  .case(AsyncActionsCatalog.getProducts.async.done, getProductsHandlerDone)
  .case(AsyncActionsCatalog.getProducts.async.failed, getProductsHandlerFailed)
  .case(ActionCatalog.setActivePromoCodes, setActivePromoCodesHandler)
  .case(ActionCatalog.sortingProducts, sortingProductsHandler)
