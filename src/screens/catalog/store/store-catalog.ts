import {
  ICatalogData,
  ICategoryItem,
  IProductItem,
  TGetBannerListResponse,
} from '../types/types'

export interface IStoreCatalog {
  banners: TGetBannerListResponse
  bannersActivePromoCode: number[]
  products: IProductItem[]
  sortedProducts: IProductItem[]
  catalogData: ICatalogData | null
  sortedCatalog: ICatalogData | null

  // normalizedProducts: {[key: string]: IProductItem[]} | null;
  categories: ICategoryItem[]
  error: boolean
  isLoading: boolean
  isLoadingBanners: boolean
  isLoadingProducts: boolean
  isLoadingCategories: boolean
}

export const initialStateCatalog: IStoreCatalog = {
  banners: [],
  bannersActivePromoCode: [],
  products: [],
  sortedProducts: [],
  catalogData: null,
  sortedCatalog: null,

  // normalizedProducts: null,
  categories: [],
  error: false,
  isLoading: false,
  isLoadingBanners: false,
  isLoadingProducts: false,
  isLoadingCategories: false,
}
