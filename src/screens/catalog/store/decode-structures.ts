import {T} from 'decodable-js';
import {IBannerItem} from '../types/types';
import {IGetCategoriesResponse} from '../api/types';

export const decodeBannersStruct: IBannerItem = {
  id: T.number,
  title: T.string,
  content: T.string,
  date_end: T.string,
  product_id: T.number,
  promocode: T.string,
  type: T.number,
  img_big: T.string,
  img_small: T.string,
  url: T.string,
};

export interface ICategoryItem {
  id: number;
  parent: number;
  title: string;
  description: string;
  image: string;
}

export const decodeCategoryItemStruct: ICategoryItem = {
  id: T.number,
  parent: T.number,
  title: T.string,
  description: T.string,
  image: T.string,
};
export const decodeCategoriesStruct: IGetCategoriesResponse = {
  data: [decodeCategoryItemStruct],
};
