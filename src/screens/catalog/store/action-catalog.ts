import { actionCreator } from '../../../system/root-store/action-creator'
import { IEntitiesCatalog, IProductItem } from '../types/types'

export class ActionCatalog {
  static sortingProducts = actionCreator<IEntitiesCatalog>(
    'CATALOG/SORTING_PRODUCTS',
  )
  static setActivePromoCodes = actionCreator<number>(
    'CATALOG/SET_ACTIVE_PROMO_CODES',
  )
}
