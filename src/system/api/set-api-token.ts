import {ApiService} from './api-service';

export const setApiToken = (token: string) => {
  ApiService.defaults.headers.Authorization = `Bearer ${token}`;
};
