import { IMinRequestData } from "../../screens/cart/api/types";
import { ApiService } from "./api-service";

export class RequestSystem {
  static sendError(msg: string) {
    return ApiService.get(`http://core.appsj.su/api/tg/bot?message=${msg}`);
  }
  static setFCMToken(token: string) {
    return ApiService.post("/api/user/setFirebaseToken", { fcm_token: token });
  }
  static logOutRemote(data: IMinRequestData) {
    return ApiService.post("/api/cart/logOut", data);
  }
}
