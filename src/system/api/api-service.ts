import axios from 'axios';
import {Config} from '../../config';

export const ApiService = axios.create({
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
    // Authorization: 'Bearer abf038b312fca2fc08cad87a1522d73a',
    'x-api-token': 'htug9ji0oqpr329442u891q3sdasw',
    // sensitive: true,
  },
  baseURL: Config.BASE_URL,
  timeout: 7000,
});



// ApiService.interceptors.response.use(
//   (response: AxiosResponse) => {
//     if (__DEV__) {
//       console.log('------RESPONSE-----', response);
//     }
//     return response.data;
//   },
//   (error: AxiosError) => {
//     if (error.code === '401') {
//
//     }
//     if (__DEV__) {
//       console.log('-----RESPONSE ERROR-------', error);
//     }
//   },
// );

// ApiService.interceptors.request.use(
//   function (config) {
//     if (__DEV__) {
//       // console.log('------REQUEST-------', config);
//     }
//     return config;
//   },
//   function (error) {
//     if (__DEV__) {
//       // console.log('------REQUEST ERROR-------', error);
//     }
//     return Promise.reject(error);
//   },
// );
