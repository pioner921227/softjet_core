import DeviceInfo from 'react-native-device-info';
import {ActionMore} from '../../screens/more/store/action-more';
import {useDispatch} from 'react-redux';
import {useTypedSelector} from './use-typed-selector';
import {useEffect} from 'react';
import {InteractionManager} from 'react-native';

export const useSetAppVersion = () => {
  const appVersion = DeviceInfo.getVersion();
  const savedAppVersion = useTypedSelector(state => state.more.appVersion);
  const dispatch = useDispatch();

  useEffect(() => {
    if (savedAppVersion !== appVersion) {
      InteractionManager.runAfterInteractions(() => {
        dispatch(ActionMore.setAppVersion(appVersion));
      });
    }
  }, []);
};
