import { useIsFocused } from '@react-navigation/native'
import { useEffect, useState } from 'react'
import { localDb } from '../../App'
import { AsyncActionCart } from '../../screens/cart'
import { AsyncActionsCatalog } from '../../screens/catalog'
import {
  getCurrentOrderTypeId,
  getCurrentOrganisationId,
  getPolygonId,
  getToken,
} from '../selectors/selectors'
import { ActionsSystem } from '../store/actions-system'
import { useClearCart } from './use-clear-cart'
import { useTypedDispatch } from './use-typed-dispatch'
import { useTypedSelector } from './use-typed-selector'

interface IDeliveryData {
  id_org: number
  id_order_type: number
}

export const useInitCatalog = (ignoreLoad: boolean = false) => {
  const currentOrderTypeId = useTypedSelector(getCurrentOrderTypeId)
  const currentOrganisationId = useTypedSelector(getCurrentOrganisationId)
  const polygonId = useTypedSelector(getPolygonId)
  const dispatch = useTypedDispatch()
  const isFocused = useIsFocused()
  const [isLoadingCatalog, setIsLoadingCatalog] = useState(false)

  const token = useTypedSelector(getToken)
  const isFirstLoadApp = useTypedSelector(
    (state) => state?.system?.isFirstLoadApp,
  )

  const clearCart = useClearCart()

  const isDifferentData = async () => {
    const oldData = await localDb.get<IDeliveryData>('data')
    return (
      currentOrderTypeId != oldData?.id_order_type ||
      currentOrganisationId != oldData?.id_org
    )
  }

  const init = async () => {
    const isDifferent = await isDifferentData()
    if (
      token &&
      currentOrderTypeId !== undefined &&
      currentOrganisationId !== undefined
    ) {
      await dispatch(
        AsyncActionCart.getCartData({
          id_org: currentOrganisationId,
          order_type: +currentOrderTypeId,
          polygon_id: polygonId,
        }),
      )
    }
    if (isDifferent) {
      clearCart()
    }
    ;(isDifferent || isFirstLoadApp) && (await loadCatalog())
  }

  const loadCatalog = async () => {
    if (
      currentOrganisationId !== undefined &&
      currentOrderTypeId !== undefined
    ) {
      dispatch(
        AsyncActionsCatalog.getBanners({
          order_type: currentOrderTypeId,
          id_org: currentOrganisationId,
        }),
      )
      setIsLoadingCatalog(true)
      await dispatch(
        AsyncActionsCatalog.getCatalog({
          order_type: currentOrderTypeId,
          id_org: currentOrganisationId,
        }),
      )
      setIsLoadingCatalog(false)

      localDb.set<IDeliveryData>({
        _id: 'data',
        id_org: currentOrganisationId,
        id_order_type: currentOrderTypeId,
      })
    }
  }

  useEffect(() => {
    if (ignoreLoad) {
      return
    }
    if (isFocused && currentOrganisationId && currentOrderTypeId) {
      init().then(() => {
        dispatch(ActionsSystem.setIsFirstLoadApp())
      })
    }
  }, [isFocused, currentOrderTypeId, currentOrganisationId])

  return {
    isLoadingCatalog,
  }
}
