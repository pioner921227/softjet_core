import {useMemo} from 'react';
import {urlValidator} from '../helpers/url-validator';
import {ImageRepository} from '../../assets/image-repository';

export const useImage = (
  url: string,
  priority: 'low' | 'normal' | 'high' = 'high',
) => {
  return useMemo(
    () =>
      url
        ? {uri: urlValidator(url), priority: priority}
        : ImageRepository.ProductImagePlaceholder,
    [url, priority],
  );
};
