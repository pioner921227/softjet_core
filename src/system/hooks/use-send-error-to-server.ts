import {useDispatch} from 'react-redux';
import {useTypedSelector} from './use-typed-selector';
import {getUserData} from '../selectors/selectors';
import {AsyncActionSystem} from '../store/async-action-system';
import {createString} from '../helpers/send-error-to-server';

export const useSendErrorToServer = () => {
  const dispatch = useDispatch();
  const phone = useTypedSelector(getUserData)?.phone;

  const sendError = (error: string, method: string) => {
    dispatch(AsyncActionSystem.sendError(createString(error, method, phone)));
  };

  return sendError;
};
