import {LayoutChangeEvent, LayoutRectangle} from 'react-native';
import {useState} from 'react';

export const useLayout = () => {
  const [values, setValues] = useState<LayoutRectangle>({
    width: 0,
    height: 0,
    x: 0,
    y: 0,
  });

  const onLayout = (props: LayoutChangeEvent) => {
    setValues({
      width: props.nativeEvent.layout.width,
      height: props.nativeEvent.layout.height,
      x: props.nativeEvent.layout.x,
      y: props.nativeEvent.layout.y,
    });
  };
  return {
    onLayout,
    values,
  };
};
