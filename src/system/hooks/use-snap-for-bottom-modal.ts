import {useTypedSelector} from './use-typed-selector';
import {
  getCurrentAddress,
  getCurrentOrderType,
  getOrderTypes,
} from '../selectors/selectors';
import { adaptiveSize } from '../helpers/responseive-font-size';

export const useSnapForBottomModal = () => {
  const orderTypes = useTypedSelector(getOrderTypes);
  const currentOrderType = useTypedSelector(getCurrentOrderType);
  const currentAddress = useTypedSelector(getCurrentAddress);

  return (withLastAddress?: boolean) => {
    const count = orderTypes?.length ?? 0;
    const optionalCount = currentOrderType?.isRemoted ? +!!currentAddress : 1;
    const buttonHeight = adaptiveSize(60);
    const bottomMargin = adaptiveSize(40);

    if (withLastAddress) {
      return 120 + (count + optionalCount) * buttonHeight;
    }
    return 100 + count * buttonHeight + bottomMargin;
  };
};
