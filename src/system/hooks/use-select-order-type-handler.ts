import { ActionDelivery } from "../../screens/delivery";
import { rootNavigate } from "../helpers/root-navigation";
import { Routes } from "../../navigation";
import { BottomSheetMenuType } from "../../screens/delivery/view/organisation-list-with-map/organisation-list-with-map";
import { useDispatch } from "react-redux";
import { IOrderTypesItem } from "../../screens/delivery/types/types";
import { useTypedSelector } from "./use-typed-selector";
import { TSetOrderTypeCallback } from "../../screens/catalog/view/components/tab-catalog/tab-catalog-header";
import { useCallback } from "react";
import { getOrganisations } from "../selectors/selectors";

interface IUseSelectOrderTypeHandler {
  offline?: TSetOrderTypeCallback;
  online?: TSetOrderTypeCallback;
  finishCallback?: () => void;
  willStartCallback?: () => void;
}

export const useSelectOrderTypeHandler = ({
  online,
  offline,
  willStartCallback,
  finishCallback,
}: IUseSelectOrderTypeHandler) => {
  const dispatch = useDispatch();

  const currentCityId = useTypedSelector(
    (state) => state.delivery.currentCity?.id
  );
  const organisations = useTypedSelector(getOrganisations);

  const callback = useCallback(
    (type: IOrderTypesItem) => {
      willStartCallback?.();
      dispatch(ActionDelivery.setCurrentOrderType(+type.id));
      setTimeout(() => {
        if (type.isRemoted) {
          if (online) {
            online(currentCityId);
          } else {
            rootNavigate(Routes.AddressList);
          }
        } else {
          if (offline) {
            offline(currentCityId);
          } else {
            if (organisations.length > 1) {
              rootNavigate(Routes.SelectOrganisationInMap, {
                bottomSheetMenuType: BottomSheetMenuType.Organisations,
              });
            } else {
              organisations?.[0]?.id !== undefined &&
                dispatch(
                  ActionDelivery.setCurrentOrganisation(organisations[0].id)
                );
            }
          }
        }
      }, 100);

      finishCallback?.();
    },
    [currentCityId]
  );
  return callback;
};
