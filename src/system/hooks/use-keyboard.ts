import {Keyboard, KeyboardEvent, Platform, ScrollView} from 'react-native';
import React, {useCallback, useEffect} from 'react';
import {useIsFocused} from '@react-navigation/native';

export const useKeyboard = (ref: React.MutableRefObject<ScrollView | null>) => {
  const isFocused = useIsFocused();

  const handler = useCallback(
    (props: KeyboardEvent) => {
      ref.current?.scrollTo({
        x: 0,
        y: props.endCoordinates.height,
        animated: true,
      });
    },
    [ref],
  );

  useEffect(() => {
    if (!isFocused) {
      Keyboard.removeListener('keyboardWillShow', handler);
      Keyboard.removeListener('keyboardDidShow', handler);
    }
  }, [isFocused]);

  useEffect(() => {
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
      handler,
    );

    return () => {
      Keyboard.removeListener('keyboardWillShow', handler);
      Keyboard.removeListener('keyboardDidShow', handler);
    };
  }, []);
};
