import {useEffect, useState} from 'react';

export const useLoading = () => {
  const [isLoading, setIsLoading] = useState(false);

  const startLoading = async () => {
    setIsLoading(true);
  };

  useEffect(() => {
    startLoading();
  }, []);
  return isLoading;
};
