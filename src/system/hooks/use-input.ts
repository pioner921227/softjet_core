import {useState} from 'react';

export interface IUseInputReturnType {
  value: string;
  onChangeText: (text: string) => void;
}

export const useInput = (initialValue: string): IUseInputReturnType => {
  const [value, setValue] = useState(initialValue);
  const onChangeText = (text: string) => {
    setValue(text);
  };
  return {
    value,
    onChangeText,
  };
};
