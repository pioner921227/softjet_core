import { useCallback, useEffect } from 'react'
import { AxiosError, AxiosResponse } from 'axios'
import { ApiService } from '../api/api-service'
import { useModal } from '../../components'
import { httpProblems } from '../helpers/http-problems'
import { InteractionManager } from 'react-native'

export const useAxiosError = () => {
  const { show, close } = useModal()

  const onPressHandler = useCallback((callback: (() => void) | null) => {
    close()
    InteractionManager.runAfterInteractions(() => {
      callback?.()
    })
  }, [])

  const onShow = ({ title, description, onPress, buttonLabel }: IShowModal) => {
    show({
      title: title,
      description: description,
      buttons: [
        {
          label: 'Закрыть',
          onPress: close,
          styles: { modifier: 'bordered' },
        },
        {
          label: buttonLabel,
          onPress: () => onPressHandler(onPress),
        },
      ],
      buttonsDirection: 'row',
    })
  }

  useEffect(() => {
    const interceptors = ApiService.interceptors.response.use(
      (response: AxiosResponse) => {
        if (__DEV__) {
          // console.log('------RESPONSE-----', response);
        }
        return response?.data
      },
      (error: AxiosError) => {
        if (!error.response?.status) {
          return
        }

        const problem = httpProblems(error.response?.status)
        if (problem?.title) {
          onShow(problem)
        }

        if (__DEV__) {
          // console.log('-----RESPONSE ERROR-------', error);
        }

        return Promise.reject(error)
      },
    )
    return () => {
      ApiService.interceptors.response.eject(interceptors)
    }
  }, [])
}

interface IShowModal {
  title: string
  description: string
  buttonLabel: string
  onPress: null | (() => void)
}
