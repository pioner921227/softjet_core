import { useTypedSelector } from './use-typed-selector'
import { useDispatch } from 'react-redux'
import { ActionCart, AsyncActionCart } from '../../screens/cart'
import {
  getCurrentOrderTypeId,
  getCurrentOrganisationId,
  getPolygonId,
} from '../selectors/selectors'
import { useCallback } from 'react'

export interface IUseClearCart {
  clearLocalCart?: boolean
}

export const useClearCart = ({ clearLocalCart = true }: IUseClearCart = {}) => {
  const currentOrganisationId = useTypedSelector(getCurrentOrganisationId)
  const currentOrderTypeId = useTypedSelector(getCurrentOrderTypeId)
  const polygonId = useTypedSelector(getPolygonId)
  const dispatch = useDispatch()

  return useCallback(async () => {
    if (
      currentOrderTypeId !== undefined &&
      currentOrganisationId !== undefined
    ) {
      await dispatch(
        AsyncActionCart.clearRemoteCart({
          order_type: +currentOrderTypeId,
          id_org: currentOrganisationId,
          polygon_id: polygonId,
        }),
      )
      if (clearLocalCart) {
        dispatch(ActionCart.clearLocalCart())
      }
    }
  }, [])
}
