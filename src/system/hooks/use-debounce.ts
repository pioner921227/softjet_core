import {useCallback, useRef} from 'react';

export const useDebounce = <T>(callback: (args: T) => void, delay: number) => {
  const timer = useRef<NodeJS.Timer | null>(null);

  const debounceCallback = useCallback(
    (args: T) => {
      if (timer.current) {
        clearTimeout(timer.current);
      }
      timer.current = setTimeout(() => {
        callback(args);
      }, delay);
    },
    [delay],
  );
  return debounceCallback;
};

