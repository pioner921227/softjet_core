import {ThunkDispatch} from 'redux-thunk';
import {IStore} from '../root-store/root-store';
import {AnyAction} from 'redux';
import {useDispatch} from 'react-redux';

type Dispatch = ThunkDispatch<IStore, any, AnyAction>;

export const useTypedDispatch = (): Dispatch => {
  return useDispatch();
};
