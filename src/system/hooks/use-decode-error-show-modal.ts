import {useModal} from '../../components';
import {useCallback} from 'react';

export const useDecodeErrorShowModal = () => {
  const {show, close} = useModal();

  const openModal = useCallback(
    (e: string) => {
      show({
        title: 'Ошибка данных с сервера',
        description: `${e}`,
        buttons: [{label: 'Принять', onPress: close}],
      });
    },
    [close, show],
  );

  return openModal;
};
