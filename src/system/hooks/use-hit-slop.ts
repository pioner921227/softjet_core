import {useMemo} from 'react';

export const useHitSlop = (value: number) => {
  return useMemo(
    () => ({top: value, bottom: value, left: value, right: value}),
    [value],
  );
};



