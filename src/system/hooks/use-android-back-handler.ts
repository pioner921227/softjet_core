import {useEffect} from 'react';
import {BackHandler, Platform} from 'react-native';
import {useNavigation} from '@react-navigation/native';

export const useAndroidBackHandler = (callback?: () => void) => {
  const {goBack} = useNavigation();
  useEffect(() => {
    if (Platform.OS === 'android') {
      const handler = BackHandler.addEventListener('hardwareBackPress', () => {
        callback?.();
        goBack();
        return true;
      });

      return handler.remove;
    }
  }, []);
};


