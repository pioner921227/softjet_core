import {useNetInfo} from '@react-native-community/netinfo';
import {useModal} from '../../components';
import {useEffect} from 'react';
import {restartApp} from '../helpers/restart-app';

export const useNetConnection = () => {
  const netInfo = useNetInfo();
  const {show, close} = useModal();

  useEffect(() => {
    if (netInfo.isConnected !== null && !netInfo.isConnected) {
      show({
        title: 'Проблемы со связью',
        description:
          'Кажется, возникли неполадки с интернетом. Проверьте соединение и перезапустите приложение',
        buttons: [
          {label: 'Отмена', onPress: close, styles: {modifier: 'bordered'}},
          {label: 'Перезагрузить', onPress: restartApp},
        ],
        buttonsDirection: 'row',
      });
    }
  }, [netInfo.isConnected]);
};
