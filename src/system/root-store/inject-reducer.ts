import {combineReducers, Reducer, Store} from 'redux';
import {Reducers} from './root-reducer';
import {persistReducer} from 'redux-persist';
import {persistConfig} from './configure-store';

export interface newReducers {
  [key: string]: Reducer;
}

export const injectReducer = (store: Store, newReducers: newReducers) => {
  store.replaceReducer(
    persistReducer(
      persistConfig,
      combineReducers({
        ...Reducers,
        ...newReducers,
      }),
    ),
  );
};
