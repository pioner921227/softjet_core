import {applyMiddleware, compose, createStore} from 'redux';
import {createLogger} from 'redux-logger';
import thunk from 'redux-thunk';
import {createTransform, persistReducer, persistStore} from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {rootReducer} from './root-reducer';
import {IStore} from './root-store';
import {PersistConfig} from 'redux-persist/es/types';
import DeviceInfo from 'react-native-device-info';
import {initialStateLogin} from '../../screens/login/store/store-login';
import {initialStateMore} from '../../screens/more/store/store-more';
import {initialStateCatalog} from '../../screens/catalog/store/store-catalog';
import {initialStateDelivery} from '../../screens/delivery/store/store-delivery';
import {initialStateProfile} from '../../screens/profile/store/store-profile';
import {initialStateCart} from '../../screens/cart/store/store-cart';
import {initialStateOrdersHistory} from '../../screens/orders/store/store-orders';
import {initialStateSystem} from '../store/store-system';
import {Config} from '../../config';
import {enhancer} from '../../../Reactorton';


const InitialStates = {
  login: initialStateLogin,
  catalog: initialStateCatalog,
  delivery: initialStateDelivery,
  profile: initialStateProfile,
  cart: initialStateCart,
  ordersHistory: initialStateOrdersHistory,
  more: initialStateMore,
  system: initialStateSystem,
};

const SetTransform = createTransform(
  (inboundState: {}, key) => {
    if (
      key === 'more' &&
      //@ts-ignore
      inboundState.appVersion !== DeviceInfo.getVersion() &&
      Config.ENABLE_CLEAR_LOCAL_STORE_WITH_INSTALL
    ) {
      return {...InitialStates[key as keyof typeof InitialStates]};
    }

    return {...inboundState};
  },
  (outboundState, key) => {
    return {...outboundState};
  },
  {blacklist: ['system', 'login']},
);

export const persistConfig: PersistConfig<any> = {
  key: 'root',
  storage: AsyncStorage,
  blacklist: ['system'],
  transforms: [SetTransform],
};

const loggerMiddleware = createLogger({
  predicate: () => __DEV__,
  collapsed: true,
  timestamp: true,
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const configureStore = (state?: IStore) => {
  const store = createStore(
    persistedReducer,
    state,
    // __DEV__ ? applyMiddleware(thunk, logger) : applyMiddleware(thunk),
    compose(applyMiddleware(thunk, loggerMiddleware), enhancer),
  );
  // const persistor = persistStore(store, {storage: createRealmPersistStorage()});
  const persistor = persistStore(store, null);
  return {store, persistor};
};

export const {store, persistor} = configureStore();

export const clearLocalStore = async () => {
  return await persistor.purge();
};
