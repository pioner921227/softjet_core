import React, {useEffect, useState} from 'react';
import {View} from 'react-native';

interface ILazyComponent {
  Component: React.ComponentType<any>;
}

const EmptyComponent = () => {
  return <View />;
};

export const LazyComponent = ({
  Component,
}: ILazyComponent): React.ComponentType<any> => {
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    return () => setIsLoading(false);
  }, []);

  return isLoading ? Component : EmptyComponent;
};
