import {restartApp} from './restart-app';
import {resetRootNavigation} from './root-navigation';
import {Routes} from '../../navigation';

const createContent = (
  title: string,
  description: string,
  buttonLabel: string,
  onPress: null | (() => void),
) => ({title, description, buttonLabel, onPress});

export const httpProblems = (code: number) => {
  switch (code) {
    case 401:
      return createContent(
        'Ошибка с учетной записью',
        'Кажется, случилась ошибка с учетной записью. Попробуйте заново войти в приложение',
        'Вход',
        () => resetRootNavigation([{name: Routes.LoginStack}]),
      );
    case 500: {
      return createContent(
        'Нет ответа от сервера',
        'Возникли проблемы с сервером. Перезапустите приложение',
        'Перезагрузить',
        restartApp,
      );
    }
    default:
      return null;
      // return createContent('Ошибка', 'Что-то пошло не так', 'Ок', null);
  }
};
