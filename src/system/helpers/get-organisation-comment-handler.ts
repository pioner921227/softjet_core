import {InteractionManager} from 'react-native';
import {TModalShowCallback} from '../../components/modal/modal-provider';
import {AsyncActionsDelivery} from '../../screens/delivery';
import {TDispatch} from '../hooks/use-typed-dispatch';

export const getOrganisationCommentHandler = async (
  dispatch: TDispatch,
  id_org: number | undefined,
  show: TModalShowCallback,
  close: () => void,
  pressCallback?: () => void,
) => {
  if (id_org !== undefined) {
    const res = await dispatch(
      AsyncActionsDelivery.getOrganisationComment(id_org),
    );
    if (!res.work_status) {
      show({
        title: res.message_title,
        description: res.message,
        buttons: [
          {
            label: 'Понятно',
            onPress: () => {
              close();
              InteractionManager.runAfterInteractions(() => {
                pressCallback?.();
              });
            },
          },
        ],
      });
    }
  }
};
