import {LatLng} from 'react-native-maps';


export const convertCoordinatesToNumber = ({latitude, longitude}: LatLng) => {
  return {
    longitude: +longitude,
    latitude: +latitude,
  };
};
