//@ts-ignore
import RNRestart from 'react-native-restart';

export const restartApp = () => {
  RNRestart.Restart();
};
