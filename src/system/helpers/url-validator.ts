import {Config} from '../../config';

export const urlValidator = (url: string) => {
  try {
    const url_ = new URL(url, Config.BASE_URL)?.href;
    return url_.endsWith('/') ? url_.slice(0, -1) : url_;
  } catch (e) {
    console.error('Url validate error', e);
  }
};
