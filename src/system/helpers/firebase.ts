import messaging from "@react-native-firebase/messaging";
import { Platform } from "react-native";
// @ts-ignore
import UUID from "uuid-js";

type TOnRegister = (token: string) => void;

class FCMService {
  cancellers = new Map<string, () => void>();

  register = (
    onRegister: TOnRegister,
    onNotification: () => void,
    onOpenNotification: () => void
  ) => {
    this.checkPermission(onRegister);
    const cancel = this.createNotificationListeners(
      onRegister,
      onNotification,
      onOpenNotification
    );
    const key = UUID.create().toString();
    this.cancellers.set(key, cancel);
    return () => this.unregister(key);
  };

  registerAppWithFCM = async () => {
    if (Platform.OS === "ios") {
      await messaging().registerDeviceForRemoteMessages();
      //await messaging().setAutoInitEnabled(true);
    }
  };

  checkPermission = (onRegister: TOnRegister) => {
    messaging()
      .hasPermission()
      .then((enabled) => {
        if (enabled) {
          this.getToken(onRegister);
        } else {
          this.requestPermission(onRegister);
        }
      })
      .catch((error: Error) => {
        console.log("[FCMService] Permission rejected", error);
      });
  };

  getToken = (onRegister: TOnRegister) => {
    messaging()
      .getToken()
      .then((fcmToken: string) => {
        if (fcmToken) {
          onRegister(fcmToken);
        } else {
          console.log("[FCMService] User does not have a device token");
        }
      })
      .catch((error: Error) => {
        console.log("[FCMService] getToken rejected", error);
      });
  };

  requestPermission = (onRegister: TOnRegister) => {
    messaging()
      .requestPermission()
      .then(() => {
        this.getToken(onRegister);
      })
      .catch((error: Error) => {
        console.log("[FCMService] Request Permission rejected", error);
      });
  };

  requestPermissionOnly = async () => {
    await messaging().requestPermission();
  };

  deleteToken = () => {
    console.log("[FCMService] deleteToken");
    messaging()
      .deleteToken()
      .catch((error: Error) => {
        console.log("[FCMService] Delete token error", error);
      });
  };

  createNotificationListeners = (
    onRegister: TOnRegister,
    onNotification: () => void,
    onOpenNotification: (msg: any) => void
  ) => {
    const removeNotificationOpenedListener =
      messaging().onNotificationOpenedApp((remoteMessage) => {
        console.log(
          "[FCMService] onNotificationOpenedApp Notification caused on to open"
        );
        if (remoteMessage) {
          onOpenNotification(remoteMessage);
        }
      });

    const removeNotificationListener = messaging().onMessage(onNotification);

    messaging()
      .getInitialNotification()
      .then((remoteMessage) => {
        console.log(
          "[FCMService] getInitialNotification Notification caused on to open"
        );
        if (remoteMessage) {
          onOpenNotification(remoteMessage);
        }
      });

    const removeTokenRefreshListener = messaging().onTokenRefresh(
      (fcmToken: string) => {
        console.log("[FCMService] New token refresh", fcmToken);
        onRegister(fcmToken);
      }
    );

    return () => {
      removeNotificationOpenedListener();
      removeNotificationListener();
      removeTokenRefreshListener();
    };
  };

  unregisterDevice = async () => {
    await messaging().unregisterDeviceForRemoteMessages();
  };

  unregister = (token: string) => {
    const fn = this.cancellers.get(token);
    if (fn) {
      fn();
      this.cancellers.delete(token);
    }
  };

  unregisterAll = () => {
    this.cancellers.forEach((fn) => {
      fn();
    });
    this.cancellers.clear();
  };
}

export const fcmService = new FCMService();
