import {request, PERMISSIONS} from 'react-native-permissions';
import {Platform} from 'react-native';

export const permissionCameraRequest = async () => {
  const requestIOS = async () => {
    await request(PERMISSIONS.IOS.CAMERA);
    await request(PERMISSIONS.IOS.PHOTO_LIBRARY);
  };
  const requestAndroid = async () => {
    await request(PERMISSIONS.ANDROID.CAMERA);
  };

  if (Platform.OS === 'ios') {
    return await requestIOS();
  }
  return await requestAndroid();
};
