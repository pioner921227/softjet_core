import { PERMISSIONS, request } from 'react-native-permissions';
import { PermissionsAndroid, Platform } from 'react-native';

export const permissionGeolocationRequest = async () => {
  if (Platform.OS === 'ios') {
    return await request(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE);
  } else {
    await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );
    //await PermissionsAndroid.request(
    //PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
    //);
  }
};
