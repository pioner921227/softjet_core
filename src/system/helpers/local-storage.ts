import PouchDB from 'pouchdb-react-native';
const db = new PouchDB('mydb');

type TId = {
  _id: string;
  _rev?: string;
};

// db.changes().on('change', data => {
//   console.log(data);
// });

export class LocalStorage {
  private db = db;

  async set<T>(values: T & TId) {
    try {
      const data = await db.get(values._id);
      db.put({
        _rev: data._rev,
        ...values,
        _id: values._id,
      });
    } catch (e) {
      return await this.db.put(values);
    }
  }
  async get<T>(key: string): Promise<T & TId> {
    return await db.get(key);
  }
  async clear() {
    return await db.destroy();
  }

  subscribe() {
    return this.db;
  }
}
