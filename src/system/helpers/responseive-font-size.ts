import {Dimensions} from 'react-native';

const WIDTH_FROM_DESIGN = 375;

export const adaptiveSize = (value: number) => {
  return Math.floor(
    value * (Dimensions.get('window').width / WIDTH_FROM_DESIGN),
  );
};
