import {windowWidth} from './window-size';

export const isSmallScreen = windowWidth < 400;

export const getFontSize = (fontSize: number) => {
  if (windowWidth >= 400) {
    return fontSize;
  } else if (windowWidth > 350) {
    return fontSize - 2;
  } else {
    return fontSize - 4;
  }
};
