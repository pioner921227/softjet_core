import {ISelectedItems} from '../../screens/cart/store/store-cart';

interface ICounterDispatchHelper {
  id: string;
  count: number;
  items: ISelectedItems;
}

export const counterDispatchHelper = ({
  id,
  count,
  items,
}: ICounterDispatchHelper) => {
  if (count === 0) {
    const newObject = {...items};
    delete newObject[id];
    return newObject;
  }
  return {
    ...items,
    [id]: {count},
  };
};

// const onSetCount = (id: string, count: number) => {
//   if (count === 0) {
//     const newObject = {...selectedProducts};
//     delete newObject[id];
//     dispatch(ActionCart.selectProduct(newObject));
//   } else {
//     dispatch(ActionCart.selectProduct({...selectedProducts, [id]: {count}}));
//   }
// };
