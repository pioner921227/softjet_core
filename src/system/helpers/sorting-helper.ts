import {
  IEntitiesCatalog,
  IProductItem,
} from '../../screens/catalog/types/types'
import { SortVariant } from '../../components/modal/modal-sorting'

// export const sortingHelper = (
//   products: IProductItem[],
//   variant: keyof typeof SortVariant,
// ): IProductItem[] => {
//   switch (variant) {
//     case SortVariant.priceLow:
//       return [...products].sort((prev, next) => prev.price - next.price);
//     case SortVariant.priceTop:
//       return [...products].sort((prev, next) => next.price - prev.price);
//     case SortVariant.favorite:
//       return [...products].sort((prev, next) => next.favorite - prev.favorite);
//     case SortVariant.rating:
//       return [...products].sort((prev, next) => next.rating - prev.rating);
//     case SortVariant.nameTop:
//       return [...products].sort((prev, next) =>
//         prev.title[0] > next.title[0] ? 1 : -1,
//       );
//     case SortVariant.priceLow:
//       [...products].sort((prev, next) =>
//         prev.title[0] < next.title[0] ? 1 : -1,
//       );
//     default:
//       return products;
//   }
// };

// const keys = Object.keys(entities)
//
// setEntities((state)=>{
//   const newState = {...state}
//   keys.forEach((el)=>{
//     const dat = data.entities[el]
//     newState[el] = sortedArrTop(dat)
//   })
//   return newState
// })

const sorting = (entities: { [key: string]: number[] }) => {
  return (callback: (a: number, b: number) => number) => {
    const keys = Object.keys(entities)
    return keys.reduce<{ [key: string]: number[] }>((acc, el) => {
      const data = entities[el]
      acc[el] = data.sort((a, b) => callback(a, b))
      return acc
    }, {})
  }
}

type Handler = (
  callback: ReturnType<typeof sorting>,
  products: { [key: string]: IProductItem },
) => IEntitiesCatalog

const sortingHandlers: Record<SortVariant, Handler> = {
  [SortVariant.priceLow]: (
    callback: ReturnType<typeof sorting>,
    products: { [key: string]: IProductItem },
  ) => callback((a, b) => products[a]?.price - products[b]?.price),
  [SortVariant.priceTop]: (
    callback: ReturnType<typeof sorting>,
    products: { [key: string]: IProductItem },
  ) => callback((a, b) => products[b]?.price - products[a]?.price),
  [SortVariant.favorite]: (
    callback: ReturnType<typeof sorting>,
    products: { [key: string]: IProductItem },
  ) => callback((a, b) => products[b]?.favorite - products[a]?.favorite),
  [SortVariant.rating]: (
    callback: ReturnType<typeof sorting>,
    products: { [key: string]: IProductItem },
  ) => callback((a, b) => products[b]?.rating - products[a]?.rating),
  [SortVariant.nameTop]: (
    callback: ReturnType<typeof sorting>,
    products: { [key: string]: IProductItem },
  ) => callback((a, b) => (products[b]?.title > products[a]?.title ? 1 : -1)),
  [SortVariant.nameLow]: (
    callback: ReturnType<typeof sorting>,
    products: { [key: string]: IProductItem },
  ) => callback((a, b) => (products[b]?.title < products[a]?.title ? 1 : -1)),
}

export const sortingHelperNew = (
  products: { [key: string]: IProductItem },
  entities: { [key: string]: number[] },
  variant: keyof typeof SortVariant,
): IEntitiesCatalog => {
  const sorting_ = sorting(entities)
  const res = sortingHandlers[variant](sorting_, products)
  return res ?? entities
}
