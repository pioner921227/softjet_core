import {useModal} from '../../components/modal/modal-provider';

export const ModalError = () => {
  const {show, close} = useModal();

  return () => {
    show({
      title: 'Ошибка',
      description: 'Что то пошло не так',
      buttons: [{label: 'Принять', onPress: close}],
    });
  };
};
