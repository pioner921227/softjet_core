import React from 'react';
import {ErrorComponent} from '../../components/error-component';

export class ErrorBoundary extends React.Component<any, any> {
  state = {error: false};

  componentDidCatch(error: Error, errorInfo: React.ErrorInfo) {}

  static getDerivedStateFromError(error: boolean) {
    return {error: true};
  }

  render() {
    if (this.state.error) {
      return <ErrorComponent />;
    }

    return this.props.children;
  }
}
