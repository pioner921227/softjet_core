export const productDeclensionWord = (count: number) => {
  switch (true) {
    case count > 20 && count.toString()[1] === '1':
      return 'товар';
    case count > 20 && +count.toString()[1] > 1 && +count.toString()[1] < 5:
      return 'товара';
    case count > 4:
      return 'товаров';
    case count > 1:
      return 'товара';
    default:
      return 'товар';
  }
};
