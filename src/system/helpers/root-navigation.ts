import * as React from 'react';
import {Routes} from '../../navigation/config/routes';
import {
  CommonActions,
  NavigationContainerRef,
  Route,
} from '@react-navigation/native';

export const navigationRef = React.createRef<NavigationContainerRef>();

export const rootNavigate = (name: keyof typeof Routes, params?: any) => {
  navigationRef.current?.navigate(name, params);
};

export const resetRootNavigation = (
  routes: Omit<Route<keyof typeof Routes>, 'key'>[],
) => {
  navigationRef.current?.dispatch(
    CommonActions.reset({
      index: 0,
      routes: routes,
    }),
  );
};
