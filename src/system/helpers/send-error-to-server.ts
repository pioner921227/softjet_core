import DeviceInfo from 'react-native-device-info';
import {RequestSystem} from '../api/request-system';

export const createString = (error: string, method: string, phone?: string) => {
  const appName = DeviceInfo.getApplicationName();
  const bundleId = DeviceInfo.getBundleId();
  const appVersion = DeviceInfo.getVersion();
  const dateStamp = Date.now();

  return `
---------------------------------
- id: ${dateStamp}
- app_name: ${appName}
- bundle_id: ${bundleId}
- v: ${appVersion}
- phone: ${phone || '_'}
- error_message: ${error}
- method_name: ${method}
---------------------------------`;
};

export const sendErrorToServer = (error: string, method: string) => {
  return RequestSystem.sendError(createString(error, method));
};
