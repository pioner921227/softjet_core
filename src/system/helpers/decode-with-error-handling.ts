import {Decodable} from 'decodable-js';
import {sendErrorToServer} from './send-error-to-server';

export const decodeWithErrorHandling = <T>(
  data: T,
  strict: T,
  methodName: string,
): T => {
  try {
    return Decodable(data, strict);
  } catch (e) {
    sendErrorToServer(e, methodName);
    throw new Error(e);
  }
};
