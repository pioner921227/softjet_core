import {Config} from '../../config';
const {Color} = Config;

export const getThemeColor = (value?: '60' | '40' | '20') => {
  const color = value
    ? Config.DEFAULT_BUTTON_THEME + `_${value}`
    : Config.DEFAULT_BUTTON_THEME;
  return Config.Color[color as keyof typeof Color];
};
