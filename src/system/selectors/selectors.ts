import {IStore} from '../root-store/root-store';

// CART

export const getCartData = (store: IStore) => {
  return store.cart.cartData;
};

export const getCart = (store: IStore) => {
  return store.cart;
};

export const getSelectedProduct = (store: IStore) => {
  return store.cart.selectedProducts;
};

export const getEnableClearCart = (store: IStore) => {
  return store.cart.enableClearCart;
};

// DELIVERY

export const getOrganisations = (store: IStore) => {
  return store.delivery.organisations;
};

export const getCurrentOrganisation = (store: IStore) => {
  return store.delivery.currentOrganisation;
};

export const getCurrentOrganisationId = (store: IStore) => {
  return store.delivery.currentOrganisation?.id;
};

export const getCities = (store: IStore) => {
  return store.delivery.cities;
};

export const getCurrentCity = (store: IStore) => {
  return store.delivery.currentCity;
};

export const getCurrentCityId = (store: IStore) => {
  return store.delivery.currentCity?.id;
};

export const getOrderTypes = (store: IStore) => {
  return store.delivery.orderTypes;
};

export const getCurrentOrderType = (store: IStore) => {
  return store.delivery.currentOrderType;
};

export const getCurrentOrderTypeId = (store: IStore) => {
  return store.delivery.currentOrderType?.id;
};

export const getAddressList = (store: IStore) => {
  return store.delivery.addressList;
};

export const getCurrentAddress = (store: IStore) => {
  return store.delivery.currentAddress;
};

export const getPolygonId = (store: IStore) => {
  return store.delivery.polygonId;
};

// CATALOG

export const getProducts = (store: IStore) => {
  return store.catalog.products;
};

export const getBanners = (store: IStore) => {
  return store.catalog.banners;
};

export const getCatalogData = (store: IStore) => {
  return store.catalog.catalogData;
};

export const getCategories = (store: IStore) => {
  return store.catalog.categories;
};

// LOGIN

export const getToken = (store: IStore) => {
  return store.login.token;
};

// PROFILE

export const getUserData = (store: IStore) => {
  return store.profile.userData;
};

// MORE

export const getAppSettings = (store: IStore) => {
  return store.more.appSettings;
};

export const getAppInfo = (store: IStore) => {
  return store.more.appInfo;
};
