import {ReducerBuilder, reducerWithInitialState} from 'typescript-fsa-reducers';
import {initialStateSystem, IStoreSystem} from './store-system';
import {ActionsSystem} from './actions-system';
import {ActionsLogin} from '../../screens/login';

const setIsFirstLoadAppHandler = (store: IStoreSystem): IStoreSystem => {
  return {
    ...store,
    isFirstLoadApp: false,
  };
};

const resetIsFirstLoadAppHandler = (store: IStoreSystem): IStoreSystem => {
  return {
    ...store,
    isFirstLoadApp: true,
  };
};

export const reducerSystem: ReducerBuilder<IStoreSystem> = reducerWithInitialState(
  initialStateSystem,
)
  .case(ActionsSystem.setIsFirstLoadApp, setIsFirstLoadAppHandler)
  .case(ActionsLogin.logoutAccount, resetIsFirstLoadAppHandler);
