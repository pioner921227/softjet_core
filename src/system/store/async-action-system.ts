import { asyncActionCreator } from "../root-store/action-creator";
import { RequestSystem } from "../api/request-system";
import { IMinRequestData } from "../../screens/cart/api/types";

export class AsyncActionSystem {
  static sendError = asyncActionCreator<string, void, Error>(
    "SYSTEM/SEND_ERROR_MESSAGE",
    RequestSystem.sendError
  );
  static setFCMToken = asyncActionCreator<string, void, Error>(
    "SYSTEM/SET_FCM_TOKEN",
    RequestSystem.setFCMToken
  );

  static logOutRemote = asyncActionCreator<IMinRequestData, void, Error>(
    "SYSTEM/LOG_OUT_REMOTE",
    RequestSystem.logOutRemote
  );
}
