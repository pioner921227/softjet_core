import {actionCreator} from '../root-store/action-creator';

export class ActionsSystem {
  static setIsFirstLoadApp = actionCreator('SYSTEM/SET_IS_FIRST_LOAD_APP');
}
