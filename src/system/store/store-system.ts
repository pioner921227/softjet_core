export interface IStoreSystem {
  isFirstLoadApp: boolean;
}

export const initialStateSystem: IStoreSystem = {
  isFirstLoadApp: true,
};
