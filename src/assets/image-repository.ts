export const ImageRepository = {
  CustomMarker: require('./images/map/custom-marker.png'),
  ProfileHeader: require('./images/profile-header.png'),
  CartImage: require('./images/sushi_cart.png'),
  Mir: require('./images/payments/mir.png'),
  Visa: require('./images/payments/visa.png'),
  MasterCard: require('./images/payments/master-card.png'),
  JCB: require('./images/payments/g6323.png'),
  ProductImagePlaceholder: require('./images/image_placeholder.png'),
  SplashScreen: require('./images/Intro.png'),
  PaymentFinishImage: require('./images/sushi_cart.png'),
};
