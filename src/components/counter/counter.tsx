import React, { useMemo } from "react";
import {
  Insets,
  StyleSheet,
  Text,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native";
import { Config } from "../../config";
import { PlusIcon } from "../icons/plus-icon";
import { MinusIcon } from "../icons/minus-icon";
import { SvgProps } from "react-native-svg";
import { adaptiveSize } from "../../system/helpers/responseive-font-size";

const { Color, UIStyles, RADIUS } = Config;

export interface ICounter {
  containerStyle?: ViewStyle;
  buttonStyle?: ViewStyle;
  buttonIconStyle?: SvgProps;
  valueStyle?: TextStyle;
  value: number;
  onChange: (value: number) => void;
  disabled?: boolean;
  disabledMinus?: boolean;
  disabledPlus?: boolean;
  isVisibleMinus?: boolean;
  isVisiblePlus?: boolean;
  buttonLeftWrapperStyle?: ViewStyle;
  buttonRightWrapperStyle?: ViewStyle;
  hitSlopLeftButton?: Insets;
  hitSlopRightButton?: Insets;
}

export const Counter: React.FC<ICounter> = React.memo(
  ({
    containerStyle,
    buttonStyle,
    buttonIconStyle,
    valueStyle,
    value = 0,
    onChange,
    disabled = false,
    disabledMinus = false,
    disabledPlus = false,
    isVisiblePlus = true,
    isVisibleMinus = true,
    buttonLeftWrapperStyle,
    buttonRightWrapperStyle,
    hitSlopLeftButton,
    hitSlopRightButton,
  }) => {
    const styleButton = StyleSheet.flatten([styles.button, buttonStyle]);
    const styleButtonIcon = StyleSheet.flatten([
      { fill: Color.WHITE },
      buttonIconStyle,
    ]);

    const onPressPlusHandler = () => {
      onChange(value + 1);
    };
    const onPressMinusHandler = () => {
      if (value > 0) {
        onChange(value - 1);
      }
    };

    const containerStyle_ = useMemo(() => {
      return {
        ...styles.container,
        ...containerStyle,
      };
    }, [containerStyle, disabled]);

    return (
      <View style={containerStyle_}>
        {isVisibleMinus ? (
          <View style={[buttonLeftContainer, buttonLeftWrapperStyle]}>
            <TouchableOpacity
              hitSlop={hitSlopLeftButton}
              disabled={disabledMinus || disabled}
              onPress={onPressMinusHandler}
              style={styleButton}
            >
              <MinusIcon {...styleButtonIcon} />
            </TouchableOpacity>
          </View>
        ) : (
          <View style={UIStyles.flex} />
        )}

        <Text style={[styles.value, valueStyle]}>{value}</Text>
        {isVisiblePlus ? (
          <View style={[buttonRightContainer, buttonRightWrapperStyle]}>
            <TouchableOpacity
              hitSlop={hitSlopRightButton}
              disabled={disabledPlus || disabled}
              onPress={onPressPlusHandler}
              style={styleButton}
            >
              <PlusIcon {...styleButtonIcon} />
            </TouchableOpacity>
          </View>
        ) : (
          <View style={UIStyles.flex} />
        )}
      </View>
    );
  }
);

const buttonLeftContainer: ViewStyle = {
  flex: 1,
  alignItems: "flex-start",
};

const buttonRightContainer: ViewStyle = {
  flex: 1,
  alignItems: "flex-end",
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: 50,
    paddingVertical: 5,
    paddingHorizontal: 12,
    backgroundColor: Color.PRIMARY,
    borderRadius: RADIUS.EXTRA_LARGE,
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "row",
  },
  value: {
    textAlign: "center",
    ...UIStyles.font15b,
    color: Color.WHITE,
  },
  button: {
    borderRadius: RADIUS.EXTRA_LARGE,
    height: "100%",
    paddingHorizontal: 43,
    backgroundColor: Color.RGBA_100,
    alignItems: "center",
    justifyContent: "center",
  },
  buttonLabel: {
    color: Color.WHITE,
    fontSize: adaptiveSize(25),
  },
});
