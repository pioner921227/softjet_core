import React, {
  useCallback,
  useEffect,
  useImperativeHandle,
  useMemo,
  useState,
} from 'react';
import {
  FlatList,
  ScrollView,
  StyleSheet,
  Text,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import {Config} from '../../config';
import {Divider} from '../divider';
import {ListItemSelectedWithColor} from '../list-item/list-item-selected-with-color';
import {ArrowBottomWithoutLine} from '../icons';
import Animated, {
  interpolate,
  runOnJS,
  useAnimatedStyle,
  useSharedValue,
  withTiming,
  Extrapolate,
  Easing,
} from 'react-native-reanimated';

const {UIStyles, RADIUS, Color} = Config;

export interface ISelectCustomMethods {
  show: () => void;
  hide: () => void;
}

export interface ISelectItem {
  id: number;
  label: string;
  value: string;
}

interface ISelectCustom {
  data: Array<ISelectItem>;
  onSelect: (item: ISelectItem) => void;
  containerStyle?: ViewStyle;
  selectedItem: ISelectItem;
  enableIcon?: boolean;
  selectStyle?: ViewStyle;
  selectTitleStyle?: TextStyle;
  listStyle?: ViewStyle;
  listItemStyle?: ViewStyle;
  listTitleStyle?: TextStyle;
  listHeight?: number;
  maxListHeight?: number;
  selectedColor?: string;
  disabled?: boolean;
  listTopInset?: number;
  disabledSelectBackgroundColor?: string;
  disabledSelectTextColor?: string;
}

export const SelectCustom = React.memo(
  React.forwardRef<
    ISelectCustomMethods,
    React.PropsWithChildren<ISelectCustom>
  >(
    (
      {
        data,
        onSelect,
        selectedItem,
        enableIcon = true,
        selectStyle,
        selectTitleStyle,
        listStyle,
        listItemStyle,
        listTitleStyle,
        listHeight,
        maxListHeight = 300,
        selectedColor,
        disabled = false,
        listTopInset = 50,
        containerStyle,
        disabledSelectBackgroundColor = Color.GREY_50,
        disabledSelectTextColor = Color.GREY_400,
      },
      ref,
    ) => {
      const animValue = useSharedValue(0);
      const [visible, setVisible] = useState(false);
      const [listItemHeight, setListItemHeight] = useState(37);

      const show = useCallback(() => {
        animValue.value = withTiming(10, {
          duration: 300,
          easing: Easing.bezier(0.25, 0.1, 0.25, 1),
        });
      }, [animValue]);

      const hide = useCallback(
        (callback?: () => void) => {
          animValue.value = withTiming(
            0,
            {duration: 300, easing: Easing.bezier(0.25, 0.1, 0.25, 1)},
            () => {
              runOnJS(setVisible)(false);
              if (typeof callback === 'function') {
                runOnJS(callback)();
              }
            },
          );
        },
        [animValue],
      );

      const onPressSelect = () => {
        setVisible(state => !state);
      };

      const onSelectHandler = useCallback(
        (item: ISelectItem) => {
          const onSelect_ = () => {
            onSelect(item);
          };
          hide(onSelect_);
        },
        [hide, onSelect],
      );

      const renderItem = useCallback(
        ({item}: {item: ISelectItem}) => {
          const onPress = () => {
            onSelectHandler(item);
          };
          return (
            <View
              onLayout={prop =>
                setListItemHeight(prop.nativeEvent.layout.height)
              }>
              <ListItemSelectedWithColor
                selectedColor={selectedColor}
                containerStyle={listItemStyle}
                textStyle={listTitleStyle}
                onSelect={onPress}
                title={item.label}
                isSelected={selectedItem?.id === item.id}
              />
            </View>
          );
        },
        [
          listItemStyle,
          listTitleStyle,
          onSelectHandler,
          selectedColor,
          selectedItem?.id,
        ],
      );

      const keyExtractor = useCallback(item => item.id, []);

      const containerStyle_ = useMemo(() => {
        return {
          ...styles.container,
          ...containerStyle,
        };
      }, [containerStyle]);

      const listStyle_ = useMemo(() => {
        return {
          ...styles.listContainer,
          top: selectStyle?.height ?? listTopInset,
          ...listStyle,
        };
      }, [listStyle, listTopInset, selectStyle?.height]);

      const selectStyle_ = useMemo(() => {
        return {
          backgroundColor: disabled
            ? disabledSelectBackgroundColor
            : Color.WHITE,
          ...styles.select,
          ...selectStyle,
        };
      }, [disabled, disabledSelectBackgroundColor, selectStyle]);

      const selectTitleStyle_ = useMemo(() => {
        return {
          ...styles.selectTitle,
          color: disabled ? disabledSelectTextColor : Color.BLACK,
          ...selectTitleStyle,
        };
      }, [disabled, disabledSelectTextColor, selectTitleStyle]);

      const listHeight_ = useMemo(() => {
        const height = data.length * 50;
        return height > maxListHeight ? maxListHeight : height;
      }, [data.length, maxListHeight]);

      const animateListStyle = useAnimatedStyle(() => {
        return {
          opacity: interpolate(
            animValue.value,
            [0, 1, 10],
            [0, 1, 1],
            Extrapolate.CLAMP,
          ),
          zIndex: interpolate(
            animValue.value,
            [0, 1, 10],
            [0, 2, 2],
            Extrapolate.CLAMP,
          ),
          height: interpolate(
            animValue.value,
            [0, 10],
            // [0, listHeight],
            [0, listHeight ?? listHeight_],
            Extrapolate.CLAMP,
          ),
        };
      });

      useImperativeHandle(ref, () => {
        return {
          show,
          hide,
        };
      });

      useEffect(() => {
        if (visible) {
          show();
        } else {
          hide();
        }
      }, [visible]);

      return (
        <>
          <View style={containerStyle_}>
            <TouchableOpacity
              disabled={disabled}
              onPress={onPressSelect}
              style={selectStyle_}>
              <Text style={selectTitleStyle_}>{selectedItem?.label}</Text>
              {enableIcon && !disabled ? (
                <ArrowBottomWithoutLine
                  stroke={disabled ? disabledSelectTextColor : Color.DARK}
                  style={{transform: [{rotateX: visible ? '180deg' : '0deg'}]}}
                />
              ) : null}
            </TouchableOpacity>
          </View>
          <Animated.View style={[listStyle_, animateListStyle]}>
            <ScrollView
              nestedScrollEnabled={true}
              contentContainerStyle={styles.listWrapper}
              bounces={false}
              scrollEnabled={false}
              horizontal={true}>
              <FlatList
                nestedScrollEnabled={true}
                ItemSeparatorComponent={Divider}
                keyExtractor={keyExtractor}
                pagingEnabled={true}
                decelerationRate={0}
                snapToInterval={listItemHeight}
                data={data}
                renderItem={renderItem}
              />
            </ScrollView>
          </Animated.View>
        </>
      );
    },
  ),
);

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    height: 60,
    // minHeight: 60,
    zIndex: 5,
  },
  select: {
    borderRadius: RADIUS.MEDIUM,
    borderWidth: 1,
    borderColor: Color.GREY_100,
    // paddingVertical: 16,
    paddingHorizontal: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  selectTitle: {
    ...UIStyles.font14,
    paddingVertical: 16,
  },
  listContainer: {
    position: 'absolute',
    alignSelf: 'center',
    zIndex: 5,
    marginTop: 5,
    height: 300,
    width: '100%',
    borderWidth: 1,
    borderColor: Color.GREY_100,
    borderRadius: RADIUS.MEDIUM,
    backgroundColor: Color.WHITE,
    paddingHorizontal: 16,
    ...UIStyles.shadowMD,
  },
  listWrapper: {width: '100%'},
});
