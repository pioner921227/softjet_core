import * as React from 'react';
import Svg, {Path, SvgProps} from 'react-native-svg';
import {Config} from '../../config';

export const CreditCardIcon: React.FC<SvgProps> = ({
  width = 25,
  height = 18,
  fill = Config.Color.BLACK,
  ...props
}) => {
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 25 18"
      fill="none"
      {...props}>
      <Path
        d="M21.522.75H2.772A2.628 2.628 0 00.147 3.375v11.25a2.628 2.628 0 002.625 2.625h18.75a2.628 2.628 0 002.625-2.625V3.375A2.628 2.628 0 0021.522.75zm-18.75 1.5h18.75c.62 0 1.125.505 1.125 1.125v1.5h-21v-1.5c0-.62.505-1.125 1.125-1.125zm18.75 13.5H2.772c-.62 0-1.125-.505-1.125-1.125v-8.25h21v8.25c0 .62-.505 1.125-1.125 1.125z"
        fill={fill}
      />
      <Path
        d="M5.397 13.5h-.75a.75.75 0 01-.75-.75V12a.75.75 0 01.75-.75h.75a.75.75 0 01.75.75v.75a.75.75 0 01-.75.75z"
        fill={fill}
      />
    </Svg>
  );
};
