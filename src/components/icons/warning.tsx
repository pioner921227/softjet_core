import * as React from 'react';
import Svg, {Path, SvgProps} from 'react-native-svg';
import {Config} from '../../config';
import {Animated} from 'react-native';

export const WarningIcon: React.FC<SvgProps> = ({
  fill = Config.Color.BLACK,
  width = 51,
  height = 44,
  ...props
}) => {
  const SvgAnim = Animated.createAnimatedComponent(Svg);

  return (
    <SvgAnim width={width} height={height} viewBox="0 0 51 44" {...props}>
      <Path
        d="M49.777 42.348L25.867.93c-.36-.618-1.375-.618-1.734 0l-24 41.57a1.014 1.014 0 000 1c.18.309.512.5.867.5h48.02c.554 0 1-.445 1-1 0-.25-.09-.477-.243-.652zM23.492 16.266c0-.215.145-.36.36-.36h2.296c.215 0 .356.145.356.36v14.46c0 .215-.14.36-.356.36h-2.296c-.215 0-.36-.145-.36-.36v-14.46zm3.086 20.672c0 .214-.144.355-.355.355H23.78c-.215 0-.355-.145-.355-.355v-2.622c0-.214.144-.355.355-.355h2.442c.215 0 .355.14.355.355v2.621z"
        fill={fill}
        fillRule="nonzero"
        stroke="none"
        strokeWidth={1}
      />
    </SvgAnim>
  );
};
