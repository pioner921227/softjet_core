import * as React from 'react';
import Svg, {Path, SvgProps} from 'react-native-svg';

export const FacebookIcon: React.FC<SvgProps> = ({
  width = 33,
  height = 32,
  ...props
}) => {
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 33 32"
      fill="none"
      {...props}>
      <Path
        d="M32.5 16c0-8.835-7.165-16-16-16S.5 7.165.5 16s7.165 16 16 16c.094 0 .188-.002.281-.004V19.541h-3.437v-4.006h3.437v-2.948c0-3.419 2.087-5.28 5.137-5.28 1.46 0 2.716.109 3.082.158v3.573h-2.103c-1.66 0-1.98.788-1.98 1.945v2.552h3.967l-.517 4.006h-3.45v11.84C27.604 29.466 32.5 23.304 32.5 16z"
        fill="#3E59B9"
      />
    </Svg>
  );
};
