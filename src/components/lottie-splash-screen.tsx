import LottieView from 'lottie-react-native';
import React, {useEffect, useRef} from 'react';

interface ILottieSplashScreen {
  source: string;
}

export const LottieSplashScreen: React.FC<ILottieSplashScreen> = ({source}) => {
  const ref = useRef<LottieView | null>(null);

  useEffect(() => {
    ref.current?.play();
  }, []);

  return <LottieView ref={ref} resizeMode={'cover'} source={source} />;
};
