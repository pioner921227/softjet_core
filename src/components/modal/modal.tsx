import React, { useContext, useEffect, useMemo } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Animated, {
  Extrapolate,
  interpolate,
  runOnJS,
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated'
import { Config } from '../../config'
import { adaptiveSize } from '../../system/helpers/responseive-font-size'
import { windowHeight, windowWidth } from '../../system/helpers/window-size'
import { ThemedButton } from '../buttons/themed-button'
import { ModalContext } from './modal-provider'

const { Color, UIStyles, RADIUS } = Config

export const Modal = React.memo(() => {
  const {
    title,
    description,
    isVisible,
    close,
    buttons,
    buttonsDirection,
    titleStyle,
    afterHandler,
  } = useContext(ModalContext)

  const wrapperStyle = useMemo(() => {
    return {
      marginBottom: adaptiveSize(8),
      flexShrink: 1,
      marginHorizontal: buttonsDirection === 'column' ? 0 : adaptiveSize(5),
    }
  }, [buttonsDirection])

  const style = useMemo(() => {
    return [styles.buttonsContainer, { flexDirection: buttonsDirection }]
  }, [buttonsDirection])

  const titleStyle_ = useMemo(() => {
    return [styles.title, titleStyle]
  }, [titleStyle])

  return (
    <ModalNew
      close={close}
      afterCloseHandler={afterHandler}
      isVisible={isVisible}
    >
      <View style={styles.modalView}>
        <Text style={titleStyle_}>{title}</Text>
        <Text style={styles.description}>{description}</Text>
        <View style={style}>
          {buttons?.map(({ styles, ...rest }, index) => {
            return (
              <ThemedButton
                labelStyle={UIStyles.font14}
                rounded={true}
                wrapperStyle={wrapperStyle}
                key={index}
                {...rest}
                {...styles}
              />
            )
          })}
        </View>
      </View>
    </ModalNew>
  )
})

const ModalNew: React.FC<{
  isVisible: boolean
  afterCloseHandler: () => void
  close: () => void
}> = React.memo(({ children, isVisible, afterCloseHandler, close }) => {
  const isVisibleAnimValue = useSharedValue(0)

  const contaimerStyle = useAnimatedStyle(() => {
    return {
      position: 'absolute',
      alignItems: 'center',
      justifyContent: 'center',
      paddingHorizontal: 16,
      width: windowWidth,
      height: windowHeight,
      opacity: isVisibleAnimValue.value,
      zIndex: interpolate(
        isVisibleAnimValue.value,
        [0, 0.01, 1],
        [-1, -1, 100],
        Extrapolate.CLAMP,
      ),
    }
  })

  const backdropStyle = useAnimatedStyle(() => {
    return {
      position: 'absolute',
      width: windowWidth,
      height: windowHeight,
      backgroundColor: '#000',
      opacity: 0.6,
    }
  })

  useEffect(() => {
    isVisibleAnimValue.value = withTiming(+isVisible, { duration: 300 }, () => {
      if (!isVisible) {
        runOnJS(afterCloseHandler)()
      }
    })
  }, [isVisible])

  return (
    <Animated.View style={contaimerStyle}>
      <Animated.View onTouchStart={close} style={backdropStyle} />
      {children}
    </Animated.View>
  )
})

const styles = StyleSheet.create({
  modalView: {
    width: '100%',
    backgroundColor: Config.Color.WHITE,
    borderRadius: RADIUS.MODAL_RADIUS,
    paddingHorizontal: adaptiveSize(20),
    paddingVertical: adaptiveSize(33),
  },
  title: {
    ...UIStyles.font20b,
    color: Color.DARK,
    textAlign: 'left',
  },
  description: {
    ...UIStyles.font15,
    color: Color.GREY_600,
    marginTop: adaptiveSize(32),
  },
  buttonsContainer: {
    marginTop: adaptiveSize(16),
  },
})
