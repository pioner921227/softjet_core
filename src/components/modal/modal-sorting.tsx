import React, {MutableRefObject, useCallback, useState} from 'react';
import {IModalMethods, ModalWithComponent} from './modal-with-component';
import {ListItemIcons} from '../list-item-icons/list-item-icons';
import {StartIcon} from '../icons/start-icon';
import {SortingArrowTop} from '../icons/sorting/sorting-arrow-top';
import {SortingArrowBottom} from '../icons/sorting/sorting-arrow-bottom';
import {SortingRatingTop} from '../icons/sorting/sorting-rating-top';
import {Config} from '../../config';
import {SvgProps} from 'react-native-svg';
import {StyleSheet} from 'react-native';

const {Color} = Config;

type TSortingItems = {
  title: string;
  Icon: React.FC<SvgProps> | null;
  variant: keyof typeof SortVariant;
};

export enum SortVariant {
  rating = 'rating',
  priceTop = 'priceTop',
  priceLow = 'priceLow',
  favorite = 'favorite',
  nameTop = 'nameTop',
  nameLow = 'nameLow',
}

const createSortingItem = (
  title: string,
  Icon: React.FC<SvgProps> | null,
  variant: keyof typeof SortVariant,
) => ({title, Icon, variant});

export const sortingItems: TSortingItems[] = [
  createSortingItem('Сортировать по рейтингу', StartIcon, SortVariant.rating),
  createSortingItem(
    'Сортировать по цене',
    SortingArrowTop,
    SortVariant.priceTop,
  ),
  createSortingItem(
    'Сортировать по цене',
    SortingArrowBottom,
    SortVariant.priceLow,
  ),
  createSortingItem(
    'Сортировать по популярности',
    SortingRatingTop,
    SortVariant.favorite,
  ),
];

interface IModalSorting {
  onSorting: (variant: keyof typeof SortVariant) => void;
  reference: MutableRefObject<IModalMethods | null>;
}

export const ModalSorting: React.FC<IModalSorting> = React.memo(
  ({onSorting, reference}) => {
    const [selected, setSelected] = useState(-1);

    const getRef = useCallback(
      (ref: IModalMethods) => {
        reference.current = ref;
      },
      [reference],
    );

    return (
      <ModalWithComponent modalStyle={styles.modal} ref={getRef}>
        {sortingItems.map((el, index) => {
          const onPress = () => {
            onSorting(el.variant);
            setSelected(index);
          };

          return (
            <ListItemIcons
              key={index}
              titleStyle={{
                color: selected === index ? Color.PRIMARY : Color.DARK,
                marginLeft: 0,
              }}
              rightComponent={el.Icon?.bind(null, {
                fill: selected === index ? Color.PRIMARY : Color.GREY_400,
              })}
              title={el.title}
              onPress={onPress}
            />
          );
        })}
      </ModalWithComponent>
    );
  },
);

const styles = StyleSheet.create({
  modal: {
    paddingVertical: 9,
  },
});
