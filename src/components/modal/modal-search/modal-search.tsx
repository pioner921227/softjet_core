import { Portal } from '@gorhom/portal'
import React, {
  Reducer,
  useCallback,
  useEffect,
  useMemo,
  useReducer,
  useRef,
  useState,
} from 'react'
import { StyleSheet, TextInput, TouchableOpacity, View } from 'react-native'
import Animated, {
  Extrapolate,
  interpolate,
  runOnJS,
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated'
import { initialWindowMetrics } from 'react-native-safe-area-context'
import { Config } from '../../../config'
import { catalogConfig } from '../../../screens/catalog'
import { ProductListNew } from '../../../screens/catalog/view/components/product-list/product-list-new'
import { windowHeight, windowWidth } from '../../../system/helpers/window-size'
import { useHitSlop } from '../../../system/hooks/use-hit-slop'
import { useInput } from '../../../system/hooks/use-input'
import { useTypedSelector } from '../../../system/hooks/use-typed-selector'
import { getCurrentOrganisation } from '../../../system/selectors/selectors'
import { CloseCrossIcon } from '../../icons/close-cross-icon'
import { IStickerMenuMethods, StickerBlock } from './sticker-block'

const { Color, UIStyles, RADIUS } = Config

interface IModalSearch {
  isVisible: boolean
  closeSearch: () => void
}

interface IState {
  stickers: Set<number>
}

const initialState: IState = { stickers: new Set() }

interface IAction {
  type: 'add' | 'delete' | 'clear'
  payload: number
}

const reducer = (state: IState, action: IAction): IState => {
  switch (action.type) {
    case 'add':
      return { stickers: state.stickers.add(action.payload) }
    case 'delete':
      const set = new Set(state.stickers)
      set.delete(action.payload)
      return { stickers: set }
    case 'clear':
      const emptySet = new Set(state.stickers)
      emptySet.clear()
      return { stickers: emptySet }
    default:
      return state
  }
}

const BackDrop = Animated.createAnimatedComponent(View)

export const ModalSearch: React.FC<IModalSearch> = React.memo(
  ({ isVisible, closeSearch }) => {
    const input = useInput('')

    const stickerMenuRef = useRef<IStickerMenuMethods | null>(null)
    const currentOrganisations = useTypedSelector(getCurrentOrganisation)

    const [state, dispatch] = useReducer<Reducer<IState, IAction>>(
      reducer,
      initialState,
    )

    // const products = useTypedSelector(getProducts);
    const products = useTypedSelector(
      (state) => state.catalog.catalogData?.products,
    )

    const productsArray = Object.values(products ?? {})

    const backdropValue = useSharedValue(0)

    const [filteredProducts, setFilteredProducts] = useState<number[]>([])

    const onSearch = useCallback(
      (text: string) => {
        input.onChangeText(text)
        if (text && products) {
          const ids_ = productsArray.reduce<number[]>((acc, el) => {
            if (el.title.toLowerCase().includes(text.toLowerCase().trim())) {
              acc.push(el.id)
            }
            return acc
          }, [])

          if (ids_) {
            // setFilteredProducts(items);
            setFilteredProducts(ids_)
          }
        } else {
          setFilteredProducts([])
        }
      },
      [input, products],
    )

    const unmount = () => {
      'worklet'
      runOnJS(closeSearch)()
    }

    const show = () => {
      backdropValue.value = withTiming(1, { duration: 300 })
    }

    const hide = () => {
      backdropValue.value = withTiming(0, { duration: 300 }, unmount)
    }

    const onPressClear = () => {
      hide()
      clear()
    }

    const clear = () => {
      input.onChangeText('')
      setFilteredProducts([])
      dispatch({ type: 'clear', payload: 0 })
    }

    const onSelectSticker = (item: { id: number; title: string }) => {
      dispatch({ type: 'add', payload: item.id })
      stickerMenuRef.current?.hide()
    }

    const contentOpacity = useAnimatedStyle(() => {
      return {
        zIndex: interpolate(
          backdropValue.value,
          [0, 1, 1],
          [-1, 10, 10],
          Extrapolate.CLAMP,
        ),
        opacity: interpolate(
          backdropValue.value,
          [0, 1],
          [0, 1],
          Extrapolate.CLAMP,
        ),
      }
    })

    useEffect(() => {
      if (state.stickers) {
        const ids_ = productsArray.reduce<number[]>((acc, product) => {
          if (product.stickers.some((el) => state.stickers.has(el.id))) {
            acc.push(product.id)
          }
          return acc
        }, [])

        setFilteredProducts(ids_)
      } else {
        setFilteredProducts([])
      }
    }, [products, state])

    useEffect(() => {
      if (isVisible) {
        show()
      } else {
        backdropValue.value = withTiming(0, { duration: 300 })
      }
    }, [backdropValue, isVisible])

    const containerStyle_ = useMemo(() => {
      return [styles.container, contentOpacity]
    }, [contentOpacity])

    const hitStop = useHitSlop(10)

    return (
      <Portal name={'search'} hostName={'root'}>
        <Animated.View style={containerStyle_}>
          <BackDrop style={styles.backdrop} />
          <View style={styles.modalView}>
            <TextInput
              placeholderTextColor={Color.GREY_400}
              placeholder={'Поиск'}
              value={input.value}
              onChangeText={onSearch}
              style={styles.select}
            />
            <TouchableOpacity
              hitSlop={hitStop}
              onPress={onPressClear}
              style={styles.clearButton}
            >
              <CloseCrossIcon />
            </TouchableOpacity>
          </View>
          <View style={styles.stickerMenuContainer}>
            {catalogConfig.isVisibleStickerMenu &&
            currentOrganisations?.stickers?.length ? (
              <StickerBlock
                selectedItems={state.stickers}
                data={currentOrganisations.stickers}
                ref={stickerMenuRef}
                onSelect={onSelectSticker}
              />
            ) : null}
          </View>
          {filteredProducts.length ? (
            <ProductListNew
              productContainerStyle={styles.productContainerStyle}
              productIds={filteredProducts}
              productImageStyle={styles.productImageStyle}
            />
          ) : null}
        </Animated.View>
      </Portal>
    )
  },
)

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    // ...UIStyles.paddingH16,
    paddingTop: initialWindowMetrics?.insets.top,
  },
  backdrop: {
    position: 'absolute',
    width: windowWidth,
    height: windowHeight,
    backgroundColor: Color.RGBA_600,
  },
  modalContainerStyle: {
    justifyContent: 'flex-start',
    paddingTop: initialWindowMetrics?.insets.top,
    paddingHorizontal: 0,
  },
  modalView: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Config.Color.WHITE,
    borderRadius: RADIUS.MEDIUM,
    paddingHorizontal: 16,
    marginHorizontal: 16,
    paddingVertical: 10,
    marginBottom: 10,
  },
  modal: {
    flexDirection: 'row',
    // borderRadius: 8,
    justifyContent: 'space-between',
    // paddingVertical: 8,
  },
  select: {
    flex: 10,
    paddingVertical: 0,
    ...UIStyles.font15,
    color: Color.DARK,
  },
  clearButton: {
    // flex: 1,
    // padding: 10,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  productContainerStyle: {
    borderRadius: RADIUS.LARGE,
    marginHorizontal: 16,
    marginBottom: 10,
    paddingVertical: 16,
  },
  productListComponentWrapper: {
    backgroundColor: Color.WHITE,
    borderRadius: RADIUS.LARGE,
  },
  stickerMenuContainer: {
    zIndex: 100,
    paddingHorizontal: 16,
    alignSelf: 'center',
    width: '100%',
  },
  productListContentContainerStyle: {
    // marginTop: 50,
    paddingBottom: (initialWindowMetrics?.insets.bottom || 0) + 50,
  },
  productImageStyle: {
    borderTopLeftRadius: RADIUS.LARGE,
    borderTopRightRadius: RADIUS.LARGE,
  },
})
