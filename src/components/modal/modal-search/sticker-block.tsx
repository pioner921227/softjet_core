import React, {useCallback, useEffect, useImperativeHandle, useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Animated, {
  Easing,
  interpolate,
  runOnJS,

  useAnimatedStyle,
  useSharedValue,
  withTiming
} from 'react-native-reanimated';
import {Config} from '../../../config';
import {IStickerItem} from '../../../screens/delivery/types/types';
import {adaptiveSize} from '../../../system/helpers/responseive-font-size';
import {ArrowBottomWithoutLine} from '../../icons/arrow-bottom-without-line';

const {Color, RADIUS, UIStyles} = Config;

interface IStickerBlock {
  onSelect: (item: {id: number; title: string}) => void;
  data:IStickerItem[]
  selectedItems:Set<number>
}

export interface IStickerMenuMethods {
  hide: () => void;
  show: () => void;
  isOpened: boolean;
}

const MENU_HEIGHT = 100

export const StickerBlock = React.forwardRef<
  IStickerMenuMethods,
  React.PropsWithChildren<IStickerBlock>
>(({onSelect, data, selectedItems}, ref) => {
  const [isOpened, setIsOpened] = useState(false);
  const height = useSharedValue(0);

  const show = useCallback(() => {
    'worklet';
    runOnJS(setIsOpened)(true);
    height.value = withTiming(MENU_HEIGHT, {
      duration: 300,
      easing: Easing.bezier(0.25, 0.1, 0.25, 1),
    });
  }, [height]);

  const hide = useCallback(() => {
    'worklet';
    runOnJS(setIsOpened)(false);
    height.value = withTiming(0, {
      duration: 300,
      easing: Easing.bezier(0.25, 0.1, 0.25, 1),
    });
  }, [height]);

  const arrowAnimStyle = useAnimatedStyle(() => {
    return {
      transform: [
        {scale: interpolate(height.value, [0, MENU_HEIGHT], [1, -1])},
      ],
    };
  });

  useEffect(() => {
    if (isOpened) {
      show();
    } else {
      hide();
    }
  }, [isOpened]);

  const animatedHeightStyle = useAnimatedStyle(() => {
    return {
      height: height.value,
    };
  });

  const opacityInterpolate = useAnimatedStyle(() => {
    return {
      opacity: interpolate(height.value, [0, 20, MENU_HEIGHT], [0, 1, 1]),
    };
  });

  const toggleMenu = () => {
    setIsOpened(state => !state);
  };

  useImperativeHandle(ref, () => {
    return {
      show,
      hide,
      isOpened,
    };
  });

  const translateY = useAnimatedStyle(() => {
    return {
      transform: [{translateY: height.value}],
    };
  });

  return (
    <View>
      <Animated.View
        style={[
          styles.container,
          {
            ...UIStyles.shadowMD,
            zIndex: 1,
          },
          animatedHeightStyle,
          opacityInterpolate,
        ]}>
        <View
          style={styles.menuContainer}>
          {data?.map(el => {
            return (
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => onSelect(el)}
                key={el.id}
                style={[
                  styles.stickerItem,
                  {
                    backgroundColor: el.color_bg,
                    borderWidth: 2,
                    borderColor: selectedItems?.has(el.id)
                      ? Color.WHITE
                      : 'transparent',
                  },
                ]}>
                <Text style={[styles.stickerLabel, {color: el.color_text}]}>
                  {el.title}
                </Text>
              </TouchableOpacity>
            );
          })}
        </View>
      </Animated.View>
      <Animated.View
        style={[styles.buttonWrapper,
          translateY,
        ]}>
        <TouchableOpacity
          activeOpacity={0.9}
          onPress={toggleMenu}
          style={styles.button}>
          <Text style={styles.label}>Фильтр</Text>
          <Animated.View style={arrowAnimStyle}>
            <ArrowBottomWithoutLine
              width={10}
              height={10}
              stroke={Color.WHITE}
            />
          </Animated.View>
        </TouchableOpacity>
      </Animated.View>
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    // backgroundColor: Color.WHITE,
    width: '100%',
    borderRadius: RADIUS.MEDIUM,
    ...UIStyles.paddingH16,
    marginBottom: 50,
  },
  menuContainer: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    paddingVertical: 10,
    overflow: 'scroll',
  },
  buttonWrapper: {
    position: 'absolute',
    zIndex: 10,
    paddingVertical: 10,
    width: '100%',
    alignItems: 'flex-end',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Color.RGBA_600,
    borderRadius: 50,
    paddingHorizontal: adaptiveSize(10),
    paddingVertical: adaptiveSize(7.5),
  },
  stickerItem: {
    marginRight: 10,
    paddingHorizontal: 8,
    paddingVertical: 6,
    borderRadius: 5,
    margin: 4,
  },
  stickerLabel: {
    ...UIStyles.font12,
  },
  label: {
    ...UIStyles.font15,
    color: Color.WHITE,
    marginRight: 15,
  },
});
