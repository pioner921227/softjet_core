import {StyleSheet, Text, View} from 'react-native';
import React, {useImperativeHandle, useMemo, useRef, useState} from 'react';
import {Config} from '../../config';
import {windowHeight, windowWidth} from '../../system/helpers/window-size';
import ModalView from 'react-native-modal';
import {
  IModalButton,
  IShowCallbackProps,
  TModalShowCallback,
} from './modal-provider';
import {ThemedButton} from '../buttons/themed-button';
import {adaptiveSize} from '../../system/helpers/responseive-font-size';

const {Color, UIStyles, RADIUS} = Config;

interface IModalComponent {}
export interface IModalComponentMethods {
  show: TModalShowCallback;
  hide: () => void;
}

export const ModalComponent = React.memo(
  React.forwardRef<
    IModalComponentMethods,
    React.PropsWithChildren<IModalComponent>
  >(({}, ref) => {
    const refModal = useRef<ModalView | null>(null);
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [buttons, setButtons] = useState<IModalButton[]>(
      [] as IModalButton[],
    );
    const [buttonsDirection, setButtonsDirection] = useState<
      'row' | 'column' | undefined
    >('row');

    const wrapperStyle = useMemo(() => {
      return {
        marginBottom: adaptiveSize(8),
        flexShrink: 1,
        marginHorizontal: buttonsDirection === 'column' ? 0 : adaptiveSize(5),
      };
    }, [buttonsDirection]);

    const style = useMemo(() => {
      return [styles.buttonsContainer, {flexDirection: buttonsDirection}];
    }, [buttonsDirection]);

    const [isVisible, setIsVisible] = useState(false);

    const show = ({
      title,
      description,
      buttons,
      buttonsDirection,
    }: IShowCallbackProps) => {
      setTitle(title);
      setDescription(description);
      setButtons(buttons);
      setButtonsDirection(buttonsDirection);

      setIsVisible(true);
    };

    const hide = () => {
      setIsVisible(false);
    };

    useImperativeHandle(ref, () => {
      return {
        show,
        hide,
      };
    });

    return (
      <ModalView
        ref={refModal}
        deviceHeight={windowHeight}
        deviceWidth={windowWidth}
        hasBackdrop={true}
        useNativeDriverForBackdrop={true}
        useNativeDriver={true}
        onBackButtonPress={hide}
        backdropColor={Config.Color.BLACK}
        backdropOpacity={0.6}
        animationIn={'fadeIn'}
        animationOut={'fadeOut'}
        onBackdropPress={hide}
        isVisible={isVisible}>
        <View style={styles.modalView}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.description}>{description}</Text>
          <View style={style}>
            {buttons?.map(({styles, ...rest}, index) => {
              return (
                <ThemedButton
                  labelStyle={UIStyles.font14}
                  rounded={true}
                  wrapperStyle={wrapperStyle}
                  key={index}
                  {...rest}
                  {...styles}
                />
              );
            })}
          </View>
        </View>
      </ModalView>
    );
  }),
);

const styles = StyleSheet.create({
  modalView: {
    width: '100%',
    backgroundColor: Config.Color.WHITE,
    borderRadius: RADIUS.MODAL_RADIUS,
    paddingHorizontal: adaptiveSize(20),
    paddingVertical: adaptiveSize(33),
  },
  title: {
    ...UIStyles.font20b,
    color: Color.DARK,
  },
  description: {
    ...UIStyles.font15,
    color: Color.GREY_600,
    marginTop: adaptiveSize(32),
  },
  buttonsContainer: {
    marginTop: adaptiveSize(16),
  },
});
