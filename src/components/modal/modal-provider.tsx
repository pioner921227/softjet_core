import React, { useCallback, useContext, useState } from 'react'
import { IThemedButton } from '../buttons/themed-button'
import { InteractionManager, TextStyle } from 'react-native'

enum ButtonDirection {
  row = 'row',
  column = 'column',
}

export interface IModalButton {
  label: string
  onPress: () => void
  styles?: Omit<IThemedButton, 'label' | 'onPress'>
}

export interface IShowCallbackProps {
  title: string
  titleStyle?: TextStyle
  description: string
  buttons: IModalButton[]
  buttonsDirection?: keyof typeof ButtonDirection
}
export type TModalShowCallback = (props: IShowCallbackProps) => void

export interface IModalProvider {
  isVisible: boolean
  title: string
  description: string
  show: TModalShowCallback
  close: () => void
  buttons: Array<IModalButton>
  buttonsDirection: keyof typeof ButtonDirection
  titleStyle?: TextStyle
  afterHandler: () => void
}

const initialState = {
  isVisible: false,
  description: '',
  title: '',
  buttons: [],
  buttonsDirection: ButtonDirection.column,
  titleStyle: {},
  handlerID: null,
}

export const ModalContext = React.createContext<IModalProvider>({
  show: () => {},
  afterHandler: () => {},
  close: () => {},
  ...initialState,
})

export const useModal = () => {
  const { show, close } = useContext(ModalContext)
  return {
    show,
    close,
  }
}

interface IState extends Required<IShowCallbackProps> {
  isVisible: boolean
  handlerID: number | null
}

export const ModalProvider: React.FC = React.memo(({ children }) => {
  const [state, setState] = useState<IState>(initialState)

  const show: TModalShowCallback = useCallback(
    ({
      title,
      description,
      buttonsDirection = ButtonDirection.column,
      buttons,
      titleStyle,
    }) => {
      setState((state) => ({
        isVisible: true,
        title: title ?? state.title,
        description: description ?? state.description,
        buttons: buttons ?? state.buttons,
        buttonsDirection: buttonsDirection ?? state.buttonsDirection,
        titleStyle: titleStyle ?? state.titleStyle,
        handlerID: null,
      }))
    },
    [],
  )
  const afterHandler = () => {
    setState(initialState)
    if (state.handlerID) {
      InteractionManager.clearInteractionHandle(state.handlerID)
    }
  }
  const close = useCallback(() => {
    const handlerID = InteractionManager.createInteractionHandle()
    setState((state) => ({ ...state, isVisible: false, handlerID }))
  }, [])

  return (
    <ModalContext.Provider
      value={{
        show,
        close,
        afterHandler,
        ...state,
      }}
    >
      {children}
    </ModalContext.Provider>
  )
})
