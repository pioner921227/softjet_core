import { InteractionManager, StyleSheet, View, ViewStyle } from 'react-native'
import React, { useCallback, useImperativeHandle, useState } from 'react'
import { Config } from '../../config'
import { windowHeight, windowWidth } from '../../system/helpers/window-size'
import Animated, {
  Extrapolate,
  interpolate,
  runOnJS,
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated'
import { Color } from '../../assets/styles'
import { Portal } from '@gorhom/portal'

interface ModalWithComponent {
  containerStyle?: ViewStyle
  modalStyle?: ViewStyle
}

export interface IModalMethods {
  show: () => void
  close: () => void
}

export const ModalWithComponent = React.memo(
  React.forwardRef<IModalMethods, React.PropsWithChildren<ModalWithComponent>>(
    ({ children, modalStyle, containerStyle }, ref) => {
      const [handlerID, setHandlerID] = useState<number | null>(null)
      const animatedValue = useSharedValue(0)

      const clear = (id: number | null) => {
        if (id !== null) {
          InteractionManager.clearInteractionHandle(id)
        }
      }

      const close = useCallback(() => {
        animatedValue.value = withTiming(0, { duration: 200 }, () => {
          'worklet'
          runOnJS(clear)(handlerID)
        })
      }, [handlerID])

      const show = useCallback(() => {
        const handlerId = InteractionManager.createInteractionHandle()
        setHandlerID(handlerId)
        animatedValue.value = withTiming(1, { duration: 200 })
      }, [])

      useImperativeHandle(ref, () => {
        return {
          show,
          close,
        }
      })

      const containerAnimatedStyle = useAnimatedStyle(() => {
        'worklet'
        return {
          zIndex: interpolate(
            animatedValue.value,
            [0, 0.01, 1],
            [-1, -1, 10],
            Extrapolate.CLAMP,
          ),
          opacity: interpolate(
            animatedValue.value,
            [0, 1],
            [0, 1],
            Extrapolate.CLAMP,
          ),
          height: windowHeight,
          width: windowWidth,
          position: 'absolute',
          alignItems: 'center',
          justifyContent: 'center',
          paddingHorizontal: 16,
        }
      })

      const overlayAnimatedStyle = useAnimatedStyle(() => {
        'worklet'
        return {
          opacity: 0.7,
          backgroundColor: Color.BLACK,
          position: 'absolute',
          width: windowWidth,
          height: windowHeight,
        }
      })

      return (
        <Portal hostName={'root'}>
          <Animated.View style={containerAnimatedStyle}>
            <Animated.View onTouchStart={close} style={overlayAnimatedStyle} />
            <View style={[styles.modalView, modalStyle]}>{children}</View>
          </Animated.View>
        </Portal>
      )
    },
  ),
)

const styles = StyleSheet.create({
  modalView: {
    width: '100%',
    backgroundColor: Config.Color.WHITE,
    borderRadius: Config.RADIUS.MODAL_RADIUS,
    paddingHorizontal: 20,
    paddingVertical: 33,
  },
})
