import React, { useCallback, useMemo } from 'react'
import { Dimensions, View } from 'react-native'
import FastImage, { FastImageProps } from 'react-native-fast-image'
import Animated, {
  useSharedValue,
  withSpring,
  withTiming,
} from 'react-native-reanimated'
import { ImageRepository } from '../assets/image-repository'

const duration = 600

const AnimFastImage = Animated.createAnimatedComponent(FastImage)
const { width } = Dimensions.get('window')
export const CustomFastImage = ({ style, ...props }: FastImageProps) => {
  const isLoading = useSharedValue(1)

  const entering = useCallback(() => {
    'worklet'
    const animations = {
      opacity: withTiming(1, { duration: duration }),
      borderRadius: withTiming(0, { duration: duration }),
      transform: [{ scale: withSpring(1, { stiffness: 100, damping: 15 }) }],
    }
    const initialValues = {
      opacity: 0,
      borderRadius: 30,
      transform: [{ scale: 0 }],
    }
    return {
      initialValues,
      animations,
    }
  }, [])

  const imageStyle_ = useMemo(() => {
    return [style]
  }, [style])

  const placeholderStyle_ = useMemo(() => {
    return [style, { position: 'absolute' }]
  }, [style])

  return (
    <View>
      <FastImage
        source={ImageRepository.ProductImagePlaceholder}
        style={placeholderStyle_}
      />
      <AnimFastImage entering={entering} {...props} style={imageStyle_} />
    </View>
  )
}
