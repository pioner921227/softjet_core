import React, { useMemo } from 'react';
import { StyleSheet, View, ViewStyle } from 'react-native';
import SegmentedControlTab from 'react-native-segmented-control-tab';
import { Color, UIStyles } from '../../assets/styles';

const BORDER_RADIUS = 8;

export interface ISegmentControl {
  values: string[];
  selectedIndex: number;
  onChange: (index: number) => void;
  activeTabColor?: Color | 'transparent';
  activeFontColor?: Color | 'transparent';
  tabContainerColor?: Color | 'transparent';
  tabColor?: Color | 'transparent';
  containerStyle?: ViewStyle;
}

export const SegmentControl: React.FC<ISegmentControl> = ({
  values,
  selectedIndex,
  onChange,
  activeTabColor = Color.WHITE,
  activeFontColor = Color.BLACK,
  tabContainerColor = Color.GREY_50,
  tabColor = 'transparent',
  containerStyle,
}) => {
  const activeTabStyle = useMemo(
    () => ({
      backgroundColor: activeTabColor,
      borderRadius: BORDER_RADIUS,
    }),
    [activeTabColor],
  );

  const tabStyle_ = useMemo(() => {
    return [styles.tabStyle, { backgroundColor: tabColor }];
  }, [tabColor]);

  const tabContainerStyle_ = useMemo(() => {
    return [styles.tabsContainerStyle, { backgroundColor: tabContainerColor }];
  }, [tabContainerColor]);

  return (
    <View style={[styles.segmentControl, containerStyle]}>
      <SegmentedControlTab
        borderRadius={BORDER_RADIUS}
        tabTextStyle={styles.tabTextStyle}
        activeTabStyle={activeTabStyle}
        tabStyle={tabStyle_}
        tabsContainerStyle={tabContainerStyle_}
        values={values}
        activeTabTextStyle={{ color: activeFontColor }}
        selectedIndex={selectedIndex}
        onTabPress={onChange}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  segmentControl: {
    width: '100%',
  },
  text: {},
  tabTextStyle: {
    ...UIStyles.font15,
    color: Color.DARK,
  },
  fontStyle: {
    color: '#000',
  },
  activeTabStyle: {
    backgroundColor: Color.PRIMARY,
    borderRadius: BORDER_RADIUS,
  },
  tabStyle: {
    backgroundColor: Color.BACKGROUND_COLOR,
    borderColor: 'transparent',
  },
  tabsContainerStyle: {
    padding: 4,
    backgroundColor: Color.BACKGROUND_COLOR,
    borderRadius: BORDER_RADIUS,
  },
  activeFont: {
    color: Color.WHITE,
  },
});
