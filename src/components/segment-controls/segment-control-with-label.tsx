import {Label} from '../../screens/product-detail/view/components/label/label';
import {ISegmentControl, SegmentControl} from './segment-control';
import React from 'react';
import {StyleSheet, View} from 'react-native';

interface ICustomSegmentControl
  extends Pick<ISegmentControl, 'values' | 'onChange' | 'selectedIndex'> {
  label: string;
}

export const SegmentControlWithLabel: React.FC<ICustomSegmentControl> =
  React.memo(({values, selectedIndex, onChange, label}) => {
    if (!Array.isArray(values) || !values.length) {
      return null;
    }

    return (
      <View style={styles.container}>
        <Label style={styles.label} text={label} />
        <SegmentControl
          values={values}
          selectedIndex={selectedIndex}
          onChange={onChange}
        />
      </View>
    );
  });

const styles = StyleSheet.create({
  container: {
    marginTop: 12,
  },
  label: {
    marginBottom: 12,
  },
});
