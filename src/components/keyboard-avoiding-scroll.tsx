import {Keyboard, ScrollView, Animated} from 'react-native';
import React, {useCallback, useEffect, useMemo, useRef, useState} from 'react';
import {useIsFocused} from '@react-navigation/native';

interface IKeyboardAvoidingScroll {
  scrollTo?: number;
}

export const KeyboardAvoidingScroll: React.FC<IKeyboardAvoidingScroll> = React.memo(
  ({children}) => {
    const [padding, setPadding] = useState(0);

    const scrollRef = useRef<ScrollView | null>(null);
    const animValue = useRef(new Animated.Value(0)).current;
    const [offset, setOffset] = useState(0);
    const [scrollOffset, setScrollOffset] = useState(0);

    const isFocused = useIsFocused();

    useEffect(() => {
      const w = Keyboard.addListener('keyboardWillShow', props => {
        Animated.timing(animValue, {
          toValue: props.endCoordinates.height,
          duration: props.duration,
          useNativeDriver: true,
        }).start(() => {
          setPadding(props.endCoordinates.height);
        });
      });

      const hideListener = Keyboard.addListener('keyboardWillHide', props => {
        scrollRef.current?.scrollTo({y: 0, x: 0, animated: true});

        Animated.timing(animValue, {
          toValue: 0,
          duration: 300,
          useNativeDriver: true,
        }).start();
      });
      const h = Keyboard.addListener('keyboardDidHide', () => {
        setPadding(0);
      });
      return () => {
        w.remove();
        h.remove();
        hideListener.remove();
      };
    }, []);

    useEffect(() => {
      if (padding > 0) {
        scrollRef.current?.scrollTo({
          y: padding + scrollOffset,
          // y: padding,
          x: 0,
          animated: true,
        });
      }
    }, [padding]);

    useEffect(() => {
      animValue.addListener(props => {
        setOffset(props.value);
      });
      return () => {
        animValue.removeAllListeners();
      };
    }, []);

    const style = useMemo(() => {
      return {
        flexGrow: 1,
        paddingBottom: offset,
      };
    }, [offset]);

    const onMomentumScrollEnd = useCallback(e => {
      setScrollOffset(e.nativeEvent.contentOffset.y);
    }, []);

    useEffect(() => {
      if (!isFocused) {
        Keyboard.dismiss();
      }
    }, [isFocused]);

    return (
      <Animated.ScrollView
        bounces={false}
        pinchGestureEnabled={true}
        // scrollToOverflowEnabled={true}
        keyboardShouldPersistTaps={'handled'}
        // disableScrollViewPanResponder={true}
        // keyboardDismissMode={'none'}
        ref={scrollRef}
        scrollEventThrottle={16}
        onMomentumScrollEnd={onMomentumScrollEnd}
        contentContainerStyle={style}>
        {children}
      </Animated.ScrollView>
    );
  },
);
