import {StyleSheet, View, ViewStyle} from 'react-native';
import React from 'react';
import {Config} from '../../config';
const {UIStyles} = Config;

export interface IScreenHeader {
  style?: ViewStyle;
}

export const ScreenHeader: React.FC<IScreenHeader> = React.memo(
  ({children, style}) => {
    return <View style={[styles.container, style]}>{children}</View>;
  },
);

const styles = StyleSheet.create({
  container: {
    ...UIStyles.paddingH16,
    height: 48,
    justifyContent: 'center',
  },
});
