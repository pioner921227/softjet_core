import React from 'react'
import { IScreenHeader, ScreenHeader } from './screen-header'
import { StyleSheet, TouchableOpacity, View, ViewStyle } from 'react-native'
import { HeaderTitle, IHeaderTitle } from './header-title'
import { Config } from '../../config'
import { TapGestureHandler } from 'react-native-gesture-handler'
const { UIStyles } = Config

export interface IScreenHeaderTitleWithIcon
  extends IHeaderTitle,
    IScreenHeader {
  ComponentLeft?: React.FC
  ComponentRight?: React.FC
  componentRightStyle?: ViewStyle
  componentLeftStyle?: ViewStyle
  onPressLeftIcon?: () => void
  onPressRightIcon?: () => void
  titleWrapperStyle?: ViewStyle
  title?: string
}

export const ScreenHeaderTitleWithIcons: React.FC<IScreenHeaderTitleWithIcon> = React.memo(
  ({
    children,
    style,
    titleStyle,
    ComponentLeft,
    ComponentRight,
    onPressLeftIcon,
    onPressRightIcon,
    titleWrapperStyle,
    componentLeftStyle,
    componentRightStyle,
    title,
  }) => {
    return (
      <ScreenHeader style={StyleSheet.flatten([styles.container, style])}>
        <View style={[UIStyles.flex, componentLeftStyle]}>
          {ComponentLeft ? (
            <TapGestureHandler onActivated={onPressLeftIcon}>
              <View style={styles.component}>
                <ComponentLeft />
              </View>
            </TapGestureHandler>
          ) : null}
        </View>
        <View style={[styles.titleWrapper, titleWrapperStyle]}>
          <HeaderTitle titleStyle={titleStyle}>{title || children}</HeaderTitle>
        </View>
        <View style={[UIStyles.flex, componentRightStyle]}>
          {ComponentRight ? (
            <TapGestureHandler onActivated={onPressRightIcon}>
              <View style={styles.component}>
                <ComponentRight />
              </View>
            </TapGestureHandler>
          ) : null}
        </View>
      </ScreenHeader>
    )
  },
)

const styles = StyleSheet.create({
  container: {
    // flexGrow: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  titleWrapper: {
    flex: 5,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  component: {
    ...UIStyles.flex,
    justifyContent: 'center',
  },
})
