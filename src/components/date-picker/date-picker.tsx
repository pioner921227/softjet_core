import React, { useCallback, useRef, useState } from 'react';
import { InteractionManager, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Config } from '../../config';
import { ArrowRightWithoutLineIcon } from '../icons';
import { IModalMethods } from '../modal/modal-with-component';
import { SegmentControl } from '../segment-controls/segment-control';
import {
  IModalDatePickerData,
  IModalDatePickerOnSelect,
  IModalPickerSelectedIndexes,
  ModalPicker
} from './components/modal-picker';
import { IScrollPickerData } from './components/scroll-picker/scroll-picker';
const { Color } = Config

const values = ['Заказать сейчас', 'Предзаказ']

export interface IDatePickerOnSuccessDate {
  dateIndex: number
  timeIndex: number
}

interface IDatePicker extends IModalPickerSelectedIndexes, IModalDatePickerData, Partial<IModalDatePickerOnSelect> {
  onSuccess: (data: IDatePickerOnSuccessDate) => void
}

const emptyArray: IScrollPickerData[] = []

export const DatePicker: React.FC<IDatePicker> = React.memo(({
  onSuccess,
  onSelectTime,
  onSelectDate,
  selectedDateIndex,
  selectedTimeIndex,
  dates,
  times
}) => {

  const [selectedIndex, setSelectedIndex] = useState(0)
  const modalPickerRef = useRef<IModalMethods | null>(null)

  const [selectedDateIndex_, setSelectedDateIndex_] = useState(0)
  const [selectedTimeIndex_, setSelectedTimeIndex_] = useState(0)



  const onSelectDate_ = (index: number) => {
    onSelectDate?.(index)
    setSelectedDateIndex_(index)

  }
  const onSelectTime_ = (index: number) => {
    onSelectTime?.(index)
    setSelectedTimeIndex_(index)
  }

  const onChange = (index: number) => {
    setSelectedIndex(index);
    if (index === 1) {
      modalPickerRef.current?.show()
    }
  }

  const onPressModalSuccess = () => {
    modalPickerRef.current?.close()
    InteractionManager.runAfterInteractions(() => {
      onSuccess({
        timeIndex: selectedTimeIndex_,
        dateIndex: selectedDateIndex_
      })
    })
  }


  const onDismiss = () => {
    if (selectedTimeIndex === 0 || selectedDateIndex === 0) {
      setSelectedIndex(0)
    }
  }

  const onPressModalClose = useCallback( () => {
    onDismiss();
    requestAnimationFrame(()=>{
      modalPickerRef.current?.close();
    });
  },[modalPickerRef.current]);

  const getTitle = useCallback(() => {
    return `${dates?.[selectedDateIndex]?.value ?? ''} ${times?.[selectedTimeIndex]?.value ?? ''}`
  }, [selectedTimeIndex, selectedDateIndex])

  return (
    <View>
      <SegmentControl
        containerStyle={styles.segmentContainer}
        selectedIndex={selectedIndex}
        onChange={onChange}
        values={values}
      />
      {selectedIndex ?
        <TouchableOpacity onPress={modalPickerRef.current?.show} style={styles.selectedDateWrapper}>
          <Text>{getTitle()}</Text>
          <ArrowRightWithoutLineIcon />
        </TouchableOpacity>
        : null
      }

      <ModalPicker
        onBackdropPress={onDismiss}
        dates={dates}
        times={times}
        selectedDateIndex={selectedDateIndex}
        selectedTimeIndex={selectedTimeIndex}
        onPressClose={onPressModalClose}
        onPressSuccess={onPressModalSuccess}
        onSelectDate={onSelectDate_}
        onSelectTime={onSelectTime_}
        ref={modalPickerRef}
      />
    </View>
  )
})

const styles = StyleSheet.create({
  selectedDateWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 8,
    borderWidth: 1,
    borderColor: Color.GREY_100,
    height: 50,
    justifyContent: 'space-between',
    padding: 16,
    marginBottom: 16,
  },
  segmentContainer: {
    paddingVertical: 10
  }
})
