import SegmentedControl, { NativeSegmentedControlIOSChangeEvent } from '@react-native-community/segmented-control';
import React from 'react';
import { NativeSyntheticEvent, StyleSheet } from 'react-native';
import { Config } from '../../../config';
const { Color, UIStyles } = Config


export type TSegmentControlOnChangeData = NativeSyntheticEvent<NativeSegmentedControlIOSChangeEvent>


interface ISegmentControl {
  onChange: (state: TSegmentControlOnChangeData) => void;
  values: string[];
  selectedIndex?: number;
}

export const SegmentControl: React.FC<ISegmentControl> = React.memo(({ onChange, values, selectedIndex = 0 }) => {


  if (!values.values) {
    return null;
  }

  return (
    <SegmentedControl
      selectedIndex={selectedIndex}
      style={styles.container}
      fontStyle={styles.fontStyle}
      onChange={onChange}
      values={values}
    />
  )
})


const styles = StyleSheet.create({
  container: {
    height: 50,
    marginBottom: 20
  },
  fontStyle: {
    ...UIStyles.font15,
    color: Color.BLACK
  }
})
