import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Config } from '../../../../config';
import { IScrollPicker, ScrollPicker } from './scroll-picker';
const { Color, UIStyles } = Config


interface ScrollPickerWithLabel extends IScrollPicker {
  label: string
}

export const ScrollPickerWithLabel: React.FC<ScrollPickerWithLabel> = React.memo(({ data, label, onSelect, selectedIndex }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.label}>{label}</Text>
      <ScrollPicker selectedIndex={selectedIndex} onSelect={onSelect} data={data} />
    </View>
  )
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  label: {
    ...UIStyles.font15,
    color: Color.GREY_600,
    textAlign: 'center',
  }
})
