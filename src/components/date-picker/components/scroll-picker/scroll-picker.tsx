import React, { useCallback, useLayoutEffect, useMemo } from 'react';
import { FlatList, InteractionManager, StyleSheet, View } from 'react-native';
import Animated, { runOnUI,runOnJS, scrollTo, useAnimatedRef, useAnimatedScrollHandler, useSharedValue } from 'react-native-reanimated';
import { useLayout } from '../../../../system/hooks/use-layout';
import { IScrollPickerItem, ScrollPickerItem } from './scroll-picker-item';



export interface IScrollPickerData {
  id: number
  value: string
}

export interface IScrollPicker {
  data: IScrollPickerData[]
  onSelect: (index: number) => void;
  selectedIndex?: number
}

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList)


const LIST_ITEM_HEIGHT = 37

export const ScrollPicker: React.FC<IScrollPicker> = React.memo(({ data, onSelect, selectedIndex }) => {

  const listRef = useAnimatedRef<FlatList>()

  const scrollY = useSharedValue(0)


  const setOffsetWithIndex = (index: number) => {
    //listRef.current?.getNode().scrollToIndex({ index: index, animated: true })
    'worklet'
    //@ts-ignore
    scrollTo(listRef, 0, index * LIST_ITEM_HEIGHT, true)
  }

  const onPress = useCallback((index: number) => {
    runOnUI(setOffsetWithIndex)(index)
    InteractionManager.runAfterInteractions(() => {
      onSelect(index);
    })
  }, [])



  const renderItem = useCallback((props: Omit<IScrollPickerItem, 'animValue'>) => {
    return (
      <ScrollPickerItem  {...props} onPress={onPress} animValue={scrollY} />
    )
  }, [])


  const keyExtractor = useCallback((item) => item?.id?.toString(), [])


  const { onLayout, values: { height } } = useLayout()

  const contentContainerStyle = useMemo(() => {
    return {
      paddingTop: height / 2,
      paddingBottom: height / 2 - LIST_ITEM_HEIGHT
    }
  }, [height])

  const snapToOffset = useMemo(() => data.map((_, index) => index * LIST_ITEM_HEIGHT), [data]);



  const onScroll = useAnimatedScrollHandler({
    onScroll: (e) => {
      scrollY.value = e.contentOffset.y
    },
    onEndDrag: (e) => {
      runOnJS(onSelect)(Math.round(e.contentOffset.y / LIST_ITEM_HEIGHT))
    }
  })


  useLayoutEffect(() => {

    if (selectedIndex !== undefined) {
      setTimeout(() => {
        setOffsetWithIndex(selectedIndex)
      }, 200)
    }
  }, [selectedIndex])


  const getItemLayout = useCallback((_, index) => (
    { length: LIST_ITEM_HEIGHT, offset: LIST_ITEM_HEIGHT * index, index }
  ), [])

  if (!data.length) {
    return null
  }

  return (
      <View onLayout={onLayout} style={styles.container}>
        <AnimatedFlatList
          getItemLayout={getItemLayout}
          ref={listRef}
          onScroll={onScroll}
          snapToOffsets={snapToOffset}
          scrollEventThrottle={16}
          decelerationRate={'fast'}
          contentContainerStyle={contentContainerStyle}
          keyExtractor={keyExtractor}
          showsVerticalScrollIndicator={false}
          data={data}
          //@ts-ignore
          renderItem={renderItem} />
      </View>
  )
})

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})

