import React, { useCallback, useMemo } from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";
import Animated, { Extrapolate, interpolate, useAnimatedStyle } from 'react-native-reanimated';
import { Config } from "../../../../config";
const { UIStyles } = Config


export interface IScrollPickerItem {
  index: number
  item: { id: number, value: string }
  animValue: Animated.SharedValue<number>
  onPress: (index: number) => void
}

export const ScrollPickerItem: React.FC<IScrollPickerItem> = React.memo(({ index, item, animValue, onPress }) => {



  const animatedStyle = useAnimatedStyle(() => {
    return {
      transform: [{
        translateX: interpolate(animValue.value,
          [(index - 1) * 37, index * 37, (index + 1) * 37],
          [0, 2, 0],
          Extrapolate.CLAMP,
        )
      },
      {
        scale: interpolate(animValue.value,
          [(index - 2) * 37, index * 37, (index + 2) * 37],
          [0.6, 1.5, 0.6],
          Extrapolate.CLAMP,
        )
      }
      ],
      opacity: interpolate(animValue.value, [(index - 2) * 37, index * 37, (index + 2) * 37], [0.5, 1, 0.5], Extrapolate.CLAMP)
    }
  })

  const style_ = useMemo(() => [styles.container, animatedStyle], [])

  const onPress_ = useCallback(() => {
    onPress(index)
  }, [index])

  return (
    <Animated.View style={style_} >
      <TouchableOpacity onPress={onPress_}>
        <Text style={styles.title}>{item.value} </Text>
      </TouchableOpacity>
    </Animated.View>
  )
})

const styles = StyleSheet.create({
  container: {
    paddingVertical: 10
  },
  title: {
    textAlign: 'center',
    ...UIStyles.font13,
  }
})
