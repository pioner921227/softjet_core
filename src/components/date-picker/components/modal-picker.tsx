import React, { PropsWithChildren } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { ScrollPickerWithLabel } from './scroll-picker/scroll-picker-with-label';
import { IScrollPickerData } from './scroll-picker/scroll-picker';
import { IModalMethods, ModalWithComponent } from '../../modal/modal-with-component';
import { ThemedButton } from '../../buttons/themed-button';
import { Config } from '../../../config';
const { Color, UIStyles } = Config

export interface IModalPickerSelectedIndexes {
  selectedDateIndex: number;
  selectedTimeIndex: number;
}

export interface IModalDatePickerData {
  dates: IScrollPickerData[]
  times: IScrollPickerData[]
}

export interface IModalDatePickerOnSelect {
  onSelectDate: (index: number) => void
  onSelectTime: (index: number) => void
}

interface IModalPicker extends IModalPickerSelectedIndexes, IModalDatePickerData, IModalDatePickerOnSelect {
  onPressSuccess: () => void;
  onPressClose: () => void;
  onBackdropPress?: () => void

}

export const ModalPicker = React.memo(React.forwardRef<IModalMethods, PropsWithChildren<IModalPicker>>(({
  onBackdropPress,
  onSelectTime,
  onSelectDate,
  onPressClose,
  onPressSuccess,
  selectedTimeIndex,
  selectedDateIndex,
  dates,
  times,
}, ref) => {
  return (
    <ModalWithComponent
      modalStyle={styles.modal}
      ref={ref}
    >
      <View style={styles.container}>
        <Text style={styles.title}>Предзаказ</Text>
        <View style={styles.pickerWrapper}>
          <ScrollPickerWithLabel
            selectedIndex={selectedDateIndex}
            onSelect={onSelectDate}
            data={dates}
            label={'Выберите дату'}
          />
          <ScrollPickerWithLabel
            selectedIndex={selectedTimeIndex}
            onSelect={onSelectTime}
            data={times}
            label={'Выберите время'}
          />
        </View>
        <View style={styles.buttonsRow}>
          <ThemedButton
            modifier='bordered'
            wrapperStyle={{ flex: 1 }}
            rounded={true}
            label={'Отмена'}
            onPress={onPressClose}
          />
          <ButtonSeparator />
          <ThemedButton
            wrapperStyle={{ flex: 1 }}
            rounded={true}
            label={'ОК'}
            onPress={onPressSuccess}
          />
        </View>
      </View>
    </ModalWithComponent>
  )
}))

const ButtonSeparator = () => {
  return (
    <View style={styles.buttonSeparator} />
  )
}
const styles = StyleSheet.create({
  container: {
    height: 360,
    width: '100%',
    borderRadius: 16,
    backgroundColor: Color.WHITE,
  },
  buttonsRow: {
    width: '100%',
    flexDirection: 'row',
    marginTop: 'auto',
  },
  buttonSeparator: {
    width: 10
  },
  title: {
    ...UIStyles.font20b,
    marginBottom: 12,
  },
  modal: {
    paddingVertical: 16,
    paddingHorizontal: 16,
  },
  pickerWrapper: {
    flexDirection: 'row',
    flex: 1,
    paddingBottom: 10
  }
})
