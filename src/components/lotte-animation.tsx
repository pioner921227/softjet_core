import LottieView from 'lottie-react-native';
import React, {useEffect, useRef} from 'react';

export const LotteAnimation = () => {
  const ref = useRef<LottieView | null>(null);

  useEffect(() => {
    ref.current?.play();
  }, []);

  return (
    <LottieView
      ref={ref}
      // autoPlay={true}
      loop={false}
      style={{width: '100%', height: '100%'}}
      source={require('../assets/lottie.json')}
    />
  );
};
