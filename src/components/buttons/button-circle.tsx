import {StyleSheet, TouchableOpacity, ViewStyle} from 'react-native';
import React from 'react';
import {Config} from '../../config';
const {Color} = Config;

export interface IButtonCircle {
  onPress: () => void;
  style?: ViewStyle;
}

export const ButtonCircle: React.FC<IButtonCircle> = React.memo(
  ({children, style, onPress}) => {
    return (
      <TouchableOpacity
        style={[styles.container, style]}
        onPress={onPress}
        hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}>
        {children}
      </TouchableOpacity>
    );
  },
);

const styles = StyleSheet.create({
  container: {
    backgroundColor: Color.WHITE,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 100,
  },
});
