import React, { useEffect, useMemo, useRef, useState } from "react";
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native";
import { style } from "../../system/helpers/styles";
import { Theme } from "../../assets/theme";
import { Config } from "../../config";
import { TouchableOpacity as TouchableOpacityWithGestureHandler } from "@gorhom/bottom-sheet";
import { adaptiveSize } from "../../system/helpers/responseive-font-size";
import { useLayout } from "../../system/hooks/use-layout";

const { Color, UIStyles, BUTTON_RADIUS, DEFAULT_BUTTON_THEME, themeNames } =
  Config;

export interface IThemedButton {
  label: string;
  onPress: () => void;
  labelStyle?: TextStyle;
  modifier?: "bordered" | "default";
  buttonStyle?: ViewStyle;
  theme?: keyof typeof themeNames;
  disabled?: boolean;
  rounded?: boolean;
  wrapperStyle?: ViewStyle;
  withGesture?: boolean;
  enableLoader?: boolean;
  isLoading?: boolean;
}

export const ThemedButton: React.FC<IThemedButton> = React.memo(
  ({
    label,
    labelStyle,
    theme = DEFAULT_BUTTON_THEME,
    buttonStyle,
    disabled = false,
    rounded = false,
    modifier = "default",
    onPress,
    wrapperStyle,
    withGesture = false,
    isLoading = false,
  }) => {
    const themeStyles = Theme[theme];

    const isBordered = modifier === "bordered";
    const activeIndicatorRef = useRef<ActivityIndicator | null>(null);

    const styleButton: ViewStyle = useMemo(
      () => ({
        backgroundColor: !isBordered
          ? disabled
            ? themeStyles.disabled
            : themeStyles.normal
          : "transparent",
        borderRadius: rounded ? BUTTON_RADIUS : 0,
        borderColor: isBordered
          ? disabled
            ? themeStyles.disabled
            : themeStyles.normal
          : "transparent",
        borderWidth: isBordered ? 1 : 0,
        ...buttonStyle,
      }),
      [
        buttonStyle,
        disabled,
        isBordered,
        rounded,
        themeStyles.disabled,
        themeStyles.normal,
      ]
    );

    const textColor = isBordered
      ? disabled
        ? themeStyles.disabled
        : themeStyles.normal
      : themeStyles.text;

    const textStyle = useMemo(
      () => ({
        ...styles.text,
        ...labelStyle,
        color: textColor,
      }),
      [labelStyle, textColor]
    );

    const Component = withGesture
      ? TouchableOpacityWithGestureHandler
      : TouchableOpacity;

    const { onLayout, values } = useLayout();

    // const {onLayout, values} = useLayout(({width}) => {
    //   activeIndicatorRef.current?.setNativeProps({
    //     style: {transform: [{translateX: width / 2 + adaptiveSize(20)}]},
    //   });
    // });

    const wrapperStyle_ = useMemo(
      () => [styles.wrapper, wrapperStyle],
      [wrapperStyle]
    );
    const containerStyle_ = useMemo(
      () => [styles.container, styleButton],
      [styleButton]
    );

    useEffect(() => {
      if (values.width) {
        activeIndicatorRef.current?.setNativeProps({
          style: {
            transform: [{ translateX: values.width / 2 + adaptiveSize(20) }],
          },
        });
      }
    }, [values.width]);

    return (
      <View style={wrapperStyle_}>
        <Component
          onPress={onPress}
          disabled={disabled}
          style={containerStyle_}
        >
          <Text onLayout={onLayout} style={textStyle}>
            {label}
          </Text>
          <ActivityIndicator
            animating={isLoading}
            ref={activeIndicatorRef}
            color={textColor}
            style={styles.activeIndicator}
          />
        </Component>
      </View>
    );
  }
);

const styles = StyleSheet.create({
  wrapper: {
    marginTop: "auto",
    width: "100%",
  },
  container: style.view({
    flexDirection: "row",
    height: adaptiveSize(40),
    width: "100%",
    marginTop: "auto",
    borderRadius: BUTTON_RADIUS,
    alignItems: "center",
    justifyContent: "center",
  }),
  text: style.text({
    ...UIStyles.font14b,
    textTransform: "uppercase",
    color: Color.WHITE,
  }),
  activeIndicator: {
    position: "absolute",
    alignSelf: "center",
  },
});
