import React from 'react';
import {StyleSheet, ViewStyle} from 'react-native';
import {ArrowLeftIcon} from '../icons/arrow-left-icon';
import {Config} from '../../config';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {useNavigation} from '@react-navigation/native';
import {ButtonCircle} from './button-circle';
import {SvgProps} from 'react-native-svg';

const {Color} = Config;

interface IButtonBackCircle {
  buttonStyle?: ViewStyle;
  iconStyle?: SvgProps;
}

export const ButtonBackCircle: React.FC<IButtonBackCircle> = React.memo(
  ({buttonStyle, iconStyle}) => {
    const insets = useSafeAreaInsets();
    const {goBack} = useNavigation();

    return (
      <ButtonCircle
        style={StyleSheet.flatten([
          styles.circle,
          {top: insets.top + 10},
          buttonStyle,
        ])}
        onPress={goBack}>
        <ArrowLeftIcon {...iconStyle} />
      </ButtonCircle>
    );
  },
);

const styles = StyleSheet.create({
  circle: {
    position: 'absolute',
    zIndex: 1,
    left: 23,
    backgroundColor: Color.WHITE,
    borderRadius: 100,
    width: 32,
    height: 32,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
