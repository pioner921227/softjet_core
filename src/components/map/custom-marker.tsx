import {Callout, LatLng, Marker} from 'react-native-maps';
import FastImage from 'react-native-fast-image';
import {ImageRepository} from '../../assets/image-repository';
import React, {useCallback} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Config} from '../../config';
import {windowWidth} from '../../system/helpers/window-size';
import {RADIUS} from '../../assets/styles';

const {Color, UIStyles} = Config;

export interface IAddMarkerRefCallback {
  (item: Marker | null, id: number): void;
}
export interface IOnPressMarker {
  (gps: LatLng, id: number): void;
}

interface ICustomMarker {
  id: number;
  latitude: number;
  addMarkerRef: IAddMarkerRefCallback;
  longitude: number;
  isEnabledCallout?: boolean;
  calloutText?: string;
  onPressMarker?: IOnPressMarker;
  gps: LatLng;
}

export const CustomMarker: React.FC<ICustomMarker> = React.memo(
  ({
    id,
    isEnabledCallout = true,
    addMarkerRef,
    calloutText,
    onPressMarker,
    gps,
    ...props
  }) => {
    const addMarkerRefHandler = useCallback(
      (ref: Marker) => {
        addMarkerRef(ref, id);
      },
      [addMarkerRef, id],
    );

    const onPressMarkerHandler = useCallback(() => {
      onPressMarker?.(gps, id);
    }, [gps, id, onPressMarker]);

    return (
      <Marker
        tracksInfoWindowChanges={false}
        // tracksViewChanges={false}
        ref={addMarkerRefHandler}
        onPress={onPressMarkerHandler}
        coordinate={props}
        key={Math.random().toString()}>
        <FastImage
          source={ImageRepository.CustomMarker}
          style={styles.customMarket}
          resizeMode={'contain'}
        />
        {isEnabledCallout ? (
          <Callout tooltip>
            <View style={styles.callout}>
              <Text>{calloutText}</Text>
            </View>
          </Callout>
        ) : null}
      </Marker>
    );
  },
);

const styles = StyleSheet.create({
  callout: {
    width: windowWidth / 2,
    backgroundColor: Color.WHITE,
    borderRadius: RADIUS.MEDIUM,
    paddingVertical: 8,
    paddingHorizontal: 24,
    alignItems: 'center',
    justifyContent: 'center',
    ...UIStyles.shadowMD,
  },
  customMarket: {
    width: 40,
    height: 40,
  },
  text: {
    ...UIStyles.font13,
    color: Color.DARK,
  },
});
