import {
  BottomSheetScrollView,
  BottomSheetTextInput,
  BottomSheetView,
} from "@gorhom/bottom-sheet";
import React, { useEffect, useState } from "react";
import {
  Keyboard,
  LayoutAnimation,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { launchCamera } from "react-native-image-picker";
import { InputContainer } from "softjet_core/src/ui-kit";
import { Config } from "../../config";
import { ordersConfig } from "../../screens/orders/view/config";
import { useTypedSelector } from "../../system";
import { isSmallScreen } from "../../system/helpers/is-small-screen";
import { adaptiveSize } from "../../system/helpers/responseive-font-size";
import { useInput } from "../../system/hooks/use-input";
import { getAppInfo } from "../../system/selectors/selectors";
import { IBottomSheetModalComponentProps } from "../bottom-sheet-menu/bottom-sheet-modal-provider";
import { ThemedButton } from "../buttons/themed-button";
import { CheckboxComponent } from "../checkbox/checkbox-component";
import { ImageIcon } from "../icons/image-icon";
import { Rating } from "../rating/rating";

const { Color, UIStyles } = Config;

const checkboxItems = [
  { title: "Не понравилось блюдо", id: 0 },
  { title: "Отсутствовала часть заказа", id: 1 },
  { title: "Доставили с опазданием", id: 2 },
  { title: "Проблема с оплатой", id: 3 },
];

interface IAddCommentBottomSheetContent
  extends IBottomSheetModalComponentProps {
  onPress: (data: IOnPressEvaluationOrderData) => void;
}

export interface IOnPressEvaluationOrderData {
  rating: number;
  variants: number[];
  comment: string;
  image: string;
}

const getRatingCountText = (value: number) => {
  switch (value) {
    case 4:
      return "четыри";
    case 3:
      return "три";
    case 2:
      return "две";
    case 1:
      return "одну";
  }
};

export const BSMElevationOrderContent: React.FC<IAddCommentBottomSheetContent> =
  React.memo(({ expand, collapse, onPress }) => {
    const commentInput = useInput("");
    const [rating, setRating] = useState(5);
    const answerOptions = useTypedSelector(getAppInfo)?.answer_options;
    const [image, setImage] = useState("");

    const [isVisibleCheckboxVariants, setIsVisibleCheckBoxVariants] =
      useState(false);
    const [selectedVariants, setSelectedVariants] = useState<Set<number>>(
      new Set()
    );

    const onPressHandler = () => {
      onPress({
        rating,
        variants: [...selectedVariants],
        comment: commentInput.value,
        image,
      });
    };

    const onRating = (value: number) => {
      LayoutAnimation.configureNext({
        duration: 700,
        create: {
          type: LayoutAnimation.Types.spring,
          property: LayoutAnimation.Properties.scaleY,
          springDamping: 0.7,
        },
        update: {
          type: LayoutAnimation.Types.spring,
          property: LayoutAnimation.Properties.scaleY,
          springDamping: 0.7,
        },
        delete: {
          duration: 400,
          type: LayoutAnimation.Types.spring,
          property: LayoutAnimation.Properties.scaleY,
          springDamping: 0.7,
        },
      });
      setRating(value);
      if (value < 5) {
        ordersConfig.EVALUATE_ORDER_MENU.IS_VISIBLE_VARIANTS_CHECKBOX &&
          expand?.();
        setIsVisibleCheckBoxVariants(true);
      } else {
        ordersConfig.EVALUATE_ORDER_MENU.IS_VISIBLE_VARIANTS_CHECKBOX &&
          collapse?.();
        setIsVisibleCheckBoxVariants(false);
      }
    };

    const onToggleCheckBox = (id: number) => {
      const set = new Set(selectedVariants);
      set[set.has(id) ? "delete" : "add"](id);
      setSelectedVariants(set);
    };

    useEffect(() => {
      const listener = Keyboard.addListener("keyboardWillHide", () => {
        !isVisibleCheckboxVariants && collapse?.();
      });
      return () => listener?.remove();
    }, [isVisibleCheckboxVariants]);

    const ButtonAddImage = () => {
      const onPress = () => {
        //  launchImageLibrary(
        //    {mediaType: 'photo', includeBase64: true},
        //    ({base64, uri}) => {
        //      if (base64) {
        //        setImage(base64);
        //      }
        //    },
        //  );

        launchCamera(
          { mediaType: "photo", cameraType: "front", includeBase64: true },
          ({ base64 }) => {
            if (base64) {
              setImage(base64);
            }
          }
        );
      };

      return (
        <TouchableOpacity onPress={onPress}>
          <ImageIcon />
        </TouchableOpacity>
      );
    };

    return (
      <BottomSheetScrollView
        bounces={false}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.contentContainer}
      >
        <BottomSheetView style={styles.container}>
          <Text style={styles.title}>Оцените заказ</Text>
          {ordersConfig.EVALUATE_ORDER_MENU.IS_VISIBLE_VARIANTS_CHECKBOX &&
          isVisibleCheckboxVariants ? (
            <Text style={styles.description}>
              Вы поставили {getRatingCountText(rating)} звезды. Что вам не
              понравилось?
            </Text>
          ) : (
            <Text style={styles.description}>
              Как вам доставка и наши блюда?
            </Text>
          )}
          <Rating
            onPress={onRating}
            containerStyle={styles.ratingContainer}
            iconStyle={styles.ratingIcon}
            isTouchable={true}
            rating={rating}
          />
          {ordersConfig.EVALUATE_ORDER_MENU.IS_VISIBLE_VARIANTS_CHECKBOX &&
          isVisibleCheckboxVariants ? (
            <View style={styles.checkBoxContainer}>
              {answerOptions?.map((el) => {
                const onToggle = () => {
                  onToggleCheckBox(el.id);
                };

                return (
                  <CheckboxComponent
                    containerStyle={styles.checkboxContainer}
                    key={el.id}
                    isChecked={selectedVariants.has(el.id)}
                    onToggle={onToggle}
                    title={el.answer_option}
                  />
                );
              })}
            </View>
          ) : null}

          <InputContainer inputStyle={styles.inputContainer}>
            <BottomSheetTextInput
              style={styles.input}
              placeholder="Комментарий"
              onChangeText={commentInput.onChangeText}
            />
            {ordersConfig.EVALUATE_ORDER_MENU.IS_VISIBLE_IMAGE_PICKER ? (
              <ButtonAddImage />
            ) : null}
          </InputContainer>
          {/*  <InputWithRightElement
            multiline={true}
            inputStyle={{marginTop: adaptiveSize(16)}}
            RightComponent={
              ordersConfig.EVALUATE_ORDER_MENU.IS_VISIBLE_IMAGE_PICKER
                ? ButtonAddImage
                : null
            }
            {...commentInput}
          />
          */}
          <ThemedButton
            withGesture={true}
            wrapperStyle={{ marginTop: adaptiveSize(17) }}
            rounded={true}
            label={"Оценить"}
            onPress={onPressHandler}
          />
        </BottomSheetView>
      </BottomSheetScrollView>
    );
  });

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    ...UIStyles.paddingH16,
    alignItems: "center",
  },
  contentContainer: {
    paddingBottom: adaptiveSize(140),
  },
  checkboxContainer: {
    paddingVertical: adaptiveSize(14),
  },
  title: {
    ...UIStyles.font24b,
    color: Color.BLACK,
    textAlign: "center",
  },
  description: {
    ...UIStyles.font15,
    color: Color.GREY_600,
    paddingHorizontal: 50,
    textAlign: "center",
    marginTop: adaptiveSize(16),
  },
  ratingContainer: {
    marginTop: adaptiveSize(isSmallScreen ? 20 : 36),
  },
  inputContainer: {
    flexDirection: "row",
    paddingHorizontal: 16,
    paddingVertical: 15,
    marginTop: 20,
    justifyContent: "space-between",
  },
  ratingIcon: {
    width: adaptiveSize(40),
  },
  checkBoxContainer: {
    marginTop: adaptiveSize(36),
  },
  input: {
    flex: 1,
  },
});
