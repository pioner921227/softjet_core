import React, {useEffect, useRef} from 'react';
import {Animated, StatusBar, StyleSheet, Text, View} from 'react-native';
import {Config} from '../config';
import {ThemedButton} from './buttons/themed-button';
import {WarningIcon} from './icons/warning';
import {restartApp} from '../system/helpers/restart-app';

const {Color, UIStyles} = Config;

export const ErrorComponent = () => {
  const animValue = useRef(new Animated.Value(0));

  const animate = Animated.loop(
    Animated.sequence([
      Animated.timing(animValue.current, {
        toValue: 10,
        duration: 2000,
        useNativeDriver: false,
      }),
      Animated.timing(animValue.current, {
        toValue: 0,
        duration: 2000,
        useNativeDriver: false,
      }),
    ]),
    {iterations: Infinity},
  );

  useEffect(() => {
    animate.start();
    return () => {
      animate.reset();
    };
  }, []);

  const animStyle = animValue.current.interpolate({
    inputRange: [0, 10],
    outputRange: [0, -20],
  });

  const animWidth = animValue.current.interpolate({
    inputRange: [0, 10],
    outputRange: [100, 150],
    extrapolate: 'clamp',
  });

  return (
    <View style={styles.container}>
      <StatusBar barStyle={'dark-content'} />
      <View style={styles.content}>
        <Animated.View style={{transform: [{translateY: animStyle}]}}>
          <WarningIcon fill={Color.PRIMARY_60} width={100} height={100} />
        </Animated.View>
        <WarningIcon
          fill={Color.GREY_50}
          //@ts-ignore
          width={animWidth}
          height={150}
          style={styles.warningShadow}
        />
        <Text style={styles.title}>Упс! Что-то пошло не так !</Text>
        <Text style={styles.description}>В приложении возникла ошибка !</Text>
        <ThemedButton
          buttonStyle={styles.buttonWrapperStyle}
          rounded={true}
          label={'Перезапустить'}
          onPress={restartApp}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.WHITE,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    ...UIStyles.font20b,
    color: Color.DARK,
    textAlign: 'center',
  },
  description: {
    marginTop: 10,
    ...UIStyles.font18,
    color: Color.DARK,
    textAlign: 'center',
  },
  content: {
    height: 370,
    width: '100%',
    alignItems: 'center',
  },
  buttonWrapperStyle: {
    width: 200,
    alignSelf: 'center',
  },
  warningShadow: {
    transform: [{rotateX: '-80deg'}],
  },
});
