import React, {useState} from 'react';
import {LayoutAnimation, StyleSheet, Text, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import {useImage} from '../../system/hooks/use-image';
import {IOrdersDataProduct} from '../../screens/orders/api/types';
import {Rating} from '../rating/rating';
import {Config} from '../../config';
import {adaptiveSize} from '../../system/helpers/responseive-font-size';
import {Divider} from '../divider';

const {Color, UIStyles, RADIUS} = Config;

export interface IOnElevateOrderItemsData {
  id: number;
  rating: number;
  comment: '';
}

interface IListItem {
  item: IOrdersDataProduct;
  index: number;
  onEvaluate: (data: IOnElevateOrderItemsData) => void;
}

export const ListItem: React.FC<IListItem> = ({item, index, onEvaluate}) => {
  const image = useImage(item?.img_url);
  const [rating, setRating] = useState(5);

  const setRatingHandler = (value: number) => {
    LayoutAnimation.configureNext({
      duration: 700,
      create: {
        type: LayoutAnimation.Types.spring,
        property: LayoutAnimation.Properties.scaleY,
        springDamping: 0.7,
      },
      delete: {
        duration: 200,
        type: LayoutAnimation.Types.spring,
        property: LayoutAnimation.Properties.scaleY,
        springDamping: 0.7,
      },
    });
    setRating(value);
    onEvaluate({id: item.product_id, rating: value, comment: ''});
  };

  return (
    <>
      <View style={styles.container}>
        <FastImage source={image} style={styles.image} />
        <View style={styles.rightBlock}>
          <Text style={styles.title}>{item.title}</Text>
          <Rating
            isTouchable={true}
            onPress={setRatingHandler}
            iconStyle={styles.ratingIcon}
            rating={rating}
          />
        </View>
      </View>
      <Divider />
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    flexDirection: 'row',
    marginVertical: 8,
  },
  image: {
    width: 120,
    height: 90,
    borderRadius: RADIUS.MEDIUM,
  },
  rightBlock: {
    // flex: 1,
    paddingLeft: 24,
    justifyContent: 'center',
  },
  title: {
    ...UIStyles.font15b,
    marginBottom: 8,
  },
  ratingIcon: {
    width: adaptiveSize(30),
  },
});
