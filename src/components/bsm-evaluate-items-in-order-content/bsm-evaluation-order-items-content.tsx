import React, { useCallback, useEffect, useRef, useState } from "react";
import {
  FlatList,
  Keyboard,
  Platform,
  Pressable,
  StyleSheet,
  Text,
  View,
} from "react-native";
import { Input } from "../input/input";
import { IOnElevateOrderItemsData, ListItem } from "./list-item";
import {
  IEvaluationOrderItemsRequestData,
  IGetOrderDataResponse,
  IRatingItem,
} from "../../screens/orders/api/types";
import { Config } from "../../config";
import { useInput } from "../../system";
import { ThemedButton } from "../buttons/themed-button";
import { adaptiveSize } from "../../system/helpers/responseive-font-size";
import { useLayout } from "../../system/hooks/use-layout";
import { windowWidth } from "../../system/helpers/window-size";
import { BottomSheetTextInput } from "@gorhom/bottom-sheet";
import { InputContainer } from "softjet_core/src/ui-kit";

const { Color, UIStyles } = Config;

interface IBSMEvaluationOrderItemsContent {
  orderData: IGetOrderDataResponse;
  onPress: (data: Omit<IEvaluationOrderItemsRequestData, "orderId">) => void;
}

export const BSMEvaluationOrderItemsContent: React.FC<IBSMEvaluationOrderItemsContent> =
  React.memo(({ orderData, onPress }) => {
    const comment = useInput("");
    const flatListRef = useRef<FlatList | null>(null);
    // const [bottomComponentHeight, setBottomComponentHeight] = useState(100);
    const keyExtractor = useCallback((item) => item.product_id, []);

    const [items, setItems] = useState<IRatingItem[]>([]);

    const onPressHandler = () => {
      onPress({
        comment: comment.value,
        ratings: items,
      });
    };

    const { onLayout, values } = useLayout();

    useEffect(() => {
      flatListRef.current?.setNativeProps({
        style: { marginBottom: values.height + 20 },
      });
      flatListRef.current?.scrollToEnd();
    }, [values.height]);

    const onEvaluateOrderItem = (data: IOnElevateOrderItemsData) => {
      setItems((state) => {
        const newData = state.filter((el) => el.id !== data.id);
        return [...newData, data];
      });
    };

    const renderItem = useCallback((props) => {
      return <ListItem onEvaluate={onEvaluateOrderItem} {...props} />;
    }, []);

    // useEffect(() => {
    //   LayoutAnimation.configureNext({
    //     duration: 700,
    //     create: {
    //       type: LayoutAnimation.Types.spring,
    //       property: LayoutAnimation.Properties.scaleY,
    //       springDamping: 0.7,
    //     },
    //   });
    // }, []);

    useEffect(() => {
      const initialItems: IOnElevateOrderItemsData[] = orderData?.products?.map(
        (el) => ({
          id: el.product_id,
          rating: 5,
          comment: "",
        })
      );
      setItems(initialItems);
    }, []);

    return (
      <View style={styles.container}>
        <Pressable onPress={Keyboard.dismiss} style={styles.backDrop} />
        <Text style={styles.title}>Оцените вкус блюд</Text>
        {orderData?.products?.length ? (
          <View style={styles.flatListContainer}>
            <FlatList
              ref={flatListRef}
              contentContainerStyle={styles.flatList}
              keyExtractor={keyExtractor}
              data={orderData?.products}
              renderItem={renderItem}
            />
          </View>
        ) : null}
        <View onLayout={onLayout} style={styles.bottomContent}>
      {Platform.OS === 'ios' ? 
          <InputContainer inputStyle={{ padding: 16, marginTop: 10 }}>
            <BottomSheetTextInput
              onChangeText={comment.onChangeText}
              placeholder={"Расскажите, что понравилось"}
              style={styles.input}
            />
          </InputContainer>
          : 
          <Input {...comment} placeholderText={'Расскажите, что понравилось'}/>
        }
          <ThemedButton
            wrapperStyle={styles.buttonWrapper}
            rounded={true}
            label={"Оценить"}
            onPress={onPressHandler}
          />
        </View>
      </View>
    );
  });

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
  },
  listContainer: {
    height: "100%",
  },
  bottomContent: {
    marginTop: "auto",
    marginBottom: 20,
    width: "100%",
    alignSelf: "center",
    backgroundColor: Color.WHITE,
  },
  flatList: {
    paddingBottom: 70,
  },
  buttonWrapper: {
    marginTop: adaptiveSize(45),
  },
  title: {
    ...UIStyles.font24b,
    textAlign: "center",
    marginBottom: 24,
  },
  input: {
    paddingTop: Platform.select({ ios: 0, android: 0 }),
  },
  backDrop: {
    position: "absolute",
    top: 0,
    left: 0,
    width: windowWidth,
    height: "100%",
    backgroundColor: Config.Color.WHITE,
  },
  flatListContainer: {
    height: "100%",
  },
});
