import React, {useCallback, useMemo} from 'react';
import {Dimensions, FlatList, StyleSheet, View, ViewStyle} from 'react-native';
import {BannerListItem} from './banner-list-item';
import {IBannerItem} from '../../screens/catalog/types/types';
import {Preloader} from '../preloader';
import {useTypedSelector} from '../../system';
import {EmptyDataTitle} from '../empty-data-title';

const width = Dimensions.get('window').width;
// const ITEM_WIDTH = 295;
const IMAGE_WIDTH = 283.3;
const ITEM_WIDTH = IMAGE_WIDTH + 16;

export interface IBannerList {
  containerStyle?: ViewStyle;
  contentContainerStyle?: ViewStyle;
  onPress: (item: IBannerItem) => void;
  banners: IBannerItem[];
}

const emptyData: IBannerItem[] = [];

export const BannerList: React.FC<IBannerList> = React.memo(
  ({contentContainerStyle, onPress, containerStyle, banners}) => {
    const renderItem = useCallback(
      ({item}) => (
        <BannerListItem onPress={onPress} key={item.id} item={item} />
      ),
      [],
    );

    const isLoading = useTypedSelector(state => state.catalog.isLoadingBanners);

    const _containerStyle = useMemo(() => {
      return {
        ...styles.container,
        ...containerStyle,
      };
    }, [containerStyle]);

    const _contentContainerStyle = useMemo(
      () => ({
        ...styles.containerContent,
        ...contentContainerStyle,
      }),
      [contentContainerStyle],
    );

    const keyExtractor = useCallback(item => item.id?.toString(), []);

    const snap = useMemo(() => {
      if (Array.isArray(banners) && banners.length > 0) {
        return banners?.map(
          (el, index) => ITEM_WIDTH * index - (width - ITEM_WIDTH) / 2 + 8,
        );
      }
      return [0];
    }, [banners]);

    const EmptyComponent = () => {
      return (
        <View style={styles.emptyComponent}>
          <EmptyDataTitle />
        </View>
      );
    };

    return (
      <View style={_containerStyle}>
        <FlatList
          style={styles.flatList}
          ListEmptyComponent={isLoading ? Preloader : EmptyComponent}
          keyExtractor={keyExtractor}
          snapToOffsets={snap}
          centerContent={true}
          initialNumToRender={3}
          windowSize={5}
          bounces={false}
          snapToAlignment={'center'}
          decelerationRate={'fast'}
          horizontal={true}
          contentContainerStyle={_contentContainerStyle}
          showsHorizontalScrollIndicator={false}
          data={banners?.length ? banners : emptyData}
          renderItem={renderItem}
        />
      </View>
    );
  },
);

const styles = StyleSheet.create({
  container: {
    marginTop: 8,
  },
  flatList: {
    // flexGrow:1,
    // width: '100%',
  },
  containerContent: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 8,
    flexGrow: 1,
    // ...UIStyles.paddingH16,
  },
  emptyComponent: {
    height: 128,
  },
});
