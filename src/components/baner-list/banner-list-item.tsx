import React, {useCallback, useMemo} from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import LinearGradient from 'react-native-linear-gradient';
import {IBannerItem} from '../../screens/catalog/types/types';
import {Config} from '../../config';
import {useImage} from '../../system/hooks/use-image';

const {Color, UIStyles, RADIUS} = Config;

export interface IBannerListItemProps {
  item: IBannerItem;
  style?: ViewStyle;
  onPress?: (item: IBannerItem) => void;
}

const gradientStart = {x: 1, y: 0};
const gradientEnd = {x: 1, y: 1};
const colors = ['rgba(0,0,0,0)', 'rgba(0,0,0,0.68)'];

export const BannerListItem: React.FC<IBannerListItemProps> = React.memo(
  ({item, style, onPress}) => {
    const image = useImage(item.img_big);

    const styleContainer = useMemo(() => {
      return {
        ...styles.container,
        ...style,
      };
    }, [style]);

    const onPressHandler = useCallback(() => {
      onPress?.(item);
    }, [item, onPress]);

    return (
      <TouchableOpacity onPress={onPressHandler} style={styleContainer}>
        <FastImage source={image} style={styles.image} resizeMode={'cover'} />
        <LinearGradient
          colors={colors}
          style={styles.gradient}
          start={gradientStart}
          end={gradientEnd}
        />
        <View style={styles.titleWrapper}>
          <Text style={styles.title}>{item.title}</Text>
        </View>
      </TouchableOpacity>
    );
  },
);

const styles = StyleSheet.create({
  container: {
    height: 128,
    backgroundColor: Color.WHITE,
    marginHorizontal: 8,
  },
  image: {
    width: 283,
    height: 128,
    borderRadius: RADIUS.LARGE,
  },
  gradient: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    borderRadius: RADIUS.LARGE,
  },
  titleWrapper: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
    padding: 8,
  },
  title: {
    ...UIStyles.font20b,
    color: Color.WHITE,
    textAlign: 'center',
  },
});
