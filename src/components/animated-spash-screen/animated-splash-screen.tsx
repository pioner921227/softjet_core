import React, {useEffect, useRef} from 'react';
import {Animated, StyleSheet, View} from 'react-native';
import {Wave1Icon} from './components/wave1-icon';
import {Wave2Icon} from './components/wave2-icon';
import {windowHeight} from '../../system/helpers/window-size';
import {FishIcon} from './components/fish-icon';
import {Logo} from './components/logo';
import LinearGradient from 'react-native-linear-gradient';

export const AnimatedSplashScreen = () => {
  const animatedValue = useRef(new Animated.Value(0)).current;
  const fishAnimated = useRef(new Animated.Value(0)).current;

  // useEffect(() => {
  //   Animated.loop(
  //     Animated.sequence([
  //       Animated.timing(animatedValue, {
  //         toValue: 1,
  //         duration: 1000,
  //         useNativeDriver: true,
  //       }),
  //       Animated.timing(animatedValue, {
  //         toValue: 0,
  //         duration: 1000,
  //         useNativeDriver: true,
  //       }),
  //     ]),
  //   ).start();
  // }, []);

  const fishLoop = () => {
    return Animated.loop(
      Animated.sequence([
        Animated.timing(fishAnimated, {
          toValue: 150,
          duration: 1000,
          useNativeDriver: true,
        }),
        Animated.timing(fishAnimated, {
          toValue: 120,
          duration: 1000,
          useNativeDriver: true,
        }),
      ]),
    );
  };

  const startAnim = () => {
    return Animated.timing(fishAnimated, {
      toValue: 150,
      duration: 800,
      useNativeDriver: true,
    });
  };

  useEffect(() => {
    startAnim().start(() => {
      fishLoop().start();
    });
    // Animated.sequence([startAnim(), fishLoop()]).start();
  }, []);

  const animatedStyleFish = fishAnimated.interpolate({
    inputRange: [0, 150],
    outputRange: [150, 0],
  });

  const logoAnimatedStyle = fishAnimated.interpolate({
    inputRange: [0, 150],
    outputRange: [0, 200],
  });

  return (
    <View style={styles.container}>
      <LinearGradient
        colors={['#ABE2F9', '#FFFFFF']}
        style={styles.gradient}
        start={{x: 1, y: 0}}
        end={{x: 1, y: 1}}
      />
      <Animated.View
        style={[styles.fish, {transform: [{translateY: animatedStyleFish}]}]}>
        <FishIcon />
      </Animated.View>
      <Animated.View
        style={[styles.logo, {transform: [{translateY: logoAnimatedStyle}]}]}>
        <Logo />
      </Animated.View>

      <Animated.View style={{position: 'absolute', bottom: 0}}>
        <Wave1Icon />
      </Animated.View>
      <Animated.View style={{position: 'absolute', bottom: 0}}>
        <Wave2Icon />
      </Animated.View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    // height: 100,
    paddingTop: 230,
    flex: 1,
  },
  waves: {
    flex: 1,
    position: 'absolute',
    top: windowHeight - 350,
  },
  wave1: {
    width: '100%',
    position: 'absolute',
  },
  wave2: {
    position: 'absolute',
  },
  fish: {
    alignSelf: 'center',
  },
  gradient: {
    width: '100%',
    height: '100%',
    position: 'absolute',
  },
  logo: {
    alignSelf: 'center',
    zIndex: 5,
  },
});
