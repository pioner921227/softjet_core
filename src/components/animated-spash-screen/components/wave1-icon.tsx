import * as React from 'react';
import Svg, {
  Path,
  Defs,
  LinearGradient,
  Stop,
  SvgProps,
} from 'react-native-svg';
import {windowWidth} from '../../../system/helpers/window-size';

export const Wave1Icon: React.FC<SvgProps> = props => {
  return (
    <Svg
      width={windowWidth}
      height={windowWidth - 20}
      viewBox="0 0 375 317"
      fill="none"
      {...props}>
      <Path
        d="M376 58.215S308.458 4.7 256 .377c-50.901-4.196-74.934 27.889-125.5 34.797-51.207 6.995-132.5 0-132.5 0V338h378V58.215z"
        fill="url(#paint0_linear)"
        fillOpacity={0.7}
      />
      <Defs>
        <LinearGradient
          id="paint0_linear"
          x1={224.5}
          y1={338}
          x2={197.94}
          y2={32.0536}
          gradientUnits="userSpaceOnUse">
          <Stop stopColor="#199DDB" />
          <Stop offset={1} stopColor="#87D6F7" />
        </LinearGradient>
      </Defs>
    </Svg>
  );
};
