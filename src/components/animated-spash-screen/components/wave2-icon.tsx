import * as React from 'react';
import Svg, {
  Path,
  Defs,
  LinearGradient,
  Stop,
  SvgProps,
} from 'react-native-svg';
import {windowWidth} from '../../../system/helpers/window-size';

export const Wave2Icon: React.FC<SvgProps> = props => {
  return (
    <Svg
      width={windowWidth}
      height={windowWidth - 20}
      viewBox="0 0 375 360"
      fill="none"
      {...props}>
      <Path
        d="M-2 62.5S65.542 5.598 118 1c50.901-4.462 74.934 29.654 125.5 37 51.207 7.439 132.5 0 132.5 0v322H-2V62.5z"
        fill="url(#paint0_linear)"
        fillOpacity={0.7}
      />
      <Defs>
        <LinearGradient
          id="paint0_linear"
          x1={149.5}
          y1={360}
          x2={179.5}
          y2={35}
          gradientUnits="userSpaceOnUse">
          <Stop stopColor="#199DDB" />
          <Stop offset={1} stopColor="#87D6F7" />
        </LinearGradient>
      </Defs>
    </Svg>
  );
};
