import React, {PropsWithChildren, useMemo} from 'react';
import {KeyboardTypeOptions, StyleSheet, TextInput, TextInputProps, TextStyle, View, ViewStyle} from 'react-native';
import {Config} from '../../config';
import {InputContainer} from './input-container';

const {Color, UIStyles, RADIUS} = Config;

export interface IInput extends TextInputProps {
  textStyle?: TextStyle;
  inputStyle?: ViewStyle;
  placeholderColor?: string;
  placeholderText?: string;
  value: string;
  onChangeText: (text: string) => void;
  keyboardType?: KeyboardTypeOptions;
  multiline?: boolean;
  disabled?: boolean;
}

export const Input = React.memo(
  React.forwardRef<TextInput, PropsWithChildren<IInput>>(
    (
      {
        textStyle,
        inputStyle,
        placeholderColor = Color.GREY_400,
        placeholderText = 'Placeholder',
        onChangeText,
        value,
        keyboardType = 'default',
        multiline = false,
        disabled = false,
        ...props
      },
      ref,
    ) => {
      // const containerStyle = useMemo(() => {
      //   return {
      //     backgroundColor: disabled ? Color.GREY_50 : Color.WHITE,
      //     ...styles.container,
      //     ...inputStyle,
      //   };
      // }, [disabled, inputStyle]);

      const textInputStyle = useMemo(() => {
        return {
          ...styles.inputStyle,
          color: disabled ? Color.GREY_400 : Color.BLACK,
          ...textStyle,
        };
      }, [disabled, textStyle]);

      return (
        <InputContainer disabled={disabled} inputStyle={inputStyle}>
          <TextInput
            {...props}
            ref={ref}
            editable={!disabled}
            multiline={multiline}
            onChangeText={onChangeText}
            value={value}
            keyboardType={keyboardType}
            style={textInputStyle}
            placeholder={placeholderText}
            placeholderTextColor={placeholderColor}
          />
        </InputContainer>
      );
    },
  ),
);

const styles = StyleSheet.create({
  // container: {
  //   width: '100%',
  //   borderWidth: 1,
  //   borderColor: Color.GREY_100,
  //   borderRadius: RADIUS.MEDIUM,
  //   justifyContent: 'center',
  //   ...UIStyles.paddingH16,
  // },
  inputStyle: {
    ...UIStyles.paddingV16,
    ...UIStyles.font15,
  },
});
