import React, {useMemo} from 'react';
import {IInput, Input} from './input';
import {StyleSheet, View, ViewStyle} from 'react-native';
import {Config} from '../../config';
import {IInputLabel, InputLabel} from './input-label';

const {Color, UIStyles} = Config;

interface IInputWithLabel extends IInput, IInputLabel {
  wrapperStyle?: ViewStyle;
  onFocus?: () => void;
}

export const InputWithLabel: React.FC<IInputWithLabel> = ({
  label = 'Label',
  labelStyle,
  wrapperStyle,
  onFocus,
  ...rest
}) => {
  const wrapperStyle_ = useMemo(() => {
    return [styles.container, wrapperStyle];
  }, [wrapperStyle]);

  return (
    <View style={wrapperStyle_}>
      <InputLabel label={label} labelStyle={labelStyle} />
      <Input onFocus={onFocus} {...rest} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
  },
  labelWrapper: {
    position: 'absolute',
    top: -8,
    left: 8,
    alignItems: 'center',
    alignSelf: 'flex-start',
  },
  background: {
    backgroundColor: Color.WHITE,
    height: 10,
    width: 200,
    zIndex: 2,
  },
});
