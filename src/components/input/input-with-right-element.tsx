import React, {useMemo} from 'react';
import {Platform, StyleSheet, TextInput, View} from 'react-native';
import {IInput} from './input';
import {Config} from '../../config';
const {Color, UIStyles, RADIUS} = Config;

interface IInputWithRightElement extends IInput {
  RightComponent: React.FC | null;
}

export const InputWithRightElement: React.FC<IInputWithRightElement> = React.memo(
  ({RightComponent, inputStyle, placeholderText, ...rest}) => {
    const containerStyle = useMemo(() => {
      return [styles.container, inputStyle];
    }, [inputStyle]);

    return (
      <View style={containerStyle}>
        <TextInput
          placeholder={placeholderText}
          placeholderTextColor={Color.RGBA_400}
          style={styles.inputStyle}
          {...rest}
        />
        {RightComponent ? <RightComponent /> : null}
      </View>
    );
  },
);

const styles = StyleSheet.create({
  container: {
    // flex:1,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: Color.GREY_100,
    borderRadius: RADIUS.MEDIUM,
    justifyContent: 'space-between',
    ...UIStyles.paddingH16,
    paddingVertical: Platform.select({ios: 16, android: 0}),
    // ...UIStyles.paddingV16,
  },
  inputStyle: {
    flex: 1,
    paddingVertical:0,
    ...UIStyles.font15,
    color: Color.BLACK,
  },
});
