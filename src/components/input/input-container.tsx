import React, {useMemo} from 'react';
import {StyleSheet, View, ViewStyle} from 'react-native';
import {Config} from '../../config';

const {Color, RADIUS, UIStyles} = Config;

interface IInputContainer {
  disabled?: boolean;
  inputStyle?: ViewStyle;
}

export const InputContainer: React.FC<IInputContainer> = React.memo(
  ({disabled, inputStyle, children}) => {
    const containerStyle = useMemo(() => {
      return {
        backgroundColor: disabled ? Color.GREY_50 : Color.WHITE,
        ...styles.container,
        ...inputStyle,
      };
    }, [disabled, inputStyle]);

    return <View style={containerStyle}>{children}</View>;
  },
);

const styles = StyleSheet.create({
  container: {
    width: '100%',
    borderWidth: 1,
    borderColor: Color.GREY_100,
    borderRadius: RADIUS.MEDIUM,
    justifyContent: 'center',
    ...UIStyles.paddingH16,
  },
  inputStyle: {
    ...UIStyles.paddingV16,
    ...UIStyles.font15,
  },
});
