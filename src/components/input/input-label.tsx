import {StyleSheet, Text, TextStyle} from 'react-native';
import React, {useMemo} from 'react';
import {Config} from '../../config';
const {Color, UIStyles} = Config;

export interface IInputLabel {
  labelStyle?: TextStyle;
  label: string;
}

export const InputLabel: React.FC<IInputLabel> = React.memo(
  ({labelStyle, label}) => {
    const labelStyle_ = useMemo(() => {
      return [styles.label, labelStyle];
    }, [labelStyle]);

    return <Text style={labelStyle_}>{label}</Text>;
  },
);

const styles = StyleSheet.create({
  label: {
    ...UIStyles.font13,
    zIndex: 1,
    color: Color.RGBA_400,
    position: 'absolute',
    top: -17,
    left: 8,
  },
});
