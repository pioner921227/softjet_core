import React, {useMemo} from 'react';
import {StyleSheet, Text, TouchableOpacity, ViewStyle} from 'react-native';
import {Config} from '../../config';
import {adaptiveSize} from '../../system/helpers/responseive-font-size';
import {cartConfig} from '../../screens/cart';

const {Color, RADIUS, UIStyles} = Config;

interface IDateTimePickerItem {
  title: string;
  isSelected: boolean;
  onSelect: () => void;
}

export const HorizontalPickerItem: React.FC<IDateTimePickerItem> = React.memo(
  ({title, isSelected, onSelect}) => {
    const containerStyle: ViewStyle = useMemo(() => {
      return {
        ...styles.container,
        backgroundColor: isSelected
          ? cartConfig.datePicker.selectedColor
          : 'transparent',
      };
    }, [isSelected]);

    const textStyle = useMemo(() => {
      return {
        ...styles.title,
        color: isSelected ? Color.WHITE : Color.DARK,
      };
    }, [isSelected]);

    return (
      <TouchableOpacity onPress={onSelect} style={containerStyle}>
        <Text style={textStyle}>{title}</Text>
      </TouchableOpacity>
    );
  },
);

const styles = StyleSheet.create({
  container: {
    marginRight: 10,
    paddingVertical: adaptiveSize(12),
    paddingHorizontal: adaptiveSize(16),
    borderRadius: RADIUS.MEDIUM,
  },
  title: {
    ...UIStyles.font15b,
    color: Color.WHITE,
  },
});
