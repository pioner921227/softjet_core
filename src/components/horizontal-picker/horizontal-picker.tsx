import React, {useCallback, useMemo, useState} from 'react';
import {FlatList, StyleSheet, View, ViewStyle} from 'react-native';
import {useLayout} from '../../system/hooks/use-layout';
import {HorizontalPickerItem} from './horizontal-picker-item';

interface IHorizontalPicker {
  onSelect: (index: number) => void;
  data: string[];
  contentContainerStyle?: ViewStyle;
}

export const HorizontalPicker: React.FC<IHorizontalPicker> = React.memo(
  ({onSelect, data, contentContainerStyle}) => {
    const [selectedDate, setSelectedDate] = useState(0);
    // const [itemWidth, setItemHeight] = useState(10);

    // const onLayout = useLayout(({width}) => {
    //   setItemHeight(width);
    // });
    const {onLayout, values} = useLayout();

    const renderItem = useCallback(
      ({item, index}) => {
        const onSelectHandler = () => {
          setSelectedDate(index);
          onSelect(index);
        };

        return (
          <View onLayout={onLayout}>
            <HorizontalPickerItem
              onSelect={onSelectHandler}
              isSelected={index === selectedDate}
              title={item}
            />
          </View>
        );
      },
      [onLayout, onSelect, selectedDate],
    );

    const snaps = useMemo(() => data.map((el, index) => values.width * index), [
      data,
      values.width,
    ]);

    const keyExtractor = useCallback((item: string) => item.toString(), []);

    const contentContainerStyle_ = useMemo(() => {
      return {
        ...styles.contentContainerStyle,
        ...contentContainerStyle,
      };
    }, [contentContainerStyle]);

    return (
      <View>
        <FlatList
          keyExtractor={keyExtractor}
          pagingEnabled={true}
          decelerationRate={'fast'}
          snapToOffsets={snaps}
          showsHorizontalScrollIndicator={false}
          nestedScrollEnabled={true}
          scrollEnabled={true}
          contentContainerStyle={contentContainerStyle_}
          horizontal={true}
          data={data}
          scrollEventThrottle={16}
          renderItem={renderItem}
        />
      </View>
    );
  },
);

const styles = StyleSheet.create({
  contentContainerStyle: {
    flexGrow: 1,
  },
});
