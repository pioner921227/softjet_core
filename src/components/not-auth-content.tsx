import {StyleSheet, Text, View} from 'react-native';
import {ProfileIcon} from './icons';
import {ThemedButton} from './buttons/themed-button';
import React from 'react';
import {Config} from '../config';
const {Color, UIStyles} = Config;

interface INotAuthContent {
  onPress: () => void;
  buttonLabel: string;
}

export const NotAuthContent: React.FC<INotAuthContent> = ({
  onPress,
  buttonLabel,
}) => {
  return (
    <View style={styles.block}>
      <ProfileIcon width={85} height={80} fill={Color.SECONDARY} />
      <View style={styles.descriptionWrapper}>
        <Text style={UIStyles.font24b}>Давайте знакомиться!</Text>
        <Text style={{marginTop: 12}}>
          Подарки на день рождения, сохраненные адреса и персональные акции!
        </Text>
        <ThemedButton
          buttonStyle={styles.button}
          rounded={true}
          label={buttonLabel}
          onPress={onPress}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {},
  block: {
    // flex:1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 154,
    paddingHorizontal: 45,
  },
  descriptionWrapper: {
    marginTop: 54,
    alignItems: 'center',
  },
  button: {
    marginTop: 25,
    width: 97,
  },
});
