import {Config} from '../../config';
import React from 'react';
//@ts-ignore
import RadioButtonRN from 'radio-buttons-react-native';
import {ColorValue, StyleSheet, TextStyle, ViewStyle} from 'react-native';
import {Color} from 'react-native-svg';

export interface IRadioButtonData {
  id: number;
  label: string;
  disabled?: boolean;
}

export interface IRadioButton {
  initialIndex?: number;
  data: IRadioButtonData[];
  style?: ViewStyle;
  inactiveColor?: ColorValue;
  activeColor?: Color;
  onSelect: (item: IRadioButtonData) => void;
  textStyle?: TextStyle;
  boxStyle?: ViewStyle;
}

export const RadioButton: React.FC<IRadioButton> = React.memo(
  ({
    data,
    style,
    onSelect,
    inactiveColor,
    activeColor,
    textStyle,
    initialIndex = 1,
    boxStyle,
  }) => {
    return (
      <RadioButtonRN
        initial={initialIndex}
        box={false}
        style={StyleSheet.flatten([styles.container, style])}
        data={data}
        circleSize={20}
        filledCircleSize={8}
        iconStyle={styles.iconStyle}
        boxStyle={boxStyle}
        deactiveColor={inactiveColor || Config.Color.BLACK}
        selectedBtn={onSelect}
        activeColor={activeColor}
        textStyle={textStyle}
      />
    );
  },
);

const styles = StyleSheet.create({
  container: {},
  boxStyle: {},
  iconStyle: {
    borderWidth: 2,
  },
});
