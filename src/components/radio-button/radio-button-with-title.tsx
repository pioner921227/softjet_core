import React, {useMemo} from 'react';
import {StyleSheet, Text, TextStyle, View, ViewStyle} from 'react-native';
import {IRadioButton, RadioButton} from './radio-button';
import {Config} from '../../config';
import {style} from '../../system/helpers/styles';
const {Color} = Config;

interface IRadioButtonWithTitle extends IRadioButton {
  title: string;
  titleStyle?: TextStyle;
  containerStyle?: ViewStyle;
}

export const RadioButtonWithTitle: React.FC<IRadioButtonWithTitle> = React.memo(
  ({title, titleStyle, containerStyle, ...props}) => {
    const containerStyle_ = useMemo(() => [styles.container, containerStyle], [
      containerStyle,
    ]);

    const titleStyle_ = useMemo(() => [styles.title, titleStyle], [titleStyle]);

    return (
      <View style={containerStyle_}>
        <Text style={titleStyle_}>{title}</Text>
        <RadioButton {...props} />
      </View>
    );
  },
);

const styles = StyleSheet.create({
  container: {},
  title: {
    color: Color.GREY_400,
  },
});
