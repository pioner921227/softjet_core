import React from 'react';
import {StyleSheet, Text} from 'react-native';
import {Config} from '../config';
const {UIStyles, Color} = Config;

export const EmptyDataTitle = () => {
  return <Text style={styles.title}>Нет данных</Text>;
};

const styles = StyleSheet.create({
  title: {
    ...UIStyles.font15,
    color: Color.DARK,
    textAlign: 'center',
  },
});
