import React from 'react';
import {StyleSheet} from 'react-native';
import {windowHeight, windowWidth} from '../system/helpers/window-size';
import FastImage, {Source} from 'react-native-fast-image';
import {LottieSplashScreen} from './lottie-splash-screen';

export enum SplashScreenTypes {
  image = 'image',
  lottie = 'lottie',
}

interface ISplashScreen {
  source: Source | string;
  type: keyof typeof SplashScreenTypes;
}

export const SplashScreen: React.FC<ISplashScreen> = ({source, type}) => {
  switch (type) {
    case 'lottie':
      return <LottieSplashScreen source={source as string} />;
    default:
      return <FastImage source={source as Source} style={styles.container} />;
  }
};

const styles = StyleSheet.create({
  container: {
    width: windowWidth + 10,
    height: windowHeight + 10,
  },
});
