import {StyleSheet, Text, TextStyle, View, ViewStyle} from 'react-native';
import React from 'react';
import {Config} from '../config';
const {Color, UIStyles} = Config;

interface ITitleWithValueRow {
  title: string;
  value: string;
  containerStyle?: ViewStyle;
  titleStyle?: TextStyle;
  valueStyle?: TextStyle;
}

export const TitleWithValueRow: React.FC<ITitleWithValueRow> = React.memo(
  ({title, value, containerStyle, titleStyle, valueStyle}) => {
    return (
      <View style={[styles.container, containerStyle]}>
        <Text style={[styles.title, titleStyle]}>{title}</Text>
        <Text numberOfLines={3} style={[styles.value, valueStyle]}>{value}</Text>
      </View>
    );
  },
);

const styles = StyleSheet.create({
  container: {
    ...UIStyles.flexRow,
    paddingVertical: 15.5,
    alignItems:'flex-start',
  },
  title: {
    marginRight:20,
    ...UIStyles.font15,
    color: Color.GREY_600,
  },
  value: {
    textAlign:'right',
    flexShrink:1,
    ...UIStyles.font15,
    color: Color.BLACK,
  },
});
