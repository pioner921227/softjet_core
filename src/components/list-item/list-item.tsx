import {Config} from '../../config';
import {StyleSheet, Text, TextStyle, ViewStyle} from 'react-native';
import React, {useMemo} from 'react';
import {TouchableOpacity} from '@gorhom/bottom-sheet';

const {UIStyles, Color} = Config;

export interface IListItem {
  title: string;
  onSelect?: () => void;
  isSelected: boolean;
  containerStyle?: ViewStyle;
  textStyle?: TextStyle;
}

export const ListItem: React.FC<IListItem> = React.memo(
  ({title, onSelect, isSelected, containerStyle, textStyle}) => {
    const containerStyle_ = useMemo(() => [styles.container, containerStyle], [
      containerStyle,
    ]);

    //@ts-ignore
    const titleStyle_: TextStyle = useMemo(
      () => [
        styles.title,
        {fontWeight: isSelected ? 'bold' : 'normal'},
        textStyle,
      ],
      [isSelected, textStyle],
    );

    return (
      <TouchableOpacity style={containerStyle_} onPress={onSelect}>
        <Text style={titleStyle_}>{title}</Text>
      </TouchableOpacity>
    );
  },
);

const styles = StyleSheet.create({
  container: {
    paddingVertical: 13.5,
  },
  title: {
    ...UIStyles.font15,
    color: Color.DARK,
  },
});
