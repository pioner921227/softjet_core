import {Config} from '../../config';
import React, {useMemo} from 'react';
import {TouchableOpacity} from '@gorhom/bottom-sheet';
import {CloseCrossIcon} from '../icons';
import {StyleSheet, View} from 'react-native';
import {IListItem, ListItem} from './list-item';

const {Color, UIStyles} = Config;

interface IListItemWithDelete extends IListItem {
  onDelete: (id: number) => void;
  id: number;
}

export const ListItemWithDelete: React.FC<IListItemWithDelete> = React.memo(
  ({title, onSelect, isSelected, onDelete, id}) => {
    const containerStyle = useMemo(() => {
      return {
        ...styles.container,
        borderColor: isSelected ? Color.PRIMARY : 'transparent',
      };
    }, [isSelected]);

    const onDeleteHandler = () => {
      onDelete(id);
    };

    return (
      <View style={containerStyle}>
        <TouchableOpacity style={styles.button} onPress={onSelect}>
          <ListItem title={title} isSelected={isSelected} onSelect={onSelect} />
        </TouchableOpacity>
        <TouchableOpacity style={styles.deleteButton} onPress={onDeleteHandler}>
          <CloseCrossIcon width={10} height={10} fill={Color.PRIMARY} />
        </TouchableOpacity>
      </View>
    );
  },
);

const styles = StyleSheet.create({
  container: {
    ...UIStyles.flexRow,
  },
  title: {
    ...UIStyles.font15,
    color: Color.DARK,
    paddingVertical: 13.5,
  },
  button: {
    flex: 1,
  },
  deleteButton: {
    width: 45,
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
