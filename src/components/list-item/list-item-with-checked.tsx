import {Config} from '../../config';
import React from 'react';
import {TouchableOpacity} from '@gorhom/bottom-sheet';
import {CheckedIcon} from '../icons';
import {StyleSheet} from 'react-native';
import {IListItem, ListItem} from './list-item';

const {Color, UIStyles} = Config;

export const ListItemWithChecked: React.FC<IListItem> = React.memo(
  ({title, onSelect, isSelected}) => {
    return (
      <TouchableOpacity onPress={onSelect} style={styles.container}>
        <ListItem title={title} isSelected={isSelected} onSelect={onSelect} />
        {isSelected ? <CheckedIcon fill={Color.PRIMARY} /> : null}
      </TouchableOpacity>
    );
  },
);


const styles = StyleSheet.create({
  container: {
    ...UIStyles.flexRow,
  },
  title: {
    ...UIStyles.font15,
    color: Color.DARK,
    paddingVertical: 13.5,
  },
});
