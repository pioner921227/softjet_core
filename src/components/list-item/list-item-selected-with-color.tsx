import React from 'react';
import {IListItem, ListItem} from './list-item';
import {Config} from '../../config';

const {Color} = Config;

interface IListItemSelectedWithColor extends IListItem {
  selectedColor?: string;
}

export const ListItemSelectedWithColor: React.FC<IListItemSelectedWithColor> = React.memo(
  ({isSelected, title, selectedColor = Color.PRIMARY, textStyle, ...rest}) => {
    return (
      <ListItem
        textStyle={{
          color: isSelected ? selectedColor : Color.DARK,
          ...textStyle,
        }}
        title={title}
        isSelected={isSelected}
        {...rest}
      />
    );
  },
);
