import React from 'react';
import Animated, {
  Extrapolate,
  interpolate,
  useAnimatedScrollHandler,
  useAnimatedStyle,
  useSharedValue
} from 'react-native-reanimated';
import {useLayout} from '../system/hooks/use-layout';
import {ScrollViewProps, View} from 'react-native';
import {Edge, SafeAreaView} from 'react-native-safe-area-context';

type T = React.ReactElement & ScrollViewProps;

interface IScrollableHeaderWithList {
  header: JSX.Element;
  list: JSX.Element;
}

const edges: Edge[] = ['top'];

export const ScrollableHeaderWithList: React.FC<IScrollableHeaderWithList> = React.memo(
  ({header, list}) => {
    // const [headerHeight, setHeaderHeight] = useState(20);
    const animValue = useSharedValue(0);
    // const onLayout = useLayout(props => {
    //   setHeaderHeight(props.height);
    // });

    const {onLayout, values} = useLayout();

    const onScrollHandler = useAnimatedScrollHandler(event => {
      animValue.value = event.contentOffset.y;
    });

    //@ts-ignore
    const translateYStyle = useAnimatedStyle(() => {
      return {
        position: 'absolute',
        zIndex: 2,
        width: '100%',
        transform: [
          {
            translateY: interpolate(
              animValue.value,
              [0, values.height],
              [0, -values.height],
              Extrapolate.CLAMP,
            ),
          },
        ],
        opacity: interpolate(animValue.value, [0, values.height / 1.5], [1, 0]),
      };
    });

    return (
      <SafeAreaView edges={edges}>
        <View>
          <Animated.View onLayout={onLayout} style={translateYStyle}>
            {header}
          </Animated.View>
          {React.cloneElement(list, {
            onScroll: onScrollHandler,
            scrollEventThrottle: 16,
            style: {paddingTop: values.height},
          })}
        </View>
      </SafeAreaView>
    );
  },
);
