import BouncyCheckbox from 'react-native-bouncy-checkbox';
import {StyleSheet, Text, View, ViewStyle} from 'react-native';
import React, {useMemo} from 'react';
import {Config} from '../../config';
import {themeNames} from '../../assets/styles';
import {getThemeColor} from '../../system/helpers/get-theme-color';
import {useHitSlop} from '../../system/hooks/use-hit-slop';

const {Color, RADIUS} = Config;

interface CheckboxComponent {
  title: string;
  isChecked: boolean;
  onToggle: (status: boolean) => void;
  containerStyle?: ViewStyle;
  theme?: keyof typeof themeNames;
  disabled?: boolean;
}

export const CheckboxComponent: React.FC<CheckboxComponent> = React.memo(
  ({title, isChecked, onToggle, containerStyle, theme, disabled}) => {
    const color = theme ? Color[theme] : getThemeColor();

    const style = useMemo(
      () =>
        StyleSheet.flatten([
          styles.iconStyle,
          {borderColor: disabled ? Color.GREY_400 : color},
        ]),
      [color, disabled],
    );

    const containerStyle_ = useMemo(() => {
      return {
        ...styles.container,
        ...containerStyle,
      };
    }, [containerStyle]);

    const hitStop = useHitSlop(10);

    return (
      <View style={containerStyle_}>
        <BouncyCheckbox
          hitSlop={hitStop}
          disabled={disabled}
          size={20}
          bounceEffect={1}
          bounceFriction={10}
          iconStyle={style}
          fillColor={color}
          onPress={onToggle}
          isChecked={isChecked}
        />
        <Text>{title}</Text>
      </View>
    );
  },
);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconStyle: {
    borderRadius: RADIUS.SMALL,
  },
});
