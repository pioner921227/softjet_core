
import React, { useMemo } from 'react';
import { Insets, StyleSheet, TouchableOpacity } from 'react-native';
import { Config } from '../../config';
import { CheckedIcon } from '../icons/checked-icon';
const {Color, RADIUS} = Config;


interface ICheckBoxElement {
  isChecked: boolean;
  onChange: (state: boolean) => void;
  disabled?: boolean;
  iconColor?: string;
}

const hitSlop: Insets = { top: 10, bottom: 10, left: 10, right: 10 };

export const CheckBoxElement: React.FC<ICheckBoxElement> = React.memo(
  ({ isChecked, onChange, disabled = false, iconColor = Color.BLACK }) => {
    const containerStyle_ = useMemo(() => {
      return {
        ...styles.container,
        backgroundColor: isChecked ? Color.PRIMARY : 'transparent',
        borderColor: disabled ? Color.PRIMARY_40 : Color.PRIMARY,
      };
    }, [isChecked, disabled]);

    const onChangeHandler = () => {
      onChange(!isChecked);
    };

    return (
      <TouchableOpacity
        disabled={disabled}
        hitSlop={hitSlop}
        onPress={onChangeHandler}
        style={containerStyle_}>
        {isChecked ? <CheckedIcon fill={iconColor} width={12} /> : null}
      </TouchableOpacity>
    );
  },
);

const styles = StyleSheet.create({
  container: {
    width: 20,
    height: 20,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: RADIUS.SMALL,
    borderWidth: 2,
    borderColor: Color.PRIMARY,
  },
});
