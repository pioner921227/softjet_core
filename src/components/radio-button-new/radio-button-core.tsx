import React, {useCallback, useMemo} from 'react';
import {StyleSheet, Text, TextStyle, TouchableOpacity, View, ViewStyle} from 'react-native';

export interface IRadioButtonData {
  id: number;
  label: string;
  disabled?: boolean;
}

interface IBorderCircleStyle {
  size?: number;
  color?: string;
  borderWidth?: number;
}
interface ICircleStyle {
  color?: string;
  size?: number;
}

export enum valuesForSelected {
  id = 'id',
  index = 'index',
}

interface IOnSelectRadioButton {
  (item: IRadioButtonData, index: number): void;
}

interface IRadioButtonNew {
  data: IRadioButtonData[];
  onSelect: IOnSelectRadioButton;
  selectedIndex: number;
  borderCircleStyle?: IBorderCircleStyle;
  circleStyle?: ICircleStyle;
  itemStyle?: ViewStyle;
  labelStyle?: TextStyle;
  containerStyle?: ViewStyle;
  disabledOpacity?: number;
  startIndex?: number;
  valueForSelected?: keyof typeof valuesForSelected;
}

export const RadioButtonCore: React.FC<IRadioButtonNew> = React.memo(
  ({
    valueForSelected = valuesForSelected.index,
    startIndex = 0,
    onSelect,
    data,
    selectedIndex,
    borderCircleStyle,
    circleStyle = {size: 8, color: 'black'},
    itemStyle,
    disabledOpacity = 0.5,
    labelStyle,
    containerStyle,
  }) => {
    const itemStyle_ = useMemo(() => ({...styles.item, ...itemStyle}), [
      itemStyle,
    ]);

    const borderCircleStyle_ = useMemo(() => {
      const {size = 20, color = 'black', borderWidth = 2} =
        borderCircleStyle || {};

      return {
        ...styles.borderCircle,
        width: size,
        height: size,
        borderWidth: borderWidth,
        borderColor: color,
      };
    }, [borderCircleStyle]);

    const circleStyle_ = useMemo(() => {
      const {color = 'black', size = 8} = circleStyle || {};
      return {
        ...styles.circle,
        width: size,
        height: size,
        backgroundColor: color,
      };
    }, [circleStyle]);

    const labelStyle_ = useMemo(
      () => ({
        ...styles.label,
        ...labelStyle,
      }),
      [labelStyle],
    );

    const isSelected = useCallback(
      (index: number, id: number) => {
        if (valueForSelected === 'id') {
          return selectedIndex === id;
        }
        return selectedIndex === index + startIndex;
      },
      [selectedIndex, startIndex, valueForSelected],
    );

    if (!data || data?.length === 0) {
      return null;
    }

    return (
      <View style={containerStyle}>
        {data?.map((el, index) => {
          const onPress = () => {
            onSelect(el, index);
          };

          return (
            <TouchableOpacity
              disabled={el.disabled}
              onPress={onPress}
              style={[
                {opacity: el?.disabled ? disabledOpacity ?? 0.5 : 1},
                itemStyle_,
              ]}
              key={el?.id}>
              <TouchableOpacity
                disabled={el?.disabled}
                onPress={onPress}
                style={borderCircleStyle_}>
                {isSelected(index, el.id) ? (
                  <View style={circleStyle_} />
                ) : null}
              </TouchableOpacity>
              <Text style={labelStyle_}>{el?.label}</Text>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  },
);

const styles = StyleSheet.create({
  container: {},
  item: {
    flexDirection: 'row',
    paddingVertical: 5,
    alignItems: 'center',
  },
  borderCircle: {
    borderWidth: 2,
    width: 20,
    height: 20,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  circle: {
    width: 10,
    height: 10,
    borderRadius: 50,
  },
  label: {
    marginLeft: 5,
  },
});
