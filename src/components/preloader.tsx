import {ActivityIndicator, StyleSheet, View} from 'react-native';
import React from 'react';

import {Config} from '../config';
const {Color} = Config;

export const Preloader = () => {
  return (
    <View style={styles.emptyComponent}>
      <ActivityIndicator color={Color.PRIMARY} size={'large'} />
    </View>
  );
};

const styles = StyleSheet.create({
  emptyComponent: {
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
