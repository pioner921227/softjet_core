import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { IThemedButton, ThemedButton } from "../../buttons/themed-button";
import { Config } from "../../../config";
import { adaptiveSize } from "../../../system/helpers/responseive-font-size";

const { Color, UIStyles } = Config;

interface IBottomSheetModalContentWithTitle {
  title: string;
  description: string;
  buttons: Array<IThemedButton>;
}

export const BottomSheetModalContentWithTitle: React.FC<IBottomSheetModalContentWithTitle> =
  React.memo(({ title, description, buttons }) => {
    return (
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.description}>{description}</Text>
        </View>
        <View>
          {buttons.map((el, index) => {
            return (
              <ThemedButton
                withGesture={true}
                key={index}
                wrapperStyle={styles.buttonWrapper}
                {...el}
              />
            );
          })}
        </View>
      </View>
    );
  });

const styles = StyleSheet.create({
  container: {
    flex: 1,
    ...UIStyles.paddingH16,
  },
  title: {
    ...UIStyles.font24b,
    color: Color.BLACK,
    textAlign: "center",
  },
  description: {
    ...UIStyles.font15,
    color: Color.GREY_800,
    textAlign: "center",
    marginTop: 14,
  },
  buttonWrapper: {
    marginBottom: adaptiveSize(16),
  },
  wrapper: {
    marginBottom: 14,
  },
});
