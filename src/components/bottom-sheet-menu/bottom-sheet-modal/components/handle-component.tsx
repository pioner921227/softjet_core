import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Config} from '../../../../config';

export const HandleComponent = React.memo(() => {
  return (
    <View style={styles.container}>
      <View style={styles.line} />
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  line: {
    width: 48,
    height: 4,
    borderRadius: Config.RADIUS.LARGE,
    backgroundColor: Config.Color.RGBA_400,
  },
});
