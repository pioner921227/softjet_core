import React, { useMemo } from 'react';
import { BottomSheetBackdropProps } from '@gorhom/bottom-sheet';
import Animated, {
  interpolate,
  useAnimatedStyle,
  Extrapolate,
} from 'react-native-reanimated';

interface ICustomBackdrop extends BottomSheetBackdropProps {
  dismissOnPanDown: boolean;
}

const CustomBackdrop = React.memo(
  ({ animatedIndex, style }: ICustomBackdrop) => {
    const animatedStyle = useAnimatedStyle(() => {
      return {
        opacity: interpolate(
          animatedIndex.value,
          [-1, 0],
          [0, 0.4],
          Extrapolate.CLAMP,
        ),
        zIndex: interpolate(
          animatedIndex.value,
          [-1, 0],
          [-1, 0],
          Extrapolate.CLAMP,
        ),
      };
    });

    const containerStyle = useMemo(
      () => [
        style,
        {
          backgroundColor: '#000',
        },
        animatedStyle,
      ],
      [style],
    );

    return <Animated.View style={containerStyle} />;
  },
);

export default CustomBackdrop;
