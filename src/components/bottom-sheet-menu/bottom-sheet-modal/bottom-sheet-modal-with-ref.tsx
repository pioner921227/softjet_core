import { BottomSheetBackdropProps, BottomSheetModal } from '@gorhom/bottom-sheet';
import CustomBackdrop from './components/custom-backdrop';
import React, { useCallback, useMemo } from 'react';
import { BottomSheetModalMethods } from '@gorhom/bottom-sheet/lib/typescript/types';
import { HandleComponent } from '../handle-component';

interface IBottomSheetModalWithRef {
  modalRef: React.MutableRefObject<BottomSheetModalMethods | null>;
  snapPoints: Array<string | number>;
  dismissOnPanDown?: boolean;
  onDismiss?: () => void;
}

export const BottomSheetModalWithRef: React.FC<IBottomSheetModalWithRef> = React.memo(
  ({ children, modalRef, snapPoints, dismissOnPanDown = true, onDismiss }) => {
    const snapPintsMemo = useMemo(() => ['30%'], []);

    const backdropComponent = useCallback(
      (props: BottomSheetBackdropProps) => {
        return (
          <CustomBackdrop dismissOnPanDown={dismissOnPanDown} {...props} />
        );
      },
      [dismissOnPanDown],
    );

    return (
      <BottomSheetModal
        enableContentPanningGesture={false}
        keyboardBehavior='interactive'
        keyboardBlurBehavior='restore'
        onDismiss={onDismiss}
        handleComponent={HandleComponent}
        enablePanDownToClose={dismissOnPanDown}
        backdropComponent={backdropComponent}
        ref={modalRef}
        snapPoints={snapPoints || snapPintsMemo}>
        {children}
      </BottomSheetModal>
    );
  },
);
