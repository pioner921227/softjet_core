import {Routes} from '../../navigation';

export interface ISelectOrderTypeModalConfig {
  isEnabled?: boolean;
  snapPoints?: Array<string | number>;
  dismissOnPanDown?: boolean;
  routes: {
    online: (route?: keyof typeof Routes) => void;
    offline: () => void;
  };
}
interface ICreateOrderTypeModalConfig
  extends Omit<ISelectOrderTypeModalConfig, 'routes'> {
  online: () => void;
  offline: () => void;
}

export const createOrderTypeModalConfig = ({
  isEnabled = true,
  snapPoints = [400],
  dismissOnPanDown = true,
  online,
  offline,
}: ICreateOrderTypeModalConfig): ISelectOrderTypeModalConfig => {
  return {
    isEnabled,
    snapPoints,
    dismissOnPanDown,
    routes: {
      online,
      offline,
    },
  };
};
