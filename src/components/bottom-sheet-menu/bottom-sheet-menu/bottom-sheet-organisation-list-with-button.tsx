import {ThemedButton} from '../../buttons/themed-button';
import React, {useCallback, useState} from 'react';
import {
  BottomSheetOrganisationList,
  IBottomSheetMenuOrganisationList,
} from './bottom-sheet-organisation-list';
import {Config} from '../../../config';
import {InteractionManager, StyleSheet} from 'react-native';
import {IOrganisationListRenderItemProps} from '../../../screens/delivery/view/components/organisation-list/organisation-list';
import {organisationListWithMapConfig} from '../../../screens/delivery/view/organisation-list-with-map/config';
import {ListItemWithChecked} from '../../list-item/list-item-with-checked';
import {IOrganisation} from '../../../screens/delivery/types/types';

type props = Omit<IBottomSheetMenuOrganisationList, 'RenderItem'>;

interface BottomSheetOrganisationListWithButton extends props {
  onPressButton: () => void;
}

export const BottomSheetOrganisationListWithButton: React.FC<BottomSheetOrganisationListWithButton> = React.memo(
  ({data, onSelectItem, onPressButton, selectedItemId}) => {

    const onSelectItemHandler = useCallback((item: IOrganisation) => {
      onSelectItem(item);
    }, []);

    const renderItem = useCallback(
      (props: IOrganisationListRenderItemProps) => {
        return <ListItemWithChecked title={props.data.title} {...props} />;
      },
      [],
    );

    const bottomComponent = useCallback(() => {
      return (
        <ThemedButton
          disabled={!selectedItemId}
          withGesture={true}
          wrapperStyle={styles.buttonWrapperStyle}
          rounded={true}
          label={organisationListWithMapConfig.nextButtonLabel}
          onPress={onPressButton}
        />
      );
    }, [selectedItemId]);

    return (
      <BottomSheetOrganisationList
        selectedItemId={selectedItemId}
        contentStyle={styles.container}
        RenderItem={renderItem}
        onSelectItem={onSelectItemHandler}
        data={data}
        BottomComponent={bottomComponent}
      />
    );
  },
);

const styles = StyleSheet.create({
  container: {
    // height: '100%',
    flexGrow: 1,
  },
  buttonWrapperStyle: {
    marginTop: 'auto',
    paddingHorizontal: 16,
    paddingTop: 5,
    backgroundColor: Config.Color.WHITE,
  },
});
