import React, {useMemo, useRef} from 'react';
import {StyleSheet, View} from 'react-native';
import BottomSheet from '@gorhom/bottom-sheet';

interface IBottomSheetMenu {
  snapPoints?: Array<string>;
}

export const BottomSheetMenu: React.FC<IBottomSheetMenu> = React.memo(
  ({snapPoints, children}) => {
    const bottomSheetRef = useRef<BottomSheet>(null);

    const snapPointsMemo = useMemo(() => ['40%'], []);

    return (
      <BottomSheet
        enableHandlePanningGesture={false}
        enableContentPanningGesture={false}
        ref={bottomSheetRef}
        handleComponent={HandleComponent}
        index={0}
        snapPoints={snapPoints || snapPointsMemo}>
        <View style={styles.container}>{children}</View>
      </BottomSheet>
    );
  },
);

const HandleComponent = () => {
  return <View style={styles.handleComponent} />;
};

const styles = StyleSheet.create({
  handleComponent: {
    height: 20,
  },
  container: {
    paddingBottom: 20,
  },
});
