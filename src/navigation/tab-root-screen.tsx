import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { Config } from '../config'
import { useTypedSelector } from '../system/hooks/use-typed-selector'
import { tabScreenConfig } from './config/tab-screen-config'
import { getCartData } from '../system/selectors/selectors'
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import { adaptiveSize } from '../system/helpers/responseive-font-size'
import { TextStyle, ViewStyle } from 'react-native'
import { fontFamily } from '../assets/styles'

const Tab = createBottomTabNavigator()
const { Color, UIStyles } = Config

export const TabRootScreen: React.FC = React.memo(() => {
  const cartData = useTypedSelector(getCartData)
  const insets = useSafeAreaInsets()

  const badgeStyle: ViewStyle & TextStyle = {
    ...UIStyles.font9b,
    backgroundColor: Color.PRIMARY,
    color: Color.WHITE,
    transform: [{ translateY: adaptiveSize(29) }, { translateX: -30 }],
    height: 18,
    width: 59,
  }

  return (
    <Tab.Navigator
      backBehavior={'firstRoute'}
      screenOptions={{
        headerShown: false,
        tabBarActiveTintColor: Color.DARK,
        tabBarInactiveTintColor: Color.GREY_400,
        tabBarStyle: {
          height: adaptiveSize(58) + (insets.bottom ? 15 : 0),
        },
        tabBarItemStyle: {
          height: adaptiveSize(51),
        },
        tabBarLabelStyle: {
          fontFamily: fontFamily,
          fontSize: 13,
          fontWeight: '400',
        },
      }}
      // tabBarOptions={{
      //   style: { height: adaptiveSize(58) + (insets.bottom ? 15 : 0) },
      //   activeTintColor: Color.DARK,
      //   inactiveTintColor: Color.GREY_400,
      //   tabStyle: { height: adaptiveSize(51) },
      //   labelStyle: {
      //     fontFamily: fontFamily,
      //     fontSize: 13,
      //     fontWeight: '400',
      //   },
      // }}
      sceneContainerStyle={{ backgroundColor: Color.WHITE }}
    >
      {Object.values(tabScreenConfig).map((el, index) => {
        return (
          <Tab.Screen
            listeners={({ navigation }) => ({
              tabPress: (e) => {
                navigation.navigate(el.route)
              },
            })}
            key={index}
            options={{
              tabBarBadgeStyle:
                index === 1 && cartData?.original_total_cost
                  ? badgeStyle
                  : null,
              //@ts-ignore
              tabBarBadge:
                index === 1 && cartData?.original_total_cost
                  ? `${cartData.original_total_cost} ₽`
                  : null,
              tabBarIcon: ({ focused }) =>
                el.Icon.call(null, {
                  fill: focused ? Color.DARK : Color.GREY_400,
                }),
              tabBarLabel:
                !cartData?.original_total_cost || index !== 1 ? el.label : '',
            }}
            name={el.route}
            component={el.component}
          />
        )
      })}
    </Tab.Navigator>
  )
})
