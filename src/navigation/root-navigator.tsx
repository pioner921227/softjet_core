import {
  createStackNavigator,
  StackNavigationOptions,
} from '@react-navigation/stack'
import React, { useEffect } from 'react'
import { InteractionManager, Platform } from 'react-native'
import { useDispatch } from 'react-redux'
import { useModal } from '../components'
import { Config } from '../config'
import { AsyncActionCart } from '../screens/cart'
import { AsyncActionMore } from '../screens/more/store/async-action-more'
import { AsyncActionProfile } from '../screens/profile'
import { ApiService } from '../system/api/api-service'
import { setApiToken } from '../system/api/set-api-token'
import { fcmService } from '../system/helpers/firebase'
import { getOrganisationCommentHandler } from '../system/helpers/get-organisation-comment-handler'
import { useAxiosError } from '../system/hooks/use-axios-error'
import { useNetConnection } from '../system/hooks/use-net-connection'
import { useSetAppVersion } from '../system/hooks/use-set-app-version'
import { useTypedSelector } from '../system/hooks/use-typed-selector'
import {
  getCurrentOrderTypeId,
  getCurrentOrganisationId,
  getToken,
} from '../system/selectors/selectors'
import { AsyncActionSystem } from '../system/store/async-action-system'
import { Routes, screenConfig } from './config'
import { TabRootScreen } from './tab-root-screen'
import {
  createNativeStackNavigator,
  NativeStackNavigationOptions,
} from '@react-navigation/native-stack'

import messaging from "@react-native-firebase/messaging";

const getToken = async () => {
  try {
    const token = await messaging().getToken();
    if (token) return token;
  } catch (error) {
    console.log(error);
  }
};

const getFCMToken = async () => {
  try {
    const authorized = await messaging().hasPermission();
    const fcmToken = await getToken();

    if (authorized) return fcmToken;

    await messaging().requestPermission();
    return fcmToken;
  } catch (error) {
    console.log(error);
  }
};

// import {FCMService} from '../system/helpers/firebase';
// import {AsyncActionSystem} from '../system/store/async-action-system';

const { Color } = Config
// const fcmService = new FCMService();

export const Stack =
  Platform.OS === 'ios' ? createNativeStackNavigator() : createStackNavigator()

export const RootNavigator: React.FC = React.memo(({ children }) => {
  const token = useTypedSelector(getToken) || getFCMToken().then(t => t)
  const serverUrl = useTypedSelector((state) => state.more.serverUrl)
  const dispatch = useDispatch()
  const currentOrganisationId = useTypedSelector(getCurrentOrganisationId)
  const currentOrderTypeId = useTypedSelector(getCurrentOrderTypeId)
  const { show, close } = useModal()

  console.log(token)

  const notificationService = async () => {
    await fcmService.requestPermissionOnly()
    await fcmService.registerAppWithFCM()
    fcmService.register(
      (token) => {
        dispatch(AsyncActionSystem.setFCMToken(token))
      },
      () => {},
      () => {},
    )
  }

  useEffect(() => {
    if (token) {
      setApiToken(token)
      InteractionManager.runAfterInteractions(() => {
        dispatch(AsyncActionProfile.getUserData())
        if (
          currentOrderTypeId !== undefined &&
          currentOrganisationId !== undefined
        ) {
          dispatch(
            AsyncActionCart.initRemoteCart({
              id_org: currentOrganisationId,
              order_type: currentOrderTypeId,
            }),
          )
        }
      })
      notificationService()
    } else {
      fcmService.unregisterDevice()
      fcmService.unregisterAll()
    }
  }, [token])

  useEffect(() => {
    InteractionManager.runAfterInteractions(() => {
      getOrganisationCommentHandler(
        dispatch,
        currentOrganisationId,
        show,
        close,
      )

      dispatch(AsyncActionMore.getAppInfo())
      dispatch(AsyncActionMore.getAppSettings())
    })
    return () => {
      // fcmService.unregisterAll();
    }
  }, [])

  useEffect(() => {
    if (serverUrl) {
      ApiService.defaults.baseURL = serverUrl
    } else {
      ApiService.defaults.baseURL = Config.BASE_URL
    }
  }, [serverUrl])

  useNetConnection()
  useAxiosError()
  useSetAppVersion()

  return (
    <Stack.Navigator
      screenOptions={screenOptions}
      initialRouteName={token ? Routes.TabRootScreen : Routes.LoginStack}
    >
      <Stack.Screen
        name={Routes.LoginStack}
        component={require('../screens/login/view/login-stack').LoginStack}
      />
      <Stack.Screen name={Routes.TabRootScreen} component={TabRootScreen} />
      <Stack.Screen name={Routes.Payment} component={screenConfig.Payment} />
      <Stack.Screen
        name={Routes.PaymentFinish}
        component={screenConfig.PaymentFinish}
      />
      <Stack.Screen
        name={Routes.AddressCreate}
        component={screenConfig.AddressCreate}
      />

      <Stack.Screen
        name={Routes.DeliveryAndPayment}
        component={screenConfig.DeliveryAndPayment}
      />

      <Stack.Screen
        name={Routes.TermsOfUse}
        component={screenConfig.TermsOfUse}
      />
      <Stack.Screen
        name={Routes.AcceptAddress}
        component={screenConfig.AcceptAddress}
      />
      <Stack.Screen name={Routes.Policy} component={screenConfig.Policy} />
      <Stack.Screen
        name={Routes.AddressList}
        component={screenConfig.AddressList}
      />
      <Stack.Screen
        name={Routes.SelectCity}
        component={screenConfig.SelectCity}
      />
      <Stack.Screen
        name={Routes.SelectOrganisationInMap}
        component={screenConfig.OrganisationListWithMap}
      />
      <Stack.Screen
        name={Routes.OrganisationList}
        component={screenConfig.OrganisationList}
      />
      <Stack.Screen name={Routes.AppInfo} component={screenConfig.AppInfo} />
      <Stack.Screen
        name={Routes.PaymentWebView}
        component={screenConfig.PaymentWebView}
      />
      <Stack.Screen
        name={Routes.ProductDetail}
        component={screenConfig.ProductDetail}
      />
      {children}
    </Stack.Navigator>
  )
})

const stackScreenOptions: StackNavigationOptions = {
  cardStyle: {
    backgroundColor: Color.WHITE,
  },
  headerShown: false,
}

export const stackNativeScreenOptions: NativeStackNavigationOptions = {
  contentStyle: {
    backgroundColor: Color.WHITE,
  },
  headerShown: false,
  orientation: 'portrait',
}

export const screenOptions:
  | StackNavigationOptions
  | NativeStackNavigationOptions =
  Platform.OS === 'ios' ? stackNativeScreenOptions : stackScreenOptions
