export {tabScreenConfig} from './config';
export {screenConfig} from './config';
export {Routes} from './config';
import {TRootStackParamList} from './types/types';
export type TRootNavigationParamsList = TRootStackParamList;
