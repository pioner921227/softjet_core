import { ProfileIcon } from '../../components/icons/profile-icon';
import { Cart2Icon } from '../../components/icons/cart-2-icon';
import { MoreIcon } from '../../components/icons/more-icon';
import { Routes } from './routes';
import React from 'react';
import { SvgProps } from 'react-native-svg';
import { screenConfig } from './screen-config';
import { ProfileStack } from '../../screens/profile/view/profile-stack';
import { MainIcon } from '../../components/icons/main-icon';

interface IScreen {
  label: string;
  Icon: React.FC<SvgProps>;
  component: React.FC;
  route: string;
}

interface ITabScreenConfig {
  [key: string]: IScreen;
}

export const tabScreenConfig: ITabScreenConfig = {
  ['0']: {
    label: 'Меню',
    Icon: MainIcon,
    component: screenConfig.Catalog,
    route: Routes.Catalog,
  },
  ['1']: {
    label: 'Заказ',
    Icon: Cart2Icon,
    component: screenConfig.CartNew,
    route: Routes.Cart,
  },
  ['2']: {
    label: 'Профиль',
    Icon: ProfileIcon,
    component: ProfileStack,
    route: Routes.Profile,
  },
  ['3']: {
    label: 'Еще',
    Icon: MoreIcon,
    component: screenConfig.More,
    route: Routes.More,
  },
};
