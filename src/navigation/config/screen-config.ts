// import {LoginPhone} from '../../screens/login/view/login-phone/login-phone';
import { Policy } from "../../screens/login/view/policy";
import { DeliveryAndPayment } from "../../screens/delivery/view/delivery-and-payment";
import { ProfileNotAuth } from "../../screens/profile/view/profile-not-auth";
import { ProfileAuth } from "../../screens/profile/view/profile-auth";
import { ProfileStack } from "../../screens/profile/view/profile-stack";
import { ProfileSettings } from "../../screens/profile/view/profile-settings/profile-settings";
import { Catalog } from "../../screens/catalog/view/catalog";
import { Cart } from "../../screens/cart/view/cart/cart";
import { AddressCreate } from "../../screens/delivery/view/address-create/address-create";
import { AcceptAddress } from "../../screens/cart/view/accept-address/accept-address";
import { OrganisationListWithMap } from "../../screens/delivery/view/organisation-list-with-map/organisation-list-with-map";
import { AddressList } from "../../screens/delivery/view/address-list/address-list";
import { More } from "../../screens/more/view/more/more";
import { TabRootScreen } from "../tab-root-screen";
import { Payment } from "../../screens/payment/view/payment/payment";
import { PaymentFinish } from "../../screens/payment/view/payment-finish/payment-finish";
import { Orders } from "../../screens/orders/view/orders";
import { OrganisationList } from "../../screens/more/view/organisation-list/organisation-list";
import { AppInfo } from "../../screens/more/view/app-info/app-info";
import { PaymentWebView } from "../../screens/payment/view/payment-web-view";
import { TermsOfUse } from "../../screens/login/view/terms-of-use";
import { CityList } from "../../screens/delivery/view/city-list/city-list";
import { CartNew } from "../../screens/cart/view/cart/cart-new";
import { ProductDetail } from "../../screens/product-detail/view/product-detail";

export const screenConfig = {
  // LoginPhone: LoginPhone
  // LoginCode: LoginCode,
  // LoginStack: LoginStack,
  Policy: Policy,
  DeliveryAndPayment: DeliveryAndPayment,
  ProfileNotAuth: ProfileNotAuth,
  ProfileAuth: ProfileAuth,
  Profile: ProfileStack,
  ProfileSettings: ProfileSettings,
  Catalog: Catalog,
  Cart: Cart,
  CartNew: CartNew,
  // AddressCreate: AddressCreate,
  AddressCreate: AddressCreate,
  AcceptAddress: AcceptAddress,
  SelectCity: CityList,
  OrganisationListWithMap: OrganisationListWithMap,
  AddressList: AddressList,
  More: More,
  TabRootScreen: TabRootScreen,
  Payment: Payment,
  PaymentFinish: PaymentFinish,
  PaymentWebView: PaymentWebView,
  Orders: Orders,
  OrganisationList: OrganisationList,
  AppInfo: AppInfo,
  TermsOfUse: TermsOfUse,
  ProductDetail: ProductDetail,
};
