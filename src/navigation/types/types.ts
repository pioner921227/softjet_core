import { Routes } from '../config/routes'
import { BottomSheetMenuType } from '../../screens/delivery/view/organisation-list-with-map/organisation-list-with-map'
import { ICreateOrderDataRequest } from '../../screens/payment/api/types'
import { IProductItem } from '../../screens/catalog'

export type TRootStackParamList = {
  [Routes.LoginStack]: undefined
  [Routes.LoginPhone]: undefined
  [Routes.OrganisationList]: undefined
  [Routes.Cart]: undefined
  [Routes.More]: undefined
  [Routes.LoginCode]: {
    phoneNumber: string
    request_id: number
  }
  [Routes.SelectCity]:
    | {
        enableBackButton?: boolean
      }
    | undefined
  [Routes.DeliveryAndPayment]: undefined
  [Routes.Policy]: undefined
  [Routes.AddressCreate]: { enableBackButton: boolean } | undefined
  [Routes.AddressList]: undefined
  [Routes.ProfileNotAuth]: undefined
  [Routes.Catalog]:
    | { ignoreModal?: boolean; ignoreGetCatalog?: boolean }
    | undefined
  [Routes.ProfileAuth]: undefined
  [Routes.SelectOrganisationInMap]: {
    bottomSheetMenuType: keyof typeof BottomSheetMenuType
    successRoute?: keyof typeof Routes
    disableLoadOrganisations?: boolean
  }
  [Routes.TabRootScreen]: undefined
  [Routes.Profile]: undefined
  [Routes.ProfileSettings]: undefined
  [Routes.PaymentFinish]: { isOnline: boolean; orderId: number; price: number }
  [Routes.Payment]:
    | {
        isVisiblePaymentErrorModal?: boolean
        enabledDeliverySale?: boolean
      }
    | undefined
  [Routes.AcceptAddress]: undefined
  [Routes.Orders]: undefined
  [Routes.AppInfo]: undefined
  [Routes.PaymentWebView]: { url: string; data: ICreateOrderDataRequest }
  [Routes.TermsOfUse]: undefined
  [Routes.ProductDetail]: { item?: IProductItem; itemId: number }
}
